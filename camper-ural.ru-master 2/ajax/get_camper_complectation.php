#!/usr/bin/php
<?php
  //$_SERVER["DOCUMENT_ROOT"] = "/home/c7106/ecospaekb.ru/www";
  $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
  define("NO_KEEP_STATISTIC", true);
  define("NOT_CHECK_PERMISSIONS", true);
  set_time_limit(0);
  define('SITE_ID', 's1');

  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

  if(isset($_POST['id'])) {

    $content = array();
    $content['LINK_COMPLECT'] = array();
    $checked = array();
    $checkedItems = array();
    // Получаем уже выбранные комплектующие
    if(isset($_POST['complectation_id'])) {
      // получаем элемент по id кемпера и id комлектации
      $arComplectSelect = Array("ID", "NAME", "PROPERTY_COMPLECT");
      $arComplectFilter = Array("IBLOCK_ID"=>42, "PROPERTY_CAMPER"=>$_POST['id'], "PROPERTY_COMPLECTATION"=>$_POST['complectation_id']);
      $resComplect = CIBlockElement::GetList(Array(), $arComplectFilter, false, Array("nPageSize"=>50), $arComplectSelect);
      while($obComplect = $resComplect->GetNextElement()) {
       $arComplectFields = $obComplect->GetFields();
       if(isset($arComplectFields['PROPERTY_COMPLECT_VALUE'])) {
         $checkedItems = $arComplectFields['PROPERTY_COMPLECT_VALUE'];
         $content['COMPLECTATION_ID'] = $obComplect;
       }
      }
    }

    $properties = CIBlockElement::GetProperty(34, $_POST['id'], array("sort" => "asc"), Array("CODE"=>"LINK_COMPLECT"));

    while ($ob = $properties->GetNext()) {
      $complect_item = array();
      $complect_item_id = $ob['VALUE'];

      $complect_item['ID'] = $complect_item_id;

      $complect_item_object_res = CIBlockElement::GetByID($complect_item_id);

      if($complect_item_object = $complect_item_object_res->GetNext()) {
        $complect_item['NAME'] = $complect_item_object['NAME'];
        $complect_item['PREVIEW_TEXT'] = $complect_item_object['PREVIEW_TEXT'];
        $complect_item['CODE'] = $complect_item_object['CODE'];
        $complect_item['ID'] = $complect_item_object['ID'];
        $complect_item['DETAIL_PICTURE'] = CFile::GetPath($complect_item_object['DETAIL_PICTURE']);
        $complect_item['CHECKED'] = false;

        $propertiesLinked = CIBlockElement::GetProperty(41, $complect_item_object['ID'], array("sort" => "asc"), Array("CODE"=>"BLOCKED"));
        while ($obLinked = $propertiesLinked->GetNext()) {
          $complect_item['LINKED'] = $obLinked;
        }

        foreach ($checkedItems as $checkedItem) {
          if($complect_item_object['ID'] == $checkedItem) {
            $complect_item['CHECKED'] = true;
          }
        }

        $propertiesPrice = CIBlockElement::GetProperty(41, $complect_item_id, array("sort" => "asc"), Array("CODE"=>"PRICE"));
        if ($obPrice = $propertiesPrice->GetNext()) {
          $complect_item['PRICE'] = $obPrice['VALUE'];
        }

        $propertiesBlocked = CIBlockElement::GetProperty(41, $complect_item_id, array("sort" => "asc"), Array("CODE"=>"BLOCKED"));
        if ($obBlocked = $propertiesBlocked->GetNext()) {
          $complect_item['BLOCKED'] = $obBlocked['BLOCKED'];
        }

        $propertiesLinked = CIBlockElement::GetProperty(41, $complect_item_id, array("sort" => "asc"), Array("CODE"=>"LINKED"));
        if ($obLinked = $propertiesLinked->GetNext()) {
          $complect_item['LINKED'] = $obLinked['LINKED'];
        }

        //$complect_item['PRICE'] = $complect_item_object['PROPERTY']['PRICE']['VALUE'];
        $complect_item['DEBUG'] = $complect_item_object;
      }

      array_push($content['LINK_COMPLECT'], $complect_item);
    }
    usort($content['LINK_COMPLECT'], 'sortByChecked');
  }

  function sortByChecked($a, $b){
      return $a['CHECKED'] < $b['CHECKED'];
  }




  echo json_encode($content);
?>
