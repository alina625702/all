#!/usr/bin/php
<?php
  //$_SERVER["DOCUMENT_ROOT"] = "/home/c7106/ecospaekb.ru/www";
  $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
  define("NO_KEEP_STATISTIC", true);
  define("NOT_CHECK_PERMISSIONS", true);
  set_time_limit(0);
  define('SITE_ID', 's1');

  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

  // Получаем информацию  о кемпере
  if(isset($_POST['id'])) {

    $res = CIBlockElement::GetByID($_POST['id']);

    $content = array();

    if($ar_res = $res->GetNext())
      $content['DETAIL_TEXT'] = $ar_res['DETAIL_TEXT'];
      $content['NAME'] = $ar_res['NAME'];
      $content['DETAIL_PICTURE'] = CFile::GetPath($ar_res['DETAIL_PICTURE']);

    $properties = CIBlockElement::GetProperty(34, $_POST['id'], array("sort" => "asc"), Array("CODE"=>"BASE_COMPLECT"));
    if ($ob = $properties->GetNext()) {
      $content['BASE_COMPLECT'] = $ob['VALUE']['TEXT'];
    }

    $properties = CIBlockElement::GetProperty(34, $_POST['id'], array("sort" => "asc"), Array("CODE"=>"PRICE"));
    if ($obPrice = $properties->GetNext()) {
      $content['PRICE'] = $obPrice['VALUE'];
    }

    echo json_encode($content);
  }

  // Получаем кемперы с выбранной комплектацией
  else if(isset($_POST['complectation_id'])) {

    // Костыль Своя комплектация
    if ($_POST['complectation_id'] == 0) $_POST['complectation_id'] = 187;

    $arFilter = Array("IBLOCK_ID"=>"42", "PROPERTY_COMPLECTATION" => $_POST['complectation_id']);
    $arSelect = Array("ID","NAME","PROPERTY_COMPLECTATION", "PROPERTY_CAMPER");
    $resComplects = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter,false,false, $arSelect);
    $campers = array();

    while($obComplects = $resComplects->GetNextElement()){
      $arFields = $obComplects->GetFields();
      $res = CIBlockElement::GetByID($arFields['PROPERTY_CAMPER_VALUE']);
      $content = array();
      if($ar_res = $res->GetNext())
        $content['DETAIL_PICTURE'] = CFile::GetPath($ar_res['DETAIL_PICTURE']);
        $content['NAME'] = $ar_res['NAME'];
        $content['ID'] = $ar_res['ID'];

      array_push($campers, $content);
    }

    echo json_encode($campers);
  }
?>
