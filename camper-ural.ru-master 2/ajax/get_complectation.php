#!/usr/bin/php
<?php
  //$_SERVER["DOCUMENT_ROOT"] = "/home/c7106/ecospaekb.ru/www";
  $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
  define("NO_KEEP_STATISTIC", true);
  define("NOT_CHECK_PERMISSIONS", true);
  set_time_limit(0);
  define('SITE_ID', 's1');

  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

  // Получаем все комплектации выбранного направления
  $arLinkedComplectsFilter = Array("IBLOCK_ID"=>"42", "ID" => $_POST['complect_id']);
  $arSelectComplectsCampers = Array("ID","NAME","PROPERTY_COMPLECT");
  $resLinkedComplects = CIBlockElement::GetList(Array("ID"=>"ASC"), $arLinkedComplectsFilter,false,false, $arSelectComplectsCampers);
  $complectation_ids = array();
  while($obComplects = $resLinkedComplects->GetNextElement()){
    $arLinkedComplectsFields = $obComplects->GetFields();
    $complect = $arLinkedComplectsFields['PROPERTY_COMPLECT_VALUE'];
    array_push($complectation_ids, $complect);
  }

  echo json_encode($complectation_ids);
?>
