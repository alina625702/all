#!/usr/bin/php7.1
<?php
  // Определяем переменные для работы скрипта с библиотеками Битрикса
  set_time_limit(0);
  ini_set('mbstring.func_overload', '2');
  ini_set('memory_limit','1024M');
  define('LANG', 's1');
  define('BX_UTF', true);
  define('NO_KEEP_STATISTIC', true);
  define('NOT_CHECK_PERMISSIONS', true);
  define('BX_BUFFER_USED', true);



  // Считываем данные из таблицы
  $id = '1_4vheP3g4o5-d9ITUTSwPIS26xqG6lkwKaYWZaceAvM';
  $gid = '1019934643';

  $csv = file_get_contents('https://docs.google.com/spreadsheets/d/' . $id . '/export?format=csv&gid=' . $gid);
  $csv = explode("\r\n", $csv);
  $array = array_map('str_getcsv', $csv);

  echo json_encode($array, JSON_UNESCAPED_UNICODE);

?>
