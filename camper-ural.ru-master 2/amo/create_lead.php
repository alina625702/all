<?php

use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\CompaniesCollection;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Collections\LinksCollection;
use AmoCRM\Collections\NullTagsCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Models\TagModel;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Filters\LeadsFilter;
use AmoCRM\Models\CompanyModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Models\NoteType\CommonNote;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\NullCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use League\OAuth2\Client\Token\AccessTokenInterface;
use AmoCRM\Models\CustomFieldsValues\SelectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\SelectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\SelectCustomFieldValueModel;

class amoCRM
{
  public function add_lead($lead_data) {
    include_once __DIR__ . '/bootstrap.php';

    $name = $lead_data['NAME'];
    $phone = $lead_data['PHONE'];
    $email = $lead_data['EMAIL'];

    $camper = $lead_data['PRODUCT'];
    $leadName = $lead_data['LEAD_NAME'];
    $complectation = $lead_data['COMPLECT'];
    $type = $lead_data['TYPE'];
    $price = $lead_data['PRICE'];
    $city = $lead_data['CITY'];

    if($price=='') $price = 0;

    $responsible = 204190;

    $description = $lead_data['TEXT'];
    
    if($complectation != '') {
      $description .= ' Комлектация: ' . $complectation;
    }

    $accessToken = getToken();

    $apiClient->setAccessToken($accessToken)
    ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
    ->onAccessTokenRefresh(
      function (AccessTokenInterface $accessToken, string $baseDomain) {
        saveToken([
            'accessToken' => $accessToken->getToken(),
            'refreshToken' => $accessToken->getRefreshToken(),
            'expires' => $accessToken->getExpires(),
            'baseDomain' => $baseDomain,
        ]);
      }
    );

    $leadsService = $apiClient->leads();

    try {
      $contacts = $apiClient->contacts()->get((new ContactsFilter())->setQuery($phone));
      $contact = $contacts[0];
    } catch(AmoCRMApiException $e) {
      $contact = new ContactModel();
      $contact->setName($name);

      $CustomFieldsValues = new CustomFieldsValuesCollection();

      $emailField = (new MultitextCustomFieldValuesModel())->setFieldCode('EMAIL');
      $emailField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($email)));

      $phoneField = (new MultitextCustomFieldValuesModel())->setFieldCode('PHONE');
      $phoneField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($phone)));

      $CustomFieldsValues->add($emailField);
      $CustomFieldsValues->add($phoneField);

      $contact->setCustomFieldsValues($CustomFieldsValues);
      $contact->setResponsibleUserId($responsible);

      try {
        $contactModel = $apiClient->contacts()->addOne($contact);
      } catch (AmoCRMApiException $e) {
        printError($e);
        die;
      }
    }

    // Создаем сделку
    $lead = new LeadModel();
    $lead->setName($leadName)->setContacts((new ContactsCollection())->add(($contact)));

    $CustomFieldsValues = new CustomFieldsValuesCollection();

    $camperField = (new SelectCustomFieldValuesModel())->setFieldId(1407589);
    $camperField->setValues((new SelectCustomFieldValueCollection())->add((new SelectCustomFieldValueModel())->setValue($camper)));

    $typeField = (new TextCustomFieldValuesModel())->setFieldId(1415449);
    $typeField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($type)));

    $utmContentField = (new TextCustomFieldValuesModel())->setFieldId(1407557);
    $utmContentField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($_COOKIE['utm_content'])));

    $utmSourceField = (new TextCustomFieldValuesModel())->setFieldId(1407549);
    $utmSourceField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($_COOKIE['utm_source'])));

    $utmMediumField = (new TextCustomFieldValuesModel())->setFieldId(1407551);
    $utmMediumField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($_COOKIE['utm_medium'])));

    $utmCampaignField = (new TextCustomFieldValuesModel())->setFieldId(1407553);
    $utmCampaignField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($_COOKIE['utm_campaign'])));

    $utmTermField = (new TextCustomFieldValuesModel())->setFieldId(1407555);
    $utmTermField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($_COOKIE['utm_term'])));

    $cityField = (new TextCustomFieldValuesModel())->setFieldId(1416299);
    $cityField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($city)));

    $CustomFieldsValues->add($utmContentField);
    $CustomFieldsValues->add($utmSourceField);
    $CustomFieldsValues->add($utmMediumField);
    $CustomFieldsValues->add($utmCampaignField);
    $CustomFieldsValues->add($utmTermField);
    $CustomFieldsValues->add($camperField);
    $CustomFieldsValues->add($typeField);
    $CustomFieldsValues->add($cityField);

    $lead->setCustomFieldsValues($CustomFieldsValues);

    $lead->setPrice($price);
    $lead->setTags((new TagsCollection())
        ->add(
            (new TagModel())
                ->setName('Заявка с сайта')
    ));
    $lead->setResponsibleUserId($responsible);

    $leadsCollection = new LeadsCollection();
    $leadsCollection->add($lead);

    try {
      $leadsCollection = $leadsService->add($leadsCollection);
      $lead_id = $leadsCollection[0]->id;
      if($companyName != '') {
        //Создадим компанию
        $company = new CompanyModel();
        $company->setName($companyName);

        $companiesCollection = new CompaniesCollection();
        $companiesCollection->add($company);
        try {
            $apiClient->companies()->add($companiesCollection);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }

        $links = new LinksCollection();
        $links->add($contact);
        try {
            $apiClient->companies()->link($company, $links);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }
      }
      if($description != '') {
        $notesCollection = new NotesCollection();
        $serviceMessageNote = new CommonNote();
        $serviceMessageNote->setEntityId($lead_id)->setText($description);

        $notesCollection->add($serviceMessageNote);

        try {
            $leadNotesService = $apiClient->notes(EntityTypesInterface::LEADS);
            $notesCollection = $leadNotesService->add($notesCollection);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }
      }
      return $lead_id;
    } catch (AmoCRMApiException $e) {
        printError($e);
        die;
    }

  }
}
