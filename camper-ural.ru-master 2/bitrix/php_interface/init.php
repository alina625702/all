<?
include_once $_SERVER['DOCUMENT_ROOT'] . 'amo/create_lead.php';
//print_r($_SERVER['DOCUMENT_ROOT'] . 'amo/create_lead.php');

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");

function OnAfterIBlockElementAddHandler(&$arFields) {

  if(!$arFields["RESULT"])
      return false;

  if(in_array($arFields['IBLOCK_ID'], [17, 20, 21, 23, 40, 43, 44])){

    $leadName = 'Заявка с сайта';
    switch ($arFields['IBLOCK_ID']) {
      case 17: $leadName = 'Заявка с сайта. Заказать комплектацию'; break;
      case 20: $leadName = 'Заявка с сайта. Подобрать кемпер'; break;
      case 21: $leadName = 'Заявка с сайта. Обратный звонок'; break;
      case 23: $leadName = 'Заявка с сайта. Нужна консультация'; break;
      case 40: $leadName = 'Заявка с сайта. Купить в кредит'; break;
      case 43: $leadName = 'Заявка с сайта. Подбор кемпера'; break;
      case 44: $leadName = 'Заявка с сайта. Квиз'; break;
    }
    $products = [
      'Капля',
      'Кочевник',
      'Кочевник Плюс',
      'Турист',
      'Турист Плюс'
    ];

    $complectations = [
      'Выезды с друзьями',
      'Семья с детьми',
      'Туризм парой',
      'Спортсмены / велосипеды',
      'Рыбалка / Охота'
    ];

    $amoCrm = new AmoCRM();
    $lead_data = array();

    $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["NAME"];
    $lead_data['PHONE'] =  $arFields["PROPERTY_VALUES"]["PHONE"];
    $lead_data['EMAIL'] = $arFields["PROPERTY_VALUES"]["EMAIL"];
    $lead_data['COMPANY'] = $arFields["PROPERTY_VALUES"]["COMPANY"];

    $lead_data['TEXT'] = $arFields["PROPERTY_VALUES"]["MESSAGE"]["VALUE"]["TEXT"];
    $lead_data['LEAD_NAME'] = $leadName;

    $lead_data['CITY'] = $arFields["PROPERTY_VALUES"]["CITY"];

    if(isset($arFields["PROPERTY_VALUES"]["FIO"])) {
      $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["FIO"];
    }


    $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["CAMPER"];
    $lead_data['TYPE'] = $arFields["PROPERTY_VALUES"]["NEED_PRODUCT"];
    $lead_data['COMPLECT'] = $arFields["PROPERTY_VALUES"]["COMPLECT"];
    $lead_data['PRICE'] = $arFields["PROPERTY_VALUES"]["COMPLECT_PRICE"];

    if(in_array($arFields["PROPERTY_VALUES"]["PRODUCT"], $products)) {
      $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["PRODUCT"];
    } else {
      $lead_data['TEXT'] .= ' Кемпер: ' .  $arFields["PROPERTY_VALUES"]["PRODUCT"];
    }

    amoCRM::add_lead($lead_data);
  }
}
