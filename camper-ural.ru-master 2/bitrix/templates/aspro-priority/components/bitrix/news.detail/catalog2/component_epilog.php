<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
if(isset($templateData['SECTION_BNR_CONTENT']) && $templateData['SECTION_BNR_CONTENT'] == true)
{
	global $SECTION_BNR_CONTENT;
	$SECTION_BNR_CONTENT = true;
}
$bOrderViewBasket = $templateData["ORDER"];
?>
<?$arOrder = explode(",", $arParams["LIST_PRODUCT_BLOCKS_ALL_ORDER"]);?>
<div class="maxwidth-theme wttabs">
	<div class="row">
		<div class="detail_left_block col-md-12">
			<?foreach($arOrder as $value):?>
				<div class="drag_block  <?=($value=='gallery' ? 'col-md-12' : 'col-md-6');?> <?=$value;?>">

					<?if($value == "char" && $templateData['CHARACTERISTICS']):?>
						<div class="wraps chars">
							<?foreach($arOrder as $value):?>
							<?if($value == "desc" && $templateData['DETAIL_TEXT']):?>
								<div class="content wttabs" itemprop="description">
									<?// element detail text?>
									<?=$templateData['DETAIL_TEXT'];?>
									</div>
							<?endif;?>
							<?endforeach;?>
						
						<div class="row">
							<div class="col-md-12">
								<!-- <div class="row main_blocks_row"><div class="col-md-3 main_blocks"><img src="/images/icons/credit-card.png"><p>Кредит</p></div><div class="col-md-9"><p>Покупка кемпера стала проще: подайте заявку на кредит сразу на сайте или в нашем офисе</p></div></div> -->
								<div class="row main_blocks_row"><div class="col-md-3 main_blocks"><img src="/images/icons/dollar.png"><p>Оплата</p></div><div class="col-md-9"><p>Обычно заказ жилого прицепа происходит так:<br>1. Мы утверждаем нужную конфигурацию и заключаем договор;<br>2. Вы вносите предоплату 70%, а мы изготавливаем кемпер;<br>3. Вы вносите остальную часть оплаты и получаете свой заказ лично или через транспортную компанию</p></div></div>
								<div class="row main_blocks_row"><div class="col-md-3 main_blocks"><img src="/images/icons/delivery.png"><p>Доставка</p></div><div class="col-md-9"><p>Доставка кемперов по России и в другие страны (в том числе Казахстан, Узбекистан и т. д.) возможна с помощью транспортных компаний.<br>Наши партнеры: Рутранс, Логистический сервис, Деловые линии, Кит, ПЭК, Энергия.</p></div></div>
									<div class="row main_blocks_row"><div class="col-md-3 main_blocks"><img src="/images/icons/guarantee.png"><p>Гарантия</p></div><div class="col-md-9"><p>12 мес на кузов, и 15 лет на раму от сквозной коррозии</p></div></div>
							</div>
						</div>

						</div>
					<?endif;?>
					<?//show tarifs block?>
					<?if($value == "tarifs" && $templateData['LINK_TARIFS']):?>
						<div class="wraps">
							<?$GLOBALS['arrTarifsFilter'] = array('ID' => $templateData['LINK_TARIFS']);?>
							<h4><?=($arParams["T_TARIF"] ? $arParams["T_TARIF"] : Loc::getMessage("T_TARIF"));?></h4>
							<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'])."/include/comp_tarifs.php");?>
						</div>
					<?endif;?>
					<?//show docs block?>
					<?if($value == "docs" && $templateData['DOCUMENTS']):?>
						<div class="wraps">
							<h4><?=($arParams["T_DOCS"] ? $arParams["T_DOCS"] : Loc::getMessage("T_DOCS"));?></h4>
							<div class="docs-block">
								<div class="docs_wrap">
									<div class="row">
										<?foreach($templateData['DOCUMENTS'] as $docID):?>
											<?$arItem = CPriority::get_file_info($docID);?>
											<div class="col-md-4">
												<?
												$fileName = substr($arItem['ORIGINAL_NAME'], 0, strrpos($arItem['ORIGINAL_NAME'], '.'));
												$fileTitle = (strlen($arItem['DESCRIPTION']) ? $arItem['DESCRIPTION'] : $fileName);

												?>
												<div class="blocks clearfix <?=$arItem["TYPE"];?>">
													<div class="inner-wrapper">
														<a href="<?=$arItem['SRC']?>" class="dark-color text" target="_blank"><?=$fileTitle?></a>
														<div class="filesize font_xs"><?=CPriority::filesize_format($arItem['FILE_SIZE']);?></div>
													</div>
												</div>
											</div>
										<?endforeach;?>
									</div>
								</div>
							</div>
						</div>
					<?endif;?>
					<?//show video block?>
					<?if($value == "video" && ($templateData['VIDEO'] || $templateData['VIDEO_IFRAME'])):?>
						<div class="wraps">
							<div class="video">
								<?
								if($templateData['VIDEO'] && $templateData['VIDEO_IFRAME'])
									$count = 2;
								elseif($templateData['VIDEO'])
									$count = count($templateData['VIDEO']);
								elseif($templateData['VIDEO_IFRAME'])
									$count = count($templateData['VIDEO_IFRAME']);
								?>
								<?if($templateData['VIDEO']):?>
									<?foreach($templateData['VIDEO'] as $i => $arVideo):?>
										<div class="item">
											<div class="video_body">
												<video id="js-video_<?=$i?>" class="video-js" controls="controls" preload="metadata" data-setup="{}">
													<source src="<?=$arVideo["path"]?>" type='video/mp4' />
													<p class="vjs-no-js">
														To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video
													</p>
												</video>
											</div>
											<div class="title"><?=(strlen($arVideo["title"]) ? $arVideo["title"] : '')?></div>
										</div>
									<?endforeach;?>
								<?endif;?>
								<?if($templateData['VIDEO_IFRAME']):?>
									<?foreach($templateData['VIDEO_IFRAME'] as $i => $video):?>
										<div class="item">
											<div class="video_body">
												<?if(strpos($video, '<iframe') !== false):?>
													<?=$video;?>
												<?else:?>
													<?
													$video = str_replace('watch?v=', 'embed/', $video);
													$video = str_replace('youtu.be/', 'youtube.com/embed/', $video)
													?>
													<iframe width="560" height="315" src="<?=$video;?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
												<?endif;?>
											</div>
											<div class="title"></div>
										</div>
									<?endforeach;?>
								<?endif;?>
							</div>
						</div>
					<?endif;?>

					<?//show projects block?>
					<?if($value == "projects" && $templateData['LINK_PROJECTS']):?>
						<div class="wraps">
							<h4><?=($arParams["T_PROJECTS"] ? $arParams["T_PROJECTS"] : Loc::getMessage("T_PROJECTS"));?></h4>
							<?$GLOBALS['arrProjectFilter'] = array('ID' => $templateData['LINK_PROJECTS']);?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"projects_linked_2",
								array(
									"IBLOCK_TYPE" => "aspro_priority_content",
									"IBLOCK_ID" => $arParams["PROJECTS_IBLOCK_ID"],
									"NEWS_COUNT" => "20",
									"SORT_BY1" => "SORT",
									"SORT_ORDER1" => "ASC",
									"SORT_BY2" => "ID",
									"SORT_ORDER2" => "DESC",
									"FILTER_NAME" => "arrProjectFilter",
									"FIELD_CODE" => array(
										0 => "NAME",
										1 => "PREVIEW_TEXT",
										2 => "PREVIEW_PICTURE",
										3 => "",
									),
									"PROPERTY_CODE" => array(
										0 => "LINK",
										1 => "TEXTCOLOR",
									),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "Y",
									"CACHE_GROUPS" => "N",
									"PREVIEW_TRUNCATE_LEN" => "",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"SET_TITLE" => "N",
									"SET_STATUS_404" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"INCLUDE_SUBSECTIONS" => "Y",
									"PAGER_TEMPLATE" => ".default",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "Y",
									"PAGER_TITLE" => "�������",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"VIEW_TYPE" => "list",
									"IMAGE_POSITION" => "left",
									"COUNT_IN_LINE" => "3",
									"SHOW_TITLE" => "Y",
									"T_PROJECTS" => ($arParams["T_PROJECTS"] ? $arParams["T_PROJECTS"] : Loc::getMessage("T_PROJECTS")),
									"AJAX_OPTION_ADDITIONAL" => ""
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						</div>
					<?endif;?>

					<?//show faq block?>
					<?if($value == "faq" && $templateData['LINK_FAQ']):?>
						<div class="wraps">
							<h4><?=($arParams["T_FAQ"] ? $arParams["T_FAQ"] : Loc::getMessage("T_FAQ"));?></h4>
							<?$GLOBALS['arrFaqFilter'] = array('ID' => $templateData['LINK_FAQ']);?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"items_list",
								array(
									"IBLOCK_TYPE" => "aspro_priority_content",
									"IBLOCK_ID" => $arParams["FAQ_IBLOCK_ID"],
									"NEWS_COUNT" => "20",
									"SORT_BY1" => "SORT",
									"SORT_ORDER1" => "ASC",
									"SORT_BY2" => "ID",
									"SORT_ORDER2" => "DESC",
									"FILTER_NAME" => "arrFaqFilter",
									"FIELD_CODE" => array(
										0 => "PREVIEW_TEXT",
										1 => "",
									),
									"PROPERTY_CODE" => array(
										0 => "LINK",
										1 => "",
									),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "Y",
									"CACHE_GROUPS" => "N",
									"PREVIEW_TRUNCATE_LEN" => "",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"SET_TITLE" => "N",
									"SET_STATUS_404" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"INCLUDE_SUBSECTIONS" => "Y",
									"PAGER_TEMPLATE" => ".default",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "Y",
									"PAGER_TITLE" => "�������",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"VIEW_TYPE" => "accordion",
									"IMAGE_POSITION" => "left",
									"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
									"COUNT_IN_LINE" => "3",
									"SHOW_TITLE" => "Y",
									"T_TITLE" => ($arParams["T_FAQ"] ? $arParams["T_FAQ"] : Loc::getMessage("T_FAQ")),
									"AJAX_OPTION_ADDITIONAL" => "",
									"SHOW_SECTION_NAME" => "N"
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						</div>
					<?endif;?>

					<?//show services block?>
					<?if($value == "services"):?>
						<?if($templateData['LINK_SERVICES']):?>
							<div class="wraps">
								<h4><?=(strlen($arParams['T_SERVICES']) ? $arParams['T_SERVICES'] : Loc::getMessage('T_SERVICES'))?></h4>
								<?$GLOBALS['arrServicesFilter'] = array('ID' => $templateData['LINK_SERVICES']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									$arParams["SERVICES_LINK_ELEMENTS_TEMPLATE"],
									array(
										"IBLOCK_TYPE" => "aspro_priority_content",
										"IBLOCK_ID" => $arParams["SERVICES_IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrServicesFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "PREVIEW_TEXT",
										),
										"PROPERTY_CODE" => array(
											0 => "ICON",
											1 => "",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif;?>
					<?//show sale block?>
					<?if($value == "sale"):?>
						<?if(count($templateData['LINK_SALE'])):?>
							<?$GLOBALS['arrSaleFilter'] = array('ID' => $templateData['LINK_SALE']); ?>
							<div class="wraps">
								<h4><?=(strlen($arParams['T_NEWS']) ? $arParams['T_NEWS'] : Loc::getMessage('T_NEWS'))?></h4>
								<div class="stockblock">
									<?$APPLICATION->IncludeComponent(
										"bitrix:news.list",
										"sales_linked",
										array(
											"IBLOCK_TYPE" => "aspro_priority_content",
											"IBLOCK_ID" => $arParams["NEWS_IBLOCK_ID"],
											"NEWS_COUNT" => "20",
											"SORT_BY1" => "SORT",
											"SORT_ORDER1" => "ASC",
											"SORT_BY2" => "ID",
											"SORT_ORDER2" => "DESC",
											"FILTER_NAME" => "arrSaleFilter",
											"FIELD_CODE" => array(
												0 => "NAME",
												1 => "PREVIEW_TEXT",
												3 => "DATE_ACTIVE_FROM",
												4 => "PREVIEW_PICTURE",
											),
											"PROPERTY_CODE" => array(
												0 => "PERIOD",
												1 => "REDIRECT",
												2 => "",
											),
											"CHECK_DATES" => "Y",
											"DETAIL_URL" => "",
											"AJAX_MODE" => "N",
											"AJAX_OPTION_JUMP" => "N",
											"AJAX_OPTION_STYLE" => "Y",
											"AJAX_OPTION_HISTORY" => "N",
											"CACHE_TYPE" => "A",
											"CACHE_TIME" => "36000000",
											"CACHE_FILTER" => "Y",
											"CACHE_GROUPS" => "N",
											"PREVIEW_TRUNCATE_LEN" => "",
											"ACTIVE_DATE_FORMAT" => "d.m.Y",
											"SET_TITLE" => "N",
											"SET_STATUS_404" => "N",
											"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
											"ADD_SECTIONS_CHAIN" => "N",
											"HIDE_LINK_WHEN_NO_DETAIL" => "N",
											"PARENT_SECTION" => "",
											"PARENT_SECTION_CODE" => "",
											"INCLUDE_SUBSECTIONS" => "Y",
											"PAGER_TEMPLATE" => ".default",
											"DISPLAY_TOP_PAGER" => "N",
											"DISPLAY_BOTTOM_PAGER" => "Y",
											"PAGER_TITLE" => "�������",
											"PAGER_SHOW_ALWAYS" => "N",
											"PAGER_DESC_NUMBERING" => "N",
											"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
											"PAGER_SHOW_ALL" => "N",
											"VIEW_TYPE" => "table",
											"BIG_BLOCK" => "Y",
											"IMAGE_POSITION" => "left",
											"COUNT_IN_LINE" => "2",
										),
										false, array("HIDE_ICONS" => "Y")
									);?>
								</div>
							</div>
						<?endif;?>
					<?endif;?>
					<?//show goods block?>

					<?if($value == "goods"):?>
						<?if($templateData['LINK_GOODS']):?>

							<div class="wraps goods-block">
								<h4><?=(strlen($arParams['T_ITEMS']) ? $arParams['T_ITEMS'] : Loc::getMessage('T_ITEMS'))?></h4>
								<?$GLOBALS['arrGoodsFilter'] = array('ID' => $templateData['LINK_GOODS']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									$templateData['CATALOG_LINKED_TEMPLATE'],
									Array(
										"S_ORDER_PRODUCT" => $arParams["S_ORDER_SERVISE"],
										"IBLOCK_TYPE" => "aspro_priority_catalog",
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrGoodsFilter",
										"FIELD_CODE" => array(
											0 => "NAME",
											1 => "PREVIEW_TEXT",
											2 => "PREVIEW_PICTURE",
											3 => "DETAIL_PICTURE",
											4 => "",
										),
										"PROPERTY_CODE" => array(
											0 => "PRICE",
											1 => "PRICEOLD",
											2 => "STATUS",
											3 => "ARTICLE",
											6 => "CATEGORY",
											7 => "RECOMMEND",
											10 => "DELIVERY",
											21 => "SUPPLIED",
											23 => "LANGUAGES",
											24 => "DURATION",
											25 => "UPDATES",
											26 => "RANGE_MEASURE",
											27 => "WORK_TEMP",
											28 => "NUM_CHANNELS",
											29 => "DIMENSIONS",
											30 => "MASS",
											31 => "MAX_SPEED",
											32 => "MODEL_ENGINE",
											33 => "VOLUME_ENGINE",
											34 => "SEATS",
											35 => "COUNTRY",
											36 => "WORK_PRESSURE",
											37 => "BENDING_ANGLE",
											38 => "ENGINE_POWER",
											39 => "WORK_SPEED",
											40 => "GUARANTEE",
											41 => "COMMUNIC_PORT",
											42 => "INNER_MEMORY",
											43 => "PRESS_POWER",
											44 => "MAXIMUM_PRESSURE",
											45 => "MAX_SIZE_ZAG",
											46 => "BENDING_SIZE",
											47 => "MAX_MASS_ZAG",
											48 => "POWER_LS",
											49 => "V_DVIGATELJA",
											50 => "RAZGON",
											51 => "BRAND",
											52 => "PROIZVODITEKNOST",
											53 => "MAX_POWER_LS",
											54 => "LINK_SERTIFICATES",
											55 => "MAX_POWER",
											56 => "AGE",
											57 => "KARTOPR",
											58 => "DEPTH",
											59 => "GRUZ",
											60 => "GRUZ_STRELI",
											61 => "DLINA_STRELI",
											62 => "DLINA",
											63 => "CLASS",
											64 => "KOL_FORMULA",
											65 => "MARK_STEEL",
											66 => "MODEL",
											67 => "POWER",
											68 => "VOLUME",
											69 => "PROIZVODSTVO",
											70 => "SIZE",
											71 => "SPEED",
											72 => "TYPE_TUR",
											73 => "THICKNESS",
											74 => "MARK",
											75 => "FREQUENCY",
											76 => "WIDTH_PROHOD",
											77 => "WIDTH_PROEZD",
											78 => "WIDTH",
											79 => "PLACE_CLOUD",
											80 => "TYPE",
											81 => "COLOR",
											82 => "",
											83 => "",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"SHOW_DETAIL_LINK" => "Y",
										"COUNT_IN_LINE" => "3",
										"IMAGE_POSITION" => "left",
										"ORDER_VIEW" => $bOrderViewBasket,
									),
								false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif;?>
					<?// show partners block?>
					<?if($value == 'partners'):?>
						<?if($templateData['LINK_PARTNERS']):?>
							<div class="wraps goods-block">
								<h4><?=(strlen($arParams['T_PARTNERS']) ? $arParams['T_PARTNERS'] : Loc::getMessage('T_PARTNERS'))?></h4>
								<?$GLOBALS['arrPartnersFilter'] = array('ID' => $templateData['LINK_PARTNERS']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"partners_linked",
									array(
										"IBLOCK_TYPE" => "aspro_priority_content",
										"IBLOCK_ID" => $arParams["PARTNERS_IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrPartnersFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "PREVIEW_TEXT",
										),
										"PROPERTY_CODE" => array(
											0 => "SITE",
											1 => "PHONE",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif?>

					<?// show staff block?>
					<?if($value == 'staff'):?>
						<?if($templateData['LINK_STAFF']):?>
							<div class="wraps goods-block">
								<h4><?=(strlen($arParams['T_STAFF']) ? $arParams['T_STAFF'] : Loc::getMessage('T_STAFF'))?></h4>
								<?$GLOBALS['arrStaffFilter'] = array('ID' => $templateData['LINK_STAFF']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"staff_linked",
									array(
										"IBLOCK_TYPE" => "aspro_priority_content",
										"IBLOCK_ID" => $arParams["STAFF_IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrStaffFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "",
										),
										"PROPERTY_CODE" => array(
											0 => "POST",
											1 => "PHONE",
											2 => "EMAIL",
											3 => "SEND_MESSAGE_BUTTON",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif?>
					<?// show vacancys block?>
					<?if($value == 'vacancys'):?>

						<?if($templateData['LINK_COMPLECT']):?>
							<div class="wraps goods-block">
								<?$GLOBALS['arrVacancysFilter'] = array('ID' => $templateData['LINK_COMPLECT']);?>

								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"vacancy_linked",
									array(
										"IBLOCK_TYPE" => "aspro_priority_catalog",
										"IBLOCK_ID" => 41,
										"CAMPER_ID" => $arResult['ID'],
										"NEWS_COUNT" => "100",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrVacancysFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "PREVIEW_TEXT",
											3 => "DETAIL_PICTURE",
										),
										"PROPERTY_CODE" => array(
											0 => "CITY",
											1 => "PAY",
											2 => "QUALITY",
											3 => "WORK_TYPE",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
								<div class="bottom-wrapper">
							<?if(strlen($arResult['PRICE_PROPERTIES']['PRICE']['VALUE'])):?>
								<div class="price">
									<div class="price_new"><span class="green_price">СТОИМОСТЬ ПРИЦЕПА: </span><span class="price_val green_price"><?=number_format($arResult['PRICE_PROPERTIES']['PRICE']['VALUE'], 0, ',', ' ')?></span><span class="green_price">Р</span></div>
									<?if(strlen($arResult['PRICE_PROPERTIES']['PRICEOLD']['VALUE'])):?>
										<div class="price_old">
											<span class="price_val">
												<?if(isset($arResult['PROPERTIES']["PRICE_CURRENCY"]) && $arResult['PROPERTIES']["PRICE_CURRENCY"]["VALUE_XML_ID"] != NULL):?>
													<?=str_replace('#CURRENCY#',$arResult['PROPERTIES']["PRICE_CURRENCY"]["VALUE"], $arResult['PRICE_PROPERTIES']['PRICEOLD']['VALUE']);?>
												<?else:?>
													<?=$arResult['PRICE_PROPERTIES']['PRICEOLD']['VALUE']?>
												<?endif;?>
											</span>
										</div>
									<?endif;?>
								</div>
							<?endif;?>
							<div class="wrapper-block-btn order">
								<div class="wrapper">
									<span class="btn btn-default animate-load" data-event="jqm" data-param-id="17" data-name="order_product" data-autoload-complect_price="<?=$arResult['PRICE_PROPERTIES']['PRICE']['VALUE'] ?>"  data-autoload-complect="" data-autoload-product="<?=CPriority::formatJsName($arResult['NAME'])?>">Заказать комплектацию</span>
								</div>
								<div class="wrapper">
									<span class="btn btn-default wide-block animate-load btn-transparent" data-event="jqm" data-param-id="40" data-autoload-complect_price="<?=$arResult['PRICE_PROPERTIES']['PRICE']['VALUE'] ?>" data-autoload-complect="" data-autoload-product="<?=CPriority::formatJsName($arResult['NAME'])?>" data-name="question"><span>Купить в кредит от <span id="button_credit"><?=number_format($arResult['PRICE_PROPERTIES']['PRICE']['VALUE']/10, 0, ',', ' ');?>Р</span></span></span>
								</div>
							</div>
							<script>

	BX.ready( function(){
		var popup = BX.PopupWindowManager.create( "popup-message", null, {
			content: BX('modal-image'),//`<img id="modal-image" src="">`,
			width: 800, // ширина окна
			height: 800, // высота окна
			offsetTop: 1,
			offsetLeft: 0,
			zIndex: 100, // z-index
			closeIcon: {
				// объект со стилями для иконки закрытия, при null - иконки не будет
				opacity: 1
			},
			//titleBar: 'Заголовок окна',
			closeByEsc: true, // закрытие окна по esc
			darkMode: false, // окно будет светлым или темным
			autoHide: true, // закрытие при клике вне окна
			draggable: false, // можно двигать или нет
			resizable: false, // можно ресайзить
			min_height: 300, // минимальная высота окна
			min_width: 300, // минимальная ширина окна
			lightShadow: true, // использовать светлую тень у окна
			angle: true, // появится уголок
			overlay: {
				// объект со стилями фона
				backgroundColor: 'black',
				opacity: 500
			},
			events: {
				onPopupShow: function(){
				},

				onPopupClose: function(){
					$( "#modal-image" ).attr( "src", '' );
					// Событие при закрытии окна
				}
			}
		} );

		$( '.img_modal' ).click( function(){ // задаем функцию при нажатиии на элемент <div>

			popup.show();
			console.log( 'click elem' );

			var imgSrc = $( this ).data( 'image' );

			$( "#modal-image" ).attr( "src", imgSrc );

				console.log( imgSrc );
			//$('.image-wrapper').html('<img id="modal-image" src="'+imgSrc+'">');

		} );
	} );

</script>
							<?// element buy block?>
							<?//if($bOrderViewBasket && $arResult['DISPLAY_PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'] == 'YES'):?>
							<?if($arResult['DISPLAY_PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'] == 'YES' || $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE_XML_ID'] == 'YES'):?>
								<?if($bOrderViewBasket && $arResult['DISPLAY_PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'] == 'YES'):?>
									<div class="buy_block lg clearfix">
										<div class="counter pull-left">
											<div class="wrap">
												<span class="minus ctrl">
													<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/minus.svg')?>
												</span>
												<div class="input"><input type="text" value="1" class="count" /></div>
												<span class="plus ctrl">
													<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/plus.svg')?>
												</span>
											</div>
										</div>
										<div class="buttons pull-right">
											<span class="btn btn-default pull-right to_cart animate-load" data-quantity="1"><span><?=Loc::getMessage('BUTTON_TO_CART')?></span></span>
											<a href="<?=$basketURL?>" class="btn btn-default pull-right in_cart"><span><?=Loc::getMessage('BUTTON_IN_CART')?></span></a>
										</div>
									</div>
								<?endif;?>
								<div class="wrapper-block-btn order<?=($bOrderViewBasket ? ' basketTrue' : '')?>">
									<?if(!$bOrderViewBasket):?>
										<div class="wrapper">
											<span class="btn btn-default animate-load" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_order_product");?>" data-name="order_product" data-autoload-product="<?=CPriority::formatJsName($arResult['NAME'])?>"><?=(strlen($arParams['S_ORDER_SERVISE']) ? $arParams['S_ORDER_SERVISE'] : Loc::getMessage('S_ORDER_SERVISE'))?></span>
										</div>
									<?endif;?>
									<?if($bFormQuestion && $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE_XML_ID'] == 'YES'):?>
										<div class="wrapper">
											<span class="btn btn-default wide-block animate-load btn-transparent" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_question");?>" data-autoload-need_product="<?=CPriority::formatJsName($arResult['NAME'])?>" data-name="question"><span><?=(strlen($arParams['S_ASK_QUESTION']) ? $arParams['S_ASK_QUESTION'] : Loc::getMessage('S_ASK_QUESTION'))?></span></span>
										</div>
									<?endif?>
								</div>
							<?else:?>
								<?if($arResult['PROPERTIES']['LINK_TARIF']['VALUE']):?>
									</div>
								<?endif;?>
							<?endif;?>
						</div>
							</div>
						<?endif;?>
					<?endif?>
					<?// show reviews block?>
					<?//show gallery block?>
					<?if($value == "gallery"):?>
						<?if(count($templateData['GALLERY_BIG'])):?>
							<?
							$bShowSmallGallery = $templateData['GALLERY_TYPE'] === 'small';
							?>
							<div class="wraps galerys-block">
								<span class="switch_gallery<?=($bShowSmallGallery ? ' small' : '');?>"></span>
								<div class="title small-gallery font_xs"<?=($bShowSmallGallery ? ' style="display:block;"' : '');?>><?=count($templateData['GALLERY_BIG']).'&nbsp;'.Loc::getMessage('T_GALLERY_TITLE')?></div>
								<div class="title big-gallery font_xs"<?=($bShowSmallGallery ? ' style="display:none;"' : '');?>><span class="slide-number">1</span> / <?=count($templateData['GALLERY_BIG'])?></div>
								<div class="big-gallery-block thmb1 flexslider unstyled row bigs wsmooth"<?=($bShowSmallGallery ? ' style="display:none;"' : '');?> id="slider" data-plugin-options='{"animation": "slide", "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "sync": ".gallery-wrapper .small-gallery", "counts": [1, 1, 1], "smoothHeight": true}'>
									<ul class="slides items">
										<?foreach($templateData['GALLERY_BIG'] as $i => $arPhoto):?>
											<li class="col-md-12 item">
												<img src="<?=$arPhoto['PREVIEW']['src']?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
												<a href="<?=$arPhoto['DETAIL']['SRC']?>" class="fancybox" rel="gallery" target="_blank" title="<?=$arPhoto['TITLE']?>">
													<span class="zoom">
														<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/zoom.svg');?>
													</span>
												</a>
											</li>
										<?endforeach;?>
									</ul>
								</div>
								<div class="small-gallery-block"<?=($bShowSmallGallery ? ' style="display:block;"' : '');?>>
									<div class="front bigs">
										<div class="items row">
											<?foreach($templateData['GALLERY_BIG'] as $i => $arPhoto):?>
												<div class="col-md-3 col-sm-4 col-xs-6">
													<div class="item">
														<div class="wrap">
															<img src="<?=$arPhoto['PREVIEW']['src']?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
														</div>
													</div>
												</div>
											<?endforeach;?>
										</div>
									</div>
								</div>
							</div>
						<?endif;?>
					<?endif;?>
					<?if($value == 'reviews'):?>
						<?if($templateData['LINK_REVIEWS']):?>
							<div class="wraps goods-block">
								<h2><?=(strlen($arParams['T_REVIEWS']) ? $arParams['T_REVIEWS'] : Loc::getMessage('T_REVIEWS'))?></h2>
								<?$GLOBALS['arrReviewsFilter'] = array('ID' => $templateData['LINK_REVIEWS']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"reviews_linked",
									array(
										"IBLOCK_TYPE" => "aspro_priority_content",
										"IBLOCK_ID" => $arParams["REVIEWS_IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrReviewsFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "PREVIEW_TEXT",
											3 => "DETAIL_PICTURE",
										),
										"PROPERTY_CODE" => array(
											0 => "NAME",
											1 => "POST",
											2 => "RATING",
											3 => "MESSAGE",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "300",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif?>
					<?// show sertificates block?>
					<?if($value == 'sertificates'):?>
						<?if($templateData['LINK_SERTIFICATES']):?>
							<div class="wraps goods-block">
								<h4><?=(strlen($arParams['T_SERTIFICATES']) ? $arParams['T_SERTIFICATES'] : Loc::getMessage('T_SERTIFICATES'))?></h4>
								<?$GLOBALS['arrSertificatesFilter'] = array('ID' => $templateData['LINK_SERTIFICATES']);?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"services-slider",
									array(
										"IBLOCK_TYPE" => "aspro_priority_content",
										"IBLOCK_ID" => $arParams["SERTIFICATES_IBLOCK_ID"],
										"NEWS_COUNT" => "20",
										"SORT_BY1" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_BY2" => "ID",
										"SORT_ORDER2" => "DESC",
										"FILTER_NAME" => "arrSertificatesFilter",
										"FIELD_CODE" => array(
											0 => "PREVIEW_PICTURE",
											1 => "NAME",
											2 => "PREVIEW_TEXT",
										),
										"PROPERTY_CODE" => array(
											0 => "",
											1 => "",
										),
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"AJAX_OPTION_HISTORY" => "N",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_FILTER" => "Y",
										"CACHE_GROUPS" => "N",
										"PREVIEW_TRUNCATE_LEN" => "",
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"SET_TITLE" => "N",
										"SET_STATUS_404" => "N",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"ADD_SECTIONS_CHAIN" => "N",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"INCLUDE_SUBSECTIONS" => "Y",
										"PAGER_TEMPLATE" => ".default",
										"DISPLAY_TOP_PAGER" => "N",
										"DISPLAY_BOTTOM_PAGER" => "Y",
										"PAGER_TITLE" => "�������",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"VIEW_TYPE" => "table",
										"BIG_BLOCK" => "Y",
										"IMAGE_POSITION" => "left",
										"COUNT_IN_LINE" => "2",
									),
									false, array("HIDE_ICONS" => "Y")
								);?>
							</div>
						<?endif;?>
					<?endif?>
					<?//show comments block?>
					<?if($value == "comments"):?>
						<?if($arParams["DETAIL_USE_COMMENTS"] == "Y"):?>
							<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/rating_likes.js");?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:catalog.comments",
								"main",
								array(
									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									"COMMENTS_COUNT" => $arParams['COMMENTS_COUNT'],
									"ELEMENT_CODE" => "",
									"ELEMENT_ID" => $arResult["ID"],
									"FB_USE" => $arParams["FB_USE"],
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"IBLOCK_TYPE" => "aspro_priority_content",
									"SHOW_DEACTIVATED" => "N",
									"TEMPLATE_THEME" => "blue",
									"URL_TO_COMMENT" => "",
									"VK_USE" => $arParams["VK_USE"],
									"AJAX_POST" => "Y",
									"WIDTH" => "",
									"COMPONENT_TEMPLATE" => ".default",
									"BLOG_USE" => $arParams["BLOG_USE"],
									"BLOG_TITLE" => $arParams["BLOG_TITLE"],
									"BLOG_URL" => $arParams["BLOG_URL"],
									"PATH_TO_SMILE" => '',
									"EMAIL_NOTIFY" => $arParams["BLOG_EMAIL_NOTIFY"],
									"SHOW_SPAM" => "Y",
									"SHOW_RATING" => "Y",
									"RATING_TYPE" => "like_graphic",
									"FB_TITLE" => $arParams["FB_TITLE"],
									"FB_USER_ADMIN_ID" => "",
									"FB_APP_ID" => $arParams["FB_APP_ID"],
									"FB_COLORSCHEME" => "light",
									"FB_ORDER_BY" => "reverse_time",
									"VK_TITLE" => $arParams["VK_TITLE"],
									"VK_API_ID" => $arParams["VK_API_ID"]
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						<?endif;?>
					<?endif;?>
				</div>
			<?endforeach;?>
		</div>
	</div>
</div>
