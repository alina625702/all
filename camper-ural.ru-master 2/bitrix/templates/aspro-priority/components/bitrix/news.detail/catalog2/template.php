<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?
global $arTheme;
use \Bitrix\Main\Localization\Loc;

$bOrderViewBasket = $arParams['ORDER_VIEW'];
$basketURL = (isset($arTheme['URL_BASKET_SECTION']) && strlen(trim($arTheme['URL_BASKET_SECTION']['VALUE'])) ? $arTheme['URL_BASKET_SECTION']['VALUE'] : SITE_DIR.'cart/');
$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arResult) : false);
$bFormQuestion = (isset($arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']) && $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE'] == 'Y');
$catalogLinkedTemplate = (isset($arTheme['ELEMENTS_TABLE_TYPE_VIEW']) && $arTheme['ELEMENTS_TABLE_TYPE_VIEW']['VALUE'] == 'catalog_table_2' ? 'catalog_linked_2' : 'catalog_linked');

/*set array props for component_epilog*/
$templateData = array(
	'DOCUMENTS' => $arResult['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'],
	'LINK_SALE' => $arResult['DISPLAY_PROPERTIES']['LINK_SALE']['VALUE'],
	'LINK_FAQ' => $arResult['DISPLAY_PROPERTIES']['LINK_FAQ']['VALUE'],
	'LINK_PROJECTS' => $arResult['DISPLAY_PROPERTIES']['LINK_PROJECTS']['VALUE'],
	'LINK_SERVICES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERVICES']['VALUE'],
	'LINK_GOODS' => $arResult['DISPLAY_PROPERTIES']['LINK_GOODS']['VALUE'],
	'LINK_PARTNERS' => $arResult['DISPLAY_PROPERTIES']['LINK_PARTNERS']['VALUE'],
	'LINK_SERTIFICATES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERTIFICATES']['VALUE'],
	'LINK_COMPLECT' => $arResult['DISPLAY_PROPERTIES']['LINK_COMPLECT']['VALUE'],
	'LINK_STAFF' => $arResult['DISPLAY_PROPERTIES']['LINK_STAFF']['VALUE'],
	'LINK_REVIEWS' => $arResult['DISPLAY_PROPERTIES']['LINK_REVIEWS']['VALUE'],
	'LINK_TARIFS' => $arResult['DISPLAY_PROPERTIES']['LINK_TARIFS']['VALUE'],
	'BRAND_ITEM' => $arResult['BRAND_ITEM'],
	// 'GALLERY_BIG' => $arResult['GALLERY_BIG'],
	'CHARACTERISTICS' => $arResult['CHARACTERISTICS'],
	'VIDEO' => $arResult['VIDEO'],
	'VIDEO_IFRAME' => $arResult['VIDEO_IFRAME'],
	'PREVIEW_TEXT' => $arResult['FIELDS']['PREVIEW_TEXT'],
	'DETAIL_TEXT' => $arResult['FIELDS']['DETAIL_TEXT'],
	'ORDER' => $bOrderViewBasket,
	'TIZERS' => $arResult['TIZERS'],
	'CATALOG_LINKED_TEMPLATE' => $catalogLinkedTemplate,
	'GALLERY_TYPE' => isset($arResult['PROPERTIES']['GALLERY_TYPE']) ? ($arResult['PROPERTIES']['GALLERY_TYPE']['VALUE'] === 'small' ? 'small' : 'big') : ($arParams['GALLERY_TYPE'] === 'small' ? 'small' : 'big'),
);
if(isset($arResult['PROPERTIES']['BNR_TOP']) && $arResult['PROPERTIES']['BNR_TOP']['VALUE_XML_ID'] == 'YES')
	$templateData['SECTION_BNR_CONTENT'] = true;
?>
<?// show top banners start?>
<?$bShowTopBanner = (isset($arResult['SECTION_BNR_CONTENT'] ) && $arResult['SECTION_BNR_CONTENT'] == true);?>
<?if($bShowTopBanner):?>
	<?$this->SetViewTarget("section_bnr_content");?>
		<?CPriority::ShowTopDetailBanner($arResult, $arParams);?>
	<?$this->EndViewTarget();?>
<?endif;?>

<?// show top banners end?>

<div class="item_wrap <?=($arResult['GALLERY'] ? '' : ' wg')?>" data-id="<?=$arResult['ID']?>"<?=($bOrderViewBasket ? ' data-item="'.$dataItem.'"' : '')?>>
	<?// element name?>
	<?if($arParams['DISPLAY_NAME'] != 'N' && strlen($arResult['NAME'])):?>
		<h2><?=$arResult['NAME']?></h2>
	<?endif;?>
	<meta itemprop="name" content="<?=$arResult["NAME"];?>" />
	<meta itemprop="sku" content="<?=$arResult['ID'];?>" />
	<?if($arResult['BRAND_ITEM']):?>
		<meta itemprop="brand" content="<?=$arResult["BRAND_ITEM"]["NAME"]?>" />
	<?endif;?>
	<div class="head<?=($arResult['GALLERY'] ? '' : ' wti')?>">
		<div class="maxwidth-theme">
			<div class="row">
				<?if($arResult['GALLERY']):?>
					<div class=" col-md-6 col-sm-6">

						<div class="galery" id="slider">
							<div class="inner zomm_wrapper-block">
								<?if($arResult['PROPERTIES']['HIT']['VALUE']):?>
									<div class="stickers">
										<div class="stickers-wrapper">
											<?foreach($arResult['PROPERTIES']['HIT']['VALUE_XML_ID'] as $key => $class):?>
												<div class="sticker_<?=strtolower($class);?>"><?=$arResult['PROPERTIES']['HIT']['VALUE'][$key]?></div>
											<?endforeach;?>
										</div>
									</div>
								<?endif;?>

								<div class="flexslider dark-nav" data-slice="Y" id="slider" data-plugin-options='{"animation": "slide", "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [1, 1, 1], "sync": ".detail .galery #carousel", "useCSS": false}'>
									<ul class="slides items">
										<?if($arResult['GALLERY']):?>
											<?$countAll = count($arResult['GALLERY']);?>
											<?foreach($arResult['GALLERY'] as $i => $arPhoto):?>
												<? if($i < 600): ?>
												<li class="item" data-slice-block="Y" data-slice-params='{"lineheight": -3}'>
													<a href="<?=$arPhoto['DETAIL']['SRC']?>" target="_blank" title="<?=$arPhoto['TITLE']?>" class="fancybox" data-fancybox-group="gallery">
														<img src="<?=$arPhoto['PREVIEW']['src']?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" itemprop="image" />
														<span class="zoom">
															<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/zoom.svg');?>
														</span>
													</a>
												</li>
											<? endif;?>
											<?endforeach;?>
										<?else:?>
											<li class="item">
												<img src="<?=SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg';?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" itemprop="image" />
											</li>
										<?endif;?>
									</ul>
								</div>
							</div>
							<?if(count($arResult["GALLERY"]) > 1):?>
								<div class="thmb_wrap">
									<div class="thmb flexslider unstyled" id="carousel">
										<ul class="slides">
											<?foreach($arResult["GALLERY"] as $arPhoto):?>
												<li>
													<img class="img-responsive inline" src="<?=$arPhoto["THUMB"]["src"]?>" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
												</li>
											<?endforeach;?>
										</ul>
									</div>
								</div>
							<?endif;?>
						</div>

					</div>
				<?else:?>
					<div class="item col-md-6 col-sm-6">
						<div class="galery" id="slider">
							<div class="inner zomm_wrapper-block">
								<ul class="items">
									<li class="item">
										<img src="<?=SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg';?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" itemprop="image" />
									</li>
								</ul>
							</div>
						</div>
					</div>
				<?endif;?>
				<div class="item col-md-6 col-sm-6<?=(isset($arParams['IMAGE_CATALOG_POSITION']) && $arParams['IMAGE_CATALOG_POSITION'] == 'left' ? ' image_left' : '');?>">
					<?$arOrder = explode(",", $arParams["LIST_PRODUCT_BLOCKS_ALL_ORDER"]);?>

					<div class="base-price-header catalog_detail_base-price-header">
						<div class="title">
							<h3 style="margin: 0px;"><?=($arParams["T_CHARACTERISTICS"] ? $arParams["T_CHARACTERISTICS"] : Loc::getMessage("T_CHARACTERISTICS"));?></h3>
						</div>
						<div class="price">
							<?if(strlen($arResult['PRICE_PROPERTIES']['PRICE']['VALUE'])):?>
									<span class=" pull-right"><?=number_format($arResult['PRICE_PROPERTIES']['PRICE']['VALUE'], 0, ',', ' ')?> ₽</span>
							<?endif;?>
						</div>
					</div>
					<div class="row">
					<!-- <div class="col-sm-1"><img src="/images/icons/free-icon.png"></div> -->
					<div class="char-wrapp col-sm-12 clearfix">
						<table class="props_table">
							<?foreach($templateData['CHARACTERISTICS'] as $arProp):?>
								<tr class="char">
									<td class="char_value">
										<span>
											<?if(is_array($arProp['DISPLAY_VALUE'])):?>
												<?foreach($arProp['DISPLAY_VALUE'] as $key => $value):?>
													<?if($arProp['DISPLAY_VALUE'][$key + 1]):?>
														<?=html_entity_decode($value).'&nbsp;/ '?>
													<?else:?>
														<?=html_entity_decode($value)?>
													<?endif;?>
												<?endforeach;?>
											<?else:?>
												<?=html_entity_decode($arProp['DISPLAY_VALUE']);?>
											<?endif;?>
										</span>
									</td>
								</tr>
							<?endforeach;?>
						</table>
					</div>
				</div>

					<div class="info<?=(strlen($arResult['FIELDS']['DETAIL_TEXT']) ? ' wpadding' : '');?>" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
						<link itemprop="url" href="<?=$arResult["DETAIL_PAGE_URL"]?>"/>
						<?
						$frame = $this->createFrame('info')->begin('');
						$frame->setAnimation(true);
						?>
						<?if($arResult['DISPLAY_PROPERTIES']['STATUS']['VALUE_XML_ID'] || strlen($arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE']) || $arResult['BRAND_ITEM']):?>
							<div class="hh">
								<?if(strlen($arResult['DISPLAY_PROPERTIES']['STATUS']['VALUE'])):?>
									<span class="status-icon <?=$arResult['DISPLAY_PROPERTIES']['STATUS']['VALUE_XML_ID']?>"><?=$arResult['DISPLAY_PROPERTIES']['STATUS']['VALUE']?></span>
									<span style="display:none;"><?=\Aspro\Functions\CAsproPriority::showSchemaAvailabilityMeta($arResult['DISPLAY_PROPERTIES']['STATUS']['VALUE_XML_ID'])?></span>
								<?endif;?>
								<?if(strlen($arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE'])):?>
									<span class="article font_upper">
										<?=Loc::getMessage('ARTICLE')?>&nbsp;<span><?=$arResult['DISPLAY_PROPERTIES']['ARTICLE']['VALUE']?></span>
									</span>
								<?endif;?>
								<?if($arResult['BRAND_ITEM']):?>
									<div class="brand">
										<meta itemprop="brand" content="<?=$arResult["BRAND_ITEM"]["NAME"]?>" />
										<?if(!$arResult["BRAND_ITEM"]["IMAGE"]):?>
											<a href="<?=$arResult["BRAND_ITEM"]["DETAIL_PAGE_URL"]?>"><?=$arResult["BRAND_ITEM"]["NAME"]?></a>
										<?else:?>
											<a class="brand_picture" href="<?=$arResult["BRAND_ITEM"]["DETAIL_PAGE_URL"]?>">
												<img  src="<?=$arResult["BRAND_ITEM"]["IMAGE"]["src"]?>" alt="<?=$arResult["BRAND_ITEM"]["NAME"]?>" title="<?=$arResult["BRAND_ITEM"]["NAME"]?>" />
											</a>
										<?endif;?>
									</div>
									<div class="clearfix"></div>
								<?endif;?>
							</div>
						<?endif;?>

						<?if(isset($arResult['DISPLAY_PROPERTIES']['DELIVERY']) && strlen($arResult['DISPLAY_PROPERTIES']['DELIVERY']['DISPLAY_VALUE'])):?>
							<div class="delivery">
								<span class="icon"><?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/info.svg');?></span>
								<div class="text"><?=$arResult['DISPLAY_PROPERTIES']['DELIVERY']['DISPLAY_VALUE'];?></div>
							</div>
						<?endif;?>
						<?if(strlen($arResult['FIELDS']['PREVIEW_TEXT'])):?>
							<div class="previewtext" itemprop="description">
								<?// element detail text?>
								<?if($arResult['PREVIEW_TEXT_TYPE'] == 'text'):?>
									<p><?=$arResult['FIELDS']['PREVIEW_TEXT'];?></p>
								<?else:?>
									<?=$arResult['FIELDS']['PREVIEW_TEXT'];?>
								<?endif;?>
							</div>
						<?endif;?>
						<?$frame->end();?>
					</div>
					<?if(strlen($arResult['FIELDS']['DETAIL_TEXT'])):?>
						<div class="link-block-more">
							<span class="font_upper dark-color"><?=Loc::getMessage('MORE_TEXT_BOTTOM');?></span>
						</div>
					<?endif;?>
				</div>

			</div>
		</div>
	</div>
</div>
<script>$(document).click( function(e){
    if ( $(e.target).closest('.popup-window-content').length ) {
        // клик внутри элемента
        return;
    }
    // клик снаружи элемента
    $('.popup-window-content').fadeOut();
});</script>
