<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?$this->setFrameMode(true);?>



<?

global $isMenu, $arTheme;

use \Bitrix\Main\Localization\Loc;


$bOrderViewBasket = $arParams['ORDER_VIEW'];

$basketURL = (isset($arTheme['URL_BASKET_SECTION']) && strlen(trim($arTheme['URL_BASKET_SECTION']['VALUE'])) ? $arTheme['URL_BASKET_SECTION']['VALUE'] : SITE_DIR.'cart/');

$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arResult) : false);

$bFormQuestion = (isset($arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']) && $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE'] == 'Y');

$catalogLinkedTemplate = (isset($arTheme['ELEMENTS_TABLE_TYPE_VIEW']) && $arTheme['ELEMENTS_TABLE_TYPE_VIEW']['VALUE'] == 'catalog_table_2' ? 'catalog_linked_2' : 'catalog_linked');



/*set array props for component_epilog*/

$templateData = array(

	'DOCUMENTS' => $arResult['DISPLAY_PROPERTIES']['DOCUMENTS']['VALUE'],

	'LINK_SALE' => $arResult['DISPLAY_PROPERTIES']['LINK_SALE']['VALUE'],

	'LINK_FAQ' => $arResult['DISPLAY_PROPERTIES']['LINK_FAQ']['VALUE'],

	'LINK_PROJECTS' => $arResult['DISPLAY_PROPERTIES']['LINK_PROJECTS']['VALUE'],

	'LINK_SERVICES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERVICES']['VALUE'],

	'LINK_GOODS' => $arResult['DISPLAY_PROPERTIES']['LINK_GOODS']['VALUE'],

	'LINK_PARTNERS' => $arResult['DISPLAY_PROPERTIES']['LINK_PARTNERS']['VALUE'],

	'LINK_BRANDS' => $arResult['PROPERTIES']['LINK_BRAND']['VALUE'],

	'LINK_SERTIFICATES' => $arResult['DISPLAY_PROPERTIES']['LINK_SERTIFICATES']['VALUE'],

	'LINK_VACANCYS' => $arResult['DISPLAY_PROPERTIES']['LINK_VACANCYS']['VALUE'],

	'LINK_STAFF' => $arResult['DISPLAY_PROPERTIES']['LINK_STAFF']['VALUE'],

	'LINK_REVIEWS' => $arResult['DISPLAY_PROPERTIES']['LINK_REVIEWS']['VALUE'],

	'LINK_TARIFS' => $arResult['DISPLAY_PROPERTIES']['LINK_TARIFS']['VALUE'],

	'BRAND_ITEM' => $arResult['BRAND_ITEM'],

	'GALLERY_BIG' => $arResult['GALLERY'],

	'CHARACTERISTICS' => $arResult['CHARACTERISTICS'],

	'VIDEO' => $arResult['VIDEO'],

	'VIDEO_IFRAME' => $arResult['VIDEO_IFRAME'],

	'PREVIEW_TEXT' => $arResult['PREVIEW_TEXT'],

	'DETAIL_TEXT' => $arResult['FIELDS']['DETAIL_TEXT'],

	'ORDER' => $bOrderViewBasket,

	'CATALOG_LINKED_TEMPLATE' => $catalogLinkedTemplate,

	'FORM_ORDER' => $arResult['DISPLAY_PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'],

	'FORM_QUESTION' => $arResult['DISPLAY_PROPERTIES']['FORM_QUESTION']['VALUE_XML_ID'],

	'PRICE' => $arResult['PROPERTIES']['PRICE']['VALUE'],

	'PRICE_OLD' => $arResult['PROPERTIES']['PRICE_OLD']['VALUE'],

	'GALLERY_TYPE' => isset($arResult['PROPERTIES']['GALLERY_TYPE']) ? ($arResult['PROPERTIES']['GALLERY_TYPE']['VALUE'] === 'small' ? 'small' : 'big') : ($arParams['GALLERY_TYPE'] === 'small' ? 'small' : 'big'),

);



if(isset($arResult['PROPERTIES']['BNR_TOP']) && $arResult['PROPERTIES']['BNR_TOP']['VALUE_XML_ID'] == 'YES')

	$templateData['SECTION_BNR_CONTENT'] = true;

?>



<?if($arResult['DETAIL_PICTURE']):?>

	<meta itemprop="image" content="<?=$arResult['DETAIL_PICTURE']['SRC'];?>" />

<?endif;?>

<meta itemprop="name" content="<?=$arResult['NAME'];?>" />

<link itemprop="url" href="<?=$arResult['DETAIL_PAGE_URL'];?>" />



<?// shot top banners start?>

<?$bShowTopBanner = (isset($arResult['SECTION_BNR_CONTENT'] ) && $arResult['SECTION_BNR_CONTENT'] == true);?>
<?if($bShowTopBanner):?>
<? $arResult['FIELDS']['PREVIEW_TEXT'] = $arResult['PREVIEW_TEXT']?>
	<?$this->SetViewTarget("section_bnr_content");?>
		<?CPriority::ShowTopDetailBanner($arResult, $arParams);?>
	<?$this->EndViewTarget();?>

<?endif;?>

<?// shot top banners end?>

<?// form question?>

<?// element name?>

<?if($arParams['DISPLAY_NAME'] != 'N' && strlen($arResult['NAME'])):?>

<?endif;?>



<?$arOrder = explode(",", $arParams["LIST_PRODUCT_BLOCKS_ORDER"]);?>
<?$arTabOrder = explode(",", $arParams["LIST_PRODUCT_BLOCKS_TAB_ORDER"]);?>
<?$bShowFormQuestion = ($templateData['FORM_QUESTION'] == 'YES');?>
<?$bAjax = $_REQUEST['AJAX_REQUEST'] == 'Y';?>
<?foreach($arOrder as $value):?>
	<div class="drag_block <?=$value;?>">

<?//show tab block?>
		<?if($value == "tab"):?>
			<?
			$i = 0;
			$bShowDetailTextTab = (strlen($templateData['DETAIL_TEXT']) ? ++$i : '');
			$bShowTarifTab = (!empty($templateData['LINK_TARIFS']) ? ++$i : '');
			$bShowPropsTab = (!empty($templateData['CHARACTERISTICS']) ? ++$i : '');
			$bShowDocsTab = (!empty($templateData['DOCUMENTS']) ? ++$i : '');
			$bShowVideoTab = (!empty($templateData['VIDEO']) || !empty($templateData['VIDEO_IFRAME']) ? ++$i : '');
			$bShowFaqTab = (!empty($templateData['LINK_FAQ']) ? ++$i : '');
			$bShowProjecTab = (!empty($templateData['LINK_PROJECTS']) ? ++$i : '');
			$bShowBrandTab = (!empty($templateData['LINK_BRANDS']) ? ++$i : '');

			if($bShowTarifTab || $bShowDetailTextTab || $bShowPropsTab || $bShowDocsTab || $bShowVideoTab || $bShowFaqTab || $bShowProjecTab || $bShowBrandTab):?>
				<div class="tabs">
					<?if($i > 1):?>
						<div class="arrow_scroll">
							<ul class="nav nav-tabs font_upper_md">
								<?$iTab = 0;?>
								<?foreach($arTabOrder as $value):?>
									<?//show desc block?>
									<?if($value == "desc"):?>
										<?if($bShowDetailTextTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#desc" data-toggle="tab"><?=($arParams["T_DESC"] ? $arParams["T_DESC"] : Loc::getMessage("T_DESC"));?></a></li>
										<?endif;?>
									<?endif;?>
									<?//show char block?>
									<?if($value == "char"):?>
										<?if($bShowPropsTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#props" data-toggle="tab"><?=($arParams["T_CHARACTERISTICS"] ? $arParams["T_CHARACTERISTICS"] : Loc::getMessage("T_CHARACTERISTICS"));?></a></li>
										<?endif;?>
									<?endif;?>
									<?//show tarifs block?>
									<?if($value == "tarifs"):?>
										<?if($bShowTarifTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#tarifs" data-toggle="tab"><?=($arParams["T_TARIF"] ? $arParams["T_TARIF"] : Loc::getMessage("T_TARIF"));?></a></li>
										<?endif;?>
									<?endif;?>

									<?//show projects block?>
									<?if($value == "projects"):?>
										<?if($bShowProjecTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#projects" class="projects-link" data-toggle="tab"><?=($arParams["T_PROJECTS"] ? $arParams["T_PROJECTS"] : Loc::getMessage("T_PROJECTS"));?></a></li>
										<?endif;?>
									<?endif;?>
									<?//show docs block?>
									<?if($value == "docs"):?>
										<?if($bShowDocsTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#docs" data-toggle="tab"><?=($arParams["T_DOCS"] ? $arParams["T_DOCS"] : Loc::getMessage("T_DOCS"));?></a></li>
										<?endif;?>
									<?endif;?>
									<?//show faq block?>
									<?if($value == "faq"):?>
										<?if($bShowFaqTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#faq" data-toggle="tab"><?=($arParams["T_FAQ"] ? $arParams["T_FAQ"] : Loc::getMessage("T_FAQ"));?></a></li>
										<?endif;?>
									<?endif;?>
									<?//show video block?>
									<?if($value == "video"):?>
										<?if($bShowVideoTab):?>
											<li class="border <?=(!($iTab++) ? 'active' : '')?>"><a href="#video" data-toggle="tab"><?=($arParams["T_VIDEO"] ? $arParams["T_VIDEO"] : Loc::getMessage("T_VIDEO"));?></a></li>
										<?endif;?>
									<?endif;?>
								<?endforeach;?>
							</ul>
						</div>
					<?endif;?>
					<div class="tab-content<?=($i <= 1 ? ' not_tabs' : '')?>">
						<?$iTab = 0;?>
						<?foreach($arTabOrder as $value):?>
							<?//show desc block?>
							<?if($value == "desc"):?>
								<?if($bShowDetailTextTab):?>
									<div class="tab-pane <?=(!($iTab++) ? 'active' : '')?>" id="desc">
										<div class="content" itemprop="description">
											<?if($templateData['PREVIEW_TEXT'] || $templateData['DETAIL_TEXT']):?>
												<div class="content" itemprop="description">
													<?if(!$templateData['SECTION_BNR_CONTENT'] && strlen($templateData['PREVIEW_TEXT'])):?>
														<div class="introtext"><?=$templateData['PREVIEW_TEXT'];?></div>
													<?endif;?>

													<?// element detail text?>
													<?if(strlen($templateData['DETAIL_TEXT'])):?>
														<?=$templateData['DETAIL_TEXT'];?>
													<?endif;?>
												</div>
											<?endif;?>
										</div>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show projects block?>
							<?if($value == "projects"):?>
								<?if($bShowProjecTab):?>
									<div class="tab-pane <?=(!($iTab++) ? 'active' : '')?>" id="projects">
										<?$GLOBALS['arrProjectFilter'] = array('ID' => $templateData['LINK_PROJECTS']);?>
										<?$APPLICATION->IncludeComponent(
											"bitrix:news.list",
											"projects_linked",
											array(
												"IBLOCK_TYPE" => "aspro_priority_content",
												"IBLOCK_ID" => $arParams["PROJECTS_IBLOCK_ID"],
												"NEWS_COUNT" => "20",
												"SORT_BY1" => "SORT",
												"SORT_ORDER1" => "ASC",
												"SORT_BY2" => "ID",
												"SORT_ORDER2" => "DESC",
												"FILTER_NAME" => "arrProjectFilter",
												"FIELD_CODE" => array(
													0 => "NAME",
													1 => "PREVIEW_TEXT",
													2 => "PREVIEW_PICTURE",
													3 => "",
												),
												"PROPERTY_CODE" => array(
													0 => "LINK",
													1 => "TEXTCOLOR",
												),
												"CHECK_DATES" => "Y",
												"DETAIL_URL" => "",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"AJAX_OPTION_HISTORY" => "N",
												"CACHE_TYPE" => "A",
												"CACHE_TIME" => "36000000",
												"CACHE_FILTER" => "Y",
												"CACHE_GROUPS" => "N",
												"PREVIEW_TRUNCATE_LEN" => "",
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"SET_TITLE" => "N",
												"SET_STATUS_404" => "N",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
												"ADD_SECTIONS_CHAIN" => "N",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"INCLUDE_SUBSECTIONS" => "Y",
												"PAGER_TEMPLATE" => ".default",
												"DISPLAY_TOP_PAGER" => "N",
												"DISPLAY_BOTTOM_PAGER" => "Y",
												"PAGER_TITLE" => "�������",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "N",
												"VIEW_TYPE" => "list",
												"IMAGE_POSITION" => "left",
												"COUNT_IN_LINE" => "3",
												"SHOW_TITLE" => "Y",
												"T_PROJECTS" => ($arParams["T_PROJECTS"] ? $arParams["T_PROJECTS"] : Loc::getMessage("T_PROJECTS")),
												"AJAX_OPTION_ADDITIONAL" => ""
											),
											false, array("HIDE_ICONS" => "Y")
										);?>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show tarifs block?>
							<?if($value == "tarifs"):?>
								<?if($bShowTarifTab):?>
									<div class="tab-pane <?=(!($iTab++) ? 'active' : '')?>" id="tarifs">
										<?$GLOBALS['arrTarifsFilter'] = array('ID' => $templateData['LINK_TARIFS']);?>
										<?include_once(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'])."/include/comp_tarifs_services.php");?>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show char block?>
							<?if($value == "char"):?>
								<?if($bShowPropsTab):?>
									<div class="tab-pane chars <?=(!($iTab++) ? 'active' : '')?>" id="props">
										<div class="char-wrapp">
										
										</div>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show docs block?>
							<?if($value == "docs"):?>
								<?if($bShowDocsTab):?>
									<div class="tab-pane docs-block <?=(!($iTab++) ? 'active' : '')?>" id="docs">
										<div class="docs_wrap">
											<div class="row">
												<?foreach($templateData['DOCUMENTS'] as $docID):?>
													<?$arItem = CPriority::get_file_info($docID);?>
													<div class="col-md-4">
														<?
														$fileName = substr($arItem['ORIGINAL_NAME'], 0, strrpos($arItem['ORIGINAL_NAME'], '.'));
														$fileTitle = (strlen($arItem['DESCRIPTION']) ? $arItem['DESCRIPTION'] : $fileName);

														?>
														<div class="blocks clearfix <?=$arItem["TYPE"];?>">
															<div class="inner-wrapper">
																<a href="<?=$arItem['SRC']?>" class="dark-color text" target="_blank"><?=$fileTitle?></a>
																<div class="filesize font_xs"><?=CPriority::filesize_format($arItem['FILE_SIZE']);?></div>
															</div>
														</div>
													</div>
												<?endforeach;?>
											</div>
										</div>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show faq block?>
							<?if($value == "faq"):?>
								<?if($bShowFaqTab):?>
									<div class="tab-pane <?=(!($iTab++) ? 'active' : '')?>" id="faq">
										<?$GLOBALS['arrFaqFilter'] = array('ID' => $templateData['LINK_FAQ']);?>
										<?$APPLICATION->IncludeComponent(
											"bitrix:news.list",
											"items_list",
											array(
												"IBLOCK_TYPE" => "aspro_priority_content",
												"IBLOCK_ID" => $arParams["FAQ_IBLOCK_ID"],
												"NEWS_COUNT" => "20",
												"SORT_BY1" => "SORT",
												"SORT_ORDER1" => "ASC",
												"SORT_BY2" => "ID",
												"SORT_ORDER2" => "DESC",
												"FILTER_NAME" => "arrFaqFilter",
												"FIELD_CODE" => array(
													0 => "PREVIEW_TEXT",
													1 => "",
												),
												"PROPERTY_CODE" => array(
													0 => "LINK",
													1 => "",
												),
												"CHECK_DATES" => "Y",
												"DETAIL_URL" => "",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"AJAX_OPTION_HISTORY" => "N",
												"CACHE_TYPE" => "A",
												"CACHE_TIME" => "36000000",
												"CACHE_FILTER" => "Y",
												"CACHE_GROUPS" => "N",
												"PREVIEW_TRUNCATE_LEN" => "",
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"SET_TITLE" => "N",
												"SET_STATUS_404" => "N",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
												"ADD_SECTIONS_CHAIN" => "N",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"INCLUDE_SUBSECTIONS" => "Y",
												"PAGER_TEMPLATE" => ".default",
												"DISPLAY_TOP_PAGER" => "N",
												"DISPLAY_BOTTOM_PAGER" => "Y",
												"PAGER_TITLE" => "�������",
												"PAGER_SHOW_ALWAYS" => "N",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "N",
												"VIEW_TYPE" => "accordion",
												"IMAGE_POSITION" => "left",
												"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
												"COUNT_IN_LINE" => "3",
												"SHOW_TITLE" => "Y",
												"T_TITLE" => ($arParams["T_FAQ"] ? $arParams["T_FAQ"] : Loc::getMessage("T_FAQ")),
												"AJAX_OPTION_ADDITIONAL" => "",
												"SHOW_SECTION_NAME" => "N"
											),
											false, array("HIDE_ICONS" => "Y")
										);?>
									</div>
								<?endif;?>
							<?endif;?>
							<?//show video block?>
							<?if($value == "video"):?>
								<?if($bShowVideoTab):?>
									<div class="tab-pane <?=(!($iTab++) ? 'active' : '')?>" id="video">
										<div class="video">
											<?
											if($templateData['VIDEO'] && $templateData['VIDEO_IFRAME'])
												$count = 2;
											elseif($templateData['VIDEO'])
												$count = count($templateData['VIDEO']);
											elseif($templateData['VIDEO_IFRAME'])
												$count = count($templateData['VIDEO_IFRAME']);
											?>
											<?if($templateData['VIDEO']):?>
												<?foreach($templateData['VIDEO'] as $i => $arVideo):?>
													<div class="item">
														<div class="video_body">
															<video id="js-video_<?=$i?>" width="350" height="217"  class="video-js" controls="controls" preload="metadata" data-setup="{}">
																<source src="<?=$arVideo["path"]?>" type='video/mp4' />
																<p class="vjs-no-js">
																	To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video
																</p>
															</video>
														</div>
														<div class="title"><?=(strlen($arVideo["title"]) ? $arVideo["title"] : '')?></div>
													</div>
												<?endforeach;?>
											<?endif;?>
											<?if($templateData['VIDEO_IFRAME']):?>
												<?foreach($templateData['VIDEO_IFRAME'] as $i => $video):?>
													<div class="item">
														<div class="video_body">
															<?if(strpos($video, '<iframe') !== false):?>
																<?=$video;?>
															<?else:?>
																<?
																$video = str_replace('watch?v=', 'embed/', $video);
																$video = str_replace('youtu.be/', 'youtube.com/embed/', $video)
																?>
																<iframe width="560" height="315" src="<?=$video;?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
															<?endif;?>
															<?//=str_replace('src=', 'width="350" height="217" src=', str_replace(array('width', 'height'), array('data-width', 'data-height'), $video));?>
														</div>
														<div class="title"></div>
													</div>
												<?endforeach;?>
											<?endif;?>
										</div>
									</div>
								<?endif;?>
							<?endif;?>
						<?endforeach;?>
					</div>
				</div>
			<?endif;?>
		<?endif;?>

		<?//show gallery block?>
		<?if($value == "gallery"):?>
			<? if(count($templateData['GALLERY_BIG'])):?>
				<? $bShowSmallGallery = $templateData['GALLERY_TYPE'] === 'small'; ?>
				<div class="wraps galerys-block">
					<!--<span class="switch_gallery<?=($bShowSmallGallery ? ' small' : '');?>"></span> -->
					<div class="title small-gallery font_xs"<?=($bShowSmallGallery ? ' style="display:block;"' : '');?>><?=count($templateData['GALLERY_BIG']).'&nbsp;'.Loc::getMessage('T_GALLERY_TITLE')?></div>
					<div class="title big-gallery font_xs"<?=($bShowSmallGallery ? ' style="display:none;"' : '');?>><span class="slide-number">1</span> / <?=count($templateData['GALLERY_BIG'])?></div>
					<!-- <div class="big-gallery-block thmb1 flexslider unstyled row bigs "<?=($bShowSmallGallery ? ' style="display:none;"' : '');?> id="slider" data-plugin-options='{"animation": "slide", "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "sync": ".gallery-wrapper .small-gallery", "counts": [1, 1, 1]}'>
						<ul class="slides items">
							<?foreach($templateData['GALLERY_BIG'] as $i => $arPhoto):?>
								<li class="col-md-12 item">
									<img src="<?=$arPhoto['PREVIEW']['src']?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
									<a href="<?=$arPhoto['DETAIL']['SRC']?>" class="fancybox" rel="gallery" target="_blank" title="<?=$arPhoto['TITLE']?>">
										<span class="zoom">
											<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/zoom.svg');?>
										</span>
									</a>
								</li>
							<?endforeach;?>
						</ul>
					</div> -->
					<div class="small-gallery-block"<?=($bShowSmallGallery ? ' style="display:block;"' : '');?>>
						<div class="front bigs">
							<div class="items row">
								<?foreach($templateData['GALLERY_BIG'] as $i => $arPhoto):?>
									<div class="col-md-3 col-sm-4 col-xs-6">
										<div class="item">
											<div class="wrap">
												<img src="<?=$arPhoto['PREVIEW']['src']?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
											</div>
										</div>
									</div>
								<?endforeach;?>
							</div>
						</div>
					</div>
				</div>
			<?endif;?>
		<?endif;?>

	</div>
<?endforeach;?>
<?// single detail image?>


<?/*if($arResult['FIELDS']['DETAIL_PICTURE']):?>

	<?

	$atrTitle = (strlen($arResult['DETAIL_PICTURE']['DESCRIPTION']) ? $arResult['DETAIL_PICTURE']['DESCRIPTION'] : (strlen($arResult['DETAIL_PICTURE']['TITLE']) ? $arResult['DETAIL_PICTURE']['TITLE'] : $arResult['NAME']));

	$atrAlt = (strlen($arResult['DETAIL_PICTURE']['DESCRIPTION']) ? $arResult['DETAIL_PICTURE']['DESCRIPTION'] : (strlen($arResult['DETAIL_PICTURE']['ALT']) ? $arResult['DETAIL_PICTURE']['ALT'] : $arResult['NAME']));

	?>

	<?if($arResult['PROPERTIES']['PHOTOPOS']['VALUE_XML_ID'] == 'LEFT'):?>

		<div class="detailimage image-left col-md-4 col-sm-4 col-xs-12"><a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="fancybox" title="<?=$atrTitle?>"><img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="img-responsive" title="<?=$atrTitle?>" alt="<?=$atrAlt?>" /></a></div>

	<?elseif($arResult['PROPERTIES']['PHOTOPOS']['VALUE_XML_ID'] == 'RIGHT'):?>

		<div class="detailimage image-right col-md-4 col-sm-4 col-xs-12"><a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="fancybox" title="<?=$atrTitle?>"><img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="img-responsive" title="<?=$atrTitle?>" alt="<?=$atrAlt?>" /></a></div>

	<?elseif($arResult['PROPERTIES']['PHOTOPOS']['VALUE_XML_ID'] == 'TOP'):?>

		<?$this->SetViewTarget('top_section_filter_content');?>

		<div class="detailimage image-head"><img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="img-responsive" title="<?=$atrTitle?>" alt="<?=$atrAlt?>"/></div>

		<?$this->EndViewTarget();?>

	<?else:?>

		<div class="detailimage image-wide"><img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" class="img-responsive" title="<?=$atrTitle?>" alt="<?=$atrAlt?>" /></div>

	<?endif;?>

<?endif;*/?>



<?if($arResult['COMPANY']):?>

	<div class="wraps barnd-block">

		<div class="item-views list list-type-block image_left">

			<?if($arResult['COMPANY']['PROPERTY_SLOGAN_VALUE']):?>

				<div class="slogan"><?=$arResult['COMPANY']['PROPERTY_SLOGAN_VALUE'];?></div>

			<?endif;?>

			<div class="items row">

				<div class="col-md-12">

					<div class="item noborder clearfix">

						<?if($arResult['COMPANY']['IMAGE-BIG']):?>

							<div class="image">

								<a href="<?=$arResult['COMPANY']['DETAIL_PAGE_URL'];?>">

									<img src="<?=$arResult['COMPANY']['IMAGE-BIG']['src'];?>" alt="<?=$arResult['COMPANY']['NAME'];?>" title="<?=$arResult['COMPANY']['NAME'];?>" class="img-responsive">

								</a>

							</div>

						<?endif;?>

						<div class="body-info">

							<?if($arResult['COMPANY']['DETAIL_TEXT']):?>

								<div class="previewtext">

									<?=$arResult['COMPANY']['DETAIL_TEXT'];?>

								</div>

							<?endif;?>

							<?if($arResult['COMPANY']['PROPERTY_SITE_VALUE']):?>

								<div class="properties">

									<div class="inner-wrapper">

										<!-- noindex -->

										<a class="property icon-block site" href="<?=$arResult['COMPANY']['PROPERTY_SITE_VALUE'];?>" target="_blank" rel="nofollow">

											<?=$arResult['COMPANY']['PROPERTY_SITE_VALUE'];?>

										</a>

										<!-- /noindex -->

									</div>

								</div>

							<?endif;?>

						</div>

					</div>

				</div>

			</div>

		</div>

		<hr>

	</div>

<?endif;?>

<?
		$links_complected = $arResult['DISPLAY_PROPERTIES']['LINK_COMPLECT']['VALUE'];

		// Получаем каждый прилинкованый кемпер
		$arLinkedCampersFilter = Array("IBLOCK_ID"=>"42", "ID"=>$links_complected);
		$arSelectLinkedCampers = Array("ID","NAME","PROPERTY_CAMPER", "PROPERTY_COMPLECT");
		$resLinkedCampers = CIBlockElement::GetList(Array("ID"=>"ASC"), $arLinkedCampersFilter,false,false, $arSelectLinkedCampers);
		while($ob = $resLinkedCampers->GetNextElement()){

			$arLinkedCamprersFields = $ob->GetFields();
			$camper_id = $arLinkedCamprersFields["PROPERTY_CAMPER_VALUE"];
			$complect_ids = $arLinkedCamprersFields["PROPERTY_COMPLECT_VALUE"];

			// Получаем информацию по кемперу
			$arCampersFilter = Array("IBLOCK_ID"=>"34", "ID"=>$camper_id);
			$arSelectCampers = Array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_TEXT", "PROPERTY_PHOTOS", "PROPERTY_BASE_COMPLECT", "PROPERTY_PRICE", "PROPERTY_GALLEY_BIG", "PROPERTY_FOR", "DETAIL_PAGE_URL");
			$resCampers = CIBlockElement::GetList(Array("ID"=>"ASC"), $arCampersFilter,false,false, $arSelectCampers);

			$i = 0;
			while($obCamper = $resCampers->GetNextElement()){
				if($i == 0) {
					$arCamprersFields = $obCamper->GetFields();
					$detail_picture = CFile::GetFileArray($arCamprersFields["DETAIL_PICTURE"]);
					$gallery_picture_arr = $arCamprersFields["PROPERTY_PHOTOS_VALUE"];
					//print_r($gallery_picture);
					$total_price = intval(str_replace(' ', '', $arCamprersFields['PROPERTY_PRICE_VALUE']));

					$arComplectsFilter = Array("IBLOCK_ID"=>"41", "ID"=>$complect_ids);
					$arSelectComplects = Array("ID", "NAME", "PROPERTY_PRICE", "CODE", "DETAIL_PICTURE", "PROPERTY_GALLEY_BIG");
					$resComplects = CIBlockElement::GetList(Array("ID"=>"ASC"), $arComplectsFilter,false,false, $arSelectComplects);
					$resComplectsPrice = CIBlockElement::GetList(Array("ID"=>"ASC"), $arComplectsFilter,false,false, $arSelectComplects);

					while($obComplectsPrice = $resComplectsPrice->GetNextElement()){
						$arComplectPrice = $obComplectsPrice->GetFields();
						$total_price += intval(str_replace(' ', '', $arComplectPrice['PROPERTY_PRICE_VALUE']));
					}
				?>
					<div class="complectation-item ci-gallery">
						<div class="catalog detail">
							<div class="row">
								<div class="col-md-6">
									<? //print_r($gallery_picture ); ?>

									 <?if(count($gallery_picture_arr) > 0):?>

									    <div class="galery" id="slider">
												<div class="inner zomm_wrapper-block">
													<div class="flexslider dark-nav" data-slice="Y" id="slider" data-plugin-options='{"animation": "slide", "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [1, 1, 1], "sync": ".detail .galery #carousel", "useCSS": false}'>
														<ul class="slides items">

															<?if(count($gallery_picture_arr) > 1):?>
																	<? $i = 0; ?>
																 <?foreach($gallery_picture_arr as $picture):?>
																	 <? if ($i < 10):?>
																	 <?
																		 $arPhoto = CFile::GetFileArray($picture);
																		 $thumb = CFile::ResizeImageGet($picture , array('width' => auto), BX_RESIZE_IMAGE_EXACT, true);
																	 ?>

																		<li class="item" data-slice-block="Y" data-slice-params='{"lineheight": -3}'>
																			<a href="<?=$arPhoto['DETAIL']['SRC']?>" target="_blank" title="<?=$arPhoto['TITLE']?>" class="fancybox" data-fancybox-group="gallery">
																				<img src="<?=$thumb["src"]?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" itemprop="image" />
																				<span class="zoom">
																					<?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/zoom.svg');?>
																				</span>
																			</a>
																		</li>
																	<? endif;?>
																	<? $i = $i+1?>
																<?endforeach;?>
															<?else:?>
																<li class="item">
																 <img src="<?=SITE_TEMPLATE_PATH.'/images/svg/noimage_product.svg';?>" class="img-responsive inline" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" itemprop="image" />
																</li>
															<?endif;?>
														</ul>
													</div>
												</div>
												<?if(count($gallery_picture_arr) > 1):?>

													<div class="thmb_wrap">
														<div class="thmb flexslider unstyled" id="carousel">
															<ul class="slides">

																<?foreach($gallery_picture_arr as $picture):?>
																	<?
																		$arPhoto = CFile::GetFileArray($picture);
																		$thumb = CFile::ResizeImageGet($picture , array('width' => 60, 'height' => 60), BX_RESIZE_IMAGE_EXACT, true);
																	?>
																	<li>
																		<img class="img-responsive inline" src="<?=$thumb["src"]?>" title="<?=$arPhoto['TITLE']?>" alt="<?=$arPhoto['ALT']?>" />
																	</li>
																<?endforeach;?>
															</ul>
														</div>
													</div>
												<?endif;?>
						          </div>
									<?else:?>
											<img src="<?=$detail_picture['SRC'];?>">
									<?endif;?>
								</div>
								<div class="col-md-6">
									<? //print_r($arResult['DISPLAY_PROPERTIES']);?>
									<h2><?=$arCamprersFields['NAME']?> для <?=$arResult['DISPLAY_PROPERTIES']['FOR']['VALUE'];?> <span class="green_price"><div class="full-price"><?=number_format($total_price, 0, ',', ' ')?> руб</div></span></h2>
									<div class="description"><?=$arCamprersFields['DETAIL_TEXT'];?></div>
								</div>
							</div>
							<div class="row complectation-description">
									<div class="col-md-6">
										<h3 style="color: #F5A732!important;font-size: 18px!important;text-transform:uppercase!important;">Базовая комплектация</h3>
										<div class="col-sm-1"><img src=" /bitrix/templates/aspro-priority/components/bitrix/news.detail/catalog2/images/free-icon.png"></div>
										<div class="base-complect col-sm-11"><?=$arCamprersFields['~PROPERTY_BASE_COMPLECT_VALUE']['TEXT'];?></div>

									</div>
									<div class="col-md-6">
										<h3 style="color: #F5A732!important;font-size: 18px!important;text-transform:uppercase!important;">Дополнительная комплектация</h3>
										<div class="advanced-complect">
									<?
										// Получаем дополнительную комплектацию для выбранного направления

										$complect_str = '';
										while($obComplects = $resComplects->GetNextElement()){
											$arComplect = $obComplects->GetFields();
											$complect_str .= $arComplect['NAME'] . ',';
											?>
												<div class="row item">
												<div class="col-md-1 col-xs-1"><input
														class=""
														type="checkbox"
														id="complect"
														name="<?=$arComplect['CODE']?>"
														data-blocked=""
														data-id="<?=$arComplect['ID']?>"
														data-name="<?=$arComplect['NAME']?>"
														data-price="<?=$arComplect['PROPERTY_PRICE_VALUE']?>"
														checked disabled
													></div>
													<div class="col-md-8 col-xs-8">
														<a href="<?=CFile::GetFileArray($arComplect['DETAIL_PICTURE'])['SRC'] ?>" data-lightbox="complect" data-title="<?=$arComplect['NAME']?>"><?=$arComplect['NAME']?></a>
													</div>
													<div class="col-md-3 col-xs-3">
														<div class="priceItems">  <?=$arComplect['PROPERTY_PRICE_VALUE'];?> ₽ </div>
													</div>
												</div>


											<?
										}
										?>

											 <div class="wrapper-block-btn order">
												<div class="wrapper">
													<span class="btn btn-default animate-load" data-event="jqm" data-param-id="17" data-name="order_product" data-autoload-need_product="<?=$arResult['NAME']?>" data-autoload-complect_price="<?=$total_price?>" data-autoload-product="<?=$arCamprersFields['NAME']?>">Заказать комплектацию</span>
												</div>
												<div class="wrapper">
													<span class="btn btn-default wide-block animate-load btn-transparent" data-event="jqm" data-param-id="40" data-name="question" data-autoload-need_product="<?=$arResult['NAME']?>"  data-autoload-complect_price="<?=$total_price?>" data-autoload-product="<?=$arCamprersFields['NAME']?>"><span>Купить в кредит от <span id="button_credit"><?=number_format($total_price / 10, 0, ',', ' ');?> ₽</span></span></span>
												</div>
											</div>
											<div class="wrapper-block-btn order">
												<div class="more"><a href="<?=$arCamprersFields["DETAIL_PAGE_URL"]?>" class="btn wide-block animate-load " href="">СОБРАТЬ СВОЮ КОМПЛЕКТАЦИЮ</a></div>
											</div>

										</div>
									</div>
							</div>
						</div>

					</div>
					<?
					$i++;
				}
			}

		}

?>


<script>
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true
	})
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

	BX.ready( function(){
		var popup = BX.PopupWindowManager.create( "popup-message", null, {
			content: BX('modal-image'),//`<img id="modal-image" src="">`,
			width: 800, // ширина окна
			height: 800, // высота окна
			offsetTop: 1,
			offsetLeft: 0,
			zIndex: 100, // z-index
			closeIcon: {
				// объект со стилями для иконки закрытия, при null - иконки не будет
				opacity: 1
			},
			//titleBar: 'Заголовок окна',
			closeByEsc: true, // закрытие окна по esc
			darkMode: false, // окно будет светлым или темным
			autoHide: true, // закрытие при клике вне окна
			draggable: false, // можно двигать или нет
			resizable: false, // можно ресайзить
			min_height: 300, // минимальная высота окна
			min_width: 300, // минимальная ширина окна
			lightShadow: true, // использовать светлую тень у окна
			angle: true, // появится уголок
			overlay: {
				// объект со стилями фона
				backgroundColor: 'black',
				opacity: 500
			},
			events: {
				onPopupShow: function(){
				},

				onPopupClose: function(){
					$( "#modal-image" ).attr( "src", '' );
					// Событие при закрытии окна
				}
			}
		} );

		$( '.img_modal' ).click( function(){ // задаем функцию при нажатиии на элемент <div>

			popup.show();
			console.log( 'click elem' );

			var imgSrc = $( this ).data( 'image' );

			$( "#modal-image" ).attr( "src", imgSrc );

				console.log( imgSrc );
			//$('.image-wrapper').html('<img id="modal-image" src="'+imgSrc+'">');

		} );
	} );

</script>

<?// date active from or dates period active?>

<?if(strlen($arResult['DISPLAY_PROPERTIES']['PERIOD']['VALUE']) || ($arResult['DISPLAY_ACTIVE_FROM'] && in_array('DATE_ACTIVE_FROM', $arParams['FIELD_CODE']))):?>

	<div class="period">

		<?if(strlen($arResult['DISPLAY_PROPERTIES']['PERIOD']['VALUE'])):?>

			<span class="date"><?=$arResult['DISPLAY_PROPERTIES']['PERIOD']['VALUE']?></span>

		<?else:?>

			<span class="date"><?=$arResult['DISPLAY_ACTIVE_FROM']?></span>

		<?endif;?>

	</div>

<?endif;?>
