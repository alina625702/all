<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
$frame = $this->createFrame()->begin();
$frame->setAnimation(true);
global $USER;
$userID = $USER->GetID();
$userID = ($userID > 0 ? $userID : 0);
global $arTheme;
$arParams["COUNT_IN_LINE"] = intval($arParams["COUNT_IN_LINE"]);
$arParams["COUNT_IN_LINE"] = (($arParams["COUNT_IN_LINE"] > 0 && $arParams["COUNT_IN_LINE"] < 12) ? $arParams["COUNT_IN_LINE"] : 3);
$colmd = floor(12 / $arParams['COUNT_IN_LINE']);
$colsm = floor(12 / round($arParams['COUNT_IN_LINE'] / 2));
$bShowImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE']);
$bOrderViewBasket = $arParams['ORDER_VIEW'];
$basketURL = (strlen(trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE'])) ? trim($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['URL_BASKET_SECTION']['VALUE']) : '');
if(count($arResult["ITEMS"]) > 1){
	$compactItemsMobile = (strlen($arTheme['CATALOG_ITEMS_COMPACT_VIEW_MOBILE_PRODUCT']['VALUE'] == 'Y') ? 'compactItemsMobile' : '');
};
?>
<?
$bHasSection = false;
if(isset($arResult['SECTION_CURRENT']) && $arResult['SECTION_CURRENT'])
	$bHasSection = true;
if($bHasSection)
{
	// edit/add/delete buttons for edit mode
	$arSectionButtons = CIBlock::GetPanelButtons($arParams['IBLOCK_ID'], 0, $arResult['SECTION_CURRENT']['ID'], array('SESSID' => false, 'CATALOG' => true));
	$this->AddEditAction($arResult['SECTION_CURRENT']['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_EDIT'));
	$this->AddDeleteAction($arResult['SECTION_CURRENT']['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
}?>
	<div class="item-views front tarifs wicons type_4 tarifs_scroll">
		<div class="maxwidth-theme">
			<div class=" top_title_block" style="display:block;margin-top:-30px;margin-bottom:40px;">
					<h2>Наши кемперы. Модельный ряд</h2>
				</div>
			<?

			$slideshowSpeed = (isset($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE']) && abs(intval($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE'])) ? $arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE'] : abs(intval($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED'])));
			$animationSpeed = (isset($arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE']) && abs(intval($arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE'])) ? $arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE'] : abs(intval($arTheme['PARTNERSBANNER_ANIMATIONSPEED'])));
			$bAnimation = (bool)$slideshowSpeed;
			?>
			<?//items?>
			<!-- <?if(isset($arParams["TITLE"]) && strlen($arParams["TITLE"])):?>
				<h2><?=$arParams["TITLE"];?></h2>
			<?endif;?> -->
			<div class="items  unstyled front dark-nav view-control navigation-vcenter" data-plugin-options='{"useCSS": false, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [4, 3, 2, 1], "itemMargin": 0}'>
				<? $i=1; ?>
				<? $k=0; ?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					// edit/add/delete buttons for edit mode
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arItem) : false);
					$bPreviewText = (isset($arItem['FIELDS']['PREVIEW_TEXT']) && strlen($arItem['FIELDS']['PREVIEW_TEXT']) ? true : false);
					$bProperties = (isset($arItem['CHARACTERISTICS']) && $arItem['CHARACTERISTICS'] ? true : false);
					$bIcon = (isset($arItem['DISPLAY_PROPERTIES']['ICON']) && $arItem['DISPLAY_PROPERTIES']['ICON']['VALUE'] ? true : false);
					$bBgIcon = (isset($arItem['PROPERTIES']['BACKGROUND']) && $arItem['PROPERTIES']['BACKGROUND']['VALUE_XML_ID'] == 'Y' ? true : false);
					$bImage = $arItem['FIELDS']['PREVIEW_PICTURE']['SRC'];

					?>
					<div class="item border shadow<?=!$bIcon ? ' wti' : ''?><?=($bBgIcon ? ' wbg' : '');?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"<?=($bOrderViewBasket ? ' data-item="'.$dataItem.'"' : '')?>>
						<div class="wrap">
							<div class="row flex">
									<div class="top_block_wrap col-md-6 col-sm-6" style="order: <?=$i%2 == 1 ? '1' : '2' ?>">
										<?if(strlen($arItem['FIELDS']['NAME'])):?>
											<div class="name title">

													<h3  style="text-transform:uppercase;"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h3>

											</div>
											<div class="short-desc" style="margin-bottom:15px;color:#269958!important;font-size:20px!important;"><?=$arItem['PROPERTIES']['SHORT_DESC']['VALUE']?></div>
										<?endif;?>
										<!-- <div class="name title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['FIELDS']['NAME'];?></a></div> -->





													<!-- <?=$arItem['FIELDS']['PREVIEW_TEXT'];?> -->
													<div class="base-complect spoiler ">
														<?foreach ($arItem['PROPERTIES']['BASE_COMPLECT']['VALUE'] as $value) {	}	echo html_entity_decode($arItem['PROPERTIES']['BASE_COMPLECT']['VALUE']['TEXT']);	?>
													</div>
													<a class="" data-toggle="collapse" href="#collapseExample<?=$k;?>" role="button" aria-expanded="false" aria-controls="collapseExample<?=$k;?>">
													<!-- <div class="price">Подробнее</span> &#8659;</div> -->
													</a>


										<div style="margin-bottom:40px;" class="order<?=($bOrderViewBasket ? ' basketTrue' : '')?>">
												<?if($arItem['PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'] == 'YES' && !$bOrderViewBasket):?>
													<!-- <span class="btn btn-default animate-load btn-transparent" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_order_product");?>" data-name="order_product" data-autoload-need_product="<?=$arItem['NAME']?><?=($defaultPrice ? ': '.$defaultPrice : '');?>"><span>Заказать комплектацию</span></span> -->
													<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn btn-default"><span>Подробнее о модели</span></a>
												<?endif;?>
											</div>

									</div>
									<div class="properties_wrap image_wrap col-md-6 col-sm-6" style="order: <?=$i%2 == 1 ? '2' : '1' ?>">
										<div class="block">
										<img style="width: 100%;max-height:334px;object-fit:cover;" src="<?=$bImage?>">
											<div class="price campers_price"><h4 >Базовая комплектация: <span class="value"><?=number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, '', ' ');?> руб</span></h4></div>
											</div>
									</div>
							</div>
						</div>
					</div>
					<? $i++;?>
					<? $k++;?>
				<?endforeach;?>
				<?/*if(isset($arParams['NEWS_COUNT']) && isset($arParams['MAX_COUNT_ELEMENTS_ON_PAGE']) && $arResult['COUNT_ELEMENTS'] > $arParams['NEWS_COUNT'] && $arParams['NEWS_COUNT'] < $arParams['MAX_COUNT_ELEMENTS_ON_PAGE']):?>
					<?
					$countSections = $arResult['COUNT_SECTIONS'] - $arParams['NEWS_COUNT'];*/
				if((int)$arResult['NAV_RESULT']->nEndPage > 1):?>
					<div class="ajax_btn">
						<span class="btn btn-default btn-sm btn-transparent" data-params="<?=urlencode(serialize($arParams))?>" data-template="<?=$this->__component->__template->__name?>" data-template_name="<?=$this->__component->__template->__name?>"><?=GetMessage('MORE_TEXT_AJAX')?></span>
					</div>
				<?endif?>
			</div>
		</div>
	</div>

<?if($bHasSection):?>
	</div>
<?endif;?>

 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"tarifs_front_4", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COUNT_SHOW_PROPRERTIES" => "4",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "32",
		"IBLOCK_TYPE" => "aspro_priority_catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MAX_COUNT_ELEMENTS_ON_PAGE" => "9",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SHOW_PROPS_NAME" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TITLE" => "",
		"COMPONENT_TEMPLATE" => "tarifs_front_4",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?><br>
<?$frame->end();?>
