<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) die();?>
<?$this->setFrameMode(true);?>
<?use \Bitrix\Main\Localization\Loc;?>
<?if($arResult['ITEMS']):?>
	<?
	global $arTheme;
	$bOrderViewBasket = $arParams['ORDER_VIEW'];

	$basketURL = (!isset($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']) && strlen($arTheme['URL_BASKET_SECTION']) ? $arTheme['URL_BASKET_SECTION'] : (isset($arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']) && strlen(trim($arTheme['URL_BASKET_SECTION']['VALUE'])) ? $arTheme['URL_BASKET_SECTION']['VALUE'] : SITE_DIR.'cart/'));
	?>
	<div class="item-views front tarifs wicons type_4 tarifs_scroll">
		<div class="maxwidth-theme">
			<div class=" top_title_block" style="display:block">
					<h2>Комплектации "под ключ"</h2>
					<div class="block-description">
						<p>Вы можете выбрать только необходимое дополнительное оборудование, без переплаты за ненужное.<br>
						Поход с друзьями, рыбалка, на велосипедах в горы, отдых с детьми или парой - для этого всего вы можете подобрать у нас кемпер из готовых комплектаций!</p>
					</div>
				</div>
			<?
			global $arTheme;
			$slideshowSpeed = (isset($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE']) && abs(intval($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE'])) ? $arTheme['PARTNERSBANNER_SLIDESSHOWSPEED']['VALUE'] : abs(intval($arTheme['PARTNERSBANNER_SLIDESSHOWSPEED'])));
			$animationSpeed = (isset($arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE']) && abs(intval($arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE'])) ? $arTheme['PARTNERSBANNER_ANIMATIONSPEED']['VALUE'] : abs(intval($arTheme['PARTNERSBANNER_ANIMATIONSPEED'])));
			$bAnimation = (bool)$slideshowSpeed;
			?>
			<?//items?>
			<?if(isset($arParams["TITLE"]) && strlen($arParams["TITLE"])):?>
				<h2><?=$arParams["TITLE"];?></h2>
			<?endif;?>
			<div class="items complectations unstyled front dark-nav view-control navigation-vcenter" data-plugin-options='{"useCSS": false, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [4, 3, 2, 1], "itemMargin": 0}'>
				<? $i=0; ?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					// edit/add/delete buttons for edit mode
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					$dataItem = ($bOrderViewBasket ? CPriority::getDataItem($arItem) : false);
					$bPreviewText = (isset($arItem['FIELDS']['PREVIEW_TEXT']) && strlen($arItem['FIELDS']['PREVIEW_TEXT']) ? true : false);
					$bProperties = (isset($arItem['CHARACTERISTICS']) && $arItem['CHARACTERISTICS'] ? true : false);
					$bIcon = (isset($arItem['DISPLAY_PROPERTIES']['ICON']) && $arItem['DISPLAY_PROPERTIES']['ICON']['VALUE'] ? true : false);
					$bBgIcon = (isset($arItem['PROPERTIES']['BACKGROUND']) && $arItem['PROPERTIES']['BACKGROUND']['VALUE_XML_ID'] == 'Y' ? true : false);
					$bImage = $arItem['FIELDS']['PREVIEW_PICTURE']['SRC'];

					?>
					<div class="item border shadow<?=!$bIcon ? ' wti' : ''?><?=($bBgIcon ? ' wbg' : '');?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"<?=($bOrderViewBasket ? ' data-item="'.$dataItem.'"' : '')?>>
						<div class="wrap">
							<div class="row flex">
									<div class="top_block_wrap col-md-6 col-sm-6" style="order: <?=$i%2 == 1 ? '1' : '2' ?>">
										<div class="name title"><h3 style="text-transform:uppercase;"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['FIELDS']['NAME'];?></a></h3></div>
										<?if($bPreviewText):?>
											<div class="previewtext"><?=$arItem['FIELDS']['PREVIEW_TEXT'];?></div>
											<div class="order<?=($bOrderViewBasket ? ' basketTrue' : '')?>">
												<?if($arItem['PROPERTIES']['FORM_ORDER']['VALUE_XML_ID'] == 'YES' && !$bOrderViewBasket):?>
													<span class="btn btn-default animate-load btn-transparent" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_order_product");?>" data-name="order_product" data-autoload-need_product="<?=$arItem['NAME']?><?=($defaultPrice ? ': '.$defaultPrice : '');?>"><span>Заказать комплектацию</span></span>
													<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn btn-default"><span>Подробнее</span></a>
												<?endif;?>
											</div>
										<?endif;?>
									</div>
									<div class="properties_wrap image_wrap col-md-6 col-sm-6" style="order: <?=$i%2 == 1 ? '2' : '1' ?>">
										<img style="width: 100%" src="<?=$bImage?>">
									</div>
							</div>
						</div>
					</div>
					<? $i++;?>
				<?endforeach;?>
				<?/*if(isset($arParams['NEWS_COUNT']) && isset($arParams['MAX_COUNT_ELEMENTS_ON_PAGE']) && $arResult['COUNT_ELEMENTS'] > $arParams['NEWS_COUNT'] && $arParams['NEWS_COUNT'] < $arParams['MAX_COUNT_ELEMENTS_ON_PAGE']):?>
					<?
					$countSections = $arResult['COUNT_SECTIONS'] - $arParams['NEWS_COUNT'];*/
				if((int)$arResult['NAV_RESULT']->nEndPage > 1):?>
					<div class="ajax_btn">
						<span class="btn btn-default btn-sm btn-transparent" data-params="<?=urlencode(serialize($arParams))?>" data-template="<?=$this->__component->__template->__name?>" data-template_name="<?=$this->__component->__template->__name?>"><?=GetMessage('MORE_TEXT_AJAX')?></span>
					</div>
				<?endif?>
			</div>
		</div>
	</div>
<?endif;?>
