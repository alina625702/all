<?
	if( !defined( 'B_PROLOG_INCLUDED' ) || B_PROLOG_INCLUDED !== true )
		die();
	$this->setFrameMode( true );
	$camperId = $arParams[ 'CAMPER_ID' ];
	$arrayCurrentComplectations = array();
	// Получаем все возможные направления
	$arLinkedCategoryFilter = array( "IBLOCK_ID" => "32" );
	$arSelectLinkedCampers = array( "ID", "NAME", "PROPERTY_LINK_COMPLECT" );
	$resLinkedCampers = CIBlockElement::GetList( array( "ID" => "ASC" ), $arLinkedCategoryFilter, false, false, $arSelectLinkedCampers );
	while( $ob = $resLinkedCampers->GetNextElement() ){
		$arLinkedCategoryFields = $ob->GetFields();
		foreach( $arLinkedCategoryFields[ 'PROPERTY_LINK_COMPLECT_VALUE' ] as $complectId ){

			// Получаем все комплектации выбранного направления
			$arLinkedComplectsFilter = array( "IBLOCK_ID" => "42", "ID" => $complectId );
			$arSelectComplectsCampers = array( "ID", "NAME", "PROPERTY_CAMPER" );
			$resLinkedComplects = CIBlockElement::GetList( array( "ID" => "ASC" ), $arLinkedComplectsFilter, false, false, $arSelectComplectsCampers );
			while( $obComplects = $resLinkedComplects->GetNextElement() ){
				$arLinkedComplectsFields = $obComplects->GetFields();
				if( $camperId == $arLinkedComplectsFields[ 'PROPERTY_CAMPER_VALUE' ] ){

					//Заносим в массив только те направления где есть выбранный кемпер
					array_push( $arrayCurrentComplectations, array( 'NAME' => $arLinkedCategoryFields[ 'NAME' ], 'COMPLECT_ID' => $complectId, 'CAMPER' => $arLinkedComplectsFields[ 'PROPERTY_CAMPER_VALUE' ] ) );
				}
			}
		}
	}
?>
<h3>ДОПОЛНИТЕЛЬНАЯ КОМПЛЕКТАЦИЯ</h3>
<ul id="tabs">
	<li class="current"><a title="tab5" id="advanced" href="#">Своя комплектация</a></li>
	<?
		foreach( $arrayCurrentComplectations as $item ){
			?>
			<li><a title="tab1" data-complect-id="<?= $item[ 'COMPLECT_ID' ] ?>"
				   id="complect_<?= $item[ 'COMPLECT_ID' ] ?>" href="#"><?= $item[ 'NAME' ] ?></a></li>
			<?
		}
	?>

</ul>
<hr style="background-color:#219754D4;margin: 5px 0px 5px;">

<? if( $arResult[ 'ITEMS' ] ): ?>
	<div class="row">
		<div class="col-md-12">
			<div class="item-views linked sections vacancys camper-complect-list">
				<div class="items row">
					<? foreach( $arResult[ 'ITEMS' ] as $arItem ): ?>
						<?
						$blockedItems = $arItem[ 'PROPERTIES' ][ 'BLOCKED' ][ 'VALUE' ];
						$linkedItems = $arItem[ 'PROPERTIES' ][ 'LINKED' ][ 'VALUE' ];
						$priceItems = $arItem[ 'PROPERTIES' ][ 'PRICE' ][ 'VALUE' ];
						$imageItems = $arItem[ 'FIELDS' ][ 'DETAIL_PICTURE' ];
						$blockedItemsIds = '';
						foreach( $blockedItems as $item ){
							$blockedItemsIds .= $item.',';
						}
						$linkedItemsIds = '';
						foreach( $linkedItems as $item ){
							$linkedItemsIds .= $item.',';
						}
						// edit/add/delete buttons for edit mode
						$this->AddEditAction( $arItem[ 'ID' ], $arItem[ 'EDIT_LINK' ], CIBlock::GetArrayByID( $arItem[ 'IBLOCK_ID' ], 'ELEMENT_EDIT' ) );
						$this->AddDeleteAction( $arItem[ 'ID' ], $arItem[ 'DELETE_LINK' ], CIBlock::GetArrayByID( $arItem[ 'IBLOCK_ID' ], 'ELEMENT_DELETE' ), array( 'CONFIRM' => GetMessage( 'CT_BNL_ELEMENT_DELETE_CONFIRM' ) ) );
						?>

						<div class="col-md-12 col-sm-12">
							<div id="item_wrap" class="item_wrap border shadow item_<?= $arItem[ 'ID' ]; ?>">
								<div class="item clearfix wti" id="<?= $this->GetEditAreaId( $arItem[ 'ID' ] ) ?>">
									<div class="col-xs-1"><input
												class="complect-item link"
												type="checkbox"
												id="complect"
												name="complect_<?= $arItem[ 'CODE' ] ?>"
												data-blocked="<?= $blockedItemsIds; ?>"
												data-linked="<?= $linkedItemsIds; ?>"
												data-id="<?= $arItem[ 'ID' ] ?>"
												data-name="<?= $arItem[ 'NAME' ] ?>"
												data-price="<?= $arItem[ 'PROPERTIES' ][ 'PRICE' ][ 'VALUE' ] ?>"
										></div>
									<div class="info col-xs-8 ">
										<? // section name?>
										<? if( in_array( 'NAME', $arParams[ 'FIELD_CODE' ] ) ): ?>
											<div class="title">
												<p><a href="<?= $imageItems[ 'SRC' ] ?>" data-lightbox="complect" data-title="<?= $arItem[ 'NAME' ]?>"><?= $arItem[ 'NAME' ] ?></a></p>

											</div>
										<? endif; ?>
										<? if( $arItem[ "DISPLAY_PROPERTIES" ] ): ?>
											<div class="properties">
												<? $i = 0; ?>
												<? foreach( $arItem[ "DISPLAY_PROPERTIES" ] as $PCODE => $arProperty ): ?>
													<?
													if( $PCODE == 'PAY' || $arProperty[ 'PROPERTY_TYPE' ] == 'E' || $arProperty[ 'PROPERTY_TYPE' ] == 'G' )
														continue;
													?>
													<div class="inner-wrapper">
														<div class="property <?= strtolower( $PCODE ); ?>">
															<span class="value font_upper">
																<? if( is_array( $arProperty[ "DISPLAY_VALUE" ] ) ): ?>
																	<? $val = implode( ",  ", $arProperty[ "DISPLAY_VALUE" ] ); ?>
																<? else: ?>
																	<? $val = $arProperty[ "DISPLAY_VALUE" ]; ?>
																<? endif; ?>
																<? if( $PCODE == "SITE" ): ?>
																	<!--noindex-->
																	<a href="<?= ( strpos( $arProperty[ 'VALUE' ], 'http' ) === false ? 'http://' : '' ).$arProperty[ 'VALUE' ]; ?>"
																	   rel="nofollow" target="_blank">
																		<?= $arProperty[ 'VALUE' ]; ?>
																	</a>
																	<!--/noindex-->
																<? elseif( $PCODE == "EMAIL" ): ?>
																	<a href="mailto:<?= $val ?>"><?= $val ?></a>
																<? else: ?>
																	<?= $val ?>
																<? endif; ?>
															</span>
															<? if( $i != count( $arItem[ "DISPLAY_PROPERTIES" ] ) - 2 ): ?>
																<span class="separator font_upper">&mdash;</span>
															<? endif; ?>
														</div>
													</div>
													<? ++$i; ?>
												<? endforeach; ?>
											</div>
										<? endif; ?>
										<? if( isset( $arItem[ "DISPLAY_PROPERTIES" ][ 'PAY' ] ) && $arItem[ "DISPLAY_PROPERTIES" ][ 'PAY' ][ 'VALUE' ] ): ?>
											<div class="pay"><?= $arItem[ "DISPLAY_PROPERTIES" ][ 'PAY' ][ 'VALUE' ]; ?></div>
										<? endif; ?>

										<? if( in_array( 'PREVIEW_TEXT', $arParams[ 'FIELD_CODE' ] ) && strlen( $arItem[ 'PREVIEW_TEXT' ] ) ): ?>
											<div class="previewtext"><?= $arItem[ 'PREVIEW_TEXT' ] ?></div>
										<? endif; ?>
									</div>
									<div class="col-xs-3 priceItems">
											<?= $priceItems; ?> ₽
									</div>
								</div>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			</div>



			<?php
				CJSCore::Init( [ 'popup' ] );
			?>
	<div ><img id="modal-image" style="display:block;" src=""></div>

		</div>
	</div>
	<script>
		$( document ).ready( function(){
			let clickCount = 0;
			let productPrice = parseInt( $( '.price_val' ).text().toString().replace( ' ', '' ) );
			// Меняем цену и вес комплектации
			$( "input#complect" ).click( function(){
				clickCount += 1;
				let checked = $( this ).is( ':checked' )
				let complectPrice = 0;
				let complect = '';
				let checkedItems = $( 'input#complect:checked' );
				checkedItems.each( function(){
					complectPrice += parseInt( $( this ).data( 'price' ).toString().replace( ' ', '' ) );
					complect += $( this ).data( 'name' ) + ', ';
				} );
				$( '.price_val' ).text( prettify( productPrice + complectPrice ) );
				$( '#button_credit' ).text( prettify( (productPrice + complectPrice) / 10 ) );
				$('.btn-default').attr('data-autoload-complect_price', productPrice + complectPrice);
				$('.btn-default').attr('data-autoload-complect', complect);
				console.log(complect);
				let blockedItems = $( this ).data( 'blocked' ).split( ',' );
				let linkedItems = $( this ).data( 'linked' ).split( ',' );

				blockedItems.forEach( function( element ){
					if( element != '' ){
						if( checked ){
							$( "input#complect[data-id='" + element + "']" ).prop( 'disabled', true );
							$( '.item_' + element ).addClass( 'dsbl' );
						}else{
							$( "input#complect[data-id='" + element + "']" ).prop( 'disabled', false );
							$( '.item_' + element ).removeClass( 'dsbl' );
						}
					}

				} );

				linkedItems.forEach( function( element ){
					if( element != '' ){
						if( checked ){
							$( "input#complect[data-id='" + element + "']" ).prop( 'checked', true );
						}
					}
				} );
				console.log(clickCount);
				if (clickCount > 4) {
					yaCounter51941264.reachGoal('calc_use');
				}
			} );
		} );

		function prettify( num ){
			var n = num.toString();
			return n.replace( /(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ' );
		}

	</script>

	<script>
		$( document ).ready( function(){
			let productPrice = parseInt( $( '.price_val' ).text().toString().replace( ' ', '' ) );
			$( '#tabs a' ).click( function( e ){
				e.preventDefault();
				$( '#tabs li' ).removeClass( 'current' );
				$( this ).parent().addClass( 'current' );

				$( '.vacancys .item_wrap' ).each( function( index ){
					$( this ).removeClass( 'dsbl' );
					$( this ).find( '.complect-item' ).removeAttr( 'disabled' );
					$( this ).find( '.complect-item' ).prop( 'checked', false );
				} );

					$thisVar = $( this );
					$.ajax( {
						url: "/ajax/get_complectation.php",
						type: "post",
						data: {complect_id: $( this ).data( 'complect-id' )},
						success: function( response ){
							response = JSON.parse( response );
							let priceComplectations = 0;
							$( '.vacancys .item_wrap' ).each( function( index ){
								let idd = $( this ).find( '.complect-item' ).data( "id" );

								if( response.some(n => n.includes(String($( this ).find( '.complect-item' ).data( "id" )))) != false ){//проверяем есть ли комплектация в массиве для показа с ajax
									$( this ).parent().show();
									if( $thisVar.attr( 'id' ) !== 'advanced' ){ //если  таб не "своя комплектация"
										$( "input#complect[data-id='" + idd + "']" ).prop( 'checked', true );
										$( "input#complect[data-id='" + idd + "']" ).prop('disabled',true);
										let complectPrice = parseInt( $( "input#complect[data-id='" + idd + "']" ).data( 'price' ).toString().replace( ' ', '' ) );
										priceComplectations += complectPrice;
									}
								}else{
									$( this ).parent().hide();
								}
							} );
							$('.btn-default').data('autoload-complect_price', productPrice + priceComplectations );
							$( '.price_val' ).text( prettify( productPrice + priceComplectations ) );
							$( '#button_credit' ).text( prettify( (productPrice + priceComplectations) / 10 ) );
						},
						error: function( jqXHR, textStatus, errorThrown ){
							console.log( textStatus, errorThrown );
						}
					});
			});
		} );
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

			<script>
		    lightbox.option({
		      'resizeDuration': 200,
		      'wrapAround': true
		    })
		</script>
<? endif; ?>
