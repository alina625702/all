					<?if(!$isIndex):?>
						<?CPriority::checkRestartBuffer();?>
					<?endif;?>
					<?IncludeTemplateLangFile(__FILE__);?>
					<?global $arTheme, $isIndex, $is404, $isCatalog, $isServices;?>
					<?if(!$isIndex && !$isCatalog && !$isProjects):?>
							<?if($is404):?>
								</div>
							<?else:?>
									<?if(!$isMenu):?>
										</div><?// class=col-md-12 col-sm-12 col-xs-12 content-md?>
									<?elseif($isMenu && $arTheme["SIDE_MENU"]["VALUE"] == "LEFT" && !$isBlog):?>
										<?CPriority::get_banners_position('CONTENT_BOTTOM');?>
										</div><?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
									<?elseif($isMenu && ($arTheme["SIDE_MENU"]["VALUE"] == "RIGHT" || $isBlog)):?>
										<?CPriority::get_banners_position('CONTENT_BOTTOM');?>
										</div><?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
										<div class="col-md-3 col-sm-3 hidden-xs hidden-sm right-menu-md">
											<?CPriority::ShowPageType('left_block')?>
										</div>
									<?endif;?>
								<?endif;?>
								</div><?// class=row?>
						<?if($APPLICATION->GetProperty("FULLWIDTH")!=='Y' || $isServices):?>
							</div><?// class="maxwidth-theme?>
						<?endif;?>
					<?elseif($isIndex):?>
						<?CPriority::ShowPageType('indexblocks');?>
					<?endif;?>
				</div><?// class=container?>
				<?CPriority::get_banners_position('FOOTER');?>
			</div><?// class=main?>
		</div><?// class=body?>
		<?CPriority::ShowPageType('footer', '', 'FOOTER_TYPE');?>
		<div class="bx_areas">
			<?CPriority::ShowPageType('bottom_counter');?>
		</div>
		<?CPriority::SetMeta();?>
		<?CPriority::ShowPageType('search_title_component');?>
		<?CPriority::ShowPageType('basket_component');?>
		<?CPriority::AjaxAuth();?>
		<script src="/bitrix/templates/aspro-priority/js/simple-adaptive-slider.min.js"></script>
		<div class="cookie_notification">
      <p>Для улучшения работы сайта и его взаимодействия с пользователями мы используем файлы cookie. Продолжая работу с сайтом, Вы разрешаете использование cookie-файлов. Вы всегда можете отключить файлы cookie в настройках Вашего браузера.</p>
      <button class="btn-xs cookie_accept">Принять</button>
		</div>
		<!-- <div data-marquiz-id="60c750490f23ba00464db8d5"></div>
		<script>(function(t, p) {window.Marquiz ? Marquiz.add([t, p]) : document.addEventListener('marquizLoaded', function() {Marquiz.add([t, p])})})('Button', {id: '60c750490f23ba00464db8d5', buttonText: 'Подобрать кемпер', bgColor: '#49945b', textColor: '#fff', shadow: 'rgba(73, 148, 91, 0.5)', blicked: true})</script> -->
		<script>
		/* Add here all your JS customizations */
		/*function checkCookies(){
				let cookieDate = localStorage.getItem('cookieDate');
				let cookieNotification = $('.cookie_notification');
				let cookieBtn = $('#cookie_notification .cookie_accept');
				// Если записи про кукисы нет или она просрочена на 1 год, то показываем информацию про кукисы
				if( !cookieDate || (+cookieDate + 31536000000) < Date.now() ){
						$('.cookie_notification').addClass('show');
				}

				// При клике на кнопку, в локальное хранилище записывается текущая дата в системе UNIX
				$('.cookie_accept').on('click', function(){
					console.log('click');
						localStorage.setItem( 'cookieDate', Date.now() );
						$('.cookie_notification').removeClass('show');
				})
		}
		checkCookies(); */
		</script>
		<script async type="text/javascript">
			(function ct_load_script() {
			var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async=true;
			ct.src = document.location.protocol+'//cc.calltracking.ru/phone.880c4.11318.async.js?nc='+Math.floor(new Date().getTime()/300000);
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
			})();
		</script>

		<!-- <?
		// $APPLICATION->IncludeFile(SITE_DIR."include/quiz.php", Array(), Array(
		// 		"MODE" => "php",
		// 		"NAME" => "Quiz",
		// 		"TEMPLATE" => "include_area.php",
		// 	)
		// );
		?> -->
		<a href="#popup:marquiz_60c750490f23ba00464db8d5" class="quiz-btn"> Подобрать кемпер <span class="flare"></span></a>
		<noscript>
   		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLDDQ9T" height="0" width="0" style="display: none; visibility: hidden"></iframe>
		</noscript>
	</body>
</html>
