<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    #сохраняем utm-метки в cookie
    if(isset($_GET["utm_source"])) setcookie("utm_source",$_GET["utm_source"],time()+3600*24*30,"/");
    if(isset($_GET["utm_medium"])) setcookie("utm_medium",$_GET["utm_medium"],time()+3600*24*30,"/");
    if(isset($_GET["utm_campaign"])) setcookie("utm_campaign",$_GET["utm_campaign"],time()+3600*24*30,"/");
    if(isset($_GET["utm_content"])) setcookie("utm_content",$_GET["utm_content"],time()+3600*24*30,"/");
    if(isset($_GET["utm_term"])) setcookie("utm_term",$_GET["utm_term"],time()+3600*24*30,"/");
?><!DOCTYPE html>
<?
if(CModule::IncludeModule("aspro.priority"))
	$arThemeValues = CPriority::GetFrontParametrsValues(SITE_ID);
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>" class="<?=($_SESSION['SESS_INCLUDE_AREAS'] ? 'bx_editmode ' : '')?><?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0' ) ? 'ie ie7' : ''?> <?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0' ) ? 'ie ie8' : ''?> <?=strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0' ) ? 'ie ie9' : ''?>">
	<head>

    <meta name="yandex-verification" content="7644b3fafa8c789a" />
		<?global $APPLICATION;?>
		<?IncludeTemplateLangFile(__FILE__);?>
		<title><?$APPLICATION->ShowTitle()?></title>
    <script>
       (function(w, d, s, l, i) {
           w[l] = w[l] || [];
           w[l].push({
               'gtm.start':
                   new Date().getTime(),
               event: 'gtm.js'
           });
           var f = d.getElementsByTagName(s)[0],
               j = d.createElement(s),
               dl = l != 'dataLayer' ? '&l=' + l : '';
           j.async = true;
           j.src =
               'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
           f.parentNode.insertBefore(j, f);
       })(window, document, 'script', 'dataLayer', 'GTM-WLDDQ9T');
    </script>
		<?$APPLICATION->ShowMeta("viewport");?>
		<?$APPLICATION->ShowMeta("HandheldFriendly");?>
		<?$APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes");?>
		<?$APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style");?>
		<?$APPLICATION->ShowMeta("SKYPE_TOOLBAR");?>
		<?$APPLICATION->ShowHead();?>
    <link href="/bitrix/templates/aspro-priority/js/lightbox/css/lightbox.min.css" rel="stylesheet" />
    <script src="/bitrix/templates/aspro-priority/js/lightbox/js/lightbox.min.js"></script>
		<?$APPLICATION->AddHeadString('<script>BX.message('.CUtil::PhpToJSObject($MESS, false).')</script>', true);?>
		<?if(CModule::IncludeModule("aspro.priority")) {CPriority::Start(SITE_ID);}?>
		<link href="/bitrix/templates/aspro-priority/css/pr-styles.css"  data-template-style="true"  rel="stylesheet" />

		<!-- Slider -->
		<link href="/bitrix/templates/aspro-priority/css/simple-adaptive-slider.min.css"  data-template-style="true"  rel="stylesheet" />
		<!-- Marquiz script start -->
		<script>
		(function(w, d, s, o){
		  var j = d.createElement(s); j.async = true; j.src = '//script.marquiz.ru/v2.js';j.onload = function() {
		    if (document.readyState !== 'loading') Marquiz.init(o);
		    else document.addEventListener("DOMContentLoaded", function() {
		      Marquiz.init(o);
		    });
		  };
		  d.head.insertBefore(j, d.head.firstElementChild);
		})(window, document, 'script', {
		    host: '//quiz.marquiz.ru',
		    region: 'eu',
		    id: '60c750490f23ba00464db8d5',
		    autoOpen: false,
		    autoOpenFreq: 'once',
		    openOnExit: false,
		    disableOnMobile: false
		  }
		);
		</script>
		<!-- Marquiz script end -->

<script type="text/javascript">(function(window,document,n,project_ids){window.GudokData=n;if(typeof project_ids !== "object"){project_ids = [project_ids]};window[n] = {};window[n]["projects"]=project_ids;config_load(project_ids.join(','));function config_load(cId){var a=document.getElementsByTagName("script")[0],s=document.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},cMrs='';s.async=true;if(document.location.search&&document.location.search.indexOf('?gudok_check=')===0)cMrs+=document.location.search.replace('?','&');s.src="//mod.gudok.tel/script.js?sid="+cId+cMrs;if(window.opera == "[object Opera]"){document.addEventListener("DOMContentLoaded", i, false)}else{i()}}})(window, document, "gd", "uqo4tuxx1u");</script>
  <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?169",t.onload=function(){VK.Retargeting.Init("VK-RTRG-892426-6ZlG3"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript>
    <img src="https://vk.com/rtrg?p=VK-RTRG-892426-6ZlG3" style="position:fixed; left:-999px;" alt=""/>
  </noscript>
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '448074273140345');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"   src="https://www.facebook.com/tr?id=448074273140345&ev=PageView&noscript=1"/></noscript>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=84ccd37e-2b5d-4d86-959f-299371371e0a" type="text/javascript"></script>
  <!-- jQuery Modal -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
  <meta property="og:image" content="https://camper-ural.ru/upload/iblock/7a7/7znw1wh620sph8e840pw7ulcl8dtphry.jpg" />
  <meta name="twitter:card" content="summary_large_image" />
  </head>

	<?$bIndexBot = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false); // is indexed yandex/google bot?>
	<body class="<?=CPriority::getConditionClass();?> mheader-v<?=$arThemeValues["HEADER_MOBILE"];?> footer-v<?=strtolower($arThemeValues['FOOTER_TYPE']);?> fill_bg_<?=strtolower($arThemeValues['SHOW_BG_BLOCK']);?> title-v<?=$arThemeValues["PAGE_TITLE"];?><?=($arThemeValues['ORDER_VIEW'] == 'Y' && $arThemeValues['ORDER_BASKET_VIEW']=='HEADER'? ' with_order' : '')?><?=($arThemeValues['CABINET'] == 'Y' ? ' with_cabinet' : '')?><?=(intval($arThemeValues['HEADER_PHONES']) > 0 ? ' with_phones' : '')?><?=($arThemeValues['DECORATIVE_INDENTATION'] == 'Y' ? ' with_decorate' : '')?> wheader_v<?=$arThemeValues['HEADER_TYPE']?><?=($arThemeValues['ROUND_BUTTON'] == 'Y' ? ' round_button' : '');?><?=($arThemeValues['PAGE_TITLE_POSITION'] == 'center' ? ' title_center' : '');?><?=(CSite::inDir(SITE_DIR."index.php") ? ' in_index' : '')?><?=(CSite::inDir(SITE_DIR."complectation/") ? ' complectation' : '')?><?=(CSite::inDir(SITE_DIR."campers/") ? ' campers' : '')?> <?=($bIndexBot ? "wbot" : "");?>">
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div class="header-tizer">
      <a href="/company/news/barhatniy-sezon/" class="text">При заказе до конца сентября дарим пакет дополнительного оборудования!</a>
    </div>
		<?if(!CModule::IncludeModule("aspro.priority")):?>
			<?$APPLICATION->SetTitle(GetMessage("ERROR_INCLUDE_MODULE_PRIORITY_TITLE"));?>
			<?$APPLICATION->IncludeFile(SITE_DIR."include/error_include_module.php");?>
			<?die();?>
		<?endif;?>
		<?CPriority::SetJSOptions();?>
		<?global $arTheme;?>
		<?$arTheme = $APPLICATION->IncludeComponent("aspro:theme.priority", "", array(), false);?>

		<?include_once('defines.php');?>
		<?CPriority::get_banners_position('TOP_HEADER');?>
		<div class="cd-modal-bg"></div>
		<?CPriority::ShowPageType('mega_menu');?>

		<div class="header_wrap visible-lg visible-md title-v<?=$arTheme["PAGE_TITLE"]["VALUE"];?><?=($isIndex ? ' index' : '')?>">
			<?CPriority::ShowPageType('header', '', 'HEADER_TYPE');?>
		</div>

		<?CPriority::get_banners_position('TOP_UNDERHEADER');?>

		<?if($arTheme["TOP_MENU_FIXED"]["VALUE"] == 'Y'):?>
			<div id="headerfixed">
				<?CPriority::ShowPageType('header_fixed');?>
			</div>
		<?endif;?>

		<div id="mobileheader" class="visible-xs visible-sm">
			<?CPriority::ShowPageType('header_mobile');?>
			<div id="mobilemenu" class="<?=($arTheme["HEADER_MOBILE_MENU_OPEN"]["VALUE"] == '1' ? 'leftside':'dropdown')?><?=($arTheme['HEADER_MOBILE_MENU_COLOR']['VALUE'] ? ' '.$arTheme['HEADER_MOBILE_MENU_COLOR']['VALUE'] : '')?>">
				<?CPriority::ShowPageType('header_mobile_menu');?>
			</div>
		</div>

		<?if($arTheme['MOBILE_FILTER_COMPACT']['VALUE'] === 'Y'):?>
				<div id="mobilefilter" class="visible-xs visible-sm scrollbar-filter"></div>
		<?endif;?>


		<div class="body <?=($isIndex ? 'index' : '')?> hover_<?=$arTheme["HOVER_TYPE_IMG"]["VALUE"];?>">
			<div class="body_media"></div>

			<div role="main" class="main banner-<?=$arTheme["BANNER_WIDTH"]["VALUE"];?>">
				<?if(!$isIndex && !$is404 && !$isForm):?>

					<?$APPLICATION->ShowViewContent('section_bnr_content');?>
					<?if($APPLICATION->GetProperty("HIDETITLE")!=='Y'):?>
						<!--title_content-->
						<? CPriority::ShowPageType('page_title');?>
						<!--end-title_content-->
					<?endif;?>

					<?$APPLICATION->ShowViewContent('top_section_filter_content');?>
				<?endif; // if !$isIndex && !$is404 && !$isForm?>

				<div class="container <?=($isCabinet ? 'cabinte-page' : '');?>">
					<?if(!$isIndex && !$isCatalog && !$isProjects):?>
						<?if($APPLICATION->GetProperty("FULLWIDTH")!=='Y' || $isServices):?>
							<div class="maxwidth-theme">
						<?endif;?>
								<div class="row">
							<?if($is404):?>
								<div class="col-md-12 col-sm-12 col-xs-12 content-md">
							<?else:?>
								<?if(!$isMenu):?>
									<div class="col-md-12 col-sm-12 col-xs-12 content-md">
								<?elseif($isMenu && $isServices):?>
										<div class="col-md-12 col-sm-12 col-xs-12 content-md">
								<?elseif($isMenu && !$isServices && ($arTheme["SIDE_MENU"]["VALUE"] == "RIGHT" || $isBlog)):?>
									<div class="col-md-9 col-sm-12 col-xs-12 content-md">
									<?CPriority::get_banners_position('CONTENT_TOP');?>
								<?elseif($isMenu && !$isServices && $arTheme["SIDE_MENU"]["VALUE"] == "LEFT" && !$isBlog):?>
									<div class="col-md-3 col-sm-3 hidden-xs hidden-sm left-menu-md">
										<?CPriority::ShowPageType('left_block')?>
									</div>
									<div class="col-md-9 col-sm-12 col-xs-12 content-md">
									<?CPriority::get_banners_position('CONTENT_TOP');?>
								<?endif;?>
							<?endif;?>
					<?endif;?>
					<?CPriority::checkRestartBuffer();?>
