<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>



<div class="mega_fixed_menu">

	<div class="maxwidth-theme">

		<div class="row">

			<div class="col-md-9">

				<div class="logo<?=$logoClass?>">

					<?=CPriority::ShowLogo();?>

				</div>

				<?CPriority::ShowPageType('search_title_component_mega_menu');?>

				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",

					array(

						"COMPONENT_TEMPLATE" => ".default",

						"PATH" => SITE_DIR."include/header/search.title.mega_menu.php",

						"AREA_FILE_SHOW" => "file",

						"AREA_FILE_SUFFIX" => "",

						"AREA_FILE_RECURSIVE" => "Y",

						"EDIT_TEMPLATE" => "include_area.php"

					),

					false, array("HIDE_ICONS" => "Y")

				);?>

				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",

					array(

						"COMPONENT_TEMPLATE" => ".default",

						"PATH" => SITE_DIR."include/header/mega_menu.php",

						"AREA_FILE_SHOW" => "file",

						"AREA_FILE_SUFFIX" => "",

						"AREA_FILE_RECURSIVE" => "Y",

						"EDIT_TEMPLATE" => "include_area.php"

					),

					false, array("HIDE_ICONS" => "Y")

				);?>

			</div>

			<div class="col-md-3">

			<?if($bPhone || $arRegion):?>
							<div class="pull-right">
								<div class="region_phone">
									<?if($arRegion):?>
										<div class="wrap_icon inner-table-block">
											<?CPriority::ShowListRegions();?>
										</div>
									<?endif?>
									<?if($bPhone):?>
										<div class="wrap_icon inner-table-block">
											<div class="phone-block">
												<div><?CPriority::ShowHeaderPhones('mask');?>
													<a class="wa-btn callback-whatsapp" target="_blank" rel="nofollow" href="https://api.whatsapp.com/send?phone=73432874430&text=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B4%D0%B5%D0%BD%D1%8C!%20%D0%9C%D0%B5%D0%BD%D1%8F%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20%D0%BA%D0%B5%D0%BC%D0%BF%D0%B5%D1%80%D0%B0">Написать в WhatsApp</a>
												</div>
												<?if($arTheme["CALLBACK_BUTTON"]["VALUE"] == "Y"):?>
													<div class="callback_wrap">
														<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_callback");?>" data-name="callback"><?=GetMessage("S_CALLBACK")?></span>
													</div>
												<?endif;?>
											</div>
										</div>
									<?endif?>
								</div>
							</div>
						<?endif?>

			</div>

		</div>

	</div>

</div>