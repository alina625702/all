<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme, $arRegion;
$headerType = ($arTheme['HEADER_TYPE'] && !is_array($arTheme['HEADER_TYPE']) ? $arTheme['HEADER_TYPE'] : $arTheme['HEADER_TYPE']['VALUE']);
$bOrder = (isset($arTheme['ORDER_VIEW']['VALUE']) && $arTheme['ORDER_VIEW']['VALUE'] == 'Y' && $arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['ORDER_BASKET_VIEW']['VALUE']=='HEADER' || $arTheme['ORDER_VIEW'] == 'Y' && $arTheme['ORDER_BASKET_VIEW'] == 'HEADER' ? true : false);
$bCabinet = ($arTheme["CABINET"]["VALUE"]=='Y' ? true : false);

if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);

$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
$fixedMenuClass = (is_array($arTheme['TOP_MENU_FIXED']) && $arTheme['TOP_MENU_FIXED']['VALUE'] == 'Y' || $arTheme['TOP_MENU_FIXED'] == 'Y' ? ' canfixed' : '');
$basketViewClass = (is_array($arTheme["ORDER_BASKET_VIEW"]) && $arTheme["ORDER_BASKET_VIEW"]["VALUE"] ? ' '. strtolower($arTheme["ORDER_BASKET_VIEW"]["VALUE"]) : ' '. strtolower($arTheme["ORDER_BASKET_VIEW"]));
?>
<header class="header-v<?=$headerType?><?=$fixedMenuClass?><?=$basketViewClass?> small-icons">
	<div class="logo_and_menu-row green-header">
		<div class="logo-row">
			<div class="header_container clearfix">
				<div class="row">
					<div class="menu-row">
					<div class="right-icons pull-right">
						<?if($bOrder):?>
							<div class="pull-right">
								<div class="wrap_icon wrap_basket">
									<?=CPriority::showBasketLink();?>
								</div>
							</div>
						<?endif;?>
						<?if($bCabinet):?>
							<div class="pull-right">
								<div class="wrap_icon wrap_cabinet">
									<?=CPriority::showCabinetLink(true, false);?>
								</div>
							</div>
						<?endif;?>
						<div class="pull-right show-fixed">
							<div class="wrap_icon">
								<?=CPriority::ShowSearch();?>
							</div>
						</div>
						<?if($bPhone || $arRegion):?>
							<div class="pull-right">
								<div class="region_phone">
									<?if($arRegion):?>
										<div class="wrap_icon inner-table-block">
											<?CPriority::ShowListRegions();?>
										</div>
									<?endif?>
									<?if($bPhone):?>
										<div class="wrap_icon inner-table-block">
											<div class="phone-block">
												<div><?CPriority::ShowHeaderPhones('mask');?>
													<a class="wa-btn callback-whatsapp" target="_blank" rel="nofollow" href="https://web.whatsapp.com/send?phone=79222057700&text=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B4%D0%B5%D0%BD%D1%8C!%20%D0%9C%D0%B5%D0%BD%D1%8F%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20%D0%BA%D0%B5%D0%BC%D0%BF%D0%B5%D1%80%D0%B0">Написать в WhatsApp</a>
												</div>
												<?if($arTheme["CALLBACK_BUTTON"]["VALUE"] == "Y"):?>
													<div class="callback_wrap">
														<span class="callback-block animate-load twosmallfont colored" data-event="jqm" data-param-id="<?=CPriority::getFormID("aspro_priority_callback");?>" data-name="callback"><?=GetMessage("S_CALLBACK")?></span>
													</div>
												<?endif;?>
											</div>
										</div>
									<?endif?>
								</div>
							</div>
						<?endif?>
					</div>
					<div class="logo-block pull-left">
						<?CPriority::ShowBurger();?>
						<div class="logo<?=$logoClass?>">
							<?=CPriority::ShowLogo();?>
						</div>
					</div>

					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"PATH" => SITE_DIR."include/header/menu.php",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => "include_area.php"
						),
						false, array("HIDE_ICONS" => "Y")
					);?>
				</div>
			</div>
		</div><?// class=logo-row?>
	</div>
	<div class="line-row visible-xs"></div>
</header>
