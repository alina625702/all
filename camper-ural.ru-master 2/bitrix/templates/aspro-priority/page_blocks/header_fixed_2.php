<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>



<?

$bOrder = (isset($arTheme['ORDER_VIEW']['VALUE']) && $arTheme['ORDER_VIEW']['VALUE'] == 'Y' && $arTheme['ORDER_VIEW']['DEPENDENT_PARAMS']['ORDER_BASKET_VIEW']['VALUE']=='HEADER' || $arTheme['ORDER_VIEW'] == 'Y' && $arTheme['ORDER_BASKET_VIEW'] == 'HEADER' ? true : false);

$headerType = ($arTheme['HEADER_TYPE'] && !is_array($arTheme['HEADER_TYPE']) ? $arTheme['HEADER_TYPE'] : $arTheme['HEADER_TYPE']['VALUE']);
if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
?>

<div class="maxwidth-theme">

	<div class="logo-row v2 menufixed green-header fixed_row_<?=$headerType;?>">

		<div class="row">

			<div class="logo-block col-md-2">

				<?CPriority::ShowBurger();?>

				<div class="logo<?=($arTheme["COLORED_LOGO"]["VALUE"] !== "Y" ? '' : ' colored')?>">

					<?=CPriority::ShowLogo();?>

				</div>

			</div>

			<div class="col-md-7 menu-block navs js-nav">

				<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",

					array(

						"COMPONENT_TEMPLATE" => ".default",

						"PATH" => SITE_DIR."include/header/menu.php",

						"AREA_FILE_SHOW" => "file",

						"AREA_FILE_SUFFIX" => "",

						"AREA_FILE_RECURSIVE" => "Y",

						"EDIT_TEMPLATE" => "include_area.php"

					),

					false, array("HIDE_ICONS" => "Y")

				);?>

			</div>

			<div class="right_wrap col-md-3 pull-right">
				<div class="row">
					<div class="col-md-10" style=" text-align: right;"><?CPriority::ShowHeaderPhones('mask');?>	<a class="wa-btn callback-whatsapp" target="_blank" rel="nofollow" href="https://api.whatsapp.com/send?phone=79222057700&text=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B4%D0%B5%D0%BD%D1%8C!%20%D0%9C%D0%B5%D0%BD%D1%8F%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20%D0%BA%D0%B5%D0%BC%D0%BF%D0%B5%D1%80%D0%B0">Написать в WhatsApp</a></div>
					<div class="col-md-2">
						<div class="right-icons">

							<?if($bOrder):?>

								<div class="pull-right">

									<div class="wrap_icon wrap_basket">

										<?=CPriority::showBasketLink();?>

									</div>

								</div>

							<?endif?>

							<?if($arTheme["CABINET"]["VALUE"]=='Y'):?>

								<div class="pull-right">

									<div class="wrap_icon wrap_cabinet">

										<?=CPriority::showCabinetLink(true, false, '');?>

									</div>

								</div>

							<?endif;?>

							<div class="pull-right">

								<div class="wrap_icon">

									<?=CPriority::ShowSearch();?>

								</div>

							</div>

						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

</div>
