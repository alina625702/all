<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<div class="mobileheader-v1">
	<?CPriority::ShowBurger();?>
	<div class="logo-block pull-left">
		<div class="logo<?=($arTheme["COLORED_LOGO"]["VALUE"] !== "Y" ? '' : ' colored')?>">
			<?=CPriority::ShowLogo();?>
		</div>
	</div>
	<div class="right-icons pull-right">
		<div class="pull-right">
			<div class="wrap_icon wrap_basket">
				<?=CPriority::showBasketLink('', '', '', '', true);?>
			</div>
		</div>

		<div class="pull-right">
			<div class="wrap_icon">
				<?=CPriority::ShowSearch();?>
			</div>
		</div>

		<div class="pull-right">
			<div class="wrap_icon wrap_phones">

				<?CPriority::ShowHeaderMobilePhones("big");?>
			</div>
		</div>
		<div class="pull-right">
			<div class="wrap_icon wrap_whatsapp">
						<a class="callback-whatsapp" href="https://api.whatsapp.com/send?phone=79222057700&text=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B4%D0%B5%D0%BD%D1%8C!%20%D0%9C%D0%B5%D0%BD%D1%8F%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0%20%D0%BA%D0%B5%D0%BC%D0%BF%D0%B5%D1%80%D0%B0">
							<i class="fa fa-whatsapp" aria-hidden="true"></i>
						</a>
			</div>
		</div>
	</div>
</div>
