var mapsData = [
  {
    'iso' : 'RU-ALT',
    'name' : 'Алтайский край',
    'campers' : [
      {
        'name' : 'Турист плюс',
        'count' : 1
      }
    ],
    'cities' : [
      {
        'name' : 'Барнаул',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-BA',
    'name' : 'Республика Башкортостан',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 3
      },
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 3
      },
    ],
    'cities' : [
      {
        'name' : 'Уфа',
        'count' : 3
      },
      {
        'name' : 'Кумертау',
        'count' : 1
      },
      {
        'name' : 'с. Барсуково',
        'count' : 1
      },
      {
        'name' : 'с. Месягутово',
        'count' : 1
      },
      {
        'name' : 'с. Языково',
        'count' : 1
      },
      {
        'name' : 'Стерлитамак',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-BEL',
    'name' : 'Белгородская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'с. Засосна',
        'count' : 1
      },
      {
        'name' : 'Старый Оскол',
        'count' : 1
      },
      {
        'name' : 'Белгород',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-VGG',
    'name' : 'Волгоградская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Волгоград',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-VLG',
    'name' : 'Вологодская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'д. Борисово',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-VOR',
    'name' : 'Воронежская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Воронеж',
        'count' : 1
      },
      {
        'name' : 'д. Новоподклетное',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-IRK',
    'name' : 'Иркутская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Иркутск',
        'count' : 4
      }
    ]
  },
  {
    'iso' : 'RU-KGD',
    'name' : 'Калининградская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Капля',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Калининград',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-KLU',
    'name' : 'Калужская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Капля',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Жуков',
        'count' : 1
      },
      {
        'name' : 'Калуга',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-KEM',
    'name' : 'Кемеровская область',
    'campers' : [
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
      {
        'name' : 'Турист',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Кемерово',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-KIR',
    'name' : 'Кировская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Киров',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-KOS',
    'name' : 'Костромская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'пос. Антропово',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-KDA',
    'name' : 'Краснодарский край',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 3
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'с. Великовечное',
        'count' : 1
      },
      {
        'name' : 'с. Сукко',
        'count' : 1
      },
      {
        'name' : 'Армавир',
        'count' : 1
      },
      {
        'name' : 'Краснодар',
        'count' : 1
      },
      {
        'name' : 'Новороссийск',
        'count' : 1
      },
      {
        'name' : 'Новороссийск',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-KRY',
    'name' : 'Республика Крым',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 6
      },
      {
        'name' : 'Кочевник',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'с. Столбовое',
        'count' : 1
      },
      {
        'name' : 'Севастополь',
        'count' : 6
      },
      {
        'name' : 'Ялта',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-LEN',
    'name' : 'Ленинградская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 6
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Капля',
        'count' : 1
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'п. Советский',
        'count' : 1
      },
      {
        'name' : 'пос. Подгорное',
        'count' : 6
      },
      {
        'name' : 'Санкт-Петербург',
        'count' : 7
      },
    ]
  },
  {
    'iso' : 'RU-LIP',
    'name' : 'Липецкая область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Липецк',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-MOS',
    'name' : 'Московская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 4
      },
      {
        'name' : 'Кочевник',
        'count' : 8
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 8
      },
      {
        'name' : 'Турист плюс',
        'count' : 7
      },
    ],
    'cities' : [
      {
        'name' : 'Балашиха',
        'count' : 1
      },
      {
        'name' : 'Волоколамск',
        'count' : 2
      },
      {
        'name' : 'г. Ивантеевка',
        'count' : 1
      },
      {
        'name' : 'Лыткарино',
        'count' : 1
      },
      {
        'name' : 'Москва',
        'count' : 13
      },
      {
        'name' : 'п. Тучково',
        'count' : 1
      },
      {
        'name' : 'Раменское',
        'count' : 1
      },
      {
        'name' : 'Химки',
        'count' : 2
      },
      {
        'name' : 'Фрязино',
        'count' : 1
      },
      {
        'name' : 'Лобня',
        'count' : 1
      },
      {
        'name' : 'Чехов',
        'count' : 1
      },
      {
        'name' : 'дер. Язвищи',
        'count' : 1
      },
      {
        'name' : 'с. Озерецкое',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-NIZ',
    'name' : 'Нижегородская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Нижний Новгород',
        'count' : 3
      },
      {
        'name' : 'Саров',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-NGR',
    'name' : 'Новгородская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Великий Новгород',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-OMS',
    'name' : 'Омская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'с. Морозовка',
        'count' : 1
      },
      {
        'name' : 'Омск',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-PNZ',
    'name' : 'Пензенская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Пенза',
        'count' : 1
      },
      {
        'name' : 'Первоуральск',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-PER',
    'name' : 'Пермский край',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Гремячинск',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-PRI',
    'name' : 'Приморский край',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Владивосток',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-PSK',
    'name' : 'Псковская область',
    'campers' : [
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Псков',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-BU',
    'name' : 'Республика Бурятия',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Северобайкальск',
        'count' : 1
      },
      {
        'name' : 'Улан-Удэ',
        'count' : 2
      },
    ]
  },
  {
    'iso' : 'RU-KK',
    'name' : 'Республика Хакасия',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'с. Калинино',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-KO',
    'name' : 'Республика Коми',
    'campers' : [
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Сыктывкар',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-ME',
    'name' : 'Республика Марий Эл',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Йошкар-Ола',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-ROS',
    'name' : 'Ростовская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Турист',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Таганрог',
        'count' : 1
      },
      {
        'name' : 'хутор Таврический',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-RYA',
    'name' : 'Рязанская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Рязань',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-SAM',
    'name' : 'Самарская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Самара',
        'count' : 3
      },
      {
        'name' : 'Тольятти',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-SAK',
    'name' : 'Сахалинская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'с. Первомайск',
        'count' : 1
      },
      {
        'name' : 'Холмск',
        'count' : 1
      },
      {
        'name' : 'Южно-Сахалинск',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-SVE',
    'name' : 'Свердловская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 4
      },
      {
        'name' : 'Кочевник',
        'count' : 9
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Березовский',
        'count' : 1
      },
      {
        'name' : 'Верхняя Пышма',
        'count' : 1
      },
      {
        'name' : 'Двуреченск',
        'count' : 1
      },
      {
        'name' : 'Екатеринбург',
        'count' : 6
      },
      {
        'name' : 'Каменск-Уральский',
        'count' : 1
      },
      {
        'name' : 'Красноуфимск',
        'count' : 1
      },
      {
        'name' : 'Лесной',
        'count' : 1
      },
      {
        'name' : 'пос. Ребристый',
        'count' : 1
      },
      {
        'name' : 'Серов',
        'count' : 1
      },
      {
        'name' : 'Новоуральск',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-STA',
    'name' : 'Ставропольский край',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'пос. Кирова',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-TVE',
    'name' : 'Тверская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Тверь',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-TOM',
    'name' : 'Томская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Северск',
        'count' : 1
      },
      {
        'name' : 'Томск',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-TUL',
    'name' : 'Тульская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Тула',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-TYU',
    'name' : 'Тюменская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 3
      },
    ],
    'cities' : [
      {
        'name' : 'Ишим',
        'count' : 1
      },
      {
        'name' : 'Тюмень',
        'count' : 4
      },
    ]
  },
  {
    'iso' : 'RU-ULY',
    'name' : 'Ульяновская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Ульяновск',
        'count' : 1
      },
      {
        'name' : 'Димитровград',
        'count' : 1
      }
    ]
  },
  {
    'iso' : 'RU-KHM',
    'name' : 'Ханты-Мансийский автономный округ',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 3
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Когалым',
        'count' : 1
      },
      {
        'name' : 'Мегион',
        'count' : 1
      },
      {
        'name' : 'Ханты-Манты',
        'count' : 2
      },
      {
        'name' : 'Нефтеюганск',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-CHE',
    'name' : 'Челябинская область',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 4
      },
      {
        'name' : 'Кочевник',
        'count' : 4
      },
      {
        'name' : 'Турист плюс',
        'count' : 5
      },
      {
        'name' : 'Капля',
        'count' : 2
      },
    ],
    'cities' : [
      {
        'name' : 'Касли',
        'count' : 1
      },
      {
        'name' : 'Магнитогорск',
        'count' : 5
      },
      {
        'name' : 'Озёрск',
        'count' : 3
      },
      {
        'name' : 'п. Петровский',
        'count' : 1
      },
      {
        'name' : 'с. Уйское',
        'count' : 1
      },
      {
        'name' : 'Челябинск',
        'count' : 4
      },
    ]
  },
  {
    'iso' : 'RU-CHU',
    'name' : 'Чукотский автономный округ',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 1
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Анадырь',
        'count' : 2
      }
    ]
  },
  {
    'iso' : 'RU-YAN',
    'name' : 'Ямало-Ненецкий автономный округ',
    'campers' : [
      {
        'name' : 'Турист',
        'count' : 2
      },
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Новый Уренгой',
        'count' : 2
      },
      {
        'name' : 'Салехард',
        'count' : 1
      },
      {
        'name' : 'Надым',
        'count' : 1
      },
    ]
  },
  {
    'iso' : 'RU-YAR',
    'name' : 'Ярославская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 2
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Ярославль',
        'count' : 3
      }
    ]
  },
  {
    'iso' : 'RU-NVS',
    'name' : 'Новосибирская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
      {
        'name' : 'Турист плюс',
        'count' : 1
      },
      {
        'name' : 'Капля',
        'count' : 3
      },
      {
        'name' : 'Турист',
        'count' : 2
      },
      {
        'name' : 'Кочевник плюс',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Новосибирск',
        'count' : 8
      }
    ]
  },
  {
    'iso' : 'RU-ORE',
    'name' : 'Оренбургская область',
    'campers' : [
      {
        'name' : 'Кочевник',
        'count' : 1
      },
    ],
    'cities' : [
      {
        'name' : 'Орск',
        'count' : 1
      }
    ]
  },
]
