<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Кемпер-Урал - производитель жилых прицепов в Екатеринбурге. Наши кемперы покупают во всех регионах страны! Звоните: +7 (343) 287-44-30");
$APPLICATION->SetTitle("Карта поставок");
Bitrix\Main\Page\Asset::getInstance()->addJs('/company/map/data.js');
?>
<style>

#map {
  width: 100%;
  height: 600px;
  padding: 0;
  margin: 0;
  position: relative;
}

table.map {
  width: 100%;
}

table.map td {
  padding: 4px;
  border-bottom: 1px solid #f7f7f7
}
.baloon h3 {
  font-size: 1.3em;
  margin: 10px 0 14px;
}

.hint-title {
  font-size: 1.1em;
  margin: 8px;
}
.map-labels-container {
  position: absolute;
  top: 10px;
  right: 30px;
  z-index: 999;
}

.map-labels {
  border: 1px solid #E7E7E7;
  width: 200px;
  padding: 8px;
  background: #FFF;
  opacity: 0.91;
  font-size: 0.8em;
  margin-bottom: 24px;
}

.map-labels .map-label-color {
  margin: 5px;
  width: 24px;
  height: 8px;
}

.map-labels td:nth-child(1) {
  width: 150px;
}

.campers-map-text {
  font-size: 1.2em;
  margin-bottom: 24px;
}

.campers-map-text strong {
  color: #F5A732;
}

@media (max-width:641px)   {
  .baloon h3 {
    font-size: 1.1em;
    margin: 10px 0 10px;
  }
  .label-cnt {
    display: none;
  }
}

</style>
<div class="maxwidth-theme">
  <div class="campers-map-text">
    Нашей компанией отгружено больше <strong>150</strong> кемперов в <strong>44</strong> региона России
  </div>
</div>

<div id="map">
  <div class="map-labels-container">
    <div class="map-labels label-cnt">
      <table>
        <tr>
          <td>Кочевник</td>
          <td><div class="map-label-count"></div>59</td>
        </tr>
        <tr>
          <td>Кочевник плюс</td>
          <td><div class="map-label-count"></div>16</td>
        </tr>
        <tr>
          <td>Турист</td>
          <td><div class="map-label-count"></div>44</td>
        </tr>
        <tr>
          <td>Турист плюс</td>
          <td><div class="map-label-count"></div>29</td>
        </tr>
        <tr>
          <td>Капля</td>
          <td><div class="map-label-count"></div>5</td>
        </tr>
      </table>
    </div>

    <div class="map-labels label-clr">
      <table>
        <tr>
          <td>0 кемперов</td>
          <td><div class="map-label-color" style="background-color:#E7E7E7"></div></td>
        </tr>
        <tr>
          <td>0-5 кемперов</td>
          <td><div class="map-label-color" style="background-color:#90EE90"></div></td>
        </tr>
        <tr>
          <td>5-10 кемперов</td>
          <td><div class="map-label-color" style="background-color:#00FA9A"></div></td>
        </tr>
        <tr>
          <td>10-20 кемперов</td>
          <td><div class="map-label-color" style="background-color:#2E8B57"></div></td>
        </tr>
        <tr>
          <td>больше 20 кемперов</td>
          <td><div class="map-label-color" style="background-color:#006400"></div></td>
        </tr>
      </table>
    </div>

  </div>

</div>

<script>
ymaps.ready(init);

function init() {

    var commonCampersCount = 0;
    var regionsCount = 0;

    var commonCampersArray = []

    var map = new ymaps.Map('map', {
        center: [58, 80],
        zoom: 1,
        type: null,
        controls: ['zoomControl']
    },{
        restrictMapArea: [[10, 10], [85,-160]]
    });

    map.behaviors.disable('scrollZoom')

    map.controls.get('zoomControl').options.set({size: 'small'});
    var highlightedDistrict;
    var districtBalloon = new ymaps.Balloon(map);
    districtBalloon.options.setParent(map.options);

    // Добавим заливку цветом.
    var pane = new ymaps.pane.StaticPane(map, {
        zIndex: 100,
        css: { width: '100%', height: '100%', backgroundColor: '#f7f7f7' }
    });
    map.panes.append('background', pane);

    var objectManager = new ymaps.ObjectManager();
    // Загрузим регионы.
    ymaps.borders.load('RU', {
        lang: 'ru',
        quality: 2
    }).then(function (result) {
      // Подготовим данные для objectManager.
      result.features.forEach(function (feature) {
          var iso = feature.properties.iso3166;
          feature.id = iso;
          feature.properties.hintContent = '';
          var color = '#E7E7E7';
          feature.properties.hintContent = `<div style="font-size:1.2em; color: #F5A732; padding-bottom: 10px">${feature.properties.name}</div>`

          feature.html = `<div class="baloon"><h3>${feature.properties.name}</h3>`;
          mapsData.forEach((regionData, i) => {
            if(regionData.iso == iso) {
              let campersCount = 0;

              feature.html += '<table class="map"><tbody>'

              regionData.campers.forEach((campersData, j) => {

                feature.html += `<tr><td>${campersData.name}</td><td>${campersData.count}</td></tr>`

                campersCount += campersData.count
                commonCampersCount += campersData.count;

                commonCampersArray = InsertInCommonCampersArray({
                  'name' : campersData.name,
                  'count' : campersData.count
                }, commonCampersArray);

              });

              regionData.cities.forEach((city, j) => {
                feature.properties.hintContent +=  `<strong>${city.name}</strong>: ${city.count}<br>`
              });

              if(campersCount < 5) {
                color = '#90EE90'
              } else if (campersCount >= 5 && campersCount < 10) {
                  color = '#00FA9A'
              } else if (campersCount >= 10 && campersCount < 20) {
                  color = '#2E8B57'
              } else {
                  color = '#006400'
              }

              feature.html += '</tbody></table>'

              if(campersCount > 0) regionsCount++
            }
          });

          feature.html += '</div>'

          district = new ymaps.GeoObjectCollection(null, {
            fillColor: color,
            strokeColor:  '#FFFFFF',
            strokeOpacity: 0.8,
            fillOpacity: 0.6,
            hintCloseTimeout: 0,
            hintOpenTimeout: 0
          });

            district.add(new ymaps.GeoObject(feature));

            // При наведении курсора мыши будем выделять федеральный округ.
            district.events.add('mouseenter', function (event) {
                var districtParent = event.get('target').getParent();
                districtParent.options.set({fillOpacity: 1});
            });

            // При выводе курсора за пределы объекта вернем опции по умолчанию.
            district.events.add('mouseleave', function (event) {
                var districtParent = event.get('target').getParent();
                if (districtParent !== highlightedDistrict) {
                    districtParent.options.set({fillOpacity: 0.6});
                }
            });

            // Подпишемся на событие клика.
            district.events.add('click', function (event) {
                var target = event.get('target');
                var districtParent = target.getParent();
                if (highlightedDistrict) {
                    highlightedDistrict.options.set({fillOpacity: 0.6})
                }
                districtBalloon.open(event.get('coords'), feature.html);
                districtParent.options.set({fillOpacity: 1});
                highlightedDistrict = districtParent;
            });

            map.geoObjects.add(district);
        });

        console.log(commonCampersCount, regionsCount);
        console.log(commonCampersArray);
    })
}

function InsertInCommonCampersArray(data, campersArray) {

  index = 0;
  exist = false

  if (campersArray.length > 0) {
    campersArray.forEach((item) => {
      if(data.name == item.name) {
        campersArray[index].count += data.count;
        exist = true;
      }
      index++;
    });
  }

  if (!exist) {
    campersArray.push({
      'name': data.name,
      'count': data.count
    });
  }
  return campersArray;
}
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
