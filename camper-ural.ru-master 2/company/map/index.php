<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Компания Кемпер-Урал - производитель жилых прицепов в Екатеринбурге. Наши кемперы покупают во всех регионах страны! Звоните: +7 (343) 287-44-30");
$APPLICATION->SetTitle("Карта поставок");
Bitrix\Main\Page\Asset::getInstance()->addJs('/company/map/data.js');
?>
<style>

  .overlay{
      display: none;
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 999;
      background: rgba(255,255,255,0.8) url("Spinner-1s-200px.gif") center no-repeat;
  }
  /* Turn off scrollbar when body element has the loading class */
  #map.loading{
      overflow: hidden;
  }
  /* Make spinner image visible when body element has the loading class */
  #map.loading .overlay{
      display: block;
  }

  #map {
    width: 100%;
    height: 700px;
    padding: 0;
    margin: 0;
    position: relative;
  }

  #map .map-label {
    position: absolute;
    right: 12px;
    bottom: 10px;
    background: #FFFFFF;
    width: 300px;
    border-radius: 12px;
    -webkit-box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
    -moz-box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
    box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
    padding: 8px 12px;
    z-index: 99;
  }

  #map .map-label .description {
    margin-bottom: 10px;
  }
  #map .map-label .description,
  #map .map-label .labels .item  {
    font-size: 0.82em;
    line-height: 1.3em;
  }

  #map .map-label .labels .item {
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin: 4px 0px;
  }

  #map .map-label .labels .item .color,
  #map .map-label .labels .item .count {
    width: 20%;
  }
  #map .map-label .labels .item .name{
    width: 60%;
  }

  #map .map-label .labels .item .color .color-box {
    width: 40px;
    height: 18px;
  }

  .balloon-content {
    width: 210px;
  }

  .balloon-content h3 {
    margin: 8px 0px;
    font-size: 1.1em;
  }

  .balloon-content .color-box {
    width: 40px;
    height: 18px;
  }

  .balloon-content td {
    padding: 4px 12px 4px 0px;
  }

  @media only screen and (max-width: 768px) {
      #map {
        height: 500px
      }
  }
</style>

<div class="maxwidth-theme">
  <div class="description">Нашей компанией отгружено <strong id="sum-count"></strong> кемпера в <strong id="sum-cities"></strong> городов России</div>
  <div id="map" class="loading">
    <div class="map-label">

      <div class="labels">

        <div class="item" id="kaplya">
          <div class="color"><div class="color-box" style="background: #820000"></div></div>
          <div class="name">Капля</div>
          <div class="count"></div>
        </div>

        <div class="item" id="turist">
          <div class="color"><div class="color-box" style="background: #B9005B"></div></div>
          <div class="name">Турист</div>
          <div class="count"></div>
        </div>

        <div class="item" id="turist-plus">
          <div class="color"><div class="color-box" style="background: #FF7C7C"></div></div>
          <div class="name">Турист Плюс</div>
          <div class="count"></div>
        </div>

        <div class="item" id="kochevnik">
          <div class="color"><div class="color-box" style="background: #229494"></div></div>
          <div class="name">Кочевник</div>
          <div class="count"></div>
        </div>

        <div class="item" id="kochevnik-plus">
          <div class="color"><div class="color-box" style="background: #554994"></div></div>
          <div class="name">Кочевник Плюс</div>
          <div class="count"></div>
        </div>
      </div>
    </div>

    <div class="overlay"></div>

  </div>
</div>

<script>
  var myMap;

  $(document).ready(function() {
    console.log('load');
    ymaps.ready(init);
  })

  function init () {
      myMap = new ymaps.Map('map', {
          center: [58.701603, 70.542625],
          zoom: 4,
          controls: ['zoomControl', 'typeSelector', 'fullscreenControl']
      }, {
          searchControlProvider: 'yandex#search'
      });


        let campers_data = [];

        $.get( "/ajax/map_data.php", function( data ) {

          var sum_cities = 0;
          var sum_campers = 0;

          var sum_kaplya = 0;
          var sum_turist = 0;
          var sum_turist_plus = 0;
          var sum_kochevnik = 0;
          var sum_kochevnik_plus = 0;

          JSON.parse(data).forEach((item, i) => {
            if(i > 0) {

              city_id = checkCityExistInArray(campers_data, item[2])
              if(city_id >= 0) {
                campers_data[city_id].count += 1;
                camper_id = checkCamperExistInCity(campers_data[city_id].campers, item[3]);
                if(camper_id >= 0) {
                  campers_data[city_id].campers[camper_id].count += 1
                } else {
                  campers_data[city_id].campers.push({
                    'name' : item[3],
                    'count' : 1
                  })
                }
              } else {

                campers_data.push({
                  'city' : item[2],
                  'count' : 1,
                  'coords' : item[4],
                  'campers' : [
                    {
                      'name' : item[3],
                      'count' : 1
                    }
                  ]
                })
              }
            }
          });

          campers_data.forEach((item, i) => {
            if(item.coords != '') {
              sum_campers += item.count;
              item.coords = item.coords.replace('%2C', ',')
              var lat = item.coords.split(',')[1]
              var lon = item.coords.split(',')[0]

              pieData = [];

              var content = `<div class="balloon-content"><h3>${item.city}</h3><table>`;
              item.campers.forEach((camper, j) => {

                var color = '#FFFFFF';

                switch (camper.name) {
                  case 'Капля': color = '#820000'; sum_kaplya += camper.count; break;
                  case 'Турист': color = '#B9005B';  sum_turist += camper.count; break;
                  case 'Турист Плюс': color = '#FF7C7C';  sum_turist_plus += camper.count; break;
                  case 'Кочевник': color = '#229494';  sum_kochevnik += camper.count; break;
                  case 'Кочевник Плюс': color = '#554994';  sum_kochevnik_plus += camper.count; break;
                }
                pieData.push({weight: camper.count, color: color})

                content += `
                  <tr>
                    <td><div class="color-box" style="background: ${color}"></div></td>
                    <td class=name"">${camper.name}</td>
                    <td class="count">${camper.count}</td>
                  </tr>`;
              });

              content += '</table></div>'

              let iconPieChartRadiusVal = 15;
              let iconPieChartCoreRadiusVal = 10;

              if(item.count >= 2 && item.count < 3) {
                iconPieChartRadiusVal = 18;
                iconPieChartCoreRadiusVal = 12;
              }
              else if(item.count >= 3 && item.count < 5) {
                iconPieChartRadiusVal = 21;
                iconPieChartCoreRadiusVal = 14;
              }
              if(item.count >= 5) {
                iconPieChartRadiusVal = 24;
                iconPieChartCoreRadiusVal = 16;
              }


              myPieChart = new ymaps.Placemark([
                  lat, lon
              ], {
                  data: pieData,
                  balloonContent: content,
              }, {
                  iconLayout: 'default#pieChart',
                  iconPieChartRadius: iconPieChartRadiusVal,
                  iconPieChartCoreRadius: iconPieChartCoreRadiusVal,
                  iconPieChartCoreFillStyle: '#ffffff',
                  iconPieChartStrokeStyle: '#ffffff',
                  iconPieChartStrokeWidth: 1,
                  iconPieChartCaptionMaxWidth: 200
              });

              myMap.geoObjects.add(myPieChart)
            }
          });

          $('#sum-cities').text(campers_data.length);
          $('#sum-count').text(sum_campers);

          $('#kaplya .count').text(sum_kaplya);
          $('#turist .count').text(sum_turist);
          $('#turist-plus .count').text(sum_turist_plus);
          $('#kochevnik .count').text(sum_kochevnik);
          $('#kochevnik-plus .count').text(sum_kochevnik_plus);

          $('#map').removeClass("loading");
        });
  }


  function checkCityExistInArray(array, city) {
    for(i = 0; i < array.length; i++) {
      if(array[i].city == city) {
        return i;
      }
    }
    return -1;
  }

  function checkCamperExistInCity(array, name) {
    for(i = 0; i < array.length; i++) {
      if(array[i].name == name) {
        return i;
      }
    }
    return -1;
  }

</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
