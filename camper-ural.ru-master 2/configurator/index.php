<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Конфигуратор. Компания Кемпер-Урал предлагает купить жилой прицеп в Екатеринбурге . Телефон +7 (343) 287-44-30");
$APPLICATION->SetTitle("Подберём легковой прицеп под ваши потребности");?>
<style>
  .scroll-to-top,
  .quiz-btn {
    display: none !important;
  }
</style>
<div id="quiz-modal" class="maxwidth-theme inline-quiz">

	<!-- Первый шаг -->
	<div id="first-step" class="tabcontent">
    <div class="quiz-nav">
      <div class="num"> Шаг 1 из 4. <strong>Выберите комплектацию</strong></div>
       <!-- <div class="nav">
        <button onclick="openTab('second-step')" class="btn btn-sm btn-default"><span>Далее</span></button>
      </div> -->
    </div>

		<div class="complectations-list">
			<?php
				$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");
				$arFilter = Array("IBLOCK_ID"=>32, "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
				while($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					?>
						<div class="complectation-item active" data-id="<?=$arFields['ID']?>" data-name="<?=$arFields['NAME']?>">
							<div class="image" onclick="openTab('second-step')"><img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>" alt="" /></div>
							<div class="name" onclick="openTab('second-step')"><?=$arFields['NAME']?></div>
						</div>
					<?
				}
			?>

			<div class="complectation-item active" data-id="0">
				<div class="image"><img src="/images/constructor.jpeg" alt="" /></div>
				<div class="name">Своя комплектация</div>
			</div>
		</div>

	</div>

	<!-- Второй шаг -->
	<div id="second-step" class="tabcontent">
    <div class="quiz-nav">
      <div class="num"> Шаг 2 из 4. <strong>Выберите кемпер</strong></div>
      <div class="nav">
        <button onclick="openTab('first-step')" class="btn btn-sm btn-default"><span>Назад</span></button>
        <button onclick="openTab('third-step')" class="btn btn-sm btn-default"><span>Далее</span></button>
      </div>
    </div>
			<div class="campers-list row"></div>
			<div class="row camper-info">
				<div class="col-md-4 col-sm-12">
					<div class="img"></div>
				</div>
				<div class="col-md-8 col-sm-12">
					<h3 id="camper-name"></h3>
					<div class="description"></div>
					<div class="base-complectation"></div>
          <div class="price"></div>
				</div>
			</div>
	</div>

	<div id="third-step" class="tabcontent">
    <div class="quiz-nav">
      <div class="num"> Шаг 3 из 4. <strong>Выберите комплектующие</strong></div>
      <div class="nav">
        <button onclick="openTab('second-step')" class="btn btn-sm btn-default"><span>Назад</span></button>
        <button onclick="openTab('fourth-step')" class="btn btn-sm btn-default"><span>Далее</span></button>
      </div>
    </div>
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="camper-name"></div>
				<div class="camper-image"></div>
				<div class="base-complectation"></div>
			</div>

			<div class="col-md-8 col-sm-12">
				<div class="complectation-name"></div>
				<div class = "complectation-items">
					<div class="list"></div>
					<div class="common-price"></div>
				</div>

			</div>
		</div>

	</div>

	<div id="fourth-step" class="tabcontent">

    <div class="quiz-nav">
      <div class="num"> Шаг 4 из 4. <strong>Оставьте заявку на покупку кемпера</strong></div>
      <div class="nav">
        <button onclick="openTab('third-step')" class="btn btn-sm btn-default"><span>Назад</span></button>
      </div>
    </div>

		<div class="quiz-form">
			 <?$APPLICATION->IncludeComponent(
				"aspro:form.priority",
				"inline-quiz",
				Array(
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"CLOSE_BUTTON_CLASS" => "btn btn-primary",
					"CLOSE_BUTTON_NAME" => "Закрыть",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"DISPLAY_CLOSE_BUTTON" => "Y",
					"IBLOCK_ID" => "44",
					"IBLOCK_TYPE" => "aspro_priority_form",
					"LICENCE_TEXT" => "btn btn-primary",
					"SEND_BUTTON_CLASS" => "btn btn-primary",
					"SEND_BUTTON_NAME" => "Отправить",
					"SHOW_LICENCE" => "Y",
					"SUCCESS_MESSAGE" => "Спасибо! Ваше сообщение отправлено!",
					"DISPLAY_CLOSE_BUTTON" => "Y"
				)
			);?>
		</div>


	</div>

</div>

<script>

  // DEFAULT
	let current_camper_id = 182
	let current_camper_name = ''
	let current_camper_base_complectation = ''

	let current_complectation_id = 187
	let current_complectation_name = 'Рыбалка / Охота'

	let current_complectation_list = ''
	let current_complectation_price = ''

	function clickOnCheckbox(code) {
		current_complectation_list = ''
		let item = $('input[name="complect_' + code +'"]');
		let productPrice = parseInt( $('.complectation-items .common-price' ).text().toString().replace( ' ', '' ) );

		console.log(productPrice);
		console.log(item);
		let itemPrice = parseInt( item.attr('data-price').toString().replace( ' ', '' ) );

		if(item.prop('checked')) {
			productPrice += itemPrice;
		} else {
			productPrice -= itemPrice;
		}

		$('.complectation-items .common-price' ).text(prettify(productPrice) + ' ₽');

		$('input[type=checkbox]').each(function () {
       if (this.checked) {
				 	current_complectation_list += $(this).data('name') + ', ';
       }
		 });

		 current_complectation_price = productPrice
	}

	openTab('first-step')

	showComplectationInfo(current_complectation_id, current_complectation_name)

	// Считаем цену
	$('.camper-item').on('click', function() {
		showCamperInfo($(this).data('id'));
	});

	$('.complectation-item').on('click', function() {
		showComplectationInfo($(this).data('id'), $(this).data('name'));
	});

	function showCamperInfo(camper_id) {

		$('.camper-item').removeClass('active');
		$('.camper-item[data-id="' + camper_id + '"]').addClass('active');

		$('.camper-info h3').empty();
		$('.camper-info .description').empty();
		$('.camper-info .price').empty();


		$.ajax( {
			url: "/ajax/get_camper_info.php",
			type: "post",
			data: {id: camper_id},
			success: function( response ){

				response = JSON.parse( response );
				$('.camper-info h3').html(response.NAME);
				$('.camper-info .description').html(response.DETAIL_TEXT);
				$('.camper-info .price').html('Цена базовой комплектации: ' + response.PRICE +  " ₽");
				$('.camper-info .img').html('<img src="' + response.DETAIL_PICTURE + '">');
				// $('.camper-info .base-complectation').html(response.BASE_COMPLECT);
				current_camper_id = camper_id;
				current_camper_name = response.NAME
				current_camper_base_complectation = response.BASE_COMPLECT
				current_camper_image = response.DETAIL_PICTURE
				current_camper_price = response.PRICE
				current_complectation_price = response.PRICE

				//console.log(response)
			},
			error: function( jqXHR, textStatus, errorThrown ){
				console.log( textStatus, errorThrown );
			}
		});
	}

	function showComplectationInfo(complectation_id, complectation_name) {

		$('.complectation-item').removeClass('active');
		$('.complectation-item[data-id="' + complectation_id + '"]').addClass('active');
		current_complectation_id = complectation_id;
		current_complectation_name = complectation_name;
	}

	function openTab(tabName) {

		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}

		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}

		document.getElementById(tabName).style.display = "block";

		if(tabName == "second-step") {
			console.log(current_complectation_id)
			$.ajax( {
				url: "/ajax/get_camper_info.php",
				type: "post",
				data: {complectation_id: current_complectation_id},
				success: function( response ){
					response = JSON.parse( response );

					$('.campers-list').empty();

					response.forEach((item, i) => {

						let activeClass = '';
						if(i == 0) {
							activeClass = 'active'
							showCamperInfo(item.ID)
						}

						$('.campers-list').append(`
						<div onclick="showCamperInfo(${item.ID})" class="camper-item ${activeClass}" data-id="${item.ID}" data-name="${item.NAME}">
							<div class="name">${item.NAME}</div>
						</div>`);

						console.log(item);
					})
				},
				error: function( jqXHR, textStatus, errorThrown ){
					console.log( textStatus, errorThrown );
				}
			});


		}

		if(tabName == "third-step") {
			$('#third-step .camper-name').html(current_camper_name);
			$('#third-step .camper-image').html(`<img src="${current_camper_image}">`);
			$('#third-step .base-complectation').html(current_camper_base_complectation.deentitize());
			$('#third-step .complectation-name').html(current_complectation_name);


			$('.complectation-items .list').empty();

			camper_price = current_camper_price;

			$.ajax( {
				url: "/ajax/get_camper_complectation.php",
				type: "post",
				data: {id: current_camper_id, complectation_id: current_complectation_id},
				success: function( response ){

					response = JSON.parse( response );

					response.LINK_COMPLECT.forEach((item, i) => {

						let href = `<a href="${item.DETAIL_PICTURE}" data-lightbox="complect" data-title="${item.NAME}">${item.NAME}</a>`
						if(!item.DETAIL_PICTURE) {
							href = item.NAME
						}

						let checked = '';

						if(item.CHECKED) {
							checked = 'checked';
							camper_price = parseInt(camper_price) + parseInt(item.PRICE.toString().replace( ' ', '' ) );
						}

						$('.complectation-items .list').append(`
						<div class="item clearfix wti">
							<div class="checkbox">
						    <input ${checked} onclick="clickOnCheckbox('${item.CODE}')" class="complect-item link" type="checkbox" id="complect" name="complect_${item.CODE}" data-blocked="" data-linked="" data-id="${item.ID}" data-name="${item.NAME}" data-price="${item.PRICE}">
						  </div>
							<div class="info">
								<div class="title"> ${href} </div>
								<div class="previewtext">${item.PREVIEW_TEXT}</div>
							</div>
							<div class="price">
									${prettify(item.PRICE)} ₽
							</div>
						</div>`)
					});

					$('.complectation-items .common-price').html(prettify(camper_price) + ' ₽');

					$('input[type=checkbox]').each(function () {
			       if (this.checked) {
							 	current_complectation_list += $(this).data('name') + ', ';
			       }
					 });

					 current_complectation_price = camper_price;
				},
				error: function( jqXHR, textStatus, errorThrown ){
					console.log( textStatus, errorThrown );
				}
			});
		}

		if(tabName == "fourth-step") {

			$('.quiz-form input[name="PRODUCT"]').val(current_camper_name);
			$('.quiz-form input[name="COMPLECT"]').val(current_complectation_list);
			$('.quiz-form input[name="NEED_PRODUCT"]').val(current_complectation_name);
			$('.quiz-form input[name="COMPLECT_PRICE"]').val(current_complectation_price);
		}

		//evt.currentTarget.className += " active";
	}

	String.prototype.deentitize = function() {
    var ret = this.replace(/&gt;/g, '>');
		ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
		ret = ret.replace('<details>', '')
		ret = ret.replace('<summary>', '')
		ret = ret.replace('</summary>', '')
		ret = ret.replace('</details>', '')

    return ret;
};

function prettify( num ){
	var n = num.toString();
	return n.replace( /(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ' );
}

lightbox.option({
  'resizeDuration': 200,
  'wrapAround': true
})

</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
