<?php
return array (
  'redirect_www' => 'Y',
  'redirect_slash' => NULL,
  'redirect_index' => 'Y',
  'redirect_multislash' => 'Y',
  'use_redirect_urls' => NULL,
  'redirect_from_uppercase' => 'Y',
  'redirect_from_404' => NULL,
);