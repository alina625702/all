#!/usr/local/bin/php
<?php
  // Определяем переменные для работы скрипта с библиотеками Битрикса
  set_time_limit(0);
  ini_set('mbstring.func_overload', '2');
  ini_set('memory_limit','1024M');
  ini_set('mbstring.internal_encoding', 'UTF-8');
  $_SERVER["DOCUMENT_ROOT"] = '/home/c32323/ugol-ok.na4u.ru/www';
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
  define('LANG', 's1');

  define('BX_UTF', true);
  define('NO_KEEP_STATISTIC', true);
  define('NOT_CHECK_PERMISSIONS', true);
  define('BX_BUFFER_USED', true);

  require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

  // Засекаем время выполнения скрипта
  $startExecTime = getmicrotime();

  $arSortProduct = array('SORT' => 'ASC', 'ID' => 'DESC');
  $arFilterProduct = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 21, 'ID' => $basketItem['PRODUCT_ID']);
  $arSelectProduct = array('ID', 'NAME', 'PROPERTY_ONEC_ID', 'NAME', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL');
  $resProduct = CIBlockElement::getList($arSortProduct, $arFilterProduct, false, array(), $arSelectProduct);
  $resProduct->SetUrlTemplates("/catalog/#SECTION_CODE#/#ELEMENT_CODE#.php");
  while ($productRow = $resProduct->fetch()) {
    print_r($productRow);
    //print_r(CFile::GetPath($productRow["DETAIL_PICTURE"]));
  }

  echo '\nScript works ' . (getmicrotime() - $startExecTime) . ' sec\n';

  require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/epilog_after.php');

?>
