#!/usr/local/bin/php
<?php
  // Определяем переменные для работы скрипта с библиотеками Битрикса
  set_time_limit(0);
  ini_set('mbstring.func_overload', '2');
  ini_set('memory_limit','1024M');
  ini_set('mbstring.internal_encoding', 'UTF-8');
  $_SERVER["DOCUMENT_ROOT"] = '/home/c32323/ugol-ok.na4u.ru/www';
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
  define('LANG', 's1');

  define('BX_UTF', true);
  define('NO_KEEP_STATISTIC', true);
  define('NOT_CHECK_PERMISSIONS', true);
  define('BX_BUFFER_USED', true);

  require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

  // Засекаем время выполнения скрипта
  $startExecTime = getmicrotime();

  $ordersXML = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><orders></orders>');

// Получаем список заказов по заданному фильтру
$arFilter = Array( "STATUS_ID" => "F", "!PAY_SYSTEM_ID" => "9");
  $db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
  while ($ar_sales = $db_sales->Fetch())
  {
    $orderItem = $ordersXML->addChild('order');
    $orderItem->addAttribute('id', $ar_sales['ID']);
    $orderItem->addAttribute('datetime', $ar_sales['DATE_INSERT']);

    // Получаем перечень товаров в заказе
    $dbBasketItems = CSaleBasket::GetList(
      array( "NAME" => "ASC", "ID" => "ASC" ),
      array( "ORDER_ID" => $ar_sales['ID'] ),
      false,
      false,
      array("ID", "CALLBACK_FUNC", "STORE_ID", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT")
    );

    $order = \Bitrix\Sale\Order::load($ar_sales['ID']);
    $shipmentCollection = $order->getShipmentCollection();
    $basket = $order->getBasket();
    $storeList = array();
    foreach ($shipmentCollection as $shipment){
      $shipmentItemCollection = $shipment->getShipmentItemCollection();
      foreach ($shipmentItemCollection as $shipmentItem) {
      $productId = $shipmentItem->getProductId();
      $shipmentItemStoreCollection = $shipmentItem->getShipmentItemStoreCollection();

       foreach ($shipmentItemStoreCollection as $shipmentItemStore) {
           $storeId = $shipmentItemStore->getField('STORE_ID');
           $basketId = $shipmentItemStore->getField('BASKET_ID');
           //print_r([$storeId, $basketId, $productId]);
           array_push($storeList, [$storeId, $basketId, $productId]);

       }
      }
    }

    while ($basketItem = $dbBasketItems->Fetch()) {
      // Получаем ONECID товара
      // print_r($basketItem);
      $storeId = 2; // По дефолту - Карла Маркса
      $onecStoreID = 'e4528890-a5e6-11e2-9c77-406186c60c7f';
      foreach ($storeList as $store) {
        if($store[1] == $basketItem['ID'] && $store[2] == $basketItem['PRODUCT_ID']) {
          $storeId = $store[0];

          $resStorage = CCatalogStore::GetList(array(), array('ACTIVE' => 'Y', 'ID' => $store), false, false, array('ID', 'TITLE', 'UF_ONEC_ID'));
          while ($storageRow = $resStorage->fetch()) {
            print_r($storageRow);
            $onecStoreID = $storageRow['UF_ONEC_ID'];
          }
          print_r(array($onecStoreID));
        }
      }

      $arSortProduct = array('SORT' => 'ASC', 'ID' => 'DESC');
      $arFilterProduct = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 21, 'ID' => $basketItem['PRODUCT_ID']);
      $arSelectProduct = array('ID', 'NAME', 'PROPERTY_ONEC_ID');
      $resProduct = CIBlockElement::getList($arSortProduct, $arFilterProduct, false, array(), $arSelectProduct);
      while ($productRow = $resProduct->fetch()) {
        // Собираем XML файл
        $orderItemProduct = $orderItem->addChild('product');
        $orderItemProduct->addAttribute('id', $basketItem['PRODUCT_ID']);
        $orderItemProduct->addAttribute('onec_id', $productRow['PROPERTY_ONEC_ID_VALUE']);
        $orderItemProduct->addAttribute('quantity', $basketItem['QUANTITY']);
        $orderItemProduct->addAttribute('name', $productRow['NAME']);
        $orderItemProduct->addAttribute('onec_store_id', $onecStoreID);
        //$orderItemProduct->addAttribute('pay_system_id', $ar_sales['PAY_SYSTEM_ID']);
        //$orderItemProduct->addAttribute('status_id', $ar_sales['STATUS_ID']);
      }
    }
  }

  // Сохраняем файл как XML
  $dom = new DOMDocument('1.0');
  $dom->preserveWhiteSpace = false;
  $dom->formatOutput = true;
  $dom->loadXML($ordersXML->asXML());
  $dom->save($_SERVER['DOCUMENT_ROOT'] . '/upload/1c/orders/orders.xml');
  print_r($_SERVER['DOCUMENT_ROOT'] . '/upload/1с/orders/orders.xml');

  echo '\nScript works ' . (getmicrotime() - $startExecTime) . ' sec\n';

  require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/epilog_after.php');

?>
