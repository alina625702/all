<?
  include_once __DIR__ . '/../vendor/autoload.php';

  $token = "a47a0ceab665604b1b8d994e3d5f7cf2fc3c363c";
  $dadata = new \Dadata\DadataClient($token, null);
  $city = $dadata->iplocate($_SERVER['REMOTE_ADDR']);

  if(isset($city['data'])) {
    $city['data']['domain'] = getDomainByRegionName($city['data']['region_with_type']);
    echo json_encode($city['data']);
    //echo json_encode($city['data']['region_with_type']);
  }

  function getDomainByRegionName($regionName) {
    $csv = array_map('str_getcsv', file('regions.csv'));

    foreach ($csv as $item) {
        if($item[2] == $regionName) {
          return $item[15];
        }
    }

    return '12';
  }
