#!/usr/bin/php7.1
<?php
  // Определяем переменные для работы скрипта с библиотеками Битрикса
  set_time_limit(0);
  ini_set('mbstring.func_overload', '2');
  ini_set('memory_limit','1024M');
  //ini_set(‘mbstring.internal_encoding', 'UTF-8');
  $_SERVER["DOCUMENT_ROOT"] = '/home/c32323/ugol-ok.na4u.ru/www';
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
  define('LANG', 's1');

  define('BX_UTF', true);
  define('NO_KEEP_STATISTIC', true);
  define('NOT_CHECK_PERMISSIONS', true);
  define('BX_BUFFER_USED', true);

  $PRICE_TYPE_ID = 1; // Розничная цена
  $CURRENCY = 'RUB'; // Обозначение валюты
  $index = 1;

  require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

  // Засекаем время выполнения скрипта
  $startExecTime = getmicrotime();

  // READ file (/www/upload/1с/goods/tovars.xml)
  $filename = $DOCUMENT_ROOT . '/upload/1c/goods/tovars.xml';
  // READ XML
  $productsData = simplexml_load_file($filename) or die("Failed to load");
  foreach ($productsData as $product) {
    $onecID = $product->attributes()->TovarID;
    $productPrice = strval($product->attributes()->TovarPrice);
    //print_r([$onecID, $productPrice]);

    // Находим торговое предложение по 1С ID
    $arSortProduct = array('SORT' => 'ASC', 'ID' => 'DESC');
    $arFilterProduct = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 21, 'PROPERTY_ONEC_ID' => $onecID);
    $arSelectProduct = array('ID', 'NAME', 'PROPERTY_ONEC_ID');
    $resProduct = CIBlockElement::getList($arSortProduct, $arFilterProduct, false, array(), $arSelectProduct);
    while ($productRow = $resProduct->fetch()) { // Если мы находим продукт по ID
      $productId = $productRow['ID'];
      $productTotalAmount = 0;
      foreach ($product->Ostatok as $ostatok) {
        $productsAmount = strval($ostatok->attributes()->SkladKol);
        $onecStorageId = strval($ostatok->attributes()->SkladID);
        $productTotalAmount += $productsAmount;
        // Ищем склад по ID
        $resStorage = CCatalogStore::GetList(array(), array('ACTIVE' => 'Y','UF_ONEC_ID'=> $onecStorageId), false, false, array("ID","TITLE"));

        while ($storageRow = $resStorage->fetch()) {
          $storageId = $storageRow['ID'];
          $arRemains = Array(
              "PRODUCT_ID" => $productId,
              "STORE_ID" => $storageId,
              "AMOUNT" => $productsAmount,
          );
          $remainsId = CCatalogStoreProduct::UpdateFromForm($arRemains);
        }

        // Обновляем цены для продукта
        $priceFields = Array(
          "PRODUCT_ID" => $productId,
          "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
          "PRICE" => $productPrice,
          "CURRENCY" => $CURRENCY,
        );

        CPrice::DeleteByProduct($productId);

        $resPrices = CPrice::GetList( array(), array( "PRODUCT_ID" => $productId, "CATALOG_GROUP_ID" => $PRICE_TYPE_ID ));
        //print_r($resPrices->Fetch());
        if ($arrPrices = $resPrices->Fetch()){
          CPrice::Update($arrPrices["ID"], $priceFields);
        }
        else  {
          CPrice::Add($priceFields);
        }
      }

      CCatalogProduct::Update(
       $productId,
       array(
         'QUANTITY' => $productTotalAmount
       )
      );
      print_r(array($index, $productId, $productTotalAmount));
    }
    $index++;
  }

  echo '\nScript works ' . (getmicrotime() - $startExecTime) . ' sec\n';

  require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/epilog_after.php');

?>
