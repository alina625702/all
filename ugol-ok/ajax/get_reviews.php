#!/usr/local/bin/php
<?php
  // Определяем переменные для работы скрипта с библиотеками Битрикса
  set_time_limit(0);
  ini_set('mbstring.func_overload', '2');
  ini_set('memory_limit','1024M');
  //ini_set(‘mbstring.internal_encoding', 'UTF-8');
  $_SERVER["DOCUMENT_ROOT"] = '/home/c32323/ugol-ok.na4u.ru/www';
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
  define('LANG', 's1');

  define('BX_UTF', true);
  define('NO_KEEP_STATISTIC', true);
  define('NOT_CHECK_PERMISSIONS', true);
  define('BX_BUFFER_USED', true);

  require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

  // Засекаем время выполнения скрипта
  $startExecTime = getmicrotime();

  // 1. Скачиваем данные из https://yandex.ru/maps/org/ugolok/1115642150/reviews/?ll=60.625700%2C56.831832&z=14
  $url = 'https://yandex.ru/maps/org/ugolok/1173929842/reviews/?ll=60.551994%2C56.833575&tab=reviews&z=14.27';
  $dom = new DomDocument;
  $dom->loadHTMLFile($url);

  $html = $dom->getElementsByTagName('body')->item(0)->textContent;

  $startReviewsText = '{"reviews":';
  $finishReviewsText = ',"params';
  $reviewsStrArray = get_sub_string($html, $startReviewsText, $finishReviewsText);

  $reviewsStrArray = json_decode($reviewsStrArray, true);

  // 3. Получаем все отзывы из Битрикса
  $arSortReviews = array('SORT' => 'ASC', 'ID' => 'DESC');
  $arFilterReviews = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 26);
  $arSelectReviews = array('ID', 'NAME', 'CODE');
  $resReviews = CIBlockElement::getList($arSortReviews, $arFilterReviews, false, array(), $arSelectReviews);
  $arrCurrentReviews = array();

  while ($reviewRow = $resReviews->fetch()) {
    array_push($arrCurrentReviews, $reviewRow);
  }

  // 4. Проверяем есть ли такой код в Битриксе?

  for ($i=0; $i<count($reviewsStrArray); $i++){ // Перечисляем отзывы с сайта
    $siteReviewCode = 'review_' . $reviewsStrArray[$i]['updatedTime'];
    $isExist = false;
    foreach ( $arrCurrentReviews as $btrxReview ) {
      if ($btrxReview['CODE'] == $siteReviewCode){
        $isExist = true;
      }
    }
    if(!$isExist) {   // 5. Если нет, то добавляем отзыв в битрикс
      $formatedDate = date("d.m.Y", strtotime($reviewsStrArray[$i]['updatedTime']));

      $PROP = array();
      $PROP[275] = $reviewsStrArray[$i]['rating'];

      print_r($reviewsStrArray[$i]['rating']);
      $el = new CIBlockElement;
      $id = $el->Add(
        array(
          'IBLOCK_ID' => 26,
          'DATE_ACTIVE_FROM' => $formatedDate,
          'NAME' => $reviewsStrArray[$i]['author']['name'],
          'CODE' => $siteReviewCode,
          'PREVIEW_TEXT' => $reviewsStrArray[$i]['text'],
          'PROPERTY_VALUES' => $PROP,
        )
      );
    }
  }

  function get_sub_string($str, $starting_word, $ending_word){
    $arr = explode($starting_word, $str);
    //print_r($arr[1]);
    if (isset($arr[1])){
        $arr = explode($ending_word, $arr[1]);
        return $arr[0];
    }
    return '';
  }

  echo '\nScript works ' . (getmicrotime() - $startExecTime) . ' sec\n';

  require($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/epilog_after.php');

?>
