<?php

use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\CompaniesCollection;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Collections\LinksCollection;
use AmoCRM\Collections\NullTagsCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Models\TagModel;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Filters\LeadsFilter;
use AmoCRM\Models\CompanyModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Models\NoteType\CommonNote;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\NullCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use League\OAuth2\Client\Token\AccessTokenInterface;
use AmoCRM\Models\CustomFieldsValues\SelectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\SelectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\SelectCustomFieldValueModel;

class amoCRM
{
  public function add_lead($lead_data) {
    include_once __DIR__ . '/bootstrap.php';

    $name = $lead_data['NAME'];
    $phone = $lead_data['PHONE'];
    $email = $lead_data['EMAIL'];
    $leadName = $lead_data['LEAD_NAME'];
    $product = $lead_data['PRODUCT_NAME'];
    $responsible = 6266290;
    $description = $lead_data['QUESTION'];
    $orderlist = '';//$lead_data['ORDER_LIST'];
    $price = $lead_data['PRICE'];

    $utmMedium = $_COOKIE['utm_medium'];
    $utmTerm = $_COOKIE['utm_term'];
    $utmSource = $_COOKIE['utm_source'];
    $utmContent = $_COOKIE['utm_content'];
    $utmCampaign = $_COOKIE['utm_campaign'];

    //print_r($description)
    $accessToken = getToken();

    $apiClient->setAccessToken($accessToken)
    ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
    ->onAccessTokenRefresh(
      function (AccessTokenInterface $accessToken, string $baseDomain) {
        saveToken([
            'accessToken' => $accessToken->getToken(),
            'refreshToken' => $accessToken->getRefreshToken(),
            'expires' => $accessToken->getExpires(),
            'baseDomain' => $baseDomain,
        ]);
      }
    );

    $leadsService = $apiClient->leads();

    try {
      $contacts = $apiClient->contacts()->get((new ContactsFilter())->setQuery($phone));
      $contact = $contacts[0];
    } catch(AmoCRMApiException $e) {
      $contact = new ContactModel();
      $contact->setName($name);

      $CustomFieldsValues = new CustomFieldsValuesCollection();

      $emailField = (new MultitextCustomFieldValuesModel())->setFieldCode('EMAIL');
      $emailField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($email)));

      $phoneField = (new MultitextCustomFieldValuesModel())->setFieldCode('PHONE');
      $phoneField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($phone)));

      $CustomFieldsValues->add($emailField);
      $CustomFieldsValues->add($phoneField);

      $contact->setCustomFieldsValues($CustomFieldsValues);
      $contact->setResponsibleUserId($responsible);

      try {
        $contactModel = $apiClient->contacts()->addOne($contact);
      } catch (AmoCRMApiException $e) {
        printError($e);
        die;
      }
    }

    // Создаем сделку
    $lead = new LeadModel();
    $lead->setName($leadName)->setContacts((new ContactsCollection())->add(($contact)));
    $CustomFieldsValues = new CustomFieldsValuesCollection();
    // if ($product != '') {
    //   $productField = (new TextCustomFieldValuesModel())->setFieldId(1299899);
    //   $productField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($product)));
    //   $CustomFieldsValues->add($productField);
    // }

    // Передача города

    $domain = '';

    switch ($lead_data['CITY']) {
      case 'Москва': $domain = 'msk.ugol-ok.com';break;
      case 'Тюмень': $domain = 'tumen.ugol-ok.com';break;
      case 'Екатеринбург': $domain = 'ugol-ok.com';break;
      default: $domain = $_SERVER['HTTP_HOST']; break;
    }

    // Передача домена
    $domainField = (new TextCustomFieldValuesModel())->setFieldId(704969);
    $domainField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($domain)));
    $CustomFieldsValues->add($domainField);

    $token = "a47a0ceab665604b1b8d994e3d5f7cf2fc3c363c";
    $dadata = new \Dadata\DadataClient($token, null);
    $city = $dadata->iplocate($_SERVER['REMOTE_ADDR']);


    if(isset($city['value'])) {
      $cityField = (new TextCustomFieldValuesModel())->setFieldId(704971);
      $cityField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($city['value'])));
      $CustomFieldsValues->add($cityField);
    }

    // UTM метки
    $utmSourceField = (new TextCustomFieldValuesModel())->setFieldId(657757);
    $utmSourceField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmSource)));
    $CustomFieldsValues->add($utmSourceField);

    $utmMediumField = (new TextCustomFieldValuesModel())->setFieldId(657759);
    $utmMediumField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmMedium)));
    $CustomFieldsValues->add($utmMediumField);

    $utmTermField = (new TextCustomFieldValuesModel())->setFieldId(657763);
    $utmTermField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmTerm)));
    $CustomFieldsValues->add($utmTermField);

    $utmContentField = (new TextCustomFieldValuesModel())->setFieldId(657765);
    $utmContentField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmContent)));
    $CustomFieldsValues->add($utmContentField);

    $utmCampaignField = (new TextCustomFieldValuesModel())->setFieldId(657761);
    $utmCampaignField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmCampaign)));
    $CustomFieldsValues->add($utmCampaignField);


    $lead->setCustomFieldsValues($CustomFieldsValues);

    $lead->setResponsibleUserId($responsible);
    $lead->setStatusId(34638574);
    $leadsCollection = new LeadsCollection();

    if($price != '') {
        $lead->setPrice($price);
    }
    $lead->setTags((new TagsCollection())
        ->add(
            (new TagModel())
                ->setName('Заявка с сайта')
    ));
    $leadsCollection->add($lead);

    try {
      $leadsCollection = $leadsService->add($leadsCollection);
      $lead_id = $leadsCollection[0]->id;
      if($companyName != '') {
        //Создадим компанию
        $company = new CompanyModel();
        $company->setName($companyName);

        $companiesCollection = new CompaniesCollection();
        $companiesCollection->add($company);
        try {
            $apiClient->companies()->add($companiesCollection);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }

        $links = new LinksCollection();
        $links->add($contact);
        try {
            $apiClient->companies()->link($company, $links);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }
      }
      if ($orderlist != '') {
        $description .= ' Состав заказа: ' . implode(';', $orderlist);
      }

      if ($description != '') {
        $notesCollection = new NotesCollection();
        $serviceMessageNote = new CommonNote();
        $serviceMessageNote->setEntityId($lead_id)->setText($description);

        $notesCollection->add($serviceMessageNote);

        try {
            $leadNotesService = $apiClient->notes(EntityTypesInterface::LEADS);
            $notesCollection = $leadNotesService->add($notesCollection);
        } catch (AmoCRMApiException $e) {
            printError($e); die;
        }
      }
      return $lead_id;
    } catch (AmoCRMApiException $e) {
        printError($e);
        die;
    }

  }
}
