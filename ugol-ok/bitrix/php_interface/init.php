<?
include_once $_SERVER['DOCUMENT_ROOT'] . 'amo/create_lead.php';

AddEventHandler('form', 'onAfterResultAdd', Array("Amo","onAfterResultAddHandler"));
AddEventHandler("sale", "OnOrderAdd", Array("Amo", "OnOrderAddHandler"));



class Amo
{
  function OnOrderAddHandler($ID, $val) {

    $lead_data['LEAD_NAME'] = 'Оформлен заказ на сайте';
    $lead_data['ORDER_URL'] = 'https://ugol-ok.com/bitrix/admin/sale_order_view.php?amp%3Bfilter=Y&%3Bset_filter=Y&lang=ru&ID=' . $val['ID'];
    $lead_data['PRICE'] = $val['PRICE'];
    $lead_data['NAME'] = $val['PROFILE_NAME'];

    $lead_data['EMAIL'] = $val['USER_EMAIL'];
    $lead_data['QUESTION'] = 'Ссылка на заказ: ' . $lead_data['ORDER_URL'];

    error_log(print_r($lead_data, true));

    if(count($val['ORDER_PROP']) > 3) {
      $lead_data['PHONE'] = $val['ORDER_PROP'][3];
      amoCRM::add_lead($lead_data);
    }

  }

  function onAfterResultAddHandler($WEB_FORM_ID, $RESULT_ID) {
    error_log("result . " . $WEB_FORM_ID , 0);
    $lead_name = 'Заявка с сайта.';
    switch ($WEB_FORM_ID) {
      case '1': $lead_name .= ' Товар под заказ'; break;
      case '2': $lead_name .= ' Задать вопрос'; break;
      case '3': $lead_name .= ' Обратная связь'; break;
      case '4': $lead_name .= ' Резюме'; break;
      case '5': $lead_name .= ' Заказать услугу'; break;
      case '6': $lead_name .= ' Заказать звонок'; break;
      case '7': $lead_name .= ' Заказать проект'; break;
      case '8': $lead_name .= ' Нашли дешевле'; break;
      case '9': $lead_name .= ' Намекни другу на подарок'; break;
      case '10': $lead_name .= ' Оставить отзыв'; break;
      case '12': $lead_name .= ' Получить прайс-лист'; break;
      case '13': $lead_name .= ' Получить консультацию'; break;

    }
    // ID OF WEB FORMS: 3; 2; 6
    $arAnswer = CFormResult::GetDataByID($RESULT_ID, array("CLIENT_NAME", "PHONE", "EMAIL", "QUESTION", "CITY"), $arResult,$arAnswer2);

    if(isset($arAnswer['CLIENT_NAME'])) $name = $arAnswer['CLIENT_NAME'][0]['USER_TEXT']; else $name = '';
    if(isset($arAnswer['PHONE'])) $phone = $arAnswer['PHONE'][0]['USER_TEXT']; else $phone = '';
    if(isset($arAnswer['EMAIL'])) $email = $arAnswer['EMAIL'][0]['USER_TEXT']; else $email = '';
    if(isset($arAnswer['QUESTION'])) $question = $arAnswer['QUESTION'][0]['USER_TEXT']; else $question = '';
    if(isset($arAnswer['PRODUCT_NAME'])) $product_name = $arAnswer['PRODUCT_NAME'][0]['USER_TEXT']; else $product_name = '';
    if(isset($arAnswer['CITY'])) $city = $arAnswer['CITY'][0]['ANSWER_TEXT']; else $city = '';


    // if ($city != '') {
    //   $question .= 'Филиал ' . $city;
    // }

    $lead_data['NAME'] = $name;
    $lead_data['PHONE'] = $phone;
    $lead_data['EMAIL'] = $email;
    $lead_data['QUESTION'] = $question;
    $lead_data['PRODUCT_NAME'] = $product_name;
    $lead_data['CITY'] = $city;
    $lead_data['LEAD_NAME'] = $lead_name;

    amoCRM::add_lead($lead_data);
    
  }
}

//print_r($_SERVER['DOCUMENT_ROOT'] . 'amo/create_lead.php');
//
// AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");
//
// function OnAfterIBlockElementAddHandler(&$arFields) {
//
//   if(!$arFields["RESULT"])
//       return false;
//
//   if(in_array($arFields['IBLOCK_ID'], [17, 20, 21, 23, 40, 43])){
//
//     $leadName = 'Заявка с сайта';
//     switch ($arFields['IBLOCK_ID']) {
//       case 17: $leadName = 'Заявка с сайта. Заказать комплектацию'; break;
//       case 20: $leadName = 'Заявка с сайта. Подобрать кемпер'; break;
//       case 21: $leadName = 'Заявка с сайта. Обратный звонок'; break;
//       case 23: $leadName = 'Заявка с сайта. Нужна консультация'; break;
//       case 40: $leadName = 'Заявка с сайта. Купить в кредит'; break;
//       case 43: $leadName = 'Заявка с сайта. Подбор кемпера'; break;
//     }
//     $products = [
//       'Капля',
//       'Кочевник',
//       'Кочевник Плюс',
//       'Турист',
//       'Турист Плюс'
//     ];
//
//     $complectations = [
//       'Выезды с друзьями',
//       'Семья с детьми',
//       'Туризм парой',
//       'Спортсмены / велосипеды',
//       'Рыбалка / Охота'
//     ];
//
//     $amoCrm = new AmoCRM();
//     $lead_data = array();
//
//     $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["NAME"];
//     $lead_data['PHONE'] =  $arFields["PROPERTY_VALUES"]["PHONE"];
//     $lead_data['EMAIL'] = $arFields["PROPERTY_VALUES"]["EMAIL"];
//     $lead_data['COMPANY'] = $arFields["PROPERTY_VALUES"]["COMPANY"];
//
//     $lead_data['TEXT'] = $arFields["PROPERTY_VALUES"]["MESSAGE"]["VALUE"]["TEXT"];
//     $lead_data['LEAD_NAME'] = $leadName;
//
//     $lead_data['CITY'] = $arFields["PROPERTY_VALUES"]["CITY"];
//
//     if(isset($arFields["PROPERTY_VALUES"]["FIO"])) {
//       $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["FIO"];
//     }
//
//
//     $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["CAMPER"];
//     $lead_data['TYPE'] = $arFields["PROPERTY_VALUES"]["NEED_PRODUCT"];
//     $lead_data['COMPLECT'] = $arFields["PROPERTY_VALUES"]["COMPLECT"];
//     $lead_data['PRICE'] = $arFields["PROPERTY_VALUES"]["COMPLECT_PRICE"];
//
//     if(in_array($arFields["PROPERTY_VALUES"]["PRODUCT"], $products)) {
//       $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["PRODUCT"];
//     } else {
//       $lead_data['TEXT'] .= ' Кемпер: ' .  $arFields["PROPERTY_VALUES"]["PRODUCT"];
//     }
//
//     amoCRM::add_lead($lead_data);
//   }
// }
