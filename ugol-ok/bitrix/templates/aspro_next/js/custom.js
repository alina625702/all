/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/

$(document).ready(function() {

  $('a[href^="mailto:"]').on('copy', function(){
      ym(34916280,'reachGoal','copy_email')
  });

  // Подмена URL для WhatsApp
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    $('a').each(function(index, element) {

      if($(this).attr('href')) {
        cur_href = $(this).attr('href')
        let newHref = cur_href.replace('https://wa.me/','whatsapp://');
        newHref = newHref.replace('https://api.whatsapp.com/','whatsapp://');

        $(element).attr('href', newHref)
      }

    });
  } else {
    $('a').each(function(index, element) {

      if($(this).attr('href')) {
        cur_href = $(this).attr('href')
        let newHref = cur_href.replace('https://wa.me/','https://web.whatsapp.com/send?phone=');
        newHref = newHref.replace('https://api.whatsapp.com/','https://web.whatsapp.com/send?phone=');

        $(element).attr('href', newHref)
      }
    })
  }

  $(".whatsapp a").on("click",function(){
      ym(34916280,'reachGoal','whatsapp_click')
  });

  $(".whats a").on("click",function(){
      ym(34916280,'reachGoal','whatsapp_click')
  });

  $.get("/ajax/get_ip_data.php", function(data, status){
    var ipData = JSON.parse(data)
    console.log(ipData);
    if(typeof(getCookie('region_dadata_domain') ) != "undefined") {
      //console.log('Повторный вход')
    } else {
      setCookie('region_dadata_domain', ipData.domain);
      var newDomain = ipData.domain + '.ugol-ok.com';
      if(ipData.domain == 'ekb') {
        newDomain = 'ugol-ok.com';
      }

      if (newDomain != document.location.host) {
          var newHref = document.location.href.replace(document.location.host, newDomain);
          //console.log(newHref)
          window.location.replace(newHref);
      }

    }
 });


 $('body').on('click', '.button_block span.to-cart', function() {
    const eventParams = {
			"products" : [{"id": $('.button_block span.to-cart').attr('data-item')}],
			"total_price" : $('.price_value').text()
		};

		VK.Retargeting.ProductEvent(331362, "add_to_cart", eventParams);
});


});

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    // при необходимости добавьте другие значения по умолчанию
    ...options
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}
