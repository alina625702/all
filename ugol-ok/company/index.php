<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Специализированный магазин отделочных материалов #REGION_NAME_DECLINE_PP#. Продажа профилей, уголков, противоскольжения, плинтуса оптом и в розницу. О компании");
$APPLICATION->SetTitle("О компании");
?>
<p>
	 Компания "Уголок" – это сеть фирменных магазинов отделочных материалов. Ассортимент наших магазинов напольные плинтусы, пороги, декоративные уголки, профиль для кафеля, деформационные швы, противоскользящие профили.
</p>
<ul>
	<li>Продажи строительных и отделочных материалов, товаров для дома и дачи, предметов интерьера.</li>
	<li>Более&nbsp;20-ти лет&nbsp;работы на рынке товаров для ремонта и обустройства.</li>
	<li>Ассортимент более 1 000&nbsp;наименований товаров.</li>
	<li>Доставка&nbsp;по всей России.</li>
	<li>Простой и&nbsp;удобный поиск товара, как по каталогу сайта, так и при помощи фильтра.</li>
	<li>Удобное и&nbsp;простое оформление заказа: по телефону, через корзину сайта, e-mail, чат.</li>
	<li>Любая&nbsp;удобная форма оплаты: наличные, эквайринг, банковские карты, электронные деньги.</li>
	<li>Безналичный расчет для юридических лиц с&nbsp;НДС.</li>
	<li>Заботливый и&nbsp;качественный сервис.</li>
	<li>Доставка в&nbsp;удобное для вас время&nbsp;или самовывоз со склада.</li>
	<li>Выгодные&nbsp;цены.</li>
	<li>Гарантия&nbsp;качества товаров&nbsp;от производителя.</li>
	<li>Гарантия&nbsp;конфиденциальности&nbsp;персональных данных.</li>
	<li>Консультации только&nbsp;профессиональных&nbsp;менеджеров.</li>
	<li>Самые&nbsp;известные&nbsp;производители отделочных материалов.</li>
	<li>Постоянное пополнение ассортимента&nbsp;новинками.</li>
</ul>
<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/mainpage/comp_instagramm.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false
);?>
<iframe width="100%;" style="height: 560px;"  src="https://www.youtube.com/embed/kt7-f7dQak8?&theme=dark&autohide=2&showinfo=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "2"
	)
);?><br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
