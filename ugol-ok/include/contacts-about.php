<h1 class="contacts__title">Адреса магазинов</h1>
	<div class="contacts__content">
		<div id="adresspg">
<p>620026, Екатеринбург, Карла Маркса, 50</p>
<p>телефон <a href="tel:+73432165124">+7 (343) 216-51-24</a>, <a href="tel:+73432625124">+7 (343) 262-51-24</a></p>
<p><a class="" href="mailto:ugolok50@gmail.com">ugolok50@gmail.com</a></p>
<p>График работы:<br /> пн-пт: 09.00 - 19.00<br /> сб-вс: 11.00 - 17.00</p>
<div class="yandex-map" id="fmasp"></div>
<p>620109, Екатеринбург, Заводская, 32/2 (вход с ключевской)</p>
<p>телефон <a href="tel:+73432167640">+7 (343) 216-76-40 (41)</a>, <a href="tel:+73433739971">+7 (343) 373-99-71</a></p>
<p><a class="" href="mailto:2167640@mail.ru">2167640@mail.ru</a></p>
<p>График работы:<br />пн-пт: 09.00 - 19.00<br />сб-вс: 11.00 - 17.00</p>
<div class="yandex-map" id="smasp"></div>
<p>Тюмень, Тульская, 7</p>
<p>телефон <a href="tel:+73452568871">+7 (3452) 56-88-71</a></p>
<p><a class="" href="mailto:ugoloktyum@gmail.com">ugoloktyum@gmail.com</a></p>
<p>График работы:</p>
<p>пн-пт: 09.00 - 19.00</p>
<p>сб-вс: 11.00 - 17.00</p>
<div class="yandex-map" id="tmasp"></div>
</div>
		
	</div>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script>

$(document).ready(function() {
	
	ymaps.ready(f_init);
	function f_init () {
		var myfMap = new ymaps.Map('fmasp', {
		center: [56.831580134318195,60.62577150000003],
		zoom: 16
		});
		var myPlacemark = new ymaps.Placemark(
		[56.831580134318195,60.62577150000003] , {
		hintContent: 'Уголок'
		}, {
		iconImageHref: 'images/mapic.png',
		iconImageSize: [60, 83],
		iconImageOffset: [-26, -105]
		});     
		myfMap.geoObjects.add(myPlacemark);
	}

	ymaps.ready(s_init);
	function s_init () {
		var myMap = new ymaps.Map('smasp', {
		center: [56.8336,60.5518],
		zoom: 16
		});
		var myPlacemark = new ymaps.Placemark(
		[56.8336,60.5518], {
		hintContent: 'Уголок'
		}, {
		iconImageHref: 'images/mapic.png',
		iconImageSize: [60, 83],
		iconImageOffset: [-26, -80] 
		});     
		myMap.geoObjects.add(myPlacemark);
	}
	
	ymaps.ready(t_init);
	function t_init () {
		var myMap = new ymaps.Map('tmasp', {
		center: [57.131325, 65.577721],
		zoom: 16
		});
		var myPlacemark = new ymaps.Placemark(
		[57.131325, 65.577721], {
		hintContent: 'Уголок'
		}, {
		iconImageHref: 'images/mapic.png',
		iconImageSize: [60, 83],
		iconImageOffset: [-26, -80] 
		});     
		myMap.geoObjects.add(myPlacemark);
	}
});
	
</script>
