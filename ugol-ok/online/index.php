<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Специализированный магазин отделочных материалов #REGION_NAME_DECLINE_PP#. Продажа профилей, уголков, противоскольжения, плинтуса оптом и в розницу. Связаться с нами");
$APPLICATION->SetTitle("Связаться с нами");
?>
<div class="callback-wrapper">
	<div class="callback-block-page">
		<h3>
			 Консультация сотрудников магазина
		</h3>
		<div class="description">
			 <strong>Покажем товар через видеозвонок</strong> <br>Выберите филиал и напишите в удобный для вас мессенджер
		</div>
		<div class="row social-table">
			<div class="col-md-4 col-xs-4">
				<div class="callback-item">
					<div class="city">
	 					<strong>Москва</strong>
					</div>
					<div class="icons whatsapp">
	 					<a target=_blank  href="https://wa.me/send?phone=79266839866"><img src="/images/logos_whatsapp.png"></a>
						<a class="telegram" target=_blank  href="https://t.me/ugolok_msk"><img src="/images/logos_telegram.png"></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4">
				<div class="callback-item">
					<div class="city">
	 				<strong>Екатеринбург</strong>
					</div>
					<div class="icons whatsapp">
	 					<a target=_blank  href="https://wa.me/send?phone=79655259541"><img src="/images/logos_whatsapp.png"></a>
						<a  class="telegram" target=_blank  href="https://t.me/ugolok_ekb"><img src="/images/logos_telegram.png"></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-4">
				<div class="callback-item">
					<div class="city">
	 					<strong>Тюмень</strong>
					</div>
					<div class="icons whatsapp">
		 				<a target=_blank  href="https://wa.me/send?phone=79097395395"><img src="/images/logos_whatsapp.png"></a>
						<a class="telegram" target=_blank  href="https://t.me/ugolok_tyum"><img src="/images/logos_telegram.png"></a>
					</div>
				</div>
			</div>
		</div>


		<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"callback",
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "?send=ok",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "13",
		"COMPONENT_TEMPLATE" => "callback",
		"VARIABLE_ALIASES" => array (
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
	</div>
</div>

<script>
	$('.ik_select_list_inner').append(`<li class="ik_select_option" title="undefined"><span class="ik_select_option_label" title="undefined">Выбрать</span></li>`);
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>