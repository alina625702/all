<?
include_once $_SERVER['DOCUMENT_ROOT'] . 'amo/create_lead.php';

AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");

function OnAfterIBlockElementAddHandler(&$arFields) {

  if(!$arFields["RESULT"])
      return false;

  if(in_array($arFields['IBLOCK_ID'], [8, 9, 10, 11, 12, 13, 14, 15, 16])){

    $formName = '';
    switch ($arFields['IBLOCK_ID']) {
      case 8: $formName = 'Пример формы'; break;
      case 9: $formName = 'Заказать проект'; break;
      case 10: $formName = 'Заказать продукт'; break;
      case 11: $formName = 'Заказать услугу'; break;
      case 12: $formName = 'Отправить резюме'; break;
      case 13: $formName = 'Оставить заявку на расчет'; break;
      case 14: $formName = 'Обратный звонок'; break;
      case 15: $formName = 'Оформление заказа'; break;
      case 16: $formName = 'Написать директору'; break;
      default: $formName = 'Неизвестная форма';  break;
    }
    $amoCrm = new AmoCRM();
    $lead_data = array();

    if (isset($_POST['FIO'])) $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["FIO"];
    else $lead_data['NAME'] =  $arFields["PROPERTY_VALUES"]["NAME"];

    if (isset($_POST['PRODUCT_PRICE'])) $lead_data['PRICE'] =  $arFields["PROPERTY_VALUES"]["PRODUCT_PRICE"];
    else $lead_data['PRICE'] =  $arFields["PROPERTY_VALUES"]["TOTAL_SUMM"];

    $lead_data['PRICE'] = str_replace(' ', '', $lead_data['PRICE']);


    if (isset($_POST['NEED_PRODUCT'])) $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["NEED_PRODUCT"];
    else if (isset($_POST['PRODUCT_NAME'])) $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["PRODUCT_NAME"];
    else $lead_data['PRODUCT'] = $arFields["PROPERTY_VALUES"]["PRODUCT"];

    $lead_data['PHONE'] =  $arFields["PROPERTY_VALUES"]["PHONE"];
    $lead_data['EMAIL'] = $arFields["PROPERTY_VALUES"]["EMAIL"];
    $lead_data['COMPANY'] = $arFields["PROPERTY_VALUES"]["COMPANY"];
    $lead_data['TEXT'] = $arFields["PROPERTY_VALUES"]["MESSAGE"]["VALUE"]["TEXT"];
    $lead_data['LEAD_NAME'] = 'Заявка с сайта. ' . $formName;
    $lead_data['ORDER_LIST'] = $arFields["PROPERTY_VALUES"]["ORDER_LIST"];

    $lead_data['UTM_MEDIUM']    = isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : '';
    $lead_data['UTM_TERM']      = isset($_COOKIE['utm_term']) ? $_COOKIE['utm_term'] : '';
    $lead_data['UTM_SOURCE']    = isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : '';
    $lead_data['UTM_CONTENT']   = isset($_COOKIE['utm_content']) ? $_COOKIE['utm_content'] : '';
    $lead_data['UTM_CAMPAIGN']  = isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '';

    amoCRM::add_lead($lead_data);
  }
}
