<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
$arComponentParams = $arParams;

if ($request['GLOBAL_FILTER']) {
    $GLOBALS[$arComponentParams['FILTER_NAME']] = unserialize(urldecode($request['GLOBAL_FILTER']));
}

$arComponentParams = array_merge($arComponentParams, [
    "COMPONENT_TEMPLATE" => $arParams['TYPE_TEMPLATE'],
    "DARK_HOVER" => true,
    "IMAGES" => "PICTURES",
    "IMAGE_POSITION" => "BG",
    "IS_AJAX" => $request['AJAX_REQUEST'] && !$request['TABS_REQUEST'],
    "ITEM_HOVER_SHADOW" => false,
    "MAXWIDTH_WRAP" => false,
    "MOBILE_SCROLLED" => true,
    "NEWS_COUNT" => $arParams['PAGE_ELEMENT_COUNT'],
    "PROPERTY_CODE" => $arParams['PROPERTY_CODE'],
    "ELEMENTS_ROW" => $arParams['ELEMENT_IN_ROW'],
    "ROUNDED" => true,
    "ROUNDED_IMAGE" => false,
    "ROW_VIEW" => false,
]);
unset($arComponentParams['TITLE'], $arComponentParams['ADVERTISING_ON_MAIN_PAGE']);

$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    $arComponentParams['TYPE_TEMPLATE'], 
    $arComponentParams,
    false,
    [
        'HIDE_ICONS' => "Y"
    ]
);
?>