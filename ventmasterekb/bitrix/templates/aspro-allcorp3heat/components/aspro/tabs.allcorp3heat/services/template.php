<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$sBufferKey = $arParams['COMPONENT_TEMPLATE']."_".$arParams['COMPONENT_CALL_INDEX'];
$bAjax = CAllcorp3Heat::checkAjaxRequest() && $request['AJAX_REQUEST'] === "Y";

if ($arResult["TABS"]):?>
	<?
		if ($bAjax) {
			$APPLICATION->RestartBuffer();
			$bAjax = CAllcorp3Heat::checkAjaxRequest() && $request['AJAX_REQUEST'] === "Y";
		}
	?>
	<?
	$arTab=array();
	$arParams['SET_TITLE'] = 'N';
	$arTmp = reset($arResult["TABS"]);
	$arParams["FILTER_HIT_PROP"] = $arTmp["CODE"];
	$arParamsTmp = urlencode(serialize($arParams));
	?>
	<div class="element-list <?=$blockClasses?> <?=$templateName?>-template" data-block_key="<?= $sBufferKey; ?>">
		<?$navHtml = '';?>
		<?if (count($arResult["TABS"]) > 1):?>
			<?ob_start();?>
				<div class="tab-nav-wrapper">
					<div class="test tab-nav font_14" data-template="<?=$arParams['TYPE_TEMPLATE']?>" >
						<?$i = 0;?>
						<?foreach ($arResult["TABS"] as $key => $arItem):?>
							<div class="tab-nav__item  bg-opacity-theme-hover bg-theme-active bg-theme-hover-active color-theme-hover-no-active <?=(!$i ? ' active clicked' : '');?>" data-code="<?=$key;?>"><?=$arItem['TITLE']?></div>
							<?++$i;?>
						<?endforeach;?>
					</div>
				</div>
			<?$navHtml = ob_get_clean();?>
		<?endif;?>
		
		<?=\Aspro\Functions\CAsproAllcorp3Heat::showTitleBlock([
			'PATH' => 'elements-list',
			'PARAMS' => $arParams,
			'CENTER_BLOCK' => $navHtml
		]);?>

		<?if($arParams['NARROW']):?>
			<div class="maxwidth-theme">
		<?elseif($arParams['ITEMS_OFFSET']):?>
			<div class="maxwidth-theme maxwidth-theme--no-maxwidth">
		<?endif;?>
				<div class="line-block line-block--align-flex-stretch line-block--block">
					<div class="wrapper_tabs line-block__item flex-1">
						<span class='js-request-data request-data' data-value=''></span>
						<div class="js-tabs-ajax">
							<?$i = 0;?>
							<?foreach ($arResult["TABS"] as $key => $arItem):?>
								<div class="tab-content-block <?=(!$i ? 'active ' : 'loading-state');?>" data-code="<?=$key?>" data-filter="<?=($arItem["FILTER"] ? urlencode(serialize($arItem["FILTER"])) : '');?>">
									<?
										if ($bAjax) 
											$APPLICATION->RestartBuffer();

										if (!$i) {
											if (!$bAjax && $arItem["FILTER"]) {
												$GLOBALS[$arParams["FILTER_NAME"]] = $arItem["FILTER"];
											}

											include __DIR__."/page_blocks/".$arParams['TYPE_TEMPLATE'].".php";
										}

										if ($bAjax) 
											CAllcorp3Heat::checkRestartBuffer(true, $sBufferKey);
									?>
								</div>
								<?++$i;?>
							<?endforeach;?>
						</div>
					</div>
				</div>
		<?if($arParams['NARROW'] || $arParams['ITEMS_OFFSET']):?>
		</div>
		<?endif;?>
	</div>
	<script>try{window.tabsInitOnReady('<?= $sBufferKey; ?>');}catch(e){console.log(e);}</script>
<?endif;?>