<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

global $arTheme, $APPLICATION;
$APPLICATION->AddViewContent('right_block_class', 'catalog_page ');

$bShowLeftBlock = ($arTheme['LEFT_BLOCK_CATALOG_ROOT']['VALUE'] === 'Y' && !defined('ERROR_404'));
$bMobileSectionsCompact = $arTheme['MOBILE_LIST_SECTIONS_COMPACT_IN_SECTIONS']['VALUE'] === 'Y';
$APPLICATION->SetPageProperty('MENU', 'N');
?>
<?if($arTheme['SHOW_CATALOG_POPULAR']['VALUE'] == "Y"):?>
<?ob_start()?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"popular", 
	array(
		"COMPONENT_TEMPLATE" => "popular",
		"PATH" => SITE_DIR."include/footer/catalog_popular.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php",
		"ITEM_1200" => ($arTheme['SHOW_CATALOG_POPULAR']['DEPENDENT_PARAMS']['SHOW_ADV']['VALUE'] == "Y") ? 3 : 4,
		"TITLE" => $arTheme['SHOW_CATALOG_POPULAR']['DEPENDENT_PARAMS']['CATALOG_POPULAR_TITLE']['VALUE'], 
		"SHOW_BANNER" => $arTheme['SHOW_CATALOG_POPULAR']['DEPENDENT_PARAMS']['SHOW_ADV']['VALUE'],
		"IS_ROUND" => "N",
	),
	false
);?>
<?$htmlBlockPopular = ob_get_contents();?>
<?ob_end_clean();?>
	<?if(strpos($htmlBlockPopular, "catalog-items") !== false) :?>
		<?$APPLICATION->AddViewContent('above_footer', $htmlBlockPopular);?>
	<?endif?>	
<?endif?>

<div class="main-wrapper flexbox flexbox--direction-row">
	<div class="section-content-wrapper <?=($bShowLeftBlock ? 'with-leftblock' : '');?> flex-1">
		<?// intro text?>
		<?ob_start();?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "inc",
				"EDIT_TEMPLATE" => ""
			)
		);?>
		<?$html = ob_get_contents();?>
		<?ob_end_clean();?>
		<?if(trim($html)):?>
			<div class="text_before_items">
				<?= $html; ?>
			</div>
		<?endif;?>
		<?unset($html);?>
		<?
		// get section items count and subsections
		$arParams['CHECK_DATES'] = 'Y';
		$arItemFilter = CAllcorp3Heat::GetCurrentSectionElementFilter($arResult["VARIABLES"], $arParams, false);
		$arSubSectionFilter = CAllcorp3Heat::GetCurrentSectionSubSectionFilter($arResult["VARIABLES"], $arParams, false);
		$itemsCnt = CAllcorp3HeatCache::CIBlockElement_GetList(array("CACHE" => array("TAG" => CAllcorp3HeatCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), $arItemFilter, array());
		$arSubSections = CAllcorp3HeatCache::CIBlockSection_GetList(array("CACHE" => array("TAG" => CAllcorp3HeatCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]), "MULTI" => "Y")), $arSubSectionFilter, false, array("ID"));
		?>
		<?if(!$itemsCnt && !$arSubSections):?>
			<div class="alert alert-warning"><?=GetMessage("SECTION_EMPTY")?></div>
		<?else:?>
			<?CAllcorp3Heat::CheckComponentTemplatePageBlocksParams($arParams, __DIR__);?>

			<?$sViewElementTemplate = ($arParams["SECTIONS_TYPE_VIEW"] == "FROM_MODULE" ? $arTheme["SECTIONS_TYPE_VIEW_CATALOG"]["VALUE"] : $arParams["SECTIONS_TYPE_VIEW"]);?>
			<?@include_once('page_blocks/'.$sViewElementTemplate.'.php');?>

			<?if(!$arSubSections):?>
				<?// section elements?>
				<?if(strlen($arParams["FILTER_NAME"])):?>
					<?$GLOBALS[$arParams["FILTER_NAME"]] = array_merge((array)$GLOBALS[$arParams["FILTER_NAME"]], $arItemFilter);?>
				<?else:?>
					<?$arParams["FILTER_NAME"] = "arrFilter";?>
					<?$GLOBALS[$arParams["FILTER_NAME"]] = $arItemFilter;?>
				<?endif;?>
				
				<?$sViewElementTemplate = ($arParams["SECTION_ELEMENTS_TYPE_VIEW"] == "FROM_MODULE" ? $arTheme["ELEMENTS_CATALOG_PAGE"]["VALUE"] : $arParams["SECTION_ELEMENTS_TYPE_VIEW"]);?>
				<?@include_once('page_blocks/'.$sViewElementTemplate.'.php');?>
			<?endif;?>
		<?endif;?>
		<?// outro text?>
		<?ob_start();?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "bottom",
				"EDIT_TEMPLATE" => ""
			)
		);?>
		<?$html = ob_get_contents();?>
		<?ob_end_clean();?>
		<?if(trim($html)):?>
			<div class="text_after_items">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "page",
						"AREA_FILE_SUFFIX" => "bottom",
						"EDIT_TEMPLATE" => ""
					)
				);?>
			</div>
		<?endif;?>
		<?unset($html);?>
	</div>
	<?if($bShowLeftBlock):?>
		<?CAllcorp3Heat::ShowPageType('left_block');?>
	<?endif;?>
</div>
