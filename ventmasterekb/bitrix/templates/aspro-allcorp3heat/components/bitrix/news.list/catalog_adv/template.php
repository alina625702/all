<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if($arResult['ITEMS']):?>

	<?
	$isRound = $arParams['IS_ROUND'] === "Y";
	?>
	<?foreach($arResult['ITEMS'] as $i => $arItem):?>
		<?$templateData = array(
		'STICKER' => (isset($arItem['PROPERTIES']['STICKER']) && $arItem['PROPERTIES']['STICKER']['VALUE'] ? $arItem['PROPERTIES']['STICKER']['VALUE'] : ''),
		'PREVIEW_TEXT' => (isset($arItem['PREVIEW_TEXT']) && $arItem['PREVIEW_TEXT'] ? $arItem['~PREVIEW_TEXT'] : ''),
		'LINK' => (isset($arItem["PROPERTIES"]["LINK"]) && $arItem["PROPERTIES"]["LINK"]["VALUE"] ? $arItem["PROPERTIES"]["LINK"]["VALUE"] : ''),
		'FORM' => (isset($arItem["PROPERTIES"]["FORM_CODE"]) && $arItem["PROPERTIES"]["FORM_CODE"]["VALUE"] ? $arItem["PROPERTIES"]["FORM_CODE"]["VALUE"] : ''),
		);?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="catalog_banner__item sticky-block dark-block-hover <?=$isRound  ? 'rounded-4' : ""?> height-100" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
			<div class="catalog_banner__item-image height-100  dark-block-after" style="background: url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>) center top / cover no-repeat;">
			<?if ($templateData['STICKER'] || $templateData['PREVIEW_TEXT']) :?>
				<div class="catalog_banner__item-text">
					<?if ($templateData['STICKER']) :?>
						<div class="catalog_banner__item__wrapper sticker--upper">
							<div class="catalog_banner__item-sticker sticker__item sticker_item--sale font_9"><?=$templateData['STICKER']?></div>
						</div>	
					<?endif?>
					<?if ($templateData['PREVIEW_TEXT']) :?>
						<div class="catalog_banner__item-description switcher-title font_18 color_light"><?=$templateData['PREVIEW_TEXT']?></div>
					<?endif?>	
				</div>
			<?endif?>
			<?if ($templateData['FORM'] || $templateData['LINK']) :?>
				<a class="catalog_banner__item-link" <?=$templateData['FORM'] ? 'data-event="jqm" data-param-id="'.CAllcorp3Heat::getFormID($templateData['FORM']).'" data-name="'.end(explode('_', $templateData['FORM'])).'"' : 'href="'.$templateData['LINK'].'"'?> ></a>
			<?endif?>
			</div>		
		</div>
	<?endforeach;?>	
<?endif;?>
