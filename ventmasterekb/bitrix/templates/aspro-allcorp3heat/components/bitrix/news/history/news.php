<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?
$arItemFilter = CAllcorp3Heat::GetIBlockAllElementsFilter($arParams);
$itemsCnt = CAllcorp3HeatCache::CIblockElement_GetList(array("CACHE" => array("TAG" => CAllcorp3HeatCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), $arItemFilter, array());
?>
<?// rss?>
<?if($arParams['USE_RSS'] !== 'N'):?>
	<?$this->SetViewTarget('cowl_buttons');?>
	<?Aspro\Functions\CAsproAllcorp3Heat::ShowRSSIcon(
		array(
			'URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['rss']
		)
	);?>
	<?$this->EndViewTarget();?>
<?endif;?>

<?// intro text?>
<?ob_start();?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$html = ob_get_contents();
ob_end_clean();?>
<?if($html && !CAllcorp3Heat::checkAjaxRequest()):?>
	<div class="text_before_items">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "inc",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
<?endif;?>

<?if(!$itemsCnt):?>
	<div class="alert alert-warning"><?=GetMessage("SECTION_EMPTY")?></div>
<?else:?>
	<?CAllcorp3Heat::CheckComponentTemplatePageBlocksParams($arParams, __DIR__);?>
	<?global $arTheme;?>
	<?// section elements?>
	<?$sViewElementsTemplate = ($arParams["SECTION_ELEMENTS_TYPE_VIEW"] == "FROM_MODULE" ? $arTheme["LICENSES_PAGE"]["VALUE"] : $arParams["SECTION_ELEMENTS_TYPE_VIEW"]);?>
	<?@include_once('page_blocks/'.$sViewElementsTemplate.'.php');?>
<?endif;?>