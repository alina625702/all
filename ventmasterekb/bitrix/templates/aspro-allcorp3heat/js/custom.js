/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/

$(document).ready(function() {
  $('a[href^="mailto:"]').on('copy', function(){
      ym(50193520,'reachGoal','copy_email')
  });

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    $('a').each(function(index, element) {

      if($(this).attr('href')) {
        cur_href = $(this).attr('href')
        let newHref = cur_href.replace('https://wa.me/','https://wa.me/');
        newHref = newHref.replace('https://api.whatsapp.com/','https://wa.me/');
        newHref = newHref.replace('https://web.whatsapp.com/send?phone=','https://wa.me/');

        $(element).attr('href', newHref)
      }

    });
  }

  console.log($('span[data-name="ocb"]'))
  $('span[data-name="ocb"]').attr('data-param-id', 10)
})
