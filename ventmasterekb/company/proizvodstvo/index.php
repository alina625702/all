<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производство воздуховодов в Екатеринбурге");?><h2>Производство</h2>
<p>
	Воздуховоды являются неотъемлемой частью любой вентиляционной системы. Это трубы, по которым в помещение подается свежий воздух с улицы, а отработанный выводится наружу.&nbsp;
</p>
<p>
	Хотя изготавливают их и из ткани, и из пластика, но наибольшее распространение получило производство оцинкованных воздуховодов. Это объясняется следующими преимуществами таких конструкций:
</p>
<ul>
	<li>Невысокая стоимость,</li>
	<li>Простота установки,</li>
	<li>Прочность,</li>
	<li>Устойчивость к коррозии.</li>
</ul>
<div class="header-button">
	<div class="btn animate-load btn-default has-ripple" data-event="jqm" data-param-id="13">
		 Связаться с нами
	</div>
</div>
<h2>Производство разных видов воздуховодов</h2>
<p>
	Компания ВентМастер занимается производством воздуховодов и других элементов системы вентиляции. Это сложный процесс, включающий, в том числе и проектирование установок.&nbsp; У нас есть линии по производству труб, отличающихся формой сечения: круглые и квадратные. Именно от формы во многом зависит, насколько сложно можно будет их установить, как долго они будут служить и какой функционал выполнять.
</p>
<ul>
	<li>Если говорить о воздухопроницаемости, то лучшие показатели у воздуховодов с круглым сечением. Все потому что при их соединении удается достигнуть почти полной герметичности и избежать потери воздуха.</li>
	<li>Воздушный поток по круглым трубам проходит равномерно, в то время как в прямоугольных постоянно возникают завихрения. Это может привести к повышению уровня шума.</li>
	<li>При производстве воздуховодов из оцинкованной стали используется меньше металла, чем при изготовлении прямоугольных моделей. А это значит, что стоимость их будет ниже.</li>
	<li>Что касается ухода и очистки, то проще иметь дело с воздуховодами круглой формы, а вот в углах квадратных моделей обычно скапливается тяжело выводимая грязь.</li>
	<li>Воздуховоды квадратной формы чаще всего используют в больших помещениях промышленного назначения. Их преимущество в том, что их изготавливают практически любого размера под заказ и они органично вписываются в интерьер.</li>
</ul>
<p>
	Кроме воздуховодов мы сами изготавливаем фасонные изделия, такие как: решетки, шиберы, зонты или шумоглушители. Все наши изделия отличает высокое качество и надежность.
</p>
<h2>Монтаж и обслуживание воздуховодов</h2>
<p>
	Чтобы вентиляционная система работала исправно необходимо обеспечить ее правильную установку. Это может сделать только профессиональная бригада. Компания «Вентмастер» предлагает полный комплекс услуг по проектированию и монтажу вентиляционных систем любой сложности в Екатеринбурге и Свердловской области. Мы индивидуально подходим к каждому заказу и даем гарантию на выполненные работы.
</p>
<p>
</p>
<h2>Промышленная вентиляция</h2>
<p>
	Вентиляция в промышленных помещениях выполняет очень важную роль, это удаление использованного воздуха и нагнетание свежего, очищенного и подготовленного для жизнеобеспечения.
</p>
<p>
	С ее помощью в цехах и офисных помещениях предприятия создают комфортную воздушную среду, соответствующую нормативным требованиям необходимую для повышения производительности труда работников предприятия.
</p>
<p>
	Все существующие вентиляционные системы группируют по 4 признакам:
</p>
<p>
	В зависимости от того, каким способом перемещается воздух, вентиляцию называют естественной, механической или искусственной, совмещенной, когда присутствуют одновременно оба варианта.
</p>
<p>
	Если исходить из направления воздушного потока, то вентиляционные системы можно разделить на приточные, вытяжные или приточно-вытяжные.
</p>
<p>
	По такому признаку, как место действия вентиляционные системы объединяют в 3 группы: общеобменную, местную, комбинированную. Основываясь на назначении, выделяют рабочую и аварийную системы.
</p>
<p>
	Компания «Вентмастер» реализует вентиляцию в промышленных помещениях различных направлений и назначений уже более 10 лет. Наши специалисты помогут Вам с разработкой проекта по вентиляции и реализацией ее под ключ.</p>
<p>
	 <? /*$APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"gallery-block",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_NOTES" => "",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "gallery-block",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "63",
		"FIELD_CODE" => array(0=>"CODE",1=>"NAME",2=>"PREVIEW_TEXT",3=>"PREVIEW_PICTURE",4=>"DETAIL_TEXT",5=>"DETAIL_PICTURE",6=>"",),
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "aspro_allcorp3heat_content",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array(0=>"MAIN_ALBUM",1=>"PHOTOS",2=>"",),
		"RIGHT_LINK" => "company/reviews/",
		"RIGHT_TITLE" => "Все акции",
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"SUBTITLE" => "",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
); */?><br>
</p>

<p>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"tizers-list",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "tizers-list",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "TYPE",
			7 => "TIZER_ICON",
			8 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "aspro_allcorp3heat_content",
		"IMAGES" => "ICONS",
		"IMAGE_POSITION" => "TOP",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"ITEMS_OFFSET" => true,
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "153",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "TIZER_ICON",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_FILTER_ELEMENTS" => "N"
	),
	false
);?><br>
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
