<div class ="popular_container grey-bg">
    <div class="maxwidth-theme">
        <div class="detail-block ordered-block goods">
            <?if ($arParams['TITLE']):?>
                <div class="ordered-block__title switcher-title font_22"><?=$arParams['TITLE']?></div>
            <?endif?>
            <div class="line-block line-block--align-flex-stretch line-block--block">
                <div class ="wrapper_slider line-block__item flex-1 flexbox--width-66-min768-max992 <?=($arParams['SHOW_BANNER'] === 'Y') ? " flexbox--width-75-min992" : " flexbox--width-100-min992"?>">
                    <?$GLOBALS['arrGoodsFilter'] = array('PROPERTY_POPULAR_ITEM_VALUE' => 'Y', 'SECTION_GLOBAL_ACTIVE' => 'Y'); ?>
                    <?\Aspro\Functions\CAsproAllcorp3Heat::showBlockHtml([
                                    'FILE' => '/detail_linked_goods.php',
                                    'PARAMS' => $arParams,   
                    ]);?>
                </div>
                <?if ($arParams['SHOW_BANNER'] === 'Y'):?>
                    <div class="catalog_banner__wrapper line-block__item flex-basis-25-min1368 flex-basis-25-max1368">	
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/blocks/catalog/adv.php",
                                "EDIT_TEMPLATE" => "include_area.php",
                                'IS_ROUND' => $arParams['IS_ROUND'],
                            )
                        );?>
                    </div>
                <?endif?>    	
            </div>
        </div>    
    </div> 
</div>       
 	