<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$indexPageOptions = $GLOBALS['arTheme']['INDEX_TYPE']['SUB_PARAMS'][$GLOBALS['arTheme']['INDEX_TYPE']['VALUE']];
$blockOptions = $indexPageOptions['SERVICES'];
$blockTemplateOptions = $blockOptions['TEMPLATE']['LIST'][$blockOptions['TEMPLATE']['VALUE']];

$bShowMore = $blockTemplateOptions["ADDITIONAL_OPTIONS"]["LINES_COUNT"]["VALUE"] === 'SHOW_MORE';
$linesCount = $bShowMore ? 1 : (intval($blockTemplateOptions["ADDITIONAL_OPTIONS"]["LINES_COUNT"]["VALUE"]) ?: 1);
?>
<?$APPLICATION->IncludeComponent(
	"aspro:tabs.allcorp3heat", 
	"services", 
	array(
		"COMPONENT_TEMPLATE" => "services",
		"IBLOCK_TYPE" => "aspro_allcorp3heat_content",
		"IBLOCK_ID" => "41",
		"PAGE_ELEMENT_COUNT" => "FROM_THEME",
		"TYPE_TEMPLATE" => "services-list",
		"SKU_IBLOCK_ID" => "21",
		"ELEMENT_SORT_FIELD" => "ACTIVE_FROM",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "SORT",
		"ELEMENT_SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arFilterServices",
		"PROPERTY_CODE" => array(
			0 => "PRICE",
			1 => "PRICEOLD",
			2 => "ECONOMY",
			3 => "LINK_FAQ",
			4 => "FOR_SHOW_5",
			5 => "MATERIAL_PICK",
			6 => "FOR_SHOW_6",
			7 => "DISEGHNER_AT_PLACE",
			8 => "FOR_SHOW_4",
			9 => "for_show_1",
			10 => "for_show_2",
			11 => "for_show_3",
			12 => "ICON",
			13 => "TRANSPARENT_PICTURE",
			14 => "",
		),
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "",
		),
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"HIT_PROP" => "",
		"SHOW_SECTION" => "Y",
		"SHOW_DISCOUNT_TIME" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_DISCOUNT_PRICE" => "Y",
		"TITLE" => "Наши услуги",
		"SKU_PROPERTY_CODE" => "",
		"SKU_TREE_PROPS" => "",
		"SKU_SORT_FIELD" => "name",
		"SKU_SORT_ORDER" => "asc",
		"SKU_SORT_FIELD2" => "sort",
		"SKU_SORT_ORDER2" => "asc",
		"SUBTITLE" => "",
		"RIGHT_TITLE" => "Все услуги",
		"RIGHT_LINK" => "services/",
		"SHOW_PREVIEW_TEXT" => "Y",
		"NARROW" => "FROM_THEME",
		"ITEMS_OFFSET" => "FROM_THEME",
		"TEXT_CENTER" => "FROM_THEME",
		"IMG_CORNER" => "FROM_THEME",
		"ADVERTISING_ON_MAIN_PAGE" => "N",
		"ELEMENT_IN_ROW" => "FROM_THEME",
		"COUNT_ROWS" => "FROM_THEME",
		"TABS_FILTER" => "SECTION",
		"CHECK_REQUEST_BLOCK" => CAllcorp3Heat::checkRequestBlock("services"),
		"INDEX_PAGE_OPTIONS" => "SERVICES"
	),
	false
);?>