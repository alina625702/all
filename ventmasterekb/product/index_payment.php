<p>
	 Мы работаем с физическими и юридическими лицами. И предоставляем сразу два варианта оплаты.
</p>
<ul>
	<li>Наличные. Вы подписываете товаросопроводительные документы, расплачиваетесь денежными средствами, получаете товар и чек. </li>
	<li>Безналичный расчет.</li>
</ul>