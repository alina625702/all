<?
require __DIR__ . '/../vendor/autoload.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

use Google\Client;
use Google\Service\Drive;


/**
 * Returns an authorized API client.
 * @return Client the authorized client object
 */
function getClient()
{
    $client = new Google\Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/drive.file']);
    $client->setAuthConfig('client_secret_624059430659-veoon69omhu07on6eloso3k0o0v8h8g7.apps.googleusercontent.com.json');
    //$client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google\Service\Sheets($client);

// Prints the names and majors of students in a sample spreadsheet:
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
try{
    // $spreadsheetId = '1MrMGKEXFo4DNL8pOb6i6ey_MDMShIOutFFD4FRJEEQc';
    // $range = 'Дверь Алюм!A22:B';
    // $response = $service->spreadsheets_values->get($spreadsheetId, $range);
    // $values = $response->getValues();
    //
    // if (empty($values)) {
    //     print "No data found.\n";
    // } else {
    //     print "Name, Major:\n";
    //     foreach ($values as $row) {
    //         // Print columns A and E, which correspond to indices 0 and 4.
    //         printf("%s, %s\n", $row[0], $row[4]);
    //     }
    // }

    // $client = getClient();
    // $service = new Google_Service_Sheets($client);
    // $spreadsheetId = '1MrMGKEXFo4DNL8pOb6i6ey_MDMShIOutFFD4FRJEEQc';  // TODO: Update placeholder value.
    // $sheetId = 0;  // TODO: Update placeholder value.
    // $requestBody = new Google_Service_Sheets_CopySheetToAnotherSpreadsheetRequest();
    // $response = $service->spreadsheets_sheets->copyTo($spreadsheetId, $sheetId, $requestBody);
    // echo '<pre>', var_export($response, true), '</pre>', "\n";
    //

    $file_num = 345;

    // Copy table
    $service = new Google_Service_Drive($client);
    $fileId = "1MrMGKEXFo4DNL8pOb6i6ey_MDMShIOutFFD4FRJEEQc"; // Google File ID
    $file = new Google_Service_Drive_DriveFile();
    $file->setName('Заказ-наряд №' . $file_num);
    $request = $service->files->copy($fileId, $file);

    echo $request['id'] . PHP_EOL;
    $new_spreadsheed_id = $request['id'];

    // Update params
    $service = new Google_Service_Sheets($client);

    $sposob = 'По проему';
    $height = 3400;
    $weight = 1250;
    $stvorki = 'Однопольная';
    $otkrivanie = 'Правое';
    $nalichniki = 'Без наличников';
    $obramlenie = 'Сверху';
    $porog = 'Да';
    $porog_height = '63,6';
    $svinec = '2,46';
    $color = 'Серый';

    $updateRow = [
      [$sposob],
      [$height],
      [$weight],
      [$stvorki],
      [$otkrivanie],
      [$nalichniki],
      [$obramlenie],
      [$porog],
      [$porog_height],
      [$svinec],
      [$color]
    ];
    $rows = $updateRow;
    $valueRange = new \Google_Service_Sheets_ValueRange();
    $valueRange->setValues($rows);
    $range = 'Дверь Алюм!B3:B13'; // where the replacement will start, here, first column and second line
    $options = ['valueInputOption' => 'USER_ENTERED'];
    $service->spreadsheets_values->update($new_spreadsheed_id, $range, $valueRange, $options);

    // Get new price
    $price_arr = $service->spreadsheets_values->get($new_spreadsheed_id, 'Дверь Алюм!B19');
    print_r($price_arr);
    echo 'Finish' . PHP_EOL;
}
catch(Exception $e) {
    // TODO(developer) - handle error appropriately
    echo 'Message: ' .$e->getMessage();
}
