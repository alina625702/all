<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);
?>

<div class="maxwidth-theme">
	<div class="col-md-12">
		<div class="row margin0 block-with-bg">
			<div class="item-views catalog blocks">
				<div class="title_block row">
					<div class="col-md-12 col-sm-12">
							<h2>Рентгенозащитные материалы по цене производителя</h2>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4 col-sm-12">
						<p>Одно из основных направлений компании <strong>Рентген Сервис</strong> - продажа рентгенозащитных материалов.
							Средства рентгенозащиты предназначены для обеспечения безопасности персонала и пациентов в медицинских учреждениях.
							Изготавливается рентгенозащита максимально эргономичной и долговечной, в производстве используют инновационные технологии.
							В составе средств защиты присутствуют такие материалы как свинец и барий, они обладают высоким коэффициентом поглощения
							излучения.</p>

						<p><a href="/product/rentgenozashchitnye-material/" class="btn btn-transparent-bg btn-default">Подробнее</a></p>
					</div>

					<div class="col-md-8 col-sm-12">
						<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"owl-gallery",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "owl-gallery",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "42",
		"IBLOCK_TYPE" => "-",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "8",
		"NORMAL_BLOCK" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "148",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SECTIONS_COUNT_IN_LINE" => "1",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SHOW_GOODS" => "Y",
		"SHOW_SECTIONS" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TITLE" => "Наша команда"
	),
	false
);?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?
if($arResult["TABS"]):?>
	<div class="maxwidth-theme">
		<div class="col-md-12">
			<div class="row margin0 block-with-bg">
				<div class="item-views catalog blocks">
					<div class="title_block row">
						<div class="col-md-3 col-sm-12">
							<?if($arParams["TITLE"]):?>
								<h3><?=$arParams["TITLE"];?></h3>
							<?endif;?>
						</div>
						<div class="col-md-6 col-sm-9 text-center">
							<div class="items head-block" <?=(count($arResult["TABS"]) == 1 ? "style='display:none;'" : "");?>>
								<div class="row margin0">
									<div class="maxwidth-theme">
										<div class="col-md-12 items-link">
											<?$firstTab = reset($arResult["TABS"]);?>
											<?$i = 0;?>
											<?foreach($arResult["TABS"] as $key => $arItem):?>
												<div class="item-link <?=(!$i ? 'active clicked' : '');?>">
													<div class="title">
														<span><?=$arItem['TITLE']?></span>
													</div>
												</div>
												<?++$i;?>
											<?endforeach;?>
										</div>
										<?
										?>
										<div class="items-link-mobile-wrap">
											<?$i = 0;?>
											<select class="items-link-mobile ">
												<option disabled><?=$firstTab["TITLE"]?></option>
												<?foreach($arResult["TABS"] as $key => $arItem):?>
													<?$selected = ($sort == $key && $order == $arItem);?>
													<option <?=($selected ? "selected='selected'" : "")?>  value="<?=$key?>" class="ordering <?=$key?>"><?=$arItem['TITLE']?></option>
													<?++$i;?>
												<?endforeach;?>
											</select>


										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="nav-direction">
								<ul class="flex-direction-nav">
									<li class="flex-nav-prev">
										<a href="javascript:void(0)" class="flex-prev"><span>Prev</span></a>
									</li>
									<li class="flex-nav-next">
										<a href="javascript:void(0)" class="flex-next"><span>Next</span></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<?$arParams['SET_TITLE'] = 'N';$arParamsTmp = urlencode(serialize($arParams));?>
					<span class='request-data' data-value='<?=$arParamsTmp?>'></span>
					<div class="tabs_ajax">
						<div class="body-block">
							<div class="row margin0">
								<?$i = 0;?>
								<?foreach($arResult["TABS"] as $key => $arItem):?>
									<div class="item-block <?=(!$i ? 'active opacity1' : '');?>" data-filter="<?=($arItem["FILTER"] ? urlencode(serialize($arItem["FILTER"])) : '');?>">
										<?if(!$i)
										{
											if($arItem["FILTER"])
												$GLOBALS[$arParams["FILTER_NAME"]] = $arItem["FILTER"];

											include(str_replace("//", "/", $_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/mainpage/comp_catalog_ajax.php"));
										}?>
									</div>
									<?++$i;?>
								<?endforeach;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?endif;?>
