<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<?
global $arTheme;
$expandedLink = $arTheme['HEADER_MOBILE_MENU_EXPAND']['VALUE'];
?>
<?if($arResult):?>
	<div class="menu top">
		<ul class="top">
			<?foreach($arResult as $arItem):?>
				<?$bShowChilds = $arParams['MAX_LEVEL'] > 1;


				global $USER;
				if (!$USER->IsAdmin()) {
					if($arItem["TEXT"] == "История" || $arItem["TEXT"] == "Сотрудники" || $arItem["TEXT"] == "Проекты" || $arItem["TEXT"] == "Возможности" || $arItem["TEXT"] == "Вопрос ответ") {
						continue;
					}
				}

				if($expandedLink == $arItem['LINK'] && $arParams['SHORT_MENU'] != 'Y')
					continue;
				?>
				<?$bParent = $arItem['CHILD'] && $bShowChilds;?>
				<li<?=($arItem['SELECTED'] ? ' class="selected"' : '')?>>
					<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>">
						<span><?=$arItem['TEXT']?></span>
						<?if($bParent):?>
							<span class="arrow"><i class="svg svg_triangle_right"></i></span>
						<?endif;?>
					</a>
					<?if($bParent):?>
						<ul class="dropdown">
							<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right_white.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
							<li class="menu_title"><a href="<?=$arItem["LINK"]?>"><?=$arItem['TEXT']?></a></li>
							<?foreach($arItem['CHILD'] as $arSubItem):?>
								<?$bShowChilds = $arParams['MAX_LEVEL'] > 2;?>

								<?php
									global $USER;
									if (!$USER->IsAdmin()) {
										if($arSubItem["TEXT"] == "История" || $arSubItem["TEXT"] == "Сотрудники" || $arSubItem["TEXT"] == "Проекты" || $arSubItem["TEXT"] == "Возможности" || $arSubItem["TEXT"] == "Вопрос ответ") {
											continue;
										}
									}
								?>

								<?$bParent = $arSubItem['CHILD'] && $bShowChilds;?>
								<li<?=($arSubItem['SELECTED'] ? ' class="selected"' : '')?>>
									<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubItem["LINK"]?>" title="<?=$arSubItem["TEXT"]?>">
										<span><?=$arSubItem['TEXT']?></span>
										<?if($bParent):?>
											<span class="arrow"><i class="svg svg_triangle_right"></i></span>
										<?endif;?>
									</a>
									<?if($bParent):?>
										<ul class="dropdown">
											<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
											<li class="menu_title"><a href="<?=$arSubItem["LINK"]?>"><?=$arSubItem['TEXT']?></a></li>
											<?foreach($arSubItem["CHILD"] as $arSubSubItem):?>
												<?$bShowChilds = $arParams['MAX_LEVEL'] > 3;?>

												<?php
													global $USER;
													if (!$USER->IsAdmin()) {
														if($arSubSubItem["TEXT"] == "История" || $arSubSubItem["TEXT"] == "Сотрудники" || $arSubSubItem["TEXT"] == "Проекты" || $arSubSubItem["TEXT"] == "Возможности" || $arSubSubItem["TEXT"] == "Вопрос ответ") {
															continue;
														}
													}
												?>

												<?$bParent = $arSubSubItem['CHILD'] && $bShowChilds;?>
												<li<?=($arSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
													<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubSubItem["LINK"]?>" title="<?=$arSubSubItem["TEXT"]?>">
														<span><?=$arSubSubItem['TEXT']?></span>
														<?if($bParent):?>
															<span class="arrow"><i class="svg svg_triangle_right"></i></span>
														<?endif;?>
													</a>
													<?if($bParent):?>
														<ul class="dropdown">
															<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><?=CAllcorp2::showIconSvg("arrow-back", SITE_TEMPLATE_PATH."/images/svg/Arrow_right.svg");?><?=GetMessage('ALLCORP2_T_MENU_BACK')?></a></li>
															<li class="menu_title"><a href="<?=$arSubSubItem["LINK"]?>"><?=$arSubSubItem['TEXT']?></a></li>
															<?foreach($arSubSubItem["CHILD"] as $arSubSubSubItem):?>
																<?php
																	global $USER;
																	if (!$USER->IsAdmin()) {
																		if($arSubSubSubItem["TEXT"] == "История" || $arSubSubSubItem["TEXT"] == "Сотрудники" || $arSubSubSubItem["TEXT"] == "Проекты" || $arSubSubSubItem["TEXT"] == "Возможности" || $arSubSubSubItem["TEXT"] == "Вопрос ответ") {
																			continue;
																		}
																	}
																?>
																<li<?=($arSubSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
																	<a class="dark-color" href="<?=$arSubSubSubItem["LINK"]?>" title="<?=$arSubSubSubItem["TEXT"]?>">
																		<span><?=$arSubSubSubItem['TEXT']?></span>
																	</a>
																</li>
															<?endforeach;?>
														</ul>
													<?endif;?>
												</li>
											<?endforeach;?>
										</ul>
									<?endif;?>
								</li>
							<?endforeach;?>
						</ul>
					<?endif;?>
				</li>
			<?endforeach;?>
		</ul>
	</div>
<?endif;?>