<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) die();?>
<?$this->setFrameMode(true);?>
<?use \Bitrix\Main\Localization\Loc;?>
<?if($arResult['ITEMS']):?>
	<div class="row margin0">
		<div class="maxwidth-theme">
			<div class="col-md-12">
				<?
				global $arTheme;
				$slideshowSpeed = '1000';
				$animationSpeed = '500';
				$bAnimation = (bool)$slideshowSpeed;
				$isNormalBlock = (isset($arParams['NORMAL_BLOCK']) && $arParams['NORMAL_BLOCK'] == 'Y');


				?>
				<div class="item-views front cert-items table-type-block blocks <?=($isNormalBlock ? 'normal' : '');?>">
					<div class="text-center certslidertitle">Лицензии и сертификаты</div>
					<div class="flexslider unstyled row front dark-nav view-control navigation-vcenter" data-plugin-options='{"directionNav": true, "touch": true, "controlNav" :false, "animationLoop": true,  "slideshow": false, <?=($slideshowSpeed >= 0 ? '"slideshowSpeed": '.$slideshowSpeed.',' : '')?> <?=($animationSpeed >= 0 ? '"animationSpeed": '.$animationSpeed.',' : '')?> <?=($isNormalBlock ? '"itemMargin": 32,' : '');?> "counts": [<?=$arParams['COUNT_IN_LINE']?>, <?=$arParams['COUNT_IN_LINE']-1?>, 1]}'>
						<ul class="slides items" data-slice="Y">

                            <? //var_dump($arResult['ITEMS'])?>
							<?foreach($arResult['ITEMS'] as $i => $arItem):?>
								<?
                           // var_dump($arItem);

//                            print ~$arItem["NAME"];
                            print $arItem["DETAIL_PICTURE"]['SRC'];
								// edit/add/delete buttons for edit mode
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								// use detail link?
								$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);
								// preview image
								$bImage = (isset($arItem['PREVIEW_PICTURE']) && $arItem['PREVIEW_PICTURE']['SRC']);
								$imageSrc = ($bImage ? $arItem['PREVIEW_PICTURE']['SRC'] : SITE_TEMPLATE_PATH.'/images/svg/Staff_noimage2.svg');

								// show active date period
								$bActiveDate = strlen($arItem['DISPLAY_PROPERTIES']['PERIOD']['VALUE']) || ($arItem['DISPLAY_ACTIVE_FROM'] && in_array('DATE_ACTIVE_FROM', $arParams['FIELD_CODE']));
								?>
								<li class="shadow1 col-md-<?=floor(12 / $arParams['COUNT_IN_LINE'])?> col-sm-<?=floor(12 / round($arParams['COUNT_IN_LINE'] / 2))?>">
									<div class="item noborder clearfix" data-slice-block="Y" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
										<?if($imageSrc):?>
											<div class="image shine <?=($bImage ? "" : "wpi" );?>">
												<?if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?endif;?>
                                                    <a href="<?=$imageSrc?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" class="img-inside fancybox" data-fancybox-group="gal">
													<img class="img-responsive" src="<?=$imageSrc?>" alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" />
                                                        <span class="zoom"></span>
                                                    </a>
                                                        <?if($bDetailLink):?></a><?endif;?>
											</div>
										<?endif;?>

											<div class="top-block-wrapper">
												<?// element name?>
												<?if(strlen($arItem['NAME'])):?>
													<div class="title cert-title">
															<?=$arItem['NAME']?>
													</div>
												<?endif;?>

											</div>


									</div>
								</li>
							<?endforeach;?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<?endif;?>