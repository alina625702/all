<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

$this->addExternalCss("/bitrix/templates/aspro-allcorp2/vendor/grid-gallery-master/css/grid-gallery.min.css");
$this->addExternalCss("https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css");
$this->addExternalJS("/bitrix/templates/aspro-allcorp2/vendor/grid-gallery-master/js/grid-gallery.min.js");
$this->addExternalJS("https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js");


?>
<?if($arResult['ITEMS']):?>
	<?if($arParams['SHOW_TITLE'] == 'Y'):?>
		<div class="title-tab-heading visible-xs"><?=$arParams['T_TITLE'];?></div>
	<?endif;?>



	<!-- <div class="head-block top">
		<div class="bottom_border"></div>
		<div class="item-link <?=($bHasYear ? '' : 'active');?>">
			<?if($bHasYear):?>
				<a class="title btn-inline black" href="<?=$arResult['FOLDER'];?>">
			<?else:?>
				<div class="title">
			<?endif;?>
					<span class="btn-inline black"><?=GetMessage('ALL_TIME');?></span>
			<?if($bHasYear):?>
				</a>
			<?else:?>
				</div>
			<?endif;?>
		</div>
		<?foreach($arYears as $value):
			$bSelected = ($bHasYear && $value == $year);?>
			<div class="item-link <?=($bSelected ? 'active' : '');?>">
				<?if($bSelected):?>
					<div class="title btn-inline black">
				<?else:?>
					<a class="title btn-inline black" href="<?=$APPLICATION->GetCurPageParam('year='.$value, array('year'));?>">
				<?endif;?>
						<span class="btn-inline black"><?=$value;?></span>
				<?if($bSelected):?>
					</div>
				<?else:?>
					</a>
				<?endif;?>
			</div>
		<?endforeach;?>
	</div> -->

<?//print_r($arResult);?>

<?
	function checkCategoryExist($categories, $category_id) {
		foreach($categories as $category) {
			if($category['category_id'] == $category_id)
				return true;
		}
		return false;
	}

	$categories = array();

	//print_r($arResult['ITEMS']);
	foreach($arResult['ITEMS'] as $arItem) {

		if(!checkCategoryExist($categories, $arItem['IBLOCK_SECTION_ID'])){
			$category_item = array();
			$category_item['category_id'] = $arItem['IBLOCK_SECTION_ID'];

			$res = CIBlockSection::GetByID($arItem['IBLOCK_SECTION_ID']);
			if($ar_res = $res->GetNext())
				$category_item['category_name'] = $ar_res['NAME'];
				$category_item['items'] = array();

			array_push($categories, $category_item);
		}

		for($i = 0; $i < count($categories); $i++) {
			if ($categories[$i]['category_id'] == $arItem['IBLOCK_SECTION_ID']) {
				array_push($categories[$i]['items'], $arItem);
			}
		}
	}

	//print_r($categories);
?>
<div class="tab">
	<?
	foreach($categories as $category) {
			?>
				<button class="tablinks" onclick="openTab(event, 'category_<?=$category['category_id']?>')"><?=$category['category_name']?></button>
			<?
	}
	?>
</div>

	<!-- Tab content -->

	<?foreach($categories as $category) {
			?>
			<div id="category_<?=$category['category_id']?>" class="tabcontent">
				<div class="gg-container">
					<div class="gg-box">

							<?foreach($category['items'] as $arItem):?>
								<?

								// edit/add/delete buttons for edit mode
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								// preview picture
								if($bShowSectionImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE'])){
									$bImage = !empty($arItem['PREVIEW_PICTURE']);
									$arSectionImage = ($bImage ? CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 254, 'height' => 254), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array());
									$imageSectionSrc = ($bImage ? $arSectionImage['src'] : SITE_TEMPLATE_PATH.'/images/noimage_sections.png');
								}
								?>
						    <a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" data-lightbox="gallery"><img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" /> </a>

							<?endforeach;?>
					</div>
				</div>
			</div>
			<?
	}
	?>

<?endif;?>

	<script>
	gridGallery({
	 // gallery selector
	 selector: ".gg-container",
	 darkMode: true,
	 layout: "horizontal",
	 gapLength: 4,
	 rowHeight: 180,
	 columnWidth: 200
	});

	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })

		function openTab(evt, tabName) {
		  // Declare all variables
		  var i, tabcontent, tablinks;

		  // Get all elements with class="tabcontent" and hide them
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }

		  // Get all elements with class="tablinks" and remove the class "active"
		  tablinks = document.getElementsByClassName("tablinks");
		  for (i = 0; i < tablinks.length; i++) {
				tablinks[1].className = tablinks[1].className.replace(" active", "");
		    tablinks[i].className = tablinks[i].className.replace(" active", "");
		  }

		  // Show the current tab, and add an "active" class to the button that opened the tab
		  document.getElementById(tabName).style.display = "block";
		  evt.currentTarget.className += " active";
		}

	</script>
