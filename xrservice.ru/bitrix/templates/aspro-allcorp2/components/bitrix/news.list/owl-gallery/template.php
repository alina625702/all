<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

$this->addExternalCss("/bitrix/templates/aspro-allcorp2/vendor/grid-gallery-master/css/grid-gallery.min.css");
$this->addExternalCss("https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css");
$this->addExternalJS("/bitrix/templates/aspro-allcorp2/vendor/grid-gallery-master/js/grid-gallery.min.js");
$this->addExternalJS("https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js");


?>
<?if($arResult['ITEMS']):?>
	<?if($arParams['SHOW_TITLE'] == 'Y'):?>
		<div class="title-tab-heading visible-xs"><?=$arParams['T_TITLE'];?></div>
	<?endif;?>
	<div class="gg-container">
		<div class="gg-box">
				<?foreach($arResult['ITEMS'] as $arItem):?>
					<?
					// edit/add/delete buttons for edit mode
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					// preview picture
					if($bShowSectionImage = in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE'])){
						$bImage = !empty($arItem['PREVIEW_PICTURE']);
						$arSectionImage = ($bImage ? CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 254, 'height' => 254), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array());
						$imageSectionSrc = ($bImage ? $arSectionImage['src'] : SITE_TEMPLATE_PATH.'/images/noimage_sections.png');
					}
					?>
			    	<a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" data-lightbox="gallery"><img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" /> </a>

				<?endforeach;?>
		</div>
	</div>
<?endif;?>

	<script>
	gridGallery({
	 // gallery selector
	 selector: ".gg-container",
	 darkMode: true,
	 layout: "horizontal",
	 gapLength: 4,
	 rowHeight: 180,
	 columnWidth: 200
	});

	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
	</script>
