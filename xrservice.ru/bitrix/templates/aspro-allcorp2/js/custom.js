/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/

$(document).ready(function() {

    // Подмена URL для WhatsApp
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
      $('a').each(function(index, element) {
        if($(this).attr('href')) {
          cur_href = $(this).attr('href')
          $(this).attr('href', cur_href.replace('https://wa.me/','https://wa.me/'))
        }
      })
    } else {
      $('a').each(function(index, element) {
        if($(this).attr('href')) {
          cur_href = $(this).attr('href')
          $(this).attr('href', cur_href.replace('https://wa.me/','https://web.whatsapp.com/send?phone='))
        }
      })
    }

    // Цель для WhatsApp
    $(".whatsapp a").on("click",function(){
        yaCounter46161126.reachGoal('whatsapp_click');
    });

    $(".svg-inline-wh").on("click",function(){
        yaCounter46161126.reachGoal('whatsapp_click');
    });


    // Отравка формы "Задать вопрос"
    $('body').on('click', 'form[name="aspro_allcorp2_question"] .btn-default', function() {

        if( $('form[name="aspro_allcorp2_question"] input[name="NAME"]').val() && $('form[name="aspro_allcorp2_question"] input[name="PHONE"]').val() && $('form[name="aspro_allcorp2_question"] textarea[name="MESSAGE"]').val() ) {
            var questForm = $('form[name="aspro_allcorp2_question"]').serialize();

            $.ajax({
                url: '/mailto.php',
                method: 'POST',
                data: questForm,
                success: function() {
                    console.log('Send Q&A form!');
                }
            });
        }
    });

    // Отравка формы "Обратный звонок"
    $('body').on('click', 'form[name="aspro_allcorp2_callback"] .btn-default', function() {

        if( $('form[name="aspro_allcorp2_callback"] input[name="FIO"]').val() && $('form[name="aspro_allcorp2_callback"] input[name="PHONE"]').val() ) {
            var questForm = $('form[name="aspro_allcorp2_callback"]').serialize();

            $.ajax({
                url: '/mailto_callback.php',
                method: 'POST',
                data: questForm,
                success: function() {
                    console.log('Send callback form!');
                }
            });
        }
    });

    // Отравка формы "Заказ продукта"
    $('body').on('click', 'form[name="aspro_allcorp2_order_product"] .btn-default', function() {

        if( $('form[name="aspro_allcorp2_order_product"] input[name="NAME"]').val() && $('form[name="aspro_allcorp2_order_product"] input[name="PHONE"]').val() && $('form[name="aspro_allcorp2_order_product"] textarea[name="MESSAGE"]').val() ) {
            var questForm = $('form[name="aspro_allcorp2_order_product"]').serialize();

            $.ajax({
                url: '/mailto_order.php',
                method: 'POST',
                data: questForm,
                success: function() {
                    console.log('Send order form!');
                }
            });
        }
    });

    // Отравка формы "Заказ услуги"
    $('body').on('click', 'form[name="aspro_allcorp2_order_services"] .btn-default', function() {

        if( $('form[name="aspro_allcorp2_order_services"] input[name="NAME"]').val() && $('form[name="aspro_allcorp2_order_services"] input[name="PHONE"]').val() && $('form[name="aspro_allcorp2_order_services"] textarea[name="MESSAGE"]').val() ) {
            var questForm = $('form[name="aspro_allcorp2_order_services"]').serialize();

            $.ajax({
                url: '/mailto_services.php',
                method: 'POST',
                data: questForm,
                success: function() {
                    console.log('Send order form!');
                }
            });
        }
    });


        $('a[href^="mailto:"]').on('copy', function(){
            ym(46161126,'reachGoal','copy_email')
        });
});
