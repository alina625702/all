<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Дентальное оборудование");
$APPLICATION->SetTitle("Дентальное оборудование");?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"dental-front-banners-big-long",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "dental-front-banners-big-long",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"PREVIEW_PICTURE",3=>"DETAIL_PICTURE",4=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "aspro_allcorp2_adv",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "155",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"BANNERTYPE",1=>"LINKIMG",2=>"BUTTON1TEXT",3=>"BUTTON1CLASS",4=>"BUTTON1LINK",5=>"",),
		"RIGHT_LINK" => "services/",
		"RIGHT_TITLE" => "Все услуги",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TITLE" => "Наши услуги"
	)
);?> <script>
    document.addEventListener('DOMContentLoaded', function () {
      const slider = new ChiefSlider('.slider1', {
        loop: true,
        autoplay: true,
        interval: 7000,
      });
    });
  </script> <script>
	    document.addEventListener('DOMContentLoaded', function () {
	      const slider = new ChiefSlider('.slider2', {
	        loop: true,
	        autoplay: true,
	        interval: 7000,
	      });
	    });
	  </script> <script>
		    document.addEventListener('DOMContentLoaded', function () {
		      const slider = new ChiefSlider('.slider3', {
		        loop: true,
		        autoplay: true,
		        interval: 7000,
		      });
		    });
		 </script> <script>
 		    document.addEventListener('DOMContentLoaded', function () {
 		      const slider = new ChiefSlider('.slider4', {
 		        loop: true,
 		        autoplay: true,
 		        interval: 7000,
 		      });
 		    });
 		 </script> <script>
 		    document.addEventListener('DOMContentLoaded', function () {
 		      const slider = new ChiefSlider('.slider5', {
 		        loop: true,
 		        autoplay: true,
 		        interval: 7000,
 		      });
 		    });
 		 </script>
<div class="container-dental dental-index">
	<div class="maxwidth-theme dental-catalog">
		<h2>Мы поставляем оборудование по всей России</h2>
		<div class="row dental-catalog-block">
			<div class="col-md-6">
				<h3>Стоматологическая установка U200</h3>
				<p>
 <strong>В комплекте:</strong>
				</p>
				<ul>
					<li>Светильник Siger LED, 33000 люкс</li>
					<li>Держатель монитора</li>
					<li>Стул врача</li>
					<li>Стул ассистента</li>
					<li>Коммуникационный блок</li>
				</ul>
				<p>
 <strong>Опции:</strong>
				</p>
				<ul>
					<li>Светильник Faro EDI, до 35 000 люкс (Италия)</li>
					<li>Встраиваемый скалер</li>
					<li>Электрический мотор</li>
					<li>Встроенная система для подключения вакуумной помпы Cattani</li>
					<li>Монитор</li>
					<li>Встроенная система для подключения вакуумной помпы Cattani</li>
					<li>Итальянская обивка</li>
				</ul>
				<div class="btn-block">
 <span class="btn-lg btn btn-primary" data-event="jqm" data-param-id="22" data-name="ask">Задать вопрос</span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="slider2">
					<div class="slider__wrapper">
						<div class="slider__items">
							<div class="slider__item">
							<img src="/upload/dental/photo_2022-07-20_18-26-21.jpg">
							</div>
							<div class="slider__item">
							<img src="/upload/dental/photo_2022-07-20_18-26-21-2.jpg">
							</div>
							<div class="slider__item">
							<img src="/upload/dental/photo_2022-07-20_18-26-21-4.jpg">
							</div>
							<div class="slider__item">
							<img src="/upload/dental/photo_2022-07-20_18-26-22.jpg">
							</div>
							<div class="slider__item">
							<img src="/upload/dental/photo_2022-07-20_18-26-22-2.jpg">
							</div>
						</div>
					</div>
				<a href="#" class="slider__control" data-slide="prev"></a> <a href="#" class="slider__control" data-slide="next"></a>
					<ol class="slider__indicators">
						<li data-slide-to="0"></li>
						<li data-slide-to="1"></li>
						<li data-slide-to="2"></li>
						<li data-slide-to="3"></li>
						<li data-slide-to="4"></li>
					</ol>
				</div>
			</div>

		</div>
		<div class="row dental-catalog-block slider-left">
			<div class="col-md-6">
				<div class="slider1">
					<div class="slider__wrapper">
						<div class="slider__items">
							<div class="slider__item">
 							<img src="/upload/dental/photo_2022-07-20_18-21-38.jpg">
							</div>
							<div class="slider__item">
 							<img src="/upload/dental/CS-8100-3D-260x260.jpg">
							</div>
							<div class="slider__item">
 							<img src="/upload/dental/photo_2022-07-20_18-21-38-2.jpg">
							</div>
						</div>
					</div>
 				<a href="#" class="slider__control" data-slide="prev"></a> <a href="#" class="slider__control" data-slide="next"></a>
					<ol class="slider__indicators">
						<li data-slide-to="0"></li>
						<li data-slide-to="1"></li>
						<li data-slide-to="2"></li>
					</ol>
				</div>
			</div>
			<div class="col-md-6">
				<h3>Стоматологический томограф CS 8100 3D</h3>
				<p>
 <strong>Опции:</strong>
				</p>
				<ul>
					<li>Лазерное позиционирование пациента (3 луча), полное компьютерное управление с предустановленными параметрами для 6-ти типов пациента (на каждую программу)</li>
					<li>Возможность ручной установки параметров</li>
					<li>Цифровой контроль экспозиции (оптимизация степени почернения снимка)</li>
					<li>Напряжение на трубке 60-90kVАнодный ток — 7 мА</li>
					<li>Анодный ток 2-15мА</li>
					<li>Панель управления ( на экране персонального компьютера) с возможностью выбора параметров излучения)</li>
					<li>Электронное управление силой тока и напряжением с автоматической корректировкой параметров kV и mA в зоне позвоночника</li>
				</ul>
				<p>
					 Универсальные приборы позволяют исследовать патологии как у детей, так и взрослых.
				</p>
				<p>
 <strong>Применение дентальных томографов дает возможность врачу-стоматологу:</strong>
				</p>
				<ul>
					<li>Проводить диагностику и разрабатывать варианты оптимального лечения кариесных осложнений, наблюдать за эндодонтическим лечением и оценивать результат;</li>
					<li>Определять аномалии зубов, тип лечения и его динамику;</li>
					<li>Планировать ортопедическое лечение, создавая достоверный прогноз;</li>
					<li> Оценить эффективность оперативного вмешательства и спрогнозировать его результат</li>
				</ul>
				<div class="btn-block">
 <span class="btn-lg btn btn-primary" data-event="jqm" data-param-id="22" data-name="ask">Задать вопрос</span>
				</div>
			</div>
		</div>
		<div class="row dental-catalog-block">
			<div class="col-md-6">
				<h3>Рентгеновская дентальная установка Carestream CS 2100</h3>
				<p>
 <strong>Опции:</strong>
				</p>
				<ul>
					<li>Генератор переменного тока — 60 кВТ</li>
					<li>Генератор постоянного тока — 300 Гц (в/ч)</li>
					<li>Фокусное пятно — 0,7 мм</li>
					<li>Анодное напряжение — 60 кВ</li>
					<li>Анодный ток — 7 мА</li>
				</ul>
				<p>
					 Рентгенаппарат с высокочастотным генератором 300 kHz и лучевой нагрузкой на 30% ниже,<br>
					 чем у других моделей. Не имеет аналогов по радиационной безопасности и удобству в работе.<br>
					 - Оснащён микропроцессорным таймером<br>
					 - Переключение режимов для работы с пленкой и радиовизиографом<br>
					 - Напряжение на рентгеновской трубке — 60 кВт, Сила тока — 7 мА<br>
					 - Система cтабилизации напряжения на генераторе.<br>
					 - Специальный режим понижения радиации на 90% при работе с радиовизиографом<br>
					 - Возможность синхронной работы с визиографами RVG HDS и RVG UI, производство снимка «нажатием одной кнопки»
				</p>
				<ul>
					<li>Оснащён микропроцессорным таймером</li>
					<li>Переключение режимов для работы с пленкой и радиовизиографом</li>
					<li>Напряжение на рентгеновской трубке — 60 кВт, Сила тока — 7 мА</li>
					<li>Система cтабилизации напряжения на генераторе.</li>
					<li>Специальный режим понижения радиации на 90% при работе с радиовизиографом</li>
					<li>Возможность синхронной работы с визиографами RVG HDS и RVG UI, производство снимка «нажатием одной кнопки»</li>
				</ul>
				<div class="btn-block">
 <span class="btn-lg btn btn-primary" data-event="jqm" data-param-id="22" data-name="ask">Задать вопрос</span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="slider3">
					<div class="slider__wrapper">
						<div class="slider__items">
							<div class="slider__item">
 <img src="/upload/dental/f2b0619b83a985c7c4c5a1b219ebbc92.jpg">
							</div>
							<div class="slider__item">
 <img src="/upload/dental/9d2f1557d3506abe06531559b4054231.jpg">
							</div>
						</div>
					</div>
 <a href="#" class="slider__control" data-slide="prev"></a> <a href="#" class="slider__control" data-slide="next"></a>
					<ol class="slider__indicators">
						<li data-slide-to="0"></li>
						<li data-slide-to="1"></li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row dental-catalog-block slider-left">
			<div class="col-md-6">
				<div class="slider4">
					<div class="slider__wrapper">
						<div class="slider__items">
							<div class="slider__item">
 <img src="/upload/dental/58eb46cae09064.25641329_rvg5200-1rgb.jpg">
								1
							</div>
							<div class="slider__item">
 <img src="/upload/dental/carestream-principal.jpg">
								2
							</div>
						</div>
					</div>
 <a href="#" class="slider__control" data-slide="prev"></a> <a href="#" class="slider__control" data-slide="next"></a>
					<ol class="slider__indicators">
						<li data-slide-to="2"></li>
						<li data-slide-to="3"></li>
					</ol>
				</div>
			</div>
			<div class="col-md-6">
				<h3>Цифровой радиовизиограф Carestream RVG5200</h3>
				<p>
 <strong>Опции:</strong>
				</p>
				<ul>
					<li>Реальное разрешение 14 пар линий/мм – не уступает по качеству снимка ни одному из конкурентов (кроме RVG6100 / RVG6500)</li>
					<li>Крепление кабеля к задней части датчика облегчает точное позиционирование</li>
					<li>Закругленные углы для удобства пациентов</li>
					<li>Гибкий USB кабель — для большего удобства использования с ПК и ноутбуками</li>
					<li>Отлично подходит для использования в клиниках с большим количеством установок</li>
				</ul>
				<p>
					 Радиовизиограф RVG 5100 работает с Kodak dental imaging software версии 6.12 и выше.
				</p>
				<p>
 <strong>Операционная система:</strong>
				</p>
				<p>
					 XP SP2/SP3 с DirectX 9.0c и выше, Windows 7 32 bits ПО для операционной системы Macintosh — по запросу.
				</p>
				<p>
 <strong>Минимальное требование к системе:&nbsp;</strong>
				</p>
				<p>
					 2 GHz Intel Duo Core, 2 GB RAM, Hard disk drive: 1.2 GB<br>
					 для инсталляции программы, 160 GB свободных для программы и данных.
				</p>
				<div class="btn-block">
 <span class="btn-lg btn btn-primary" data-event="jqm" data-param-id="22" data-name="ask">Задать вопрос</span>
				</div>
			</div>
		</div>
	</div>
	<div class="maxwidth-theme dental-tizers">
		<h2>Преимущества сотрудничества с ООО «Рентген Сервис»</h2>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"dental-flat",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "flat",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "aspro_allcorp2_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MEDIA_PROPERTY" => "",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SEARCH_PAGE" => "/search/",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SLIDER_PROPERTY" => "",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_RATING" => "N",
		"USE_SHARE" => "N"
	)
);?>
	</div>
	<div class="maxwidth-theme dental-proizvodstvo">
		<h2>Наше производство</h2>
		<div class="row">
			<div class="col-md-6">
				<p>
					 ООО «Рентген Сервис» – лидирующая компания в области радиационной безопасности и защиты от ионизирующего излучения. Профессиональную деятельность компания осуществляет на всей территории России, офис и склад расположены в г. Екатеринбурге. «<br>
					 Рентген Сервис» комплексно решает задачи заказчика по созданию рентгеновского кабинета: подбор помещения, проектирование, поставка рентгенозащитного оборудования, изделий и материалов для конструирования защиты.
				</p>
				<p>
					 В ООО «Рентген Сервис» можно приобрести:
				</p>
				<ul>
					<li>широкий спектр оборудования для рентгеновских кабинетов и фотолабораторий;</li>
					<li>аппараты для стоматологической рентгенодиагностики, не имеющие аналогов на рынке;</li>
					<li>рентгенозащитные изделия собственного производства по индивидуальному заказу;</li>
					<li>специализированные материалы для строительства, ремонта и монтажа защиты от излучения в рентгеновских боксах и камерах;</li>
					<li>рентгенозащитную одежду и принадлежности для рентген-кабинетов;</li>
					<li>расходные материалы для рентгенографии и многое другое.</li>
				</ul>
			</div>
			<div class="col-md-6">
				<div class="slider5">
					<div class="slider__wrapper">
						<div class="slider__items">
							<div class="slider__item">
 <img src="/upload/dental/c4179cf25310dbf2b3013cf1f7b79aa8.jpg">
							</div>
							<div class="slider__item">
 <img src="/upload/dental/cf1c68100ec3cb790f7a7a58ae4811e2.jpg">
							</div>
							<div class="slider__item">
 <img src="/upload/dental/508c0769cc978294229a7ee313fe77de.jpg">
							</div>
						</div>
					</div>
 <a href="#" class="slider__control" data-slide="prev"></a> <a href="#" class="slider__control" data-slide="next"></a>
					<ol class="slider__indicators">
						<li data-slide-to="0"></li>
						<li data-slide-to="1"></li>
						<li data-slide-to="2"></li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"front_map",
	Array(
		"API_KEY" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONTROLS" => array(),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:56.88735483552952;s:10:\"yandex_lon\";d:60.646808925288994;s:12:\"yandex_scale\";i:17;s:10:\"PLACEMARKS\";a:2:{i:0;a:3:{s:3:\"LON\";d:60.64718453718;s:3:\"LAT\";d:56.886668861784;s:4:\"TEXT\";s:23:\"ООО \"ПРОТЕКС\"\";}i:1;a:3:{s:3:\"LON\";d:60.68197377670207;s:3:\"LAT\";d:56.87744472688306;s:4:\"TEXT\";s:10:\"Склад\";}}}",
		"MAP_HEIGHT" => "500",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING")
	)
);?>
<div class="dental-index">
	<div class="maxwidth-theme dental-zapros dental-banner">
		<h2>Отправьте запрос на расчет заказа</h2>
		 <?$APPLICATION->IncludeComponent(
	"aspro:form.allcorp2",
	"dental-contacts",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CLOSE_BUTTON_CLASS" => "btn btn-primary",
		"CLOSE_BUTTON_NAME" => "Закрыть",
		"COMPONENT_TEMPLATE" => "dental-contacts",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DISPLAY_CLOSE_BUTTON" => "Y",
		"IBLOCK_ID" => "23",
		"IBLOCK_TYPE" => "aspro_allcorp2_form",
		"LICENCE_TEXT" => "btn btn-primary",
		"SEND_BUTTON_CLASS" => "btn btn-primary",
		"SEND_BUTTON_NAME" => "Отправить",
		"SHOW_LICENCE" => "Y",
		"SUCCESS_MESSAGE" => "Спасибо! Ваша заявка отправлена! В ближайшее время мы с вами свяжемся!"
	)
);?>
	</div>
</div>
	<script>
		$('.form-control').on('focus blur', function(e){
				$(this).parent()[(e.type=='focus' || $.trim($(this).val()).length)?'addClass':'removeClass']('input-focused');
		});
	</script>
	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
