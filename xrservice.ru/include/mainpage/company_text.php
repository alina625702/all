<br>
 <br>
<h3>О компании</h3>
<p>
	 ООО «Рентген Сервис» комплексно решает задачи заказчика по созданию рентгеновского кабинета: подбор помещения, проектирование, поставка рентгенозащитных изделий и материалов для конструирования защиты.
</p>
<p>
	 Компания имеет собственное производство и выпускает под маркой «ProteX»: рентгенозащитные двери, ворота, ставни, жалюзи, шторы, окна, панели, ширмы и шторки для интроскопов.
</p>
 <?$APPLICATION->IncludeComponent(
	"bitrix:highloadblock.list", 
	"company", 
	array(
		"BLOCK_ID" => "1",
		"CHECK_PERMISSIONS" => "N",
		"DETAIL_URL" => "",
		"FILTER_NAME" => "",
		"PAGEN_ID" => "page",
		"ROWS_PER_PAGE" => "",
		"COMPONENT_TEMPLATE" => "company",
		"SORT_FIELD" => "ID",
		"SORT_ORDER" => "DESC",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?><br>