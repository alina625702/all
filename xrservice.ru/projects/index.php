<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

global $USER;

if (!$USER->IsAdmin()) {

	if (!defined("ERROR_404"))
		define("ERROR_404", "Y");

	\CHTTP::setStatus("404 Not Found");

	if ($APPLICATION->RestartWorkarea())
	{
		require(\Bitrix\Main\Application::getDocumentRoot() . "/404.php");
		die();
	}

}

$APPLICATION->SetTitle("Проекты - Рентген Сервис");
?><?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"projects",
	Array(
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ARTICLES_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_articles"][0],
		"BLOG_TITLE" => "Комментарии",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "100000",
		"CACHE_TYPE" => "A",
		"CATALOG_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_catalog"]["aspro_allcorp2_catalog"][0],
		"CHECK_DATES" => "Y",
		"COMMENTS_COUNT" => "5",
		"COMPONENT_TEMPLATE" => "projects",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_BLOG_EMAIL_NOTIFY" => "Y",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_BRAND_PROP_CODE" => array(0=>"",1=>"TIZERS",2=>"",),
		"DETAIL_BRAND_USE" => "Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FB_APP_ID" => "",
		"DETAIL_FB_USE" => "Y",
		"DETAIL_FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"DETAIL_TEXT",3=>"DETAIL_PICTURE",4=>"",),
		"DETAIL_LINKED_TEMPLATE" => "linked",
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(0=>"AUTHOR",1=>"DATA",2=>"TASK_PROJECT",3=>"SITE",4=>"ORDERER",5=>"FORM_QUESTION",6=>"FORM_ORDER",7=>"LINK_COMPANY",8=>"LINK_PROJECTS",9=>"LINK_REVIEWS",10=>"LINK_TIZERS",11=>"LINK_SALE",12=>"LINK_STUDY",13=>"LINK_NEWS",14=>"LINK_ARTICLES",15=>"LINK_GOODS",16=>"LINK_FAQ",17=>"LINK_STAFF",18=>"LINK_SERVICES",19=>"link_team",20=>"PRICE",21=>"PRICEOLD",22=>"STATUS",23=>"ARTICLE",24=>"SUPPLIED",25=>"AGE",26=>"KARTOPR",27=>"HEIGHT",28=>"DEPTH",29=>"DEEP",30=>"GRUZ_STRELI",31=>"GRUZ",32=>"DIAGONAL",33=>"DLINA_STRELI",34=>"DLINA",35=>"CATEGORY",36=>"CLASS",37=>"CLIMAT_CLASS",38=>"KOL_FORMULA",39=>"USERS",40=>"EXTENSION",41=>"MARK_STEEL",42=>"MASS",43=>"MODEL",44=>"POWER",45=>"UPDATES",46=>"VOLUME",47=>"PROIZVODSTVO",48=>"SIZE",49=>"PLACE",50=>"RESOLUTION",51=>"LIGHTS_LOCATION",52=>"RECOMMEND",53=>"SERIES",54=>"SPEED",55=>"DURATION",56=>"TEMPERATURE",57=>"TYPE",58=>"TYPE_TUR",59=>"THICKNESS",60=>"MARK",61=>"INCREASE",62=>"COLOR",63=>"FREQUENCY",64=>"FREQUENCE",65=>"WIDTH_PROHOD",66=>"WIDTH_PROEZD",67=>"WIDTH",68=>"LANGUAGES",69=>"DOCUMENTS",70=>"PHOTOS",71=>"test",72=>"FORM_PROJECT",73=>"DOCUMENTS",74=>"PHOTOS",75=>"",),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_VK_API_ID" => "",
		"DETAIL_VK_USE" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_NAME" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENTS_TABLE_TYPE_VIEW" => "FROM_MODULE",
		"ELEMENT_TYPE_VIEW" => "element_1",
		"FAQ_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_faq"][0],
		"FB_TITLE" => "Facebook",
		"FILTER_FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "arProjectFilter",
		"FILTER_PROPERTY_CODE" => array(0=>"",1=>"",),
		"FORM_ID" => "",
		"FORM_ID_ORDER_SERVISE" => "",
		"GALLERY_TYPE" => "big",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "32",
		"IBLOCK_TYPE" => "aspro_allcorp2_content",
		"IMAGE_POSITION" => "left",
		"IMAGE_WIDE" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LINE_ELEMENT_COUNT" => "2",
		"LINE_ELEMENT_COUNT_LIST" => "3",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(0=>"NAME",1=>"PREVIEW_TEXT",2=>"PREVIEW_PICTURE",3=>"",),
		"LIST_PRODUCT_BLOCKS_ORDER" => "tizers,sale,news,desc,docs,company,char,gallery,projects,services,faq,reviews,staff,study,articles,dops,goods,comments",
		"LIST_PROPERTY_CODE" => array(0=>"",1=>"",),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"NEWS_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_news"][0],
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROJECTS_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_projects"][0],
		"REVIEWS_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_reviews"][0],
		"SALES_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_sales"][0],
		"SECTION_ELEMENTS_TYPE_VIEW" => "FROM_MODULE",
		"SEF_FOLDER" => "/projects/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array("news"=>"","section"=>"#SECTION_CODE_PATH#/","detail"=>"#SECTION_CODE_PATH#/#ELEMENT_CODE#/",),
		"SERVICES_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_services"][0],
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ADDITIONAL_TAB" => "N",
		"SHOW_DETAIL_LINK" => "Y",
		"SHOW_NEXT_ELEMENT" => "Y",
		"SHOW_SECTION_DESCRIPTION" => "Y",
		"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STAFF_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_staff"][0],
		"STRICT_SECTION_CHECK" => "N",
		"STUDY_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_catalog"]["aspro_allcorp2_timetable"][0],
		"S_ASK_QUESTION" => "",
		"S_ORDER_PROJECT" => "Заказать проект",
		"S_ORDER_SERVISE" => "Заказать проект",
		"TAB_DOPS_NAME" => "",
		"TIZERS_IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_allcorp2_content"]["aspro_allcorp2_front_tizers"][0],
		"T_ARTICLES" => "",
		"T_CHARACTERISTICS" => "",
		"T_CLIENTS" => "",
		"T_COMPANY" => "",
		"T_DOCS" => "",
		"T_FAQ" => "",
		"T_GALLERY" => "",
		"T_GOODS" => "",
		"T_NEWS" => "",
		"T_NEXT_LINK" => "Следующий проект",
		"T_PREV_LINK" => "",
		"T_PROJECTS" => "Похожие проекты",
		"T_REVIEWS" => "Отзыв клиента",
		"T_SERVICES" => "",
		"T_STUDY" => "",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "Y",
		"VK_TITLE" => "Вконтакте"
	)
);?> <br>
 <?$APPLICATION->IncludeComponent(
	"aspro:tabs.allcorp2", 
	"slider", 
	array(
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"FILTER_NAME" => "",
		"HIT_PROP" => "HIT",
		"IBLOCK_ID" => "37",
		"IBLOCK_TYPE" => "aspro_allcorp2_catalog",
		"NEWS_COUNT" => "20",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SHOW_SECTION" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"S_MORE_PRODUCT" => "Подробнее",
		"S_ORDER_PRODUCT" => "Заказать",
		"TITLE" => "Наши продукты",
		"COMPONENT_TEMPLATE" => "slider"
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>