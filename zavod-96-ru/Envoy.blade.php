@servers(['web' => 'prosto@80.87.108.135'])

@task('list', ['on' => 'web'])
    cd /var/www/html/zavod-96-ru
    git pull origin main
    php artisan down
    composer install --no-interaction --optimize-autoloader --no-dev
    php artisan migrate
    php artisan storage:link
    php artisan up
@endtask
