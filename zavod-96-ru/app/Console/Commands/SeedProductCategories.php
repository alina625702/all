<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Review;
use Illuminate\Console\Command;

class SeedProductCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:seed-product-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->seedArticles();
    }

    private function findParentCode ($parent_id) {
        $csvFile = file(public_path('csv') . '/stelz_categories.csv');
        $data = [];
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);
            $id = $item[0];
            $code = $item[5];

            if($parent_id == $id) return $code;

        }
    }

    private function seedCategories() {
        $csvFile = file(public_path('csv') . '/stelz_categories.csv');
        $data = [];
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);

            $id = $item[0];
            $parent_id = $item[1];
            $name = $item[2];
            $preview_image = $item[3];
            $description = $item[4];
            $code = $item[5];
            $detail_image = $item[6];

            $category = ProductCategory::where('slug', $code)->first();
            $category->detail_text = $description;

            // $category->title = $name;
            // $category->slug = $code;
            // $category->preview_image = 'products/' . $preview_image;
            // $category->detail_image = 'products/' . $detail_image;
            // $category->detail_text = $description;
            // print_r($parent_id);
            // if($parent_id != '') {
            //     $parent_code = $this->findParentCode($parent_id);
            //     $subCategory = ProductCategory::where('slug', $parent_code)->first();
            //     $category->parent_id = $subCategory->id;
            // }
        
            $category->save();

            echo ' - ' . $name . PHP_EOL;
        }
    }

    private function seedProducts() {
        $csvFile = file(public_path('csv') . '/stelz_products.csv');
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);

            $id = $item[0];
            $section_code = $item[1];
            $name = $item[2];
            $preview_text = $item[3];
            $detail_text = $item[4];
            $preview_image = $item[5];
            $detail_image = $item[6];
            $code = $item[7];
            $sort = $item[8];

            $product = new Product();

            $product->title = $name;
            $product->slug = $code;
            $product->preview_image = 'products/' . $preview_image;
            $product->detail_image = 'products/' . $detail_image;
            $product->preview_text = $preview_text;
            $product->detail_text = $detail_text;
            $product->sort = $sort;

            if($section_code != '') {
                $subCategory = ProductCategory::where('slug', $section_code)->first();
                $product->category_id = $subCategory->id;
            }
        
            $product->save();

            echo ' - ' . $name . PHP_EOL;
        }
    }

    private function seedPrices() {
        $csvFile = file(public_path('csv') . '/product_prices.csv');
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);

            $code = $item[0];
            $price = $item[1];

            echo $code . ' - ' . $price . PHP_EOL;
            if($price == '') $price = 0;


            $product = Product::where('slug', $code)->first();
            if($product) {
                $product->price = $price;
                $product->save();
            }
        }
    }

    private function seedReviews() {
        $csvFile = file(public_path('csv') . '/stelz_reviews.csv');
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);

            $title = $item[0];
            $preview_image = $item[1];
            $detail_image = $item[2];
            $region = $item[3];

            $review = new Review();

            $review->title = $title;
            $review->preview_image = $preview_image;
            $review->detail_image = $detail_image;
            $review->region = $region;

            echo $title . PHP_EOL;

            $review->save();

        }
    }

    private function seedArticles() {
        $csvFile = file(public_path('csv') . '/stelz_articles.csv');
        foreach ($csvFile as $line) {
            $item = str_getcsv($line);

            $title = $item[0];
            $published_at = $item[1];
            $preview_image = $item[2];
            $detail_image = $item[3];
            $detail_text = $item[4];
            $slug = $item[5];

            $article = new Article();

            $article->title = $title;
            $article->preview_image = $preview_image;
            $article->detail_image = $detail_image;
            $article->published_at = \Carbon\Carbon::parse($published_at)->format('Y-m-d');
            $article->detail_text = $detail_text;
            $article->slug = $slug;

            echo $title . PHP_EOL;

            $article->save();

        }
    }
}
