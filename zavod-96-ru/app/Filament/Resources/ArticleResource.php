<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ArticleResource\Pages;
use App\Filament\Resources\ArticleResource\RelationManagers;
use App\Models\Article;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Forms\Components\Tabs;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;

class ArticleResource extends Resource
{
    protected static ?string $model = Article::class;
    protected static ?string $navigationIcon = 'heroicon-o-document-text';
    protected static ?string $navigationGroup = 'Контент';
    protected static ?string $navigationLabel = 'Статьи';
    protected static ?string $modelLabel = 'Статьи';
    protected static ?string $pluralModelLabel = 'Статьи';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('Tabs')
                    ->tabs([
                        Tabs\Tab::make('Информация')
                            ->schema([
                                Forms\Components\TextInput::make('title')->required()->label('Название'),
                                Forms\Components\TextInput::make('slug')->required()->label('Код'),
                                Forms\Components\DatePicker::make('published_at')->default(now())->required()->label('Дата публикации'),
                            ]),
                        Tabs\Tab::make('Изображения')
                            ->schema([
                                Forms\Components\FileUpload::make('preview_image')->directory('articles')->label('Анонс изображения'),
                                Forms\Components\FileUpload::make('detail_image')->directory('articles')->label('Детальное изображение'),
                            ])->columns(2),
                        Tabs\Tab::make('Контент')
                            ->schema([
                                TinyEditor::make('preview_text')->profile('custom')->maxHeight(400),
                                TinyEditor::make('detail_text')->profile('custom')->maxHeight(800),
                            ]),
                        Tabs\Tab::make('SEO')
                            ->schema([
                                Forms\Components\TextInput::make('seo_title')->label('Seo title'),
                                Forms\Components\TextInput::make('seo_description')->label('Seo description'),
                            ])->columns(2),
                    ])
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')->label('Название')->searchable(),
                Tables\Columns\TextColumn::make('published_at')->dateTime('d.m.Y')->label('Дата публикации'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListArticles::route('/'),
            'create' => Pages\CreateArticle::route('/create'),
            'edit' => Pages\EditArticle::route('/{record}/edit'),
        ];
    }
}
