<?php

namespace App\Filament\Resources;

use App\Filament\Resources\BlueprintResource\Pages;
use App\Filament\Resources\BlueprintResource\RelationManagers;
use App\Models\Blueprint;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class BlueprintResource extends Resource
{
    protected static ?string $model = Blueprint::class;

    protected static ?string $navigationIcon = 'heroicon-o-building-storefront';
    protected static ?string $navigationLabel = 'Чертежи';
    protected static ?string $navigationGroup = 'Контент';

    protected static ?string $modelLabel = 'Чертежи';
    protected static ?string $pluralModelLabel = 'Чертежи';

    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Forms\Components\TextInput::make('title')->required()->label('Название'),
            Forms\Components\Group::make([
                Forms\Components\FileUpload::make('preview_image')->directory('reviews')->required()->label('Предпросмотр изображения'),
                Forms\Components\FileUpload::make('detail_image')->directory('reviews')->required()->label('Детальное изображение'),
            ])->columns(2),
        ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')->label('Название'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBlueprints::route('/'),
            'create' => Pages\CreateBlueprint::route('/create'),
            'edit' => Pages\EditBlueprint::route('/{record}/edit'),
        ];
    }
}
