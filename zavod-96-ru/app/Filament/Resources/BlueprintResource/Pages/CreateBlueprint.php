<?php

namespace App\Filament\Resources\BlueprintResource\Pages;

use App\Filament\Resources\BlueprintResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateBlueprint extends CreateRecord
{
    protected static string $resource = BlueprintResource::class;
}
