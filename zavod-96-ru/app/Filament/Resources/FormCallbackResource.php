<?php

namespace App\Filament\Resources;

use App\Filament\Resources\FormCallbackResource\Pages;
use App\Filament\Resources\FormCallbackResource\RelationManagers;
use App\Models\FormCallback;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class FormCallbackResource extends Resource
{
    protected static ?string $model = FormCallback::class;

    protected static ?string $navigationIcon = 'heroicon-o-chat-bubble-bottom-center';
    protected static ?string $navigationLabel = 'Обратный звонок';
    protected static ?string $navigationGroup = 'Заявки';
    protected static ?string $modelLabel = 'Обратный звонок';
    protected static ?string $pluralModelLabel = 'Обратный звонок';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')->required()->label('ФИО'),
                Forms\Components\TextInput::make('phone')->required()->label('Телефон'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('created_at')->dateTime('d.m.Y H:i:s'),
                Tables\Columns\TextColumn::make('name')->label('ФИО'),
                Tables\Columns\TextColumn::make('phone')->label('Телефон'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFormCallbacks::route('/'),
            'create' => Pages\CreateFormCallback::route('/create'),
            'edit' => Pages\EditFormCallback::route('/{record}/edit'),
        ];
    }
}
