<?php

namespace App\Filament\Resources\FormCallbackResource\Pages;

use App\Filament\Resources\FormCallbackResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFormCallback extends CreateRecord
{
    protected static string $resource = FormCallbackResource::class;
}
