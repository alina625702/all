<?php

namespace App\Filament\Resources\FormCallbackResource\Pages;

use App\Filament\Resources\FormCallbackResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditFormCallback extends EditRecord
{
    protected static string $resource = FormCallbackResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
