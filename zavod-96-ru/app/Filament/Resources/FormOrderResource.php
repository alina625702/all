<?php

namespace App\Filament\Resources;

use App\Filament\Resources\FormOrderResource\Pages;
use App\Filament\Resources\FormOrderResource\RelationManagers;
use App\Models\FormOrder;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class FormOrderResource extends Resource
{
    protected static ?string $model = FormOrder::class;
    protected static ?string $navigationIcon = 'heroicon-o-chat-bubble-bottom-center';
    protected static ?string $navigationLabel = 'Онлайн-заявка';
    protected static ?string $navigationGroup = 'Заявки';
    protected static ?string $modelLabel = 'Онлайн-заявки';
    protected static ?string $pluralModelLabel = 'Онлайн-заявки';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')->required()->label('ФИО'),
                Forms\Components\TextInput::make('phone')->required()->label('Телефон'),
                Forms\Components\TextInput::make('email')->label('Email'),
                Forms\Components\TextInput::make('message')->label('Сообщение'),
                Forms\Components\TextInput::make('region')->label('Регион'),
                Forms\Components\TextInput::make('company')->label('Компания'),                
                Forms\Components\TextInput::make('product_name')->label('Продукт'),
                Forms\Components\FileUpload::make('filename')->directory('forms')->label('Файл'),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([ 
                Tables\Columns\TextColumn::make('created_at')->dateTime('d.m.Y H:i:s'),
                Tables\Columns\TextColumn::make('name')->label('ФИО'),
                Tables\Columns\TextColumn::make('email')->label('Email'),
                Tables\Columns\TextColumn::make('phone')->label('Телефон'),
                Tables\Columns\TextColumn::make('product_name')->label('Продукт'),
                Tables\Columns\TextColumn::make('region')->label('Регион'),
                Tables\Columns\TextColumn::make('company')->label('Компания'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFormOrders::route('/'),
            'create' => Pages\CreateFormOrder::route('/create'),
            'edit' => Pages\EditFormOrder::route('/{record}/edit'),
        ];
    }
}
