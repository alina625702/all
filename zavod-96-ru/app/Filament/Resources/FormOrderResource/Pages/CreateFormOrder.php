<?php

namespace App\Filament\Resources\FormOrderResource\Pages;

use App\Filament\Resources\FormOrderResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateFormOrder extends CreateRecord
{
    protected static string $resource = FormOrderResource::class;
}
