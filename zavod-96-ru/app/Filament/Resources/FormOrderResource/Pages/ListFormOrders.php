<?php

namespace App\Filament\Resources\FormOrderResource\Pages;

use App\Filament\Resources\FormOrderResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListFormOrders extends ListRecords
{
    protected static string $resource = FormOrderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
