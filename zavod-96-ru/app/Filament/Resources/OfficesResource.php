<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OfficesResource\Pages;
use App\Filament\Resources\OfficesResource\RelationManagers;
use App\Models\Office;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OfficesResource extends Resource
{
    protected static ?string $model = Office::class;

    protected static ?string $navigationIcon = 'heroicon-o-building-office';

    protected static ?string $navigationLabel = 'Офисы';
    protected static ?string $navigationGroup = 'Контент';

    protected static ?string $modelLabel = 'Офисы';
    protected static ?string $pluralModelLabel = 'Офисы';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')->required()->label('Название'),
                Forms\Components\TextInput::make('address')->required()->label('Адрес'),
                Forms\Components\TextInput::make('phone')->label('Телефон'),
                Forms\Components\TextInput::make('ext_phone')->label('Доп. Телефон'),
                Forms\Components\TextInput::make('email')->email()->label('Email'),
                Forms\Components\TextInput::make('work_time')->label('Рабочее время'),
                Forms\Components\TextInput::make('coords')->label('Координаты'),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')->label('Название'),
                Tables\Columns\TextColumn::make('address')->label('Адрес'),
                Tables\Columns\TextColumn::make('phone')->label('Телефон'),
                Tables\Columns\TextColumn::make('ext_phone')->label('Доп. Телефон'),
                Tables\Columns\TextColumn::make('email')->label('Email'),
                Tables\Columns\TextColumn::make('work_time')->label('Рабочее время'),
                Tables\Columns\TextColumn::make('coords')->label('Координаты'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOffices::route('/'),
            'create' => Pages\CreateOffices::route('/create'),
            'edit' => Pages\EditOffices::route('/{record}/edit'),
        ];
    }

}
