<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductCategoryResource\Pages;
use App\Filament\Resources\ProductCategoryResource\RelationManagers;
use App\Filament\Resources\ProductCategoryResource\RelationManagers\ProductCategoriesRelationManager;
use App\Models\ProductCategory;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Forms\Components\Tabs;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use CodeWithDennis\FilamentSelectTree\SelectTree;
use Filament\Tables\Columns\ImageColumn;
use Filament\Support\Enums\Alignment;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;



class ProductCategoryResource extends Resource
{
    protected static ?string $model = ProductCategory::class;
    protected static ?string $navigationGroup = 'Продукция';
    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';
    protected static ?string $navigationLabel = 'Категории продукции';
    protected static ?string $modelLabel = 'Категории продукции';
    protected static ?string $pluralModelLabel = 'Категории продукции';


    public static function form(Form $form): Form
    {

        return $form
            ->schema([
                Tabs::make('Tabs')
                    ->tabs([
                        Tabs\Tab::make('Информация')
                            ->schema([
                                Forms\Components\TextInput::make('title')->required()->label('Название'),
                                Forms\Components\TextInput::make('slug')->required()->label('Код'),
                                Forms\Components\TextInput::make('sort')->numeric()->inputMode('decimal')->required()->label('Сортировка'),
                                SelectTree::make('parent_id')
                                    ->enableBranchNode()
                                    ->placeholder(__('Выберите категорию'))
                                    ->withCount()
                                    ->relationship('parent', 'title', 'parent_id', function ($query) {
                                        return $query;
                                    })->label('Родительская категория'),
                            ]),
                        Tabs\Tab::make('Изображения')
                            ->schema([
                                Forms\Components\FileUpload::make('preview_image')->directory('products')->label('Анонс изображения'),
                                Forms\Components\FileUpload::make('detail_image')->directory('products')->label('Детальное изображение'),
                            ])->columns(2),
                        Tabs\Tab::make('Контент')
                            ->schema([
                                TinyEditor::make('preview_text')->profile('custom')->maxHeight(400),
                                TinyEditor::make('detail_text')->profile('custom')->maxHeight(800),
                            ]),
                        
                        Tabs\Tab::make('SEO')
                            ->schema([
                                Forms\Components\TextInput::make('seo_title')->label('Seo title'),
                                Forms\Components\TextInput::make('seo_description')->label('Seo description'),
                            ])->columns(2),
                    ])
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('preview_image')->label('Изображение')->square()->alignment(Alignment::Center),
                Tables\Columns\TextColumn::make('title')->label('Название')->searchable(),
                Tables\Columns\TextColumn::make('parent.title')->label('Родительская категория'),
                Tables\Columns\TextColumn::make('sort')->label('Сортировка'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            ProductCategoriesRelationManager::class,
        ];
}

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProductCategories::route('/'),
            'create' => Pages\CreateProductCategory::route('/create'),
            'edit' => Pages\EditProductCategory::route('/{record}/edit'),
        ];
    }
}