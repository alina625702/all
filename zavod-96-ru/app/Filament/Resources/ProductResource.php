<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductResource\Pages;
use App\Filament\Resources\ProductResource\RelationManagers;
use App\Models\Product;
use Filament\Forms;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Forms\Components\Tabs;
use CodeWithDennis\FilamentSelectTree\SelectTree;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Columns\ImageColumn;
use Filament\Support\Enums\Alignment;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;


class ProductResource extends Resource
{
    protected static ?string $model = Product::class;
    protected static ?string $navigationGroup = 'Продукция';
    protected static ?string $navigationIcon = 'heroicon-o-circle-stack';
    protected static ?string $navigationLabel = 'Товары';
    protected static ?string $modelLabel = 'Товары';
    protected static ?string $pluralModelLabel = 'Товары';
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('Tabs')
                    ->tabs([
                        Tabs\Tab::make('Информация')
                            ->schema([
                                Forms\Components\TextInput::make('title')->required()->label('Название'),
                                Forms\Components\TextInput::make('slug')->required()->label('Код'),
                                Forms\Components\TextInput::make('price')->numeric()->inputMode('decimal')->required()->label('Цена'),
                                Forms\Components\TextInput::make('sort')->numeric()->inputMode('decimal')->required()->label('Сортировка'),
                                Forms\Components\Toggle::make('favorite')->label('Избранное'),
                                SelectTree::make('category_id')
                                    ->relationship('category', 'title', 'parent_id', function ($query) {
                                        // dd($query->toSql());
                                        return $query;
                                    })->label('Категория'),
                            ]),
                        Tabs\Tab::make('Изображения')
                            ->schema([
                                Forms\Components\FileUpload::make('preview_image')->directory('products')->label('Анонс изображения'),
                                Forms\Components\FileUpload::make('detail_image')->directory('products')->label('Детальное изображение'),
                            ])->columns(2),
                        Tabs\Tab::make('Контент')
                            ->schema([
                                TinyEditor::make('preview_text')->profile('custom')->maxHeight(400),
                                TinyEditor::make('detail_text')->profile('custom')->maxHeight(800),
                            ]),
                        Tabs\Tab::make('SEO')
                            ->schema([
                                Forms\Components\TextInput::make('seo_title')->label('Seo title'),
                                Forms\Components\TextInput::make('seo_description')->label('Seo description'),
                            ])->columns(2),
                    ])
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('preview_image')->label('Изображение')->square()->alignment(Alignment::Center),
                Tables\Columns\TextColumn::make('title')->label('Название')->searchable(),
                Tables\Columns\TextColumn::make('price')->label('Цена'),
                Tables\Columns\TextColumn::make('category.title')->label('Категория'),
                Tables\Columns\TextColumn::make('sort')->label('Сортировка'),
                Tables\Columns\ToggleColumn::make('favorite')->label('Избранное')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProducts::route('/'),
            'create' => Pages\CreateProduct::route('/create'),
            'edit' => Pages\EditProduct::route('/{record}/edit'),
        ];
    }
}
