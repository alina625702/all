<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function index() {
        $pageTitle = 'Статьи';
        $articles = Article::query()->orderBy('published_at', 'desc')->paginate(16);

        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('article.index', compact(['pageTitle', 'articles', 'breadcrumb_items']));
    }

    public function show($slug) {
        
        $article = Article::where('slug', $slug)->firstOrFail();
        $pageTitle = $article->title;

        $breadcrumb_items = [
            [ 'title' => 'Статьи', 'route' => 'article.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('article.show', compact(['pageTitle', 'article', 'breadcrumb_items']));
    }
}
