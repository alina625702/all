<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public $component_title = 'Каталог продукции';

    public function index() {
        $pageTitle = $this->component_title;

        $categories = ProductCategory::query()->whereNull('parent_id')->orderBy('sort', 'asc')->get();
        $favorite_products = Product::query()->where('favorite', true)->get();

        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('catalog.index', compact(['pageTitle','breadcrumb_items', 'categories', 'favorite_products']));
    }

    public function category($category)
    {
        $currentCategory = ProductCategory::where('slug', $category)->firstOrFail();
        $products = Product::where('category_id', $currentCategory->id)->orderBy('sort', 'asc')->get();
        
        $pageTitle = $currentCategory->title;
        
        $categoriesMenu = $this->getCategoriesTree($currentCategory->slug);

        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'catalog.index', 'current' => false],            
        ];

        $path = $this->findCategoryPath($currentCategory->slug);

        foreach ($path as $slug) {
            if($slug == end($path)) break;
            $pathCategody = ProductCategory::where('slug', $slug)->firstOrFail();
            $breadcrumb_items[] = [ 'title' => $pathCategody->title, 'route' => 'catalog.category', 'route_params' => [$slug], 'current' => false];
        }

        $breadcrumb_items[] = [ 'title' => $pageTitle, 'current' => true ];

        return view('catalog.category', compact([
            'currentCategory',
            'products', 
            'categoriesMenu', 
            'pageTitle',
            'breadcrumb_items'
        ]));   
    }

    public function product($category, $product)
    {
        $product = Product::where('slug', $product)->firstOrFail();
        $pageTitle = $product->title;
        $favorite_products = Product::query()->where('favorite', true)->get();

        $currentCategory = ProductCategory::where('slug', $category)->firstOrFail();

        $categoriesMenu = $this->getCategoriesTree($currentCategory->slug);

        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'catalog.index', 'current' => false],            
        ];

        $path = $this->findCategoryPath($currentCategory->slug);

        foreach ($path as $slug) {
            $pathCategody = ProductCategory::where('slug', $slug)->firstOrFail();
            $breadcrumb_items[] = [ 'title' => $pathCategody->title, 'route' => 'catalog.category', 'route_params' => [$slug], 'current' => false];
        }

        $breadcrumb_items[] = [ 'title' => $pageTitle, 'current' => true ];

        return view('catalog.product', compact([
            'product',
            'favorite_products', 
            'categoriesMenu', 
            'pageTitle',
            'breadcrumb_items'
        ]));

    }

    private function getCategoriesTree($slug)
    {
        $path = $this->findCategoryPath($slug);

        // Получаем все категории без родителей (верхний уровень)
        $topCategories = ProductCategory::whereNull('parent_id')->get();

        // Создаем массив для древовидного меню
        $menu = [];

        // Рекурсивная функция для построения дерева
        $buildTree = function ($categories) use (&$buildTree, $path) {
            $tree = [];

            foreach ($categories as $category) {
                $node = $category->toArray();
                $node['children'] = $buildTree($category->children);
                $node['active'] = in_array($category->slug, $path);
                $tree[] = $node;
            }

            return $tree;
        };

        // Перебираем верхние категории и строим древовидное меню
        foreach ($topCategories as $topCategory) {
            $menu[] = [
                'id' => $topCategory->id,
                'title' => $topCategory->title,
                'slug' => $topCategory->slug,
                'active' => in_array($topCategory->slug, $path),
                'children' => $buildTree($topCategory->children),
            ];
        }

        return $menu;
    }

    public function findCategoryPath($slug)
    {
        // Находим категорию по slug
        $category = ProductCategory::where('slug', $slug)->first();

        // Если категория не найдена, можно вернуть, например, ошибку или пустой массив
        if (!$category) {
            return ['error' => 'Category not found.'];
        }

        // Рекурсивная функция для построения пути
        $buildPath = function ($category) {
            $path = [];

            while ($category->parent) {
                $path[] = $category->parent->slug;
                $category = $category->parent;
            }

            return array_reverse($path);
        };

        // Строим путь
        $categoryPath = $buildPath($category);

        // Теперь $categoryPath содержит путь в виде массива
        $categoryPath[] = $slug;
        return $categoryPath;
    }
}
