<?php

namespace App\Http\Controllers;

use App\Models\License;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Models\Review;

class CompanyController extends Controller
{
    public $component_title = 'О компании';

    public function index() {
        $pageTitle = $this->component_title;
        
        $categories = ProductCategory::query()->whereNull('parent_id')->orderBy('sort', 'asc')->get();
        $licenses = License::query()->get();

        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.index', compact(['pageTitle', 'breadcrumb_items', 'categories', 'licenses']));
    }

    public function review() {
        $pageTitle = 'Отзывы';
        $reviews = Review::paginate(12);

        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'company.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.review', compact(['pageTitle', 'reviews', 'breadcrumb_items']));
    }

    public function payment() {
        $pageTitle = 'Информация об оплате';

        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'company.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.payment', compact(['pageTitle','breadcrumb_items']));
    }

    public function delivery() {
        $pageTitle = 'Информация о доставке';

        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'company.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.delivery', compact(['pageTitle','breadcrumb_items']));
    }

    public function requisites() {
        $pageTitle = 'Реквизиты';
        
        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'company.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.requisites', compact(['pageTitle','breadcrumb_items']));
    }

    public function licenses() {
        $pageTitle = 'Лицензии и сертификаты';
        $licenses = License::query()->get();
        $breadcrumb_items = [
            [ 'title' => $this->component_title, 'route' => 'company.index', 'current' => false],
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('company.licenses', compact(['pageTitle', 'breadcrumb_items', 'licenses']));
    }

    public function price() {
        $pageTitle = 'Прайс-лист';
        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        $priceArray = ProductCategory::getCategoryTree();

        return view('company.price', compact(['pageTitle', 'breadcrumb_items', 'priceArray']));
    }
}
