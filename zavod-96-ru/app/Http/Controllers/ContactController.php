<?php

namespace App\Http\Controllers;

use App\Models\Office;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ContactController extends Controller
{
    /**
     * Display contacts page.
     */
    public function index(): View
    {
        $pageTitle = 'Контакты';
        $offices = Office::query()->get();
        $coords = Setting::getValue('coords');

        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('contact.index', compact(['offices', 'coords', 'pageTitle', 'breadcrumb_items']));
    }
}
