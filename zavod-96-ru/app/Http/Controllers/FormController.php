<?php

namespace App\Http\Controllers;

use App\Models\FormCallback;
use App\Models\FormOrder;
use App\Models\FormQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\AmoCRMModel;

class FormController extends Controller
{
    public function callback(Request $request) {
        $validator = Validator::make($request->all(), FormCallback::$rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
        $callback = new FormCallback($request->all());
        $callback->save();

        $result = AmoCRMModel::sendDataToAmoCRM($callback);
        dd($callback);
        return $callback;

    }

    public function order(Request $request) {
        $validator = Validator::make($request->all(), FormOrder::$rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $callback = new FormOrder($request->all());

        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $uniqueFilename = $file->hashName();
            $filePath = $file->storeAs('public/forms', $uniqueFilename);
            $callback->filename = 'forms/' . $uniqueFilename;
        }
        
        $callback->save();
        AmoCRMModel::sendDataToAmoCRM($callback);
        return $callback;
    }

    public function question(Request $request) {
        $validator = Validator::make($request->all(), FormQuestion::$rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $callback = new FormQuestion($request->all());

        if ($request->hasFile('filename')) {
            $file = $request->file('filename');
            $uniqueFilename = $file->hashName();
            $filePath = $file->storeAs('public/forms', $uniqueFilename);
            $callback->filename = 'forms/' . $uniqueFilename;
        }
        
        $callback->save();
        AmoCRMModel::sendDataToAmoCRM($callback);
        return $callback;
    }
}
