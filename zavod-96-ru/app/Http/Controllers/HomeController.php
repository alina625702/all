<?php

namespace App\Http\Controllers;

use App\Models\License;
use App\Models\Review;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        $pageTitle = 'Главная';
        $reviews = Review::latest()->take(8)->get();
        $licenses = License::latest()->take(3)->get();
        return view('index', compact(['pageTitle', 'reviews', 'licenses']));
    }

    public function privacy() {
        $pageTitle = 'Политика конфиденциальности';
        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('policy', compact(['pageTitle','breadcrumb_items']));
    }
}
