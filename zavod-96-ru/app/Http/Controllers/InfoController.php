<?php

namespace App\Http\Controllers;

use App\Models\Blueprint;
use Illuminate\Http\Request;
use App\Models\Questionnaire;

class InfoController extends Controller
{
    public function questionnaire() {
        $pageTitle = 'Опросные листы';
        $questionnaires = Questionnaire::query()->get();
        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('info.questionnaire', compact(['pageTitle','breadcrumb_items', 'questionnaires']));
    }

    public function blueprint() {
        $pageTitle = 'Чертежи';
        $blueprints = Blueprint::query()->get();
        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        return view('info.blueprint', compact(['pageTitle','breadcrumb_items', 'blueprints']));
    }
}
