<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->input('search');

        $pageTitle = 'Поиск';
        $breadcrumb_items = [
            [ 'title' => $pageTitle, 'current' => true ],
        ];

        if($query != '') {
            $products = Product::where('title', 'LIKE', '%' . $query . '%')
                ->orWhere('detail_text', 'LIKE', '%' . $query . '%')
                ->orWhere('preview_text', 'LIKE', '%' . $query . '%')
                ->get();

            $categories = ProductCategory::where('title', 'LIKE', '%' . $query . '%')
                ->orWhere('detail_text', 'LIKE', '%' . $query . '%')
                ->orWhere('preview_text', 'LIKE', '%' . $query . '%')
                ->get();

            $articles = Article::where('title', 'LIKE', '%' . $query . '%')
                ->orWhere('detail_text', 'LIKE', '%' . $query . '%')
                ->orWhere('preview_text', 'LIKE', '%' . $query . '%')
                ->get();
        }
        
        else {
            $products = $categories = $articles = [];
        }

        return view('search.index', compact(
            'pageTitle',
            'breadcrumb_items',
            'products', 
            'categories', 
            'articles'
        ));
    }
}
