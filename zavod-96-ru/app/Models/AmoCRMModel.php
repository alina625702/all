<?php

namespace App\Models;
use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Models\TagModel;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Models\ContactModel;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Models\NoteType\CommonNote;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use Illuminate\Database\Eloquent\Model;
use League\OAuth2\Client\Token\AccessTokenInterface;


class AmoCRMModel
{
    public static function sendDataToAmoCRM(Model $callback)
    {

        include_once __DIR__ . '/../../bootstrap/amo/bootstrap.php';

        // Ответственный
        $responsible = 6033397;

        // ID воронки
        $pipelineId = 1170511;

        // ID параметров
        $utmSourceFieldId = 435747;
        $utmMediumFieldId = 495977;
        $utmCampaignFieldId = 495979;
        $utmContentFieldId = 918979;
        $utmTermFieldId = 495981;
        $yidFieldId = 918983;
        $productNameFieldId = 464775;
        $regionFieldId = 484511;
        

        // Название лида
        $leadName = 'Заявка с сайта';

        // Получаем UTM метки
        $utmMedium = isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : '';
        $utmTerm = isset($_COOKIE['utm_term']) ? $_COOKIE['utm_term'] : '';
        $utmSource = isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : '';
        $utmContent = isset($_COOKIE['utm_content']) ? $_COOKIE['utm_content'] : '';
        $utmCampaign = isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '';
        $yid = isset($_COOKIE['_ym_uid']) ? $_COOKIE['_ym_uid'] : '';
        $imz = isset($_COOKIE['__imz']) ? $_COOKIE['__imz'] : '';

        // Получаем органические метки
        if ($utmMedium == '' && $utmSource == '') {
            $pattern = "/utmcsr=([^|]+)/";
            preg_match($pattern, $imz, $utmcsrMatches);

            $pattern = "/utmcmd=([^|]+)/";
            preg_match($pattern, $imz, $utmcmdMatches);

            if (count($utmcsrMatches) > 1) {
                $utmcsrValue = urldecode($utmcsrMatches[1]);
                $utmcsrValue = str_replace("(", "", $utmcsrValue);
                $utmcsrValue = str_replace(")", "", $utmcsrValue);
                $utmSource = $utmcsrValue;
            }

            if (count($utmcmdMatches) > 1) {
                $utmcmdValue = urldecode($utmcmdMatches[1]);
                $utmcmdValue = str_replace("(", "", $utmcmdValue);
                $utmcmdValue = str_replace(")", "", $utmcmdValue);
                $utmMedium = $utmcmdValue;
            }

            if ($utmSource == 'direct')
                $utmMedium = 'direct';
        }

        // Получаем токен
        $accessToken = getToken();

        $apiClient->setAccessToken($accessToken)
            ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
            ->onAccessTokenRefresh(
                function (AccessTokenInterface $accessToken, string $baseDomain) {
                    saveToken([
                        'accessToken' => $accessToken->getToken(),
                        'refreshToken' => $accessToken->getRefreshToken(),
                        'expires' => $accessToken->getExpires(),
                        'baseDomain' => $baseDomain,
                    ]);
                }
        );

        $leadsService = $apiClient->leads();

        // Определяем или добавляем контакт
        try {
            $contacts = $apiClient->contacts()->get((new ContactsFilter())->setQuery($callback->phone));
            $contact = $contacts[0];
        } catch (AmoCRMApiException $e) {
            $contact = new ContactModel();
            $contact->setName($callback->name);

            $CustomFieldsValues = new CustomFieldsValuesCollection();

            $emailField = (new MultitextCustomFieldValuesModel())->setFieldCode('EMAIL');
            $emailField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($callback->email)));

            $phoneField = (new MultitextCustomFieldValuesModel())->setFieldCode('PHONE');
            $phoneField->setValues((new MultitextCustomFieldValueCollection())->add((new MultitextCustomFieldValueModel())->setEnum('WORK')->setValue($callback->phone)));

            $CustomFieldsValues->add($emailField);
            $CustomFieldsValues->add($phoneField);

            $contact->setCustomFieldsValues($CustomFieldsValues);
            $contact->setResponsibleUserId($responsible);

            try {
                $contactModel = $apiClient->contacts()->addOne($contact);
            } catch (AmoCRMApiException $e) {
                printError($e);
                die;
            }
        }

        // Создаем сделку
        $lead = new LeadModel();
        $lead->setName($leadName)->setContacts((new ContactsCollection())->add(($contact)));
        $CustomFieldsValues = new CustomFieldsValuesCollection();

        // UTM метки
        $utmSourceField = (new TextCustomFieldValuesModel())->setFieldId($utmSourceFieldId);
        $utmSourceField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmSource)));
        $CustomFieldsValues->add($utmSourceField);

        $utmMediumField = (new TextCustomFieldValuesModel())->setFieldId($utmMediumFieldId);
        $utmMediumField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmMedium)));
        $CustomFieldsValues->add($utmMediumField);

        $utmTermField = (new TextCustomFieldValuesModel())->setFieldId($utmTermFieldId);
        $utmTermField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmTerm)));
        $CustomFieldsValues->add($utmTermField);

        $utmContentField = (new TextCustomFieldValuesModel())->setFieldId($utmContentFieldId);
        $utmContentField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmContent)));
        $CustomFieldsValues->add($utmContentField);

        $utmCampaignField = (new TextCustomFieldValuesModel())->setFieldId($utmCampaignFieldId);
        $utmCampaignField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($utmCampaign)));
        $CustomFieldsValues->add($utmCampaignField);

        $yidField = (new TextCustomFieldValuesModel())->setFieldId($yidFieldId);
        $yidField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($yid)));
        $CustomFieldsValues->add($yidField);

        // Продукт
        if(isset($callback->product_name)) {
            $productNameField = (new TextCustomFieldValuesModel())->setFieldId($productNameFieldId);
            $productNameField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($callback->product_name)));
            $CustomFieldsValues->add($productNameField);
        }
        // Регион
        if(isset($callback->region)) {
            $regionField = (new TextCustomFieldValuesModel())->setFieldId($regionFieldId);
            $regionField->setValues((new TextCustomFieldValueCollection())->add((new TextCustomFieldValueModel())->setValue($callback->region)));
            $CustomFieldsValues->add($regionField);
        }

        // Цена
        if(isset($callback->price)) {
            $lead->setPrice($price);
        }

        $lead->setCustomFieldsValues($CustomFieldsValues);

        $lead->setResponsibleUserId($responsible);

        // Установка тегов
        $lead->setTags((new TagsCollection())
            ->add(
                (new TagModel())
                    ->setName('Заявка с сайта')
        ));

        $lead->setTags((new TagsCollection())
            ->add(
                (new TagModel())
                    ->setName('zavod-96.ru')
        ));

        $lead->setPipelineId($pipelineId);

        $leadsCollection = new LeadsCollection();
        $leadsCollection->add($lead);

        try {
            $leadsCollection = $leadsService->add($leadsCollection);
            $lead_id = $leadsCollection[0]->id;

            // Прикрепляем сообщение
            if (isset($callback->message) && $callback->message != '') {
                $notesCollection = new NotesCollection();
                $serviceMessageNote = new CommonNote();
                $serviceMessageNote->setEntityId($lead_id)->setText($callback->description);

                $notesCollection->add($serviceMessageNote);

                try {
                    $leadNotesService = $apiClient->notes(EntityTypesInterface::LEADS);
                    $notesCollection = $leadNotesService->add($notesCollection);
                } catch (AmoCRMApiException $e) {
                    printError($e);
                    die;
                }
            }

            // Прикрепляем файл
            if (isset($callback->filename)) {
                $notesCollection = new NotesCollection();
                $serviceMessageNote = new CommonNote();
                $serviceMessageNote->setEntityId($lead_id)->setText('Прикрепленный файл: https://zavod-96.ru/uploads/' .  $callback->filename);
                $notesCollection->add($serviceMessageNote);

                try {
                    $leadNotesService = $apiClient->notes(EntityTypesInterface::LEADS);
                    $notesCollection = $leadNotesService->add($notesCollection);
                } catch (AmoCRMApiException $e) {
                    printError($e);
                    die;
                }
            }
            return $lead_id;
        } catch (AmoCRMApiException $e) {
            printError($e);
            die;
        }
    }


}
