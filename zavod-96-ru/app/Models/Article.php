<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'preview_image',
        'detail_image',
        'preview_text',
        'detail_text',
        'seo_title',
        'seo_description',
        'sort',
        'published_at'
    ];

    protected $casts = [
        'public_date' => 'datetime:Y-m-d',
    ];
}
