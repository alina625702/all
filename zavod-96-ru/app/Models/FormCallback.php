<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormCallback extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
    ];

    public static array $rules = [
        'name' => ['required', 'string', 'max:100'],
        'phone' => ['required', 'string', 'max:20'],
        'g-recaptcha-response' => 'required|recaptcha',
    ];

}
