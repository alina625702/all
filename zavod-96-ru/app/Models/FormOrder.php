<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'message',
        'region',
        'company',
        'filename',
        'product_name'
    ];

    public static array $rules = [
        'name' => ['required', 'string', 'max:100'],  
        'phone' => ['required', 'string', 'max:20'],      
        'email' => ['nullable', 'string', 'max:50'],
        'message' => ['nullable', 'string', 'max:10000'],
        'region' => ['nullable', 'string', 'max:50'],
        'company' => ['nullable', 'string', 'max:50'],
        'product_name' => ['nullable', 'string', 'max:255'],
        'filename' => ['nullable', 'file', 'max:15360'], // 15 MB
        'g-recaptcha-response' => 'required|recaptcha',
    ];

}
