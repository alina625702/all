<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'address',
        'phone',
        'ext_phone',
        'email',
        'work_time',
        'coords',
    ];
}