<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'title',
        'slug',
        'preview_image',
        'detail_image',
        'preview_text',
        'detail_text',
        'category_id',
        'price',
        'seo_title',
        'seo_description',
        'sort'
    ];

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function getFormattedPriceAttribute()
    {
        return number_format($this->price, 0, '', ' ');
    }
}
