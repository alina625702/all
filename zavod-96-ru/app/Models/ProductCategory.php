<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class ProductCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'preview_image',
        'detail_image',
        'preview_text',
        'detail_text',
        'parent_id',
        'seo_title',
        'seo_description',
        'sort'
    ];

    public function parent()
    {
        return $this->belongsTo(ProductCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id');
    }

    public function subcategories()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public static function getCategoryTree()
    {
        $categories = self::with('children')
            ->whereNull('parent_id')
            ->whereIn('slug', ['komplektnye_transformatornye_podstancii_ktp', 'transformatory_silovye'])
            ->orderBy('sort', 'asc')
            ->get();

        $tree = [];

        foreach ($categories as $category) {
            $tree[] = self::buildTree($category);
        }

        return $tree;
    }

    protected static function buildTree($category)
    {
        $tree = [
            'id' => $category->id,
            'name' => $category->title,
            'products' => $category->products->map(function ($product) {
                return [
                    'id' => $product->id,
                    'name' => $product->title,
                    'price' => $product->formatted_price . ' руб.',
                ];
            })->toArray(),
            'children' => [],
        ];

        foreach ($category->children as $child) {
            $tree['children'][] = self::buildTree($child);
        }

        return $tree;
    }
}
