<?php

namespace App\Providers;

use App\Models\ProductCategory;
use App\Models\Setting;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrapFive();
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
        View::composer('*', function ($view) {
            $rootCategories = ProductCategory::query()->whereNull('parent_id')->orderBy('sort', 'asc')->get();
            $settings = new Setting();
            $view->with('rootCategories', $rootCategories);
            $view->with('settings', $settings);
        });
    }
}
