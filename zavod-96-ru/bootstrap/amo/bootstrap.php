<?php

use AmoCRM\Client\AmoCRMApiClient;

include_once __DIR__ . '/../../vendor/autoload.php';

$clientId = config('amo.CLIENT_ID');
$clientSecret = config('amo.CLIENT_SECRET');
$redirectUri = config('amo.CLIENT_REDIRECT_URI');

$apiClient = new AmoCRMApiClient($clientId, $clientSecret, $redirectUri);

include_once __DIR__ . '/token_actions.php';
include_once __DIR__ . '/error_printer.php';

