<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */

    public function up(): void
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->id();
            
            $table->string('title');            
            $table->string('address');
            $table->string('phone', 20)->nullable();
            $table->string('ext_phone', 20)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('work_time')->nullable();
            $table->string('coords', 30)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offices');
    }
};
