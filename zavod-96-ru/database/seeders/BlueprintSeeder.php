<?php

namespace Database\Seeders;

use App\Models\Blueprint;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BlueprintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $blueprint = Blueprint::latest()->first();

        for($i=0; $i<7; $i++) {
            $new_blueprint = new Blueprint(
                [
                    'title' => $blueprint->title,
                    'preview_image' => $blueprint->preview_image,
                    'detail_image' => $blueprint->detail_image
                ]
            );
            $new_blueprint->save();
        }
    }
}
