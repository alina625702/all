<?php

namespace Database\Seeders;

use App\Models\Questionnaire;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $questionnaire = Questionnaire::latest()->first();

        for($i=0; $i<8; $i++) {
            $new_questionnaire= new Questionnaire(
                [
                    'title' => $questionnaire->title,
                    'description'  => $questionnaire->description,
                    'file' => $questionnaire->file,
                ]
            );
            $new_questionnaire->save();
        }
    }
}
