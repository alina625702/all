<?php

namespace Database\Seeders;

use App\Models\Review;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $review = Review::latest()->first();

        for($i=0; $i<40; $i++) {
            $new_review = new Review(
                [
                    'title' => $review->title,
                    'region'  => $review->region,
                    'preview_image' => $review->preview_image,
                    'detail_image' => $review->detail_image
                ]
            );
            $new_review->save();
        }
    }
}
