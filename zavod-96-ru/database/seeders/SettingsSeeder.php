<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Setting::create([
            'key' => 'site_email',
            'label' => 'Email на сайте',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'callback_email',
            'label' => 'Email для обратной связи',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'address',
            'label' => 'Адрес офиса',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'whatsapp',
            'label' => 'Whatsapp',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'coords',
            'label' => 'Координаты',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'phone1',
            'label' => 'Основной номер телефона',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'phone2',
            'label' => 'Дополнительный номер телефона',
            'value' => null,
            'type' => 'text',
        ]);

        Setting::create([
            'key' => 'header_scripts',
            'label' => 'Скрипты в Header',
            'value' => null,
            'type' => 'textarea',
        ]);

        Setting::create([
            'key' => 'footer_scripts',
            'label' => 'Скрипты в Footer',
            'value' => null,
            'type' => 'textarea',
        ]);
    }
}
