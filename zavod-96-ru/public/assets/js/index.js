// скрипт для мобильного меню
$(document).ready(function () {
  var $header = $('header');
  var $nav = $('nav');
  var $headerBlock = $('.header-block');
  var $navs = $('.navs');
  var $mobileNav = $('.mobile-nav');
  var $body = $('body');
  var menuOpen = false;

  function toggleClasses() {
    $mobileNav.toggleClass('active');
    $header.toggleClass('active');
    $nav.toggleClass('active');
    menuOpen = !menuOpen;
    $body.toggleClass('lock');
    handleBlockDuplication();
  }

  function handleNavPosition() {
    if ($(window).width() < 1200) {
      if (!$headerBlock.find('nav').length) {
        $nav.detach().appendTo($headerBlock);
      }
      $header.addClass('fixed');
    } else {
      if ($(window).scrollTop() > 50) {
        if (!$headerBlock.find('nav').length) {
          $nav.detach().appendTo($headerBlock);
        }
        $header.addClass('fixed');
      } else {
        if ($header.find('.header-block + nav').length === 0) {
          $nav.detach().insertAfter($headerBlock);
        }
        $header.removeClass('fixed');
      }
    }
    handleBlockDuplication();
  }

  function handleBlockDuplication() {
    if ($(window).width() < 1400 && $header.hasClass('active') && $header.hasClass('fixed')) {
      if ($navs.find('.search, .contact-block').length === 0) {
        $headerBlock.find('.search, .contact-block').clone().appendTo($navs);
      }
    } else {
      $navs.find('.search, .contact-block').remove();
    }
  }

  $mobileNav.on('click', function (e) {
    e.stopPropagation();
    toggleClasses();
  });

  $(document).on('click', function (e) {
    var isClickInsideMobileNav = $mobileNav.is(e.target) || $mobileNav.has(e.target).length !== 0;
    var isClickInsideNav = $nav.is(e.target) || $nav.has(e.target).length !== 0;
    if (menuOpen && !isClickInsideMobileNav && !isClickInsideNav) {
      toggleClasses();
    }
  });

  $('.dropdown').on('click', function () {
    if ($(window).width() < 1400) {
      $(this).toggleClass('active');
    }
  });

  $(window).scroll(handleNavPosition);
  $(window).resize(handleNavPosition);

  handleNavPosition();
});

// Автозаполнение имени товара
$(document).ready(function () {
  $('button[data-bs-toggle="modal"]').on('click', function() {
    $('form input[name="product_name"]').val($(this).attr('data-product_name'));
  });
});

// скрипт для разворачивания поиска
$(document).ready(function () {
  $('.search').on('click', function (event) {
    event.stopPropagation();
    $(this).addClass('active');
  });

  $(document).on('click', function (event) {
    var $search = $('.search');
    if (!$search.is(event.target) && $search.has(event.target).length === 0) {
      $search.removeClass('active');
    }
  });
});

//скрипт для переноса карты сайта в подвале
$(document).ready(function () {
  function checkScreenSize() {
    var $mapLink = $('.flogo .map');
    var $navList = $('.fnav ul');

    if ($(window).width() < 992) {
      if (!$navList.find('.map').length) {
        $navList.append('<li class="map-item"></li>');
        $('.fnav ul li.map-item').append($mapLink);
      }
    } else {
      if ($navList.find('.map').length) {
        $('.flogo').append($navList.find('.map'));
        $navList.find('.map-item').remove();
      }
    }
  }

  checkScreenSize();
  $(window).resize(checkScreenSize);
});


//скрипт для кнопки наверх
$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('#toTop').fadeIn();
    } else {
      $('#toTop').fadeOut();
    }
  });
  $('#toTop').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
  });
});

// слайдер для отзывов
var swiper = new Swiper(".reviewsSwiper", {
  navigation: {
    nextEl: ".review-next",
    prevEl: ".review-prev",
  },
  slidesPerView: 4,
  spaceBetween: 40,
  freeMode: true,
  breakpoints: {
    992: {
      slidesPerView: 4,
      spaceBetween: 24,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 24,
    },
    576: {
      slidesPerView: "auto",
      spaceBetween: 12,
    },
    0: {
      slidesPerView: "auto",
      spaceBetween: 10,
    }
  }
});

// слайдер для партнеров
var swiper2 = new Swiper(".partnersSwiper", {
  navigation: {
    nextEl: ".partners-next",
    prevEl: ".partners-prev",
  },
  slidesPerView: 4,
  spaceBetween: 40,
  freeMode: true,
  breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 24,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 14,
    },
    576: {
      slidesPerView: "auto",
      spaceBetween: 12,
    },
    0: {
      slidesPerView: "auto",
      spaceBetween: 10,
    }
  }
});

// слайдер для партнеров
var swiper3 = new Swiper(".sertificatesSwiper", {
  slidesPerView: 3,
  spaceBetween: 40,
  freeMode: true,
  breakpoints: {
    992: {
      slidesPerView: 3,
      spaceBetween: 40,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 24,
    },
    0: {
      slidesPerView: "auto",
      spaceBetween: 11,
    }
  }
});

// слайдер для каталога
var swiper4 = new Swiper(".catalogSwiper", {
  freeMode: true,
  navigation: {
    nextEl: ".catalog-next",
    prevEl: ".catalog-prev",
  },
  breakpoints: {
    992: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    992: {
      slidesPerView: 4,
      spaceBetween: 14,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 14,
    },
    0: {
      slidesPerView: "auto",
      spaceBetween: 10,
    }
  }
});

//скрипт для переноса кнопки в блоке - связаться с нами
$(document).ready(function () {
  moveWhatsappLink();

  $(window).resize(function () {
    moveWhatsappLink();
  });

  function moveWhatsappLink() {
    var windowWidth = $(window).width();
    if (windowWidth < 768) {
      $('.call-block').append($('.call-info .write-wapp'));
    } else {
      $('.call-info').append($('.call-block .write-wapp'));
    }
  }
});

//скрипт для переноса кнопки воспроизведения видео
$(document).ready(function () {
  var playBlock = $(".benefits .row .play-blk");

  function checkScreenSize() {
    var playItem = $(".benefit-info ul li.benefit6");

    if ($(window).width() < 1200) {
      if (!playItem.length) {
        playItem = $("<li class='benefit6'></li>").append(playBlock);
        $(".benefit-info ul").append(playItem);
      }
    } else {
      if (playItem.length) {
        playItem.remove();
        $(".benefits .row").append(playBlock);
      }
    }
  }

  // Вызываем функцию при загрузке страницы и при изменении размера окна
  checkScreenSize();
  $(window).resize(checkScreenSize);
});

//скрипт для переноса ссылки в блоке отзывы на главной
$(document).ready(function () {
  function moveAllReviews() {
    var screenWidth = $(window).width();
    if (screenWidth < 768) {
      $('.all-reviews').appendTo('.reviews .container');
    } else {
      $('.all-reviews').appendTo('.reviews .title-flex');
    }
  }

  moveAllReviews();

  $(window).resize(function () {
    moveAllReviews();
  });
});

//скрипт дл выбора файла в форме
$(document).ready(function () {
  $('.adds').on('click', 'button', function (event) {
    event.preventDefault();
    $(this).siblings('input[type="file"]').click();
  });

  $('.adds').on('change', 'input[type="file"]', function () {
    var fileName = $(this).val().split('\\').pop();
    $(this).siblings('span').text(fileName ? fileName : "Файл не выбран");
  });
});


// Валидация формы
(function () {
  'use strict';

  var forms = document.querySelectorAll('.needs-validation');

  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();
        if (!form.checkValidity()) {
          
          event.preventDefault();
          event.stopPropagation();
          
        } else {

          var url = $(this).attr("action");
          let formData = new FormData(this);

          $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false,
            success: (response) => {
              console.log(response)
              $('.input-group-captcha .invalid-feedback').hide();
              $('#Call, #Call-back, #Online-order').modal('hide');
              $('#sent-modal').modal('show');
              ym(49402078,'reachGoal','goal_webform_success')
            },
            error: function (response) {
              console.log(response.responseJSON.errors);
              if(typeof response.responseJSON.errors['g-recaptcha-response'] !== "undefined") {
                $('.input-group-captcha .invalid-feedback').show();
                $('.input-group-captcha .invalid-feedback').html('Пожалуйста, подтвердите что вы не робот');
              }
            }
          });

        }

        form.classList.add('was-validated');
      }, false);
    });
})();

$(document).ready(function () {
  $('.form-control').each(function () {
    if ($(this).val() !== "") {
      $(this).prev('label').css('opacity', 0);
    }
  });

  $('label').click(function () {
    $(this).next('.form-control').focus();
  });

  $('.form-control').focus(function () {
    $(this).prev('label').css('opacity', 0);
  });

  $('.form-control').blur(function () {
    if ($(this).val() === "") {
      $(this).prev('label').css('opacity', 1);
    }
  });
});

//скрипт для опросных листов
function updateBorder() {
  var container = document.querySelector('.questionnaires .row');
  var items = container.querySelectorAll('.questionnaires-blk');

  items.forEach(function (item) {
    item.style.borderBottom = '';
    item.style.paddingBottom = '';
  });

  var itemsPerRow = window.innerWidth >= 992 ? 4 : window.innerWidth >= 768 ? 3 : 2;

  for (var i = 0; i < items.length; i += itemsPerRow) {
    if (i + itemsPerRow >= items.length) {
      for (var j = i; j < items.length; j++) {
        items[j].style.borderBottom = 'none';
        items[j].style.paddingBottom = '0';
      }
      break;
    }
  }
}

window.onload = updateBorder;
window.onresize = updateBorder;


//скрипт для левого меню в категориях
$(document).ready(function () {
  // $('.category-nav button[data-bs-toggle="collapse"]').each(function() {
  //     if (!$(this).hasClass('collapsed')) {
  //         $(this).closest('li').addClass('active');
  //     }
  // });

  $('.category-nav').on('click', 'button[data-bs-toggle="collapse"]', function () {
    setTimeout(() => {
      if (!$(this).hasClass('collapsed')) {
        $(this).closest('li').addClass('active');
      } else {
        $(this).closest('li').removeClass('active');
      }
    }, 0);
  });
});


$(document).ready(function () {
  function checkWindowSize() {
    if ($(window).width() < 992) {
      if ($('.btn-catalog').length === 0) {
        $('<button class="btn btn-danger btn-catalog">Каталог</button>').insertBefore('.category-nav');
      }
    } else {
      $('.btn-catalog').remove();
    }
  }

  checkWindowSize();
  $(window).on('resize', checkWindowSize);

  $(document).on('click', '.btn-catalog', function () {
    $('.category-nav, .btn-catalog').toggleClass('active');
    $('.category-nav li').removeClass('active');
    $('.category-nav ul').removeClass('show');
  });

  $(document).on('click', function (event) {
    if (!$(event.target).closest('.btn-catalog, .category-nav').length) {
      $('.category-nav, .btn-catalog').removeClass('active');
    }
  });
});


// скрипт для макси телефона
jQuery(function ($) {
  $(".phone").mask("+7(999)999-9999");
});


//скрипт для модальных окон
const Modal = document.getElementById('CallBack')
Modal.addEventListener('show.bs.modal', event => {
  const buttonn = event.relatedTarget
  const recipients = buttonn.getAttribute('data-bs-title')
  const modalTitles = Modal.querySelector('.title')
  modalTitles.textContent = `${recipients}`
})


// скрипт для модального окна без скролла
const modals = document.querySelectorAll(".modal");

modals.forEach(function (modal) {
  modal.addEventListener("show.bs.modal", function () {
    if (document.documentElement.scrollHeight > window.innerHeight) {
      document.body.classList.add("modal-open-noscroll");
    } else {
      document.body.classList.remove("modal-open-noscroll");
    }
  });

  modal.addEventListener("hide.bs.modal", function () {
    document.body.classList.remove("modal-open-noscroll");
  });
});