@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="reviews-block padb articles">
        <div class="container">
            <h1>{{$pageTitle}}</h1>
            <div class="row">

                @foreach ($articles as $article)
                <div class="col-lg-4 col-md-4 col-12">
                    <a href="{{route('article.show', $article->slug)}}">
                        <img src="/uploads/{{$article->preview_image}}" alt="">
                    </a>
                    
                    <div class="review-info">
                        <span class="review-title">
                            <a href="{{route('article.show', $article->slug)}}">
                                {{$article->title}}
                            </a>
                        </span>
                        <span class="review-city">
                            {{ \Carbon\Carbon::parse($article->published_at)->format('d.m.Y')}}
                        </span>                        
                    </div>
                </div>
                @endforeach
            </div>
            
            {{ $articles->links('vendor.pagination.main') }}

        </div>
    </div>   
    
    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
