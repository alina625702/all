@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="reviews-block padb">
        <div class="container">
            <h1>{{$pageTitle}}</h1>

            <div class="row content">
                <div class="col-md-12">
                    {!! $article->detail_text !!}
                </div>
            </div>
        </div>
    </div>   
    
    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
