<div class="catalog-slide padd">
    <div class="container">
        <h2>Наша продукция</h2>
        <div class="slider-block">
            <div
                class="swiper catalogSwiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-free-mode swiper-backface-hidden">
                <div class="swiper-wrapper" id="swiper-wrapper-ce2f97619f9b5d7e" aria-live="polite"
                    style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                    
                    @foreach ($favorite_products as $product)
                        <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 5"
                            style="width: 202.5px; margin-right: 14px;">
                            <a href="{{route('catalog.product', [$product->category->slug, $product->slug])}}" class="catalog-product">
                                <div class="catalog-product-image">
                                    <img src="/uploads/{{$product->preview_image}}" alt="{{$product->title}}">
                                </div>
                                <div class="catalog-product-description">
                                    <span class="catalog-product-title">{{$product->title}}</span>
                                    <p>{{ $product->category->title}}</p>
                                    <span class="catalog-product-prise">{{ $product->formatted_price }} руб.</span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
            <div class="swiper-button-next catalog-next" tabindex="0" role="button" aria-label="Next slide"
                aria-controls="swiper-wrapper-ce2f97619f9b5d7e" aria-disabled="false"></div>
            <div class="swiper-button-prev catalog-prev swiper-button-disabled" tabindex="-1" role="button"
                aria-label="Previous slide" aria-controls="swiper-wrapper-ce2f97619f9b5d7e" aria-disabled="true"></div>
        </div>
    </div>
</div>