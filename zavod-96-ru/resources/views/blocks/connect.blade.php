<div class="connect">
    <div class="container">
        <div class="connect-block flex blue">
            <div class="connect-info">
                <h2>Связаться с нами</h2>
                    <p>Чтобы оформить заказ, заполните <a href="{{route('info.questionnaire')}}">опросный лист</a> и прикрепите его к онлайн-заявке</p>
                    <a href="#" class="btn btn-danger btn-arrow" data-bs-toggle="modal" data-bs-title="Онлайн-заявка" data-bs-target="#Call">Онлайн-заявка</a>
                    
            </div>
            <a href="{{route('info.questionnaire')}}"><img src="assets/img/oprosnyy-list.png" alt=""></a>
        </div>
    </div>
</div>