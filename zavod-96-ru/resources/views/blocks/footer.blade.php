<footer>
    <div class="container">
        <div class="footer-block flex">
            <div class="fblk flogo">
                <a href="{{route('home')}}">
                    <img src="{{ asset('assets/img/flogo.svg') }}" alt="">
                </a>
                {{-- <a href="/sitemap.xml" class="map">Карта сайта</a> --}}
            </div>
            <div class="fblk fcat">
                <span>Каталог продукции</span>
                <ul class="unstyled">
                    @foreach ($rootCategories as $category)
                            <li><a href="{{ route('catalog.category', $category->slug)}}">{{ $category->title }}</a></li>
                        @endforeach
                </ul>
            </div>
            <div class="fblk fnav">
                <ul class="unstyled">
                    <li><a href="{{route('company.index')}}">Компания</a></li>
                    <li><a href="{{route('company.price')}}">Цены</a></li>
                    <li><a href="{{route('contact.index')}}">Контакты</a></li>
                    <li><a href="{{route('company.review')}}">Отзывы</a></li>                    
                    <li><a href="{{route('info.questionnaire')}}">Опросные листы</a></li>
                    <li><a href="{{route('info.blueprint')}}">Чертежи</a></li>
                    <li><a href="{{route('article.index')}}">Статьи</a></li>
                    <li><a href="{{route('company.licenses')}}">Лицензии и сертификаты</a></li>
                </ul>
            </div>
            <div class="fblk ftel">
                <span>Контакты</span>
                <div class="contact-block tel-block">
                    <ul class="unstyled">
                        <li><a href="tel:{{$settings->getValue('phone1')}}">{{$settings->getValue('phone1')}}</a></li>
                        <li><a href="tel:{{$settings->getValue('phone2')}}">{{$settings->getValue('phone2')}}</a></li>
                    </ul>
                </div>
                <div class="call">
                    <button type="button" class="btn" data-bs-toggle="modal" data-bs-title="Заказать звонок"
                        data-bs-target="#Call-back">Заказать звонок</button>
                </div>
                <div class="contact-block">
                    <ul class="unstyled">
                        <li>
                            <a href="mailto:{{$settings->getValue('site_email')}}" class="email">{{$settings->getValue('site_email')}}</a>
                        </li>
                        <li>
                            <p class="adress">{{$settings->getValue('address')}}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom flex">
            <p>© 2023 Все права защищены.</p>
            <a href="{{ route('home.privacy') }}">Согласие на обработку персональных данных</a>
            <a target="_blank" href="https://prostodelovito.ru">Разработка сайта ПростоДеловито</a>
        </div>
    </div>
</footer>