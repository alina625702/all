<form id="callback" action="{{route('form.order')}}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    @csrf
    <div class="flex">
        <div class="input-group">
            <label>Имя <span>*</span></label>
            <input name="name" type="text" class="form-control" required tabindex="11">
            <div class="invalid-feedback">Введите Ваше имя</div>
        </div>
        <div class="input-group">
            <input name="company" type="text" class="form-control" placeholder="Компания" tabindex="15">
        </div>
        <div class="input-group">
            <label>Телефон <span>*</span></label>
            <input name="phone" pattern="+7 ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}"
                type="tel" class="form-control phone" required tabindex="12">
            <div class="invalid-feedback">Введите ваш номер</div>
        </div>
        <div class="input-group">
            <input name="region" type="text" class="form-control" placeholder="Город" tabindex="16">
        </div>
        <div class="input-group">
            <input name="email" type="email" class="form-control" placeholder="E-mail" tabindex="13">
        </div>
        <div class="adds">
            <input id="filename" name="filename" type="file"  style="display: none;"/>
            <button type="button" tabindex="17">Выберите файл</button>
            <span>Файл не выбран</span>
        </div>
        <div class="input-group">
            <textarea name="message" class="form-control required-field" name="message" placeholder="Сообщение" tabindex="14"></textarea>
            <div class="invalid-feedback">Введите сообщение</div>
        </div>

        @include('blocks.form.recaptcha')
        
    </div>
    <button type="submit" class="btn btn-danger btn-sent" data-toggle="modal"
        data-target="#sent-modal">Отправить</button>
    <label class="checks">
        <input class="checkbox" type="checkbox" checked="" name="checkbox-test"
            required>
        <span class="checkbox-custom"></span>
        <span class="label">Я согласен на обработку <a href="{{ route('home.privacy') }}">персональных
                данных</a></span>
        <div class="invalid-feedback">Поставьте своё согласие на обработку персональных
            данных</div>
    </label>

</form>