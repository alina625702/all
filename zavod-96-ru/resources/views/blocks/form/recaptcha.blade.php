@if(config('services.recaptcha.key'))
    <div class="input-group input-group-captcha">
        <div class="g-recaptcha"
            data-sitekey="{{config('services.recaptcha.key')}}">
        </div>
        <div class="invalid-feedback"></div>
    </div>
@endif