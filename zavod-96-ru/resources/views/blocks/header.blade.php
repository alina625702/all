<header>
    <div class="header-block flex">
        <div class="logo">
            <a href="/"><img src="{{ asset('assets/img/logo.svg') }}" alt="stelz"></a>
        </div>
        <div class="search">
            <form action="/search/" method="GET" class="flex">
                <input value="{{ app('request')->input('search') }}" type="text" name="search" placeholder="Поиск по сайту">
                <button type="submit"></button>
            </form>
        </div>
        <div class="contact-block tel-block">
            <ul class="unstyled">
                <li><a href="tel:{{$settings->getValue('phone1')}}">{{$settings->getValue('phone1')}}</a></li>
                <li><a href="tel:{{$settings->getValue('phone2')}}">{{$settings->getValue('phone2')}}</a></li>
            </ul>
        </div>
        <div class="contact-block whatsapp-block">
            <ul class="unstyled">
                <li><a href="mailto:{{$settings->getValue('site_email')}}" class="email">{{$settings->getValue('site_email')}}</a></li>
                <li><a href="https://wa.me/{{$settings->getValue('whatsapp')}}" class="whatsapp">Написать в WhatsApp</a></li>
            </ul>
        </div>
        <div class="application">
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-title="Онлайн заявка"
                data-bs-target="#Online-order">Онлайн заявка</button>
        </div>
    </div>
    <nav class="grey">
        <button class="btn btn-danger mobile-nav">Меню</button>
        <div class="navs">
            <ul class="nav-blk unstyled">
                <li class="dropdown">
                    <div class="flex">
                        <a href="{{route('company.index')}}">О компании</a>
                        <button class="btn" type="button"></button>
                    </div>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('company.delivery')}}">Информация о доставке</a></li>
                        <li><a href="{{route('company.payment')}}">Информация об оплате</a></li>
                        <li><a href="{{route('company.licenses')}}">Лицензии и сертификаты</a></li>
                        <li><a href="{{route('company.requisites')}}">Реквизиты</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <div class="flex">
                        <a href="{{route('catalog.index')}}">Каталог продукции</a>
                        <button class="btn" type="button"></button>
                    </div>
                    <ul class="dropdown-menu">
                        @foreach ($rootCategories as $category)
                            <li><a href="{{ route('catalog.category', $category->slug)}}">{{ $category->title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="{{route('company.price')}}">Прайс-лист</a></li>
                <li><a href="{{route('info.blueprint')}}">Чертежи</a></li>
                <li><a href="{{route('info.questionnaire')}}">Опросные листы</a></li>
                <li><a href="{{route('company.review')}}">Отзывы</a></li>
                <li><a href="{{route('contact.index')}}">Контакты</a></li>
            </ul>
        </div>
    </nav>
</header>