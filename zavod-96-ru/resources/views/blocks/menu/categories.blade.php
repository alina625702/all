<ul class="category-nav">

    @foreach ($categoriesMenu as $category)
        <li class="@if($category['active']) active @endif">
            <div class="flex cat-title">
                <a href="{{route('catalog.category', $category['slug'])}}">{{$category['title']}}</a>
                @if (count($category['children']) > 0)
                    <button type="button" data-bs-toggle="collapse" data-bs-target="#{{$category['slug']}}"></button>
                @endif                
            </div>

            @if (count($category['children']) > 0)
                @foreach ($category['children'] as $subcategory)
                <ul class="collapse @if($category['active']) show @endif" id="{{$category['slug']}}">
                    <li class="@if($subcategory['active']) active @endif">
                        <div class="flex">
                            <a href="{{route('catalog.category', $subcategory['slug'])}}">{{$subcategory['title']}}</a>
                            @if (count( $subcategory['children']) > 0)
                                <button type="button" data-bs-toggle="collapse" data-bs-target="#{{$subcategory['slug']}}" class="collapsed"></button>    
                            @endif                            
                        </div>

                        @if (count( $subcategory['children']) > 0)
                            <ul class="collapse @if($subcategory['active']) show @endif" id="{{$subcategory['slug']}}">
                                @foreach ($subcategory['children'] as $subsubcategory)
                                    <li class="@if($subsubcategory['active']) active @endif"><a href="{{route('catalog.category', $subsubcategory['slug'])}}">{{$subsubcategory['title']}}</a></li>
                                @endforeach                                
                            </ul>
                        @endif

                    </li>
                </ul>
                @endforeach
            @endif
        </li>
    @endforeach
</ul>