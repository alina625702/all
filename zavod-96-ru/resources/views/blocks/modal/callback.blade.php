<div class="modal fade" id="Call-back">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            <div class="modal-header">
                <img src="{{ asset('assets/img/flogo.svg') }}" alt="">
            </div>
            <div class="modal-body">
                <span class="title-h2">Заказать звонок</span>
                <p>Наши специалисты с радостью ответят на ваши вопросы, произведут расчет стоимости продукции и
                    подготовят индивидуальное коммерческое предложение</p>
                <form id="callback" action="{{route('form.callback')}}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    <div class="input-group">
                        <label>Имя <span>*</span></label>
                        <input name="name" type="text" class="form-control" required>
                        <div class="invalid-feedback">Введите Ваше имя</div>
                    </div>
                    <div class="input-group">
                        <label>Телефон <span>*</span></label>
                        <input name="phone" pattern="+7 ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}" type="tel"
                            class="form-control phone" required>
                        <div class="invalid-feedback">Введите ваш номер</div>
                    </div>

                    @include('blocks.form.recaptcha')
                    
                    <button type="submit" class="btn btn-danger" data-toggle="modal"
                        data-target="#sent-modal">Отправить</button>
                    <label class="checks">
                        <input class="checkbox" type="checkbox" checked="" name="checkbox-test" required>
                        <span class="checkbox-custom"></span>
                        <span class="label">Я согласен на обработку <a href="{{ route('home.privacy')}}">персональных
                                данных</a></span>
                        <div class="invalid-feedback">Поставьте своё согласие на обработку персональных данных
                        </div>
                    </label>

                </form>
            </div>
        </div>
    </div>
</div>