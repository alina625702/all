<div class="modal fade" id="Online-order">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            <div class="modal-body order">
                <div class="row">
                    <div class="col-md-4">
                        <span class="title-h2">Онлайн-заявка</span>
                        <p>Если Вам необходима трансформаторная подстанция — опишите ее или прикрепите <a
                                href="#">опросный лист</a> и отправьте нам — и Вы получите <a
                                href="#">бесплатный расчет</a> в течение 1 дня</p>
                        <img src="{{ asset('assets/img/modal-image.jpg') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="order-block">
                            <p class="title-h4">Оставьте заявку и получите бесплатный расчет в течение 1 дня</p>
                            <form id="callback" action="{{route('form.order')}}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
                                @csrf
                                <div class="flex">
                                    <div class="input-group">
                                        <label>Имя <span>*</span></label>
                                        <input name="name" type="text" class="form-control" required tabindex="1">
                                        <div class="invalid-feedback">Введите Ваше имя</div>
                                    </div>
                                    <div class="input-group">
                                        <input name="company" type="text" class="form-control" placeholder="Компания" tabindex="5">
                                    </div>
                                    <div class="input-group">
                                        <label>Телефон <span>*</span></label>
                                        <input name="phone" pattern="+7 ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}"
                                            type="tel" class="form-control phone" required tabindex="2">
                                        <div class="invalid-feedback">Введите ваш номер</div>
                                    </div>
                                    <div class="input-group">
                                        <input name="region" type="text" class="form-control" placeholder="Город" tabindex="6">
                                    </div>
                                    <div class="input-group">
                                        <input name="email" type="email" class="form-control" placeholder="E-mail" tabindex="3">
                                    </div>                                        
                                    
                                    <div class="adds">
                                        <input id="filename" name="filename" type="file"  style="display: none;"/>
                                        <button type="button" tabindex="7">Выберите файл</button>
                                        <span>Файл не выбран</span>
                                    </div>
                                    <div class="input-group">
                                        <textarea name="message" class="form-control required-field" name="message" placeholder="Сообщение" tabindex="4"></textarea>
                                        <div class="invalid-feedback">Введите сообщение</div>
                                    </div>

                                    @include('blocks.form.recaptcha')
                                </div>
                                <input type="hidden" name="product_name" type="text" class="form-control" placeholder="Продукт">                                

                                <button type="submit" class="btn btn-danger btn-sent" data-toggle="modal"
                                    data-target="#sent-modal">Отправить</button>
                                <label class="checks">
                                    <input class="checkbox" type="checkbox" checked="" name="checkbox-test"
                                        required>
                                    <span class="checkbox-custom"></span>
                                    <span class="label">Я согласен на обработку <a href="{{ route('home.privacy') }}">персональных
                                            данных</a></span>
                                    <div class="invalid-feedback">Поставьте своё согласие на обработку персональных
                                        данных</div>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>