<div class="modal fade" id="sent-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            <div class="modal-body">
                <span class="title-h3">Спасибо, заявка оформлена</span>
                <p>Наш менеджер свяжется с вами в ближайшее время</p>
            </div>
        </div>
    </div>
</div>