<div class="order grey padd">
    <div class="container">
        <div class="row">
            <div class="col-md-4 order-left">
                <h2>Онлайн-заявка</h2>
                <p>Если Вам необходима трансформаторная подстанция — опишите ее или прикрепите <a
                        href="{{ route('info.questionnaire')}}">опросный лист</a> и отправьте нам — и Вы получите <strong>бесплатный
                        расчет</strong> в течение 1 дня</p>
                <img src="{{ asset('assets/img/modal-image.jpg') }}" alt="">
            </div>
            <div class="col-md-8 order-right">
                <div class="order-block">
                    <p class="title-h4">Оставьте заявку и получите бесплатный расчет в течение 1 дня</p>                        
                    @include('blocks.form.contact')
                </div>
            </div>
        </div>
    </div>
</div>