<div class="questions quest">
    <div class="container">
        <div class="question-block flex align-items-md-center">
            <p class="title-h4">Оформите заявку на сайте, мы свяжемся с вами в ближайшее время и ответим на все
                интересующие вопросы</p>
            <button type="button" class="btn btn-danger btn-arrow" data-bs-toggle="modal"
                data-bs-title="Заказать расчет стоимости" data-bs-target="#Online-order">Заказать расчет
                стоимости</button>
        </div>
    </div>
</div>
