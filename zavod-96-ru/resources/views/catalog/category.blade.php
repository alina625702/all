@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="catalog-page padb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <div class="row">
                <div class="col-lg-3 category-left">
                    {{-- CATEGORIES MENU --}}
                    @include('blocks.menu.categories')
                </div>

                <div class="col-lg-9 category-right">
                    @if (count($currentCategory->children))
                        <div class="category-block padb">
                            <div class="row">

                                @foreach ($currentCategory->children as $subcategory)
                                    <div class="col-md-6">
                                        <a href="{{ route('catalog.category', $subcategory->slug) }}" class="cat-blk">
                                            <div class="cat-blk-image">
                                                <img src="/uploads/{{ $subcategory->preview_image }}"
                                                    alt="{{ $subcategory->title }}">
                                            </div>
                                            <span>{{ $subcategory->title }}</span>
                                        </a>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    @endif
                    @if (count($products))
                        <div class="category-prod">
                            <div class="row">
                                @foreach ($products as $product)
                                    <div class="col-md-4 col-6">
                                        <a href="{{route('catalog.product', [$currentCategory->slug, $product->slug])}}" class="catalog-product">
                                            <div class="catalog-product-image">
                                                <img src="/uploads/{{$product->preview_image}}" alt="{{$product->title}}">
                                            </div>
                                            <div class="catalog-product-description">
                                                <span class="catalog-product-title">{{$product->title}}</span>
                                                <span class="catalog-product-prise">
                                                    @if ($product->price > 0)
                                                        от {{ $product->formatted_price}} руб.
                                                    @else
                                                        цена по запросу
                                                    @endif
                                                </span>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach                                
                            </div>
                        </div>
                    @endif


                    <div class="category-content">
                        {!! $currentCategory->detail_text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
