@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="catalog-page padb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <div class="info-text">
                <p>Производственное Объединение STELZ предлагает Вам полный перечень электротехнического оборудования,
                    напряжением от 6 Кв до 35 Кв и мощностью до 6300 кВа.</p>
            </div>

            <div class="catalog-block row">
                @foreach ($categories as $category)
                    <div class="col-lg-4 col-sm-6">
                        <a href="{{ route('catalog.category', $category->slug) }}" class="catblk">
                            <span>{{ $category->title }}</span>
                            <img src="/uploads/{{ $category->preview_image }}" alt="{{ $category->title }}">
                        </a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>



    {{-- CONNECT --}}
    @include('blocks.connect')

    {{-- FAVORITE PRODUCTS --}}
    @include('blocks.catalog.favorite', ['favorite_products' => $favorite_products])

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
