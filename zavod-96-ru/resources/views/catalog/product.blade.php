@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="catalog-page padb">
        <div class="container">            
            <div class="row">
                <div class="col-lg-3 category-left">
                    {{-- CATEGORIES MENU --}}
                    @include('blocks.menu.categories')
                </div>

                <div class="col-lg-9 category-right">
                    <div class="card-product">
                        <div class="card-block d-flex">
                            <div class="card-image">
                                <img src="/uploads/{{ $product->detail_image }}" alt="{{ $product->title }}">
                            </div>
                            <div class="card-info">
                                <h1 class="title-h3">{{ __($pageTitle) }}</h1>

                                <span class="product-prise">
                                    @if ($product->price > 0)
                                        от {{ $product->formatted_price}} руб.
                                    @else
                                        цена по запросу
                                    @endif
                                </span>
                                <p>Чтобы купить {{ $product->title }}, опишите ее или прикрепите опросный лист и
                                    отправьте нам — и Вы получите бесплатный расчет в течение 1 дня</p>
                                <a href="{{route('info.questionnaire')}}" class="red">Заполнить опросный лист&gt;</a>
                                <button type="button" class="btn btn-danger btn-arrow" data-bs-toggle="modal"
                                    data-bs-title="Оставить заявку" data-bs-target="#Online-order" data-product_name="{{ $product->title }}">Оставить заявку</button>
                            </div>
                        </div>
                    </div>


                    <div class="category-content">
                        {!! $product->detail_text !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- QUESTION --}}
    @include('blocks.question')

    {{-- FAVORITE PRODUCTS --}}
    @include('blocks.catalog.favorite', ['favorite_products' => $favorite_products])

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
