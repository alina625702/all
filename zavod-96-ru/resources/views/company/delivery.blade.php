@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="delivery-page">
        <div class="delivery">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-8">
                        <div class="delivery-block">
                            <h1>{{ __($pageTitle) }}</h1>
                            <div class="delivery-info">
                                <p>Доставка продукции возможна по всей России и в страны СНГ</p>
                            </div>
                            <p class="title-h3">При покупке продукции в ПО «STELZ» мы предлагаем нашим клиентам следующие
                                виды доставки:</p>
                            <ol class="unstyled circle">
                                <li>Манипулятором по Екатеринбургу и Свердловской области</li>
                                <li>Автотранспортом (догрузом) через наши партнерские транспортные компании</li>
                                <li>
                                    <div>
                                        <p>Самовывоз. Вы можете воспользоваться услугами любой транспортной компании на ваш
                                            выбор или прислать собственный транспорт. Погрузка нашими силами и техникой</p>
                                        <p>Самовывоз производится с нашего склада по предварительной договоренности</p>
                                    </div>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="information-block padd">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="propose-block">
                            <span class="title-h4">Для получения продукции необходимо иметь:</span>
                            <ul class="list">
                                <li>Доверенность на право получения товара по счету</li>
                                <li>Документ, удостоверяющий личность - паспорт</li>
                                <li>Наши менеджеры подберут для вас индивидуальный и удобный вариант доставки</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="propose-block">
                            <span class="title-h4">Требования к автотранспорту, подаваемому на погрузку:</span>
                            <ul class="list">
                                <li>Открытый верх (снимающийся тент), так как погрузка товара производится путем верхней
                                    загрузки</li>
                                <li>Для стяжки оборудования в кузове должны быть узлы крепления</li>
                                <li>Кузов должен иметь деревянный пол</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
