@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="company-block">
        <div class="container">
            <div class="company-info">
                <h1>О компании</h1>
                <div class="info-text">
                    <p>Производственное объединение STELZ было основано в 2001 году и сразу взяло высокую планку уровня
                        качества в производстве электротехнического оборудования</p>
                </div>
            </div>
            <div class="company-video">
                <a class="video-play" data-fancybox="why-us" data-type="iframe" href="https://youtu.be/oPAFOmgrZg0">
                    <img src="{{ asset('assets/img/company-image.jpg') }}" alt="">
                </a>
            </div>

            <div class="company-content padd">
                <div class="row">
                    <div class="col-xl-6 company-content-left">
                        <span class="title-h3">ПО СТЭЛЗ взяло высокую планку уровня качества в производстве</span>
                        <p>Широкий спектр электрооборудования, надежность произведенной продукции и доступная цена были по
                            достоинству оценены множеством потребителей и теперь по праву считаются эталоном качества
                            современного отечественного производителя</p>
                    </div>
                    <div class="col-xl-6 company-content-right">
                        <ul class="unstyled tegs">
                            @foreach ($categories as $category)
                                <li><a href="{{ route('catalog.category', $category->slug) }}">{{ $category->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="company-propose">
                <div class="row">
                    <div class="col-md-6">
                        <div class="propose-block">
                            <span class="title-h4">Быстрые темпы развития</span>
                            <p>За короткий срок ПО STELZ стал постоянным поставщиком электросетевых, строительных,
                                нефтегазовых и
                                промышленных компаний.</p>
                            <p>В первые годы работы высокими темпами увеличивались объемы производства продукции,
                                расширялась
                                номенклатура, было освоено около 100 наименований изделий.</p>
                            <p>В 2003 году завод начал поставки электротехнического оборудования в страны СНГ и активно
                                наращивает
                                их объемы. Помимо РФ, основные страны-покупатели: Казахстан, Узбекистан, Армения,
                                Азербайджан,
                                Киргизия.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="propose-block">
                            <span class="title-h4">Новые технологии — высокое качество</span>
                            <p>В 2004 году было возведено и оснащено современным оборудованием новое здание цеха
                                трансформаторных
                                подстанций КТП.</p>
                            <p>На новых площадях введена линия порошковой покраски деталей трансформаторных подстанций,
                                усовершенствованы технологии, механизированы грузоподъемные операции. Открыт цех для
                                производства
                                трансформаторных подстанций контейнерного типа.</p>
                            <p>Вся продукция, произведенная на нашем заводе, отвечает самым высоким требованиям и не
                                уступает
                                мировым аналогам. Оснащенность современным оборудованием, а штат укомплектован только
                                высококлассными специалистами.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="company-garantee padd">
                <h3>Гарантия надежности</h3>
                <div class="row">
                    <div class="col-md-6">
                        <p>Приобретенное у нас электрооборудование успешно и долго работает на предприятиях, промышленных,
                            частных и
                            коммунальных объектах России и стран СНГ, что является объективным показателем надежности и
                            долговечности нашей продукции.</p>
                        <p>В настоящее время ПО STELZ, предоставляя высококачественную и востребованную продукцию своим
                            благодарным
                            клиентам, с уверенностью и оптимизмом смотрит в будущее.</p>
                    </div>
                    <div class="col-md-6">
                        <ul class="unstyled lists">
                            <li>
                                <p>Опыт работы <span>более 12 лет</span></p>
                            </li>
                            <li>
                                <p>Гарантия на продукцию <span>до 5 лет</span></p>
                            </li>
                            <li>
                                <p>Вся продукция <span>сертифицирована</span></p>
                            </li>
                            <li>
                                <p>Доставка <span>по России и СНГ</span></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <section class="why-us marg why-company padd">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 why-us-info">
                    <img src="assets/img/logo2.svg" alt="О компании Stelz">
                    <p>Мы те, кто профессионально и быстро рассчитает и изготовит электрооборудование по Вашему запросу.</p>
                    <img src="assets/img/why-company.jpg" alt="О компании Stelz" class="why-picture">
                </div>
                <div class="col-xl-7 why-us-block">
                    <div class="why-us-description">
                        <p class="title-h3">Почему Вам выгодно работать с нами?</p>
                        <ul class="unstyled">
                            <li>Мы профессионалы с многолетним опытом работы в сфере производства электрооборудования</li>
                            <li>Вы получаете продукцию напрямую от производителя -соответственно вы получаете минимальные
                                цены</li>
                            <li>Высокое качество всей предлагаемой продукции: что подтверждается сертификатами, техническими
                                условиями, а также многолетним сотрудничеством с крупными заказчиками по всей России</li>
                            <li>Производственные мощности STELZ оснащены современным оборудованием для изготовления изделий
                                по самым высоким международным стандартам качества</li>
                            <li>Вы получаете гарантию до 5 лет на всю выпускаемую нами продукцию</li>
                            <li>Собственный парк автотранспорта, позволяет осуществлять доставку продукции в любую точку
                                России и СНГ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sertificates padd">
        <div class="container">
            <h2>Лицензии и сертификаты</h2>
            <div
                class="col-xl-10 sertificates-block swiper sertificatesSwiper swiper-initialized swiper-horizontal swiper-pointer-events swiper-free-mode swiper-backface-hidden">
                <div class="swiper-wrapper" id="swiper-wrapper-f45106cae9d4e792c" aria-live="polite"
                    style="transform: translate3d(0px, 0px, 0px);">
                    
                    @foreach ($licenses as $license)
                        <div class="sertificate-blk swiper-slide swiper-slide-active" role="group"
                        style="width: 232px; margin-right: 24px;">
                            <a href="/uploads/{{ $license->preview_image }}" data-fancybox="sertificate">
                                <img src="/uploads/{{ $license->detail_image }}" alt="{{ $license->title }}">
                            </a>
                            <span>{{ $license->title }}</span>
                            <a target="_blank" href="/uploads/{{ $license->detail_image }}">Скачать</a>
                        </div>
                    @endforeach

                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
        </div>
    </section>

    {{-- QUESTIONS --}}
    <div class="questions padb">
        <div class="container">
            <div class="question-block flex">
                <p class="title-h4">Наши специалисты с радостью ответят на ваши вопросы, произведут расчет стоимости продукции и подготовят индивидуальное коммерческое предложение</p>
                <button type="button" class="btn btn-danger btn-arrow" data-bs-toggle="modal" data-bs-title="Заказать звонок" data-bs-target="#Call">Задать вопрос</button>
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
