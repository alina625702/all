@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')

    <div class="sertificates-block padb smb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <div class="row">

                @foreach ($licenses as $license)
                    <div class="col-sm-4 col-6 sertificate-blk">
                        <a href="/uploads/{{ $license->detail_image}}" data-fancybox="sertificate">
                            <img src="/uploads/{{ $license->preview_image}}" alt="{{ $license->title}}">
                        </a>
                        <span>{{ $license->title}}</span>
                        <a href="/uploads/{{ $license->detail_image}}">Скачать</a>
                    </div>
                @endforeach
               
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
