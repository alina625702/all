@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="information-block padb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="propose-block">
                        <span class="title-h4">При производстве продукции под заказ:</span>
                        <p>Предоплата 70% на расчетный счет перед началом изготовления, оставшиеся 30% при готовности
                            продукции к отгрузке.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="propose-block">
                        <span class="title-h4">Если продукция в наличии на складе и готова к отгрузке:</span>
                        <p>Предоплата 100%, после чего продукция отгружается любым удобным для Вас способом.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
