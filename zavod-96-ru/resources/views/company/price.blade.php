@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')

    <div class="sertificates-block padb smb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <p class="title-h3">Цены с НДС на продукцию Производственного Обьединения «STELZ»</p>
            <p>Внимание: цены на подстанции указаны без учета трансформатора.</p>
            <div class="prise-blk">

                @foreach ($priceArray as $category)
                    <p class="title-h3 red">{{ $category['name'] }}</p>
                    @if (count($category['children']))
                        @foreach ($category['children'] as $subcategory)
                        <p class="title-h4">{{ $subcategory['name'] }}</p>
                        <div class="table-block">
                            @if (count($subcategory['products']))
                                <table class="table">
                                    <tbody>
                                        @foreach ($subcategory['products'] as $product)
                                            <tr>
                                                <td>{{ $product['name'] }}</td>
                                                <td>{{ $product['price'] }}</td>
                                            </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            @endif

                            @if (count($subcategory['children']))
                                @foreach ($subcategory['children'] as $subsubcategory)
                                    <p class="bold">{{ $subsubcategory['name']}}</p>     
                                    
                                    @if (count($subsubcategory['products']))
                                        <table class="table">
                                            <tbody>
                                                @foreach ($subsubcategory['products'] as $product)
                                                    <tr>
                                                        <td>{{ $product['name'] }}</td>
                                                        <td>{{ $product['price'] }}</td>
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    @endif
                                @endforeach
                            @endif
                            
                        </div>
                        @endforeach
                        
                    @endif
                @endforeach

            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
