@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="requisites padb">
        <div class="container">
            <h1>{{ __($pageTitle) }}</h1>
            <table class="table">
                <tbody>
                    <tr>
                        <td>Полное наименование:</td>
                        <td>ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ПРОИЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ "СТЭЛЗ"</td>
                    </tr>
                    <tr>
                        <td>Расчетный счет:</td>
                        <td>40702810038230001505 в филиал «Екатеринбургский» ОАО «АЛЬФА-БАНК»</td>
                    </tr>
                    <tr>
                        <td>Корреспондентский счет:</td>
                        <td>30101810100000000964</td>
                    </tr>
                    <tr>
                        <td>БИК:</td>
                        <td>046577964</td>
                    </tr>
                    <tr>
                        <td>ИНН:</td>
                        <td>6671032347</td>
                    </tr>
                    <tr>
                        <td>КПП:</td>
                        <td>667101001</td>
                    </tr>
                    <tr>
                        <td>ОГРН:</td>
                        <td>1169658004515</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
