@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="reviews-block padb">
        <div class="container">
            <h1>{{$pageTitle}}</h1>
            <div class="row">

                @foreach ($reviews as $review)
                <div class="col-lg-3 col-md-4 col-6">
                    <a class="review-item" href="/uploads/{{$review->detail_image}}" data-fancybox="review">
                        <img src="/uploads/{{$review->preview_image}}" alt="">
                    </a>
                    <div class="review-info">
                        <span class="review-title">{{$review->title}}</span>
                        <span class="review-city">{{$review->region}}</span>
                    </div>
                </div>
                @endforeach
            </div>
            
            {{ $reviews->links('vendor.pagination.main') }}

        </div>
    </div>   
    
    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
