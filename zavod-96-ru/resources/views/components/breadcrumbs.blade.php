@props(['items' => []])

<div class="breadcrumb-blk">
    <div class="container">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Главная') }}</a></li>
            @foreach ($items as $item)
                @php
                    if(!isset($item['route_params'])) $item['route_params'] = [];
                @endphp

                @if ($item['current'] == false)
                    <li class="breadcrumb-item">
                        <a href="{{ route($item['route'], $item['route_params']) }}">
                            {{ $item['title'] }}
                        </a>
                    </li>
                @else
                    <li class="breadcrumb-item active" aria-current="page">
                        {{ __($item['title']) }}
                    </li>
                @endif
            @endforeach            
        </ul>
    </div>
</div>