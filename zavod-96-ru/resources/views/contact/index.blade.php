@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="contact-page">
        <div class="container">
            <h1>{{$pageTitle}}</h1>
            <p>Вы можете связаться с нами по любому из указанных контактов, в зависимости от города, где вы находитесь</p>
        </div>
    </div>

    {{-- CONTACTS --}}
    <div class="contact-info padb">
        <div class="container">
            <div class="contact-description">

                @foreach ($offices as $office)
                <div class="contact-blk col-sm-6">
                    <span class="title-h3">{{ $office->title }}</span>
                    <div class="contact-info-blk">
                        <p class="contact-adress icons">{{ $office->address }}</p>
                        @isset ($office->phone)
                            <div class="contact-tel icons">
                                <ul class="unstyled">
                                    @isset ($office->phone)
                                        <li><a href="tel:{{ $office->phone }}">{{ $office->phone }}</a></li>    
                                    @endisset

                                    @isset ($office->ext_phone)
                                        <li><a href="tel:{{ $office->ext_phone }}">{{ $office->ext_phone }}</a></li>    
                                    @endisset
                                </ul>
                            </div>
                        @endisset
                        
                        @isset ($office->email)
                            <a href="mailto:{{$office->email}}" class="contact-email icons">{{$office->email}}</a>
                        @endisset

                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>

    {{-- QUESTIONS --}}
    <div class="questions">
        <div class="container">
            <div class="question-block flex">
                <p class="title-h4">Наши специалисты с радостью ответят на ваши вопросы, произведут расчет стоимости
                    продукции и подготовят индивидуальное коммерческое предложение</p>
                <button type="button" class="btn btn-danger btn-arrow" data-bs-toggle="modal"
                    data-bs-title="Заказать звонок" data-bs-target="#Call">Задать вопрос</button>
            </div>
        </div>
    </div>

    {{-- MAP --}}
    <div class="map-block padd">
        <div class="container">
            <iframe
                src="https://yandex.ru/map-widget/v1/?ll=60.553061%2C56.776473&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgg1NjA3MDcyNxJz0KDQvtGB0YHQuNGPLCDQodCy0LXRgNC00LvQvtCy0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsINCV0LrQsNGC0LXRgNC40L3QsdGD0YDQsywg0YPQu9C40YbQsCDQkNC80YPQvdC00YHQtdC90LAsIDEwNyIKDVY2ckIVHBtjQg%2C%2C&z=16.72"
                frameborder="1" allowfullscreen="true"></iframe>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')
    
@endsection