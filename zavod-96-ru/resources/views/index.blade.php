@extends('layouts.main')

@section('content')
    <section class="offer marg">
        <div class="container">
            <h1>Производство трансформаторных подстанций и другого электротехнического оборудования</h1>
            <p>Предлагаем полный перечень электротехнического оборудования напряжением от 6 Кв до 35 Кв и мощностью до 6300
                кВа.</p>
            <a href="{{ route('catalog.index') }}" class="btn btn-danger btn-arrow">Наша продукция</a>
        </div>
    </section>


    <section class="catalog">
        <div class="container">
            <h2>Каталог продукции</h2>
            <div class="catalog-block row">

                @foreach ($rootCategories as $category)
                    <div class="col-lg-4 col-sm-6">
                        <a href="{{ route('catalog.category', $category->slug) }}" class="catblk">
                            <span>{{ $category->title }}</span>
                            <img src="/uploads/{{ $category->preview_image }}" alt="{{ $category->title }}">
                        </a>
                    </div>
                @endforeach

            </div>
            <a href="{{ route('catalog.index') }}" class="btn btn-danger btn-arrow btn-ctlg">Перейти в каталог</a>
        </div>
    </section>

    <section class="why-us marg padd">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 why-us-info left">
                    <h2 class="titles">О компании</h2>
                    <img src="assets/img/logo2.svg" alt="О компании Stelz">
                    <p>Мы те, кто профессионально и быстро рассчитает и изготовит электрооборудование по Вашему запросу.</p>
                    <img src="assets/img/why-us.jpg" alt="О компании Stelz" class="why-picture">
                </div>
                <div class="col-xl-7 why-us-block">
                    <div class="why-us-description">
                        <p class="title-h3">Почему Вам выгодно работать с нами?</p>
                        <ul class="unstyled">
                            <li>Мы профессионалы с многолетним опытом работы в сфере производства электрооборудования</li>
                            <li>Вы получаете продукцию напрямую от производителя -соответственно вы получаете минимальные
                                цены</li>
                            <li>Высокое качество всей предлагаемой продукции: что подтверждается сертификатами, техническими
                                условиями, а также многолетним сотрудничеством с крупными заказчиками по всей России</li>
                            <li>Производственные мощности STELZ оснащены современным оборудованием для изготовления изделий
                                по самым высоким международным стандартам качества</li>
                            <li>Вы получаете гарантию до 5 лет на всю выпускаемую нами продукцию</li>
                            <li>Собственный парк автотранспорта позволяет осуществлять доставку продукции в любую точку
                                России и СНГ</li>
                        </ul>
                    </div>
                    <div class="why-us-video">
                        <div class="why-us-video-block flex">
                            <a class="video-play" data-fancybox="why-us" data-type="iframe"
                                href="https://www.youtube.com/embed/oPAFOmgrZg0"><img src="assets/img/why-us-video.jpg"
                                    alt=""></a>
                            <p>В 2003 году Производственное объединение STELZ начало поставки электротехнического
                                оборудования в страны СНГ и с тех пор активно наращивает их объемы. Нашими покупателями
                                также являются компании из Казахстана и Узбекистана, Армении, Азербайджана и Киргизии.</p>
                        </div>
                        <a href="{{ route('company.index') }}" class="comp">О компании &gt;</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="reviews">
        <div class="container">
            <div class="title-flex">
                <h2>Отзывы клиентов</h2>
                <a href="{{ route('company.review') }}" class="all-reviews">Все отзывы ></a>
            </div>
            <div class="slider-block">
                <div class="swiper reviewsSwiper">
                    <div class="swiper-wrapper">

                        @foreach ($reviews as $review)
                            <div class="swiper-slide">
                                <a href="/uploads/{{ $review->detail_image }}" data-fancybox="review">
                                    <img src="/uploads/{{ $review->preview_image }}" alt="">
                                </a>
                                <div class="review-info">
                                    <span class="review-title">{{ $review->title }}</span>
                                    <span class="review-city">{{ $review->region }}</span>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="swiper-button-next review-next"></div>
                <div class="swiper-button-prev review-prev"></div>
            </div>
        </div>
    </section>

    <section class="benefits padd marg">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 benefit-info">
                    <h2 class="titles">Преимущества</h2>
                    <img src="assets/img/logo2.svg" alt="">
                    <ul class="unstyled">
                        <li class="benefit1">Опыт работы более 12 лет</li>
                        <li class="benefit2">Гарантия на продукцию до 5 лет</li>
                        <li class="benefit3">Вся продукция сертифицирована</li>
                        <li class="benefit4">Оборудование напрямую от производителя</li>
                        <li class="benefit5">Доставка по России и СНГ</li>
                        {{-- <li class="benefit0">Конкурентные цены</li> --}}
                        <li class="benefit6">
                            <div class="col-xl-2 play-blk">
                                <a class="play" data-fancybox="video" data-type="iframe"
                                    href="https://www.youtube.com/embed/oPAFOmgrZg0"></a>
                                <span>Смотреть видео о компании</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <section class="sertificates padd">
        <div class="container">
            <h2>Лицензии и сертификаты</h2>
            <div class="col-xl-10 sertificates-block swiper sertificatesSwiper">
                <div class="swiper-wrapper">
                    @foreach ($licenses as $license)
                        <div class="sertificate-blk swiper-slide">
                            <a href="/uploads/{{ $license->detail_image }}" data-fancybox="sertificate">
                                <img src="/uploads/{{ $license->preview_image }}" alt="{{ $license->title }}">
                            </a>
                            <span>{{ $license->title }}</span>
                            <a target="_blank" href="/uploads/{{ $license->detail_image }}">Скачать</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="call-us grey padd">
        <div class="container">
            <div class="row flex">
                <div class="col-lg-4 col-md-6 call-info">
                    <h2>Связаться с нами</h2>
                    <p>Мы предлагаем Вам оборудование со сроком гарантии до 3 лет и сроком службы более 25 лет. В комплекте
                        прилагаются декларации о соответствии, паспорта на продукцию и комплектующие.</p>
                    <a href="https://api.whatsapp.com/send?phone=73432887387" class="write-wapp">Написать нам в WhatsApp</a>
                </div>
                <div class="col-lg-7 col-md-6 call-block">
                    <div class="call-us-block">
                        <p>Наши специалисты с радостью ответят на ваши вопросы, произведут расчет стоимости продукции и
                            подготовят индивидуальное коммерческое предложение</p>
                        <button type="button" class="btn btn-danger btn-arrow" data-bs-toggle="modal"
                            data-bs-title="Заказать звонок" data-bs-target="#Call">Задать вопрос</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection
