@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="information-block padb">
        <div class="blueprints padb smb">
            <div class="container">
                <h1>{{ __($pageTitle) }}</h1>
                <div class="row">

                    @foreach ($blueprints as $blueprint)
                    <div class="col-lg-3 col-md-4 col-6 sertificate-blk">
                        <a href="/uploads/{{ $blueprint->detail_image}}" data-fancybox="blueprint">
                            <img src="/uploads/{{ $blueprint->preview_image}}" alt="{{ $blueprint->title}}">
                        </a>
                        <span>{{ $blueprint->title}}</span>
                        <a target="_blank" href="/uploads/{{ $blueprint->detail_image}}">Скачать</a>
                    </div> 
                    @endforeach                    

                </div>
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

    @endsection
