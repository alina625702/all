@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="information-block padb">
        <div class="questionnaires padb">
            <div class="container">
                <h1>{{ __($pageTitle) }}</h1>
                <div class="row">
                    @foreach ($questionnaires as $questionnaire)
                        <div class="col-lg-3 col-md-4 col-6 questionnaires-blk">
                            <span>{{ $questionnaire->title }}</span>
                            <p>{{ $questionnaire->description }}</p>
                            <a href="/uploads/{{ $questionnaire->file }}">Скачать</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

@endsection
