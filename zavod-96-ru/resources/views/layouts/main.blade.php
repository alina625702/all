@php
    // сохраняем utm-метки в cookie
    if(isset($_GET["utm_source"])) setcookie("utm_source",$_GET["utm_source"],time()+3600*24*30,"/");
    if(isset($_GET["utm_medium"])) setcookie("utm_medium",$_GET["utm_medium"],time()+3600*24*30,"/");
    if(isset($_GET["utm_campaign"])) setcookie("utm_campaign",$_GET["utm_campaign"],time()+3600*24*30,"/");
    if(isset($_GET["utm_content"])) setcookie("utm_content",$_GET["utm_content"],time()+3600*24*30,"/");
    if(isset($_GET["utm_term"])) setcookie("utm_term",$_GET["utm_term"],time()+3600*24*30,"/");
    if(isset($_GET["__imz"])) setcookie("__imz",$_GET["__imz"],time()+3600*24*30,"/");

@endphp

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="">

    <title>@yield('page.title', config('seo.title'))</title>
    <meta name="description" content="@yield('page.description', config('seo.description'))">

        
    <meta property="og:title" content="{{ config('seo.title') }}">
    <meta property="og:site_name" content="{{ config('seo.title') }}">
    <meta property="og:url" content="zavod-96.ru">
    <meta property="og:description" content="{{ config('seo.description') }}">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ asset('/assets/img/why-us.jpg') }}">
    

    <link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/swiper-bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/js/jquery-3.4.0.min.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    {!!$settings->getValue('header_scripts')!!}
    
</head>

<body>
    {{-- HEADER --}}
    @include('blocks.header')

    {{-- CONTENT --}}
    <main>
        @if(!Route::is('home') )
            <x-breadcrumbs :items="$breadcrumb_items" />
        @endif        
        @yield('content')
    </main>

    {{-- FOOTER --}}
    @include('blocks.footer')

    <div id="toTop"></div>

    {{-- MODAL FORMS --}}
    @include('blocks.modal.call')
    @include('blocks.modal.callback')
    @include('blocks.modal.order')
    @include('blocks.modal.send')

    <!-- Scripts for site -->
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.maskedinput.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.selectbox.min.js') }}"></script>
    <script src="{{ asset('assets/js/swiper-bundle.js') }}"></script>
    <script src="{{ asset('assets/js/index.js') }}"></script>

    {!!$settings->getValue('footer_scripts')!!}

    <div style="display:none" itemscope itemtype="http://schema.org/Organization">
        <span itemprop="name">ПО СТЭЛЗ</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress"> Амундсена 107</span>
          <span itemprop="postalCode"> 620016</span>
          <span itemprop="addressLocality">Екатеринбург</span>,
        </div>
        <span itemprop="telephone">+7 (343) 288-73-87</span>
        <span itemprop="email">ktp@zavod-96.ru</span>
    </div>

</body>

</html>
