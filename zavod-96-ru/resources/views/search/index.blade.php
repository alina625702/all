@extends('layouts.main')

@section('page.title',  __($pageTitle) . ' | ' . config('seo.title'))
@section('page.description', config('seo.description') . '. ' . __($pageTitle))

@section('content')
    <div class="container">
        <h1>Результаты поиска</h1>
    
        @if (!count($products) && !count($categories) && !count($articles))
        <div class="search-block">
            Нет данных по заданным параметрам поиска
        </div>
        @endif

        @if (count($products))
            <div class="search-block">
                <div class="search_subtitle"><strong>Товары:</strong></div> 
                <ul>
                    @foreach ($products as $product)
                        <li><a href="{{route('catalog.product', [$product->category->slug, $product->slug])}}">{{ $product->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
        

        @if (count($categories))
            <div class="search-block">
                <div class="search_subtitle"><strong>Категории товаров:</strong></div> 
                <ul>
                    @foreach ($categories as $category)
                        <li><a href="{{route('catalog.category', $category->slug)}}">{{ $category->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (count($articles))
            <div class="search-block">
                <div class="search_subtitle"><strong>Категории товаров:</strong></div> 
                <ul>
                    @foreach ($articles as $article)
                        <li><a href="{{route('article.show', $article->slug)}}">{{ $article->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    
    

    {{-- ORDER --}}
    @include('blocks.order')

    {{-- PROPOSE --}}
    @include('blocks.propose')

    @endsection
