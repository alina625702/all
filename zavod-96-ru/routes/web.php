<?php

use App\Http\Controllers\AmoController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Route;
use Spatie\Sitemap\SitemapGenerator;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/privacy', [HomeController::class, 'privacy'])->name('home.privacy');

Route::get('/catalog', [CatalogController::class, 'index'])->name('catalog.index');
Route::get('/catalog/{category}', [CatalogController::class, 'category'])->name('catalog.category');
Route::get('/catalog/{category}/{product}', [CatalogController::class, 'product'])->name('catalog.product');

Route::get('/contacts', [ContactController::class, 'index'])->name('contact.index');

Route::get('/company', [CompanyController::class, 'index'])->name('company.index');
Route::get('/company/reviews', [CompanyController::class, 'review'])->name('company.review');
Route::get('/company/payment', [CompanyController::class, 'payment'])->name('company.payment');
Route::get('/company/delivery', [CompanyController::class, 'delivery'])->name('company.delivery');
Route::get('/company/requisites', [CompanyController::class, 'requisites'])->name('company.requisites');
Route::get('/company/licenses/', [CompanyController::class, 'licenses'])->name('company.licenses');
Route::get('/company/price/', [CompanyController::class, 'price'])->name('company.price');

Route::get('/info/oprosnye-listy', [InfoController::class, 'questionnaire'])->name('info.questionnaire');
Route::get('/info/chertezhi', [InfoController::class, 'blueprint'])->name('info.blueprint');

Route::get('/info/articles', [ArticleController::class, 'index'])->name('article.index');
Route::get('/info/articles/{slug}', [ArticleController::class, 'show'])->name('article.show');

Route::get('/search', [SearchController::class, 'index'])->name('search.index');

Route::post('form/callback', [FormController::class,'callback'])->name('form.callback'); 
Route::post('form/question', [FormController::class,'question'])->name('form.question'); 
Route::post('form/order', [FormController::class,'order'])->name('form.order'); 

Route::get('amo/gettoken', [AmoController::class,'getToken'])->name('amo.getToken');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/sitemap', function () {
    SitemapGenerator::create('https://zavod-96.ru')->writeToFile(public_path('sitemap.xml'));
    return 'Sitemap generated successfully.';
});

require __DIR__.'/auth.php';
